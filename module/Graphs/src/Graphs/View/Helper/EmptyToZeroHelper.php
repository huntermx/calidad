<?php

namespace Graphs\View\Helper;
use Zend\View\Helper\AbstractHelper;

class EmptyToZeroHelper extends AbstractHelper
{

	public function __invoke($value)
	{
		if(empty($value))
		{
			return 0;
		}

		return $value;
	}
	
}