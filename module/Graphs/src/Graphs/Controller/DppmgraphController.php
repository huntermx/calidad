<?php

namespace Graphs\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;
use Captura\Model\Entity\Maquinasactivas;
use Captura\Model\Entity\Maquinasdetalle;

use Graphs\Model\ControlClasses\ClassYearControl;
use Graphs\Model\ControlClasses\ClassKeyValueArray;

use Graphs\Forms\FormSelectYear;
use Graphs\Forms\FormSelectFamily;


use Graphs\Model\Dppm\ClassQtyInspeccionada;
use Graphs\Model\Dppm\ClassQtyRechazada;
use Graphs\Model\Dppm\ClassDppm;

use Graphs\Model\HighchartsWapper\HcCategories;
use Graphs\Model\HighchartsWapper\HcSerie;
use Graphs\Model\HighchartsWapper\HcSerieMarker;
use Graphs\Model\HighchartsWapper\HcSeriesGlue;

class DppmgraphController extends AbstractGraphsController
{
  
    protected $meses = array(1 => 'Enero', 2 => 'Febrero',3 => 'Marzo',4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto',
                             9 => 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');




    public function selectfamilyAction()
    {
        $request   = $this->getRequest();  
        print_r($request->getPost());

    }


    public function indexAction()
	{
        $formSelect = $this->getFormSelectYear();
        $objYearControl = new ClassYearControl($this->getInspDimDao()->getFirstDataYear(),false,false);
        $objYearControl->setObjData($this->getInspDimDao());
        $yearArray =$objYearControl->getVectorYear();
        $formSelect->get('selectyear')->setAttribute('options' ,$yearArray);
        $objFamily = $this->getPartesDao()->getAllFamilys();
        $familyArray = $this->returnArrayFamily($objFamily);
        $formFamily =$this->getFormSelectFamily();
        $formFamily->get('selectfamily')->setAttribute('options' ,$familyArray);
        $request   = $this->getRequest();   
       
        $objRequest =$request->getPost();
       



        if($request->isPost())
        {

            if($objRequest['selectyear'])
            {
                $formSelect->setData($request->getPost());
                if($formSelect->isValid())
                {
                    
                    $requestData = $formSelect->getData();
                    $highchartYear = $requestData['selectyear'];
                    $family = $requestData['family'];
                }

            }


            if($objRequest['selectfamily'])
            {
                $formFamily->setData($request->getPost());
                if($formFamily->isValid())
                {
                    
                    $requestData = $formFamily->getData();
                    $highchartYear = (int)$requestData['year'];  
                    $family = $requestData['selectfamily'];
                }
            }



           // $formSelect->setData($request->getPost());

           
        }
        else
        {   // Si no hay Post
            $highchartYear = date("Y");
            $family = "Todas";

        }

        $formSelect->get('family')->setValue($family);
        $formFamily->get('year')->setValue($highchartYear);
       


        
        $objMes = new ClassKeyValueArray($this->meses);
        //Da formato HighCharts al Array de meses
        $hcCateg = new HcCategories($objMes->getValueArray());

       

        

        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        
        return array(
              'titulo' => $this->getTitulo($highchartYear,$family),
              'formYear' => $formSelect,
              'formFamily' => $formFamily,
              'highChartsTitle' => "''",
              'highChartsSubTitle' => "''",
              'cat' => $hcCateg->getFormattedData(),
              'obj' => $this->getObjHcPzsRechazadas($highchartYear,$family),

                );
	}



    private function getTitulo($year,$family)
    {
        if($family =="Todas" || $family==false)
        {
            $titulo = 'Grafica Dppm ' . $year;
        }
        else
        {
             $titulo = "Grafica {$family} Dppm  {$year}";
        }

        return $titulo;
    }



    private function returnArrayFamily($obj)
    {
        $tempArray = array();
        $tempArray['Todas'] = 'Todas';
        while($family=$obj->current())
        {
            $tempArray[$family['division']] = $family['division'];
        }
        return $tempArray;
    }

    private function getObjHcPzsRechazadas($year,$family=false)
    {

        $objQtyInsp = new ClassQtyInspeccionada($this->getInspDimDao(),$this->getInspVisDao(),$year,$family);
        $objKVArray = new ClassKeyValueArray($objQtyInsp->getYearQty());
        $hcAvail[0] = new HcSerie($objKVArray->getValueArray());
        $hcAvail[0]->setName("Qty INSPECTED");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");
        $hcAvail[0]->setYAxis(1);
        //$hcAvail[0]->setColumnDlabel(false);
        $hcAvail[0]->setColumnDataLabels(false);


        $objQtyRech = new ClassQtyRechazada($this->getInspDimDao(),$this->getInspVisDao(),$year,$family);
        $objKVArray2 = new ClassKeyValueArray($objQtyRech->getYearQty());
        $hcAvail[1] = new HcSerie($objKVArray2->getValueArray());
        $hcAvail[1]->setName("Qty REJECTED");
        $hcAvail[1]->setColor("#000");
        $hcAvail[1]->setType("spline");
        $hcAvail[1]->setYAxis(2);
        $hcAvail[1]->setColumnDataLabels(false);


        
        $objDppm = new ClassDppm($objQtyInsp,$objQtyRech,$year);
        $objKVArray3 = new ClassKeyValueArray($objDppm->getYearQty($year));
        $hcAvail[2] = new HcSerie($objKVArray3->getValueArray());
        $hcAvail[2]->setName("QC DPPM");
        $hcAvail[2]->setColor("#339933");
        $hcAvail[2]->setType("spline");
        $hcAvail[2]->setColumnDataLabels(false);


        $objKVArray4 = new ClassKeyValueArray($objDppm->getYearAcumm());
        $hcAvail[3] = new HcSerie($objKVArray4->getValueArray());
        $hcAvail[3]->setName("RMONTH DPPM");
        $hcAvail[3]->setColor("#CC0000");
        $hcAvail[3]->setType("spline");
        $hcAvail[3]->setColumnDataLabels(false);

       
        /******************************************************/
        $dppmGoal = $this->getDppmGoalDao()->getDppmGoalForYear($year);
        $tempArray = array();

        for($i=0; $i<12; $i++)
        {
            $tempAarray[$i] = $dppmGoal->dppm_goal;
        }


        $hcAvail[4] = new HcSerie($tempAarray);
        $hcAvail[4]->setName("DPPM GOAL");
        $hcAvail[4]->setColor("#00CC00");
        $hcAvail[4]->setType("spline");
        $hcAvail[4]->setDashStyle("shortdot");
        $hcAvail[4]->setColumnDataLabels(false);

        $hcMarker = new HcSerieMarker();
        $hcMarker->setEnable(false);
        $hcAvail[4]->setObjMarker($hcMarker);   

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $hcSGlue->setObjSeries($hcAvail[2]);
        $hcSGlue->setObjSeries($hcAvail[3]);
        $hcSGlue->setObjSeries($hcAvail[4]);
        return $hcSGlue;
    }




	private function SplitKeyValueArray(array $arrayKeyValue)
    {
        foreach ($arrayKeyValue as $key=>$value)
        {

        }
    }

	
}