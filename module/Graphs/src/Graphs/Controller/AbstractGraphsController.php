<?php

namespace Graphs\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container as SessionContainer;
use Zend\Form\Annotation\AnnotationBuilder;

use Graphs\Forms\FormSelectYear;
use Graphs\Forms\FormSelectFamily;
use Graphs\Forms\FormSelectYmType;





abstract class AbstractGraphsController extends AbstractActionController
{
    protected $logTable;
    protected $formSelectYear;
    protected $formSelectFamily;
    protected $inspeccionDimDao;
    protected $inspeccionVisDao;
    protected $dppmGoalDao;
    protected $partesDao;
    protected $formSelectYm;
    protected $inspeccionRegDao;



   public function getDppmGoalDao()
    {
        if (!$this->dppmGoalDao) {
            $sm = $this->getServiceLocator();
            $this->dppmGoalDao = $sm->get('Graphs\Model\Dao\DppmGoalTable');
        }
        return $this->dppmGoalDao;
    }


    public function getFormSelectYm()
    {
        if (!$this->formSelectYm) {
            $temp            = new FormSelectYmType();
            $builder         = new AnnotationBuilder();
            $this->formSelectYm    = $builder->createForm($temp);
        }
        return $this->formSelectYm;
    }


    public function __construct()
    {
        $this->session    = new SessionContainer('labodigi');        
    }

    public function getFormSelectYear()
    {
        if (!$this->formSelectYear) {
            $temp            = new FormSelectYear();
            $builder         = new AnnotationBuilder();
            $this->formSelectYear    = $builder->createForm($temp);
        }
        return $this->formSelectYear;
    }

    public function getFormSelectFamily()
    {
        if (!$this->formSelectFamily) {
            $temp            = new FormSelectFamily();
            $builder         = new AnnotationBuilder();
            $this->formSelectFamily    = $builder->createForm($temp);
        }
        return $this->formSelectFamily;
    }    

    public function getInspDimDao()
    {
        if (!$this->inspeccionDimDao) {
            $sm = $this->getServiceLocator();
            $this->inspeccionDimDao = $sm->get('Graphs\Model\Dao\InspeccionDimensionalTable');
        }
        return $this->inspeccionDimDao;
    }

    public function getInspeccionRegistro()
    {
        if (!$this->inspeccionRegDao) {
            $sm = $this->getServiceLocator();
            $this->inspeccionRegDao = $sm->get('Graphs\Model\Dao\InspeccionRegistroTable');
        }
        return $this->inspeccionRegDao;
    }

    public function getPartesDao()
    {
        if (!$this->partesDao) {
            $sm = $this->getServiceLocator();
            $this->partesDao = $sm->get('Graphs\Model\Dao\PartesTable');
        }
        return $this->partesDao;
    }

    public function getInspVisDao()
    {
        if (!$this->inspeccionVisDao) {
            $sm = $this->getServiceLocator();
            $this->inspeccionVisDao = $sm->get('Graphs\Model\Dao\InspeccionVisualTable');
        }
        return $this->inspeccionVisDao;
    }

        public function getLastAction()
    {     
         $usrAction = $this->getLogTable()->getLastUserAction($this->session->username);
         $lastAction = $usrAction->current();
         return $lastAction['accion'];
    
    }

     public function getLogTable()
    {
        if (!$this->logTable) {
            $sm = $this->getServiceLocator();
            $this->logTable = $sm->get('core\Model\Dao\LogTable');
        }
        return $this->logTable;
    }

}