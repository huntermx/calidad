<?php

namespace Graphs\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Captura\Model\Entity\Maquinasactivas;
use Captura\Model\Entity\Maquinasdetalle;

use Graphs\Model\ControlClasses\ClassYearControl;
use Graphs\Model\ControlClasses\ClassKeyValueArray;

use Graphs\Forms\FormSelectYear;
use Graphs\Forms\FormSelectFamily;


use Graphs\Model\Dppm\ClassQtyInspeccionada;
use Graphs\Model\Dppm\ClassQtyRechazada;
use Graphs\Model\Dppm\ClassDppm;

use Graphs\Model\HighchartsWapper\HcCategories;
use Graphs\Model\HighchartsWapper\HcSerie;
use Graphs\Model\HighchartsWapper\HcSerieMarker;
use Graphs\Model\HighchartsWapper\HcSeriesGlue;

class RechazosPorPeriodoController extends AbstractGraphsController
{
  
    protected $meses = array(01 => 'Enero', 02 => 'Febrero',03 => 'Marzo',4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto',
                             9 => 'Septiembre', 10=> 'Octubre', 11=> 'Noviembre', 12=> 'Diciembre');



    public function indexAction()
	{
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        $request   = $this->getRequest();   
      //  $objRequest =$request->getPost();
        $formYm = $this->getFormSelectYm();
        if($request->isPost())
        {
            $formYm->setData($request->getPost());
            if($formYm->isValid())
            {
                $arrayDate = $this->splitStringArray($formYm->getData());
                $queryResult = $this->getInspeccionRegistro()->getReporteRechazos($arrayDate[0],$arrayDate[1]);
               
               $mesNumber =$arrayDate[1];

               $mesNumber = (int) $mesNumber;
               
                $view = new ViewModel(array(
                            'titulo' => 'Reporte rechazos ' . $this->meses[$mesNumber] . ' ' .$arrayDate[0] ,
                            'resultQuery' => $queryResult,
                            
                ));


                $view->setTemplate('graphs/rechazos-por-periodo/resultQuery.phtml');
                return $view;
            }
        }

        
        return array(
              'titulo' =>  'Reporte de Rechazos por Mes',
              'form' =>  $formYm,

                );
	}



    private function splitStringArray($stringDate)
    {
       return explode("-",$stringDate['selectYearMonth']);
    }


	
}