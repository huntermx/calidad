<?php

namespace Graphs\Model\HighchartsWapper;


class HcSeriesGlue
{

    protected $cont;
    protected $objArray;


    public function __construct()
    {
        $this->cont = 0;
        
    }

    public function setObjSeries(HcSerie $obj)
    {
        $this->objArray[$this->cont] = $obj;
        $this->cont++;
    }

    public function getContObj()
    {
        return $this->cont;
    }


    private function getObjString()
    {
        $temp;

        foreach($this->objArray as $obj)
        {
            $temp[] = $obj->getData();
        }

         $comma_separated = implode(",",$temp);
         return $comma_separated;
    }


    public function getData()
    {
        return $this->getObjString();
    }

}