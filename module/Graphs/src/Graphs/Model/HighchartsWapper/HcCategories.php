<?php

namespace Graphs\Model\HighchartsWapper;


class HcCategories
{

   protected $arrayData;
   protected $formatSData;
   protected $objApostrophe;
   


    public function __construct(array $arrayData)
    {
        $this->setArrayData($arrayData);
        
    }



    public function setArrayData(array $array)
    {
         $this->objApostrophe = new HcApostropheArrayFormat($array);
         $this->arrayData = $this->objApostrophe->getFormatedArray();
         $this->formatSData = FALSE;
    }

    

    private function formatHc()
    {
       
        $comma_separated = implode(",",$this->arrayData);
        $this->formatSData = "categories: [" . $comma_separated . "]";
    }



    public function getFormattedData()
    {
        if(!$this->formatSData)
        {
            $this->formatHc();
        }
        return $this->formatSData;
    }





}