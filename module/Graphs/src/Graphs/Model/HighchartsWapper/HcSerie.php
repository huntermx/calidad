<?php

namespace Graphs\Model\HighchartsWapper;

use Graphs\Model\HighchartsWapper\HcSerieMarker;


class HcSerie
{

   protected $arrayData;
   protected $formatSData;
   protected $name;
   protected $color;
   protected $type;
   protected $columnDataLabel;
   protected $marker;
   protected $rotation;
   protected $labelTextColor;
   protected $yAxis;
   protected $dashStyle;
   protected $objMarker;
 //  protected $marker;



    public function __construct(array $arrayData)
    {
        $this->setArrayData($arrayData);
        $this->color = "'#005487'";
        $this->type = false;
        $this->name = false;
        $this->cdlabel = true;
        $this->marker = false;
        $this->rotation ="-90";
        $this->labelTextColor = '#FFF';
        $this->yAxis =false;
        $this->dashStyle = false;
        $this->marker = false;
        $this->objMarker = false;
        
    }

    public function setYAxis($num)
    {
        if(is_numeric($num) == false)
        {
             throw new Exception('Axix must be valid number (1-2)');
        }
        $this->yAxis =$num;
    }

    public function setName($name)
    {
        if(!empty($name))
        {
            $this->name= "'{$name}'";
        }else{
            $this->name = false;
        }

    }

    public function setRotation($rotation)
    {
        if(empty($rotation))
        {
            $this->rotation= "-90";
        }else{
            $this->rotation = $rotation;
        }
    }

    public function setLabelTextColor($textColor)
    {
        if(empty($textColor))
        {
            $this->labelTextColor= "#FFF";
        }else{
            $this->labelTextColor = $textColor;
        }

    }

    public function setColor($color)
    {
        if(!empty($color))
        {
            $this->color= "'{$color}'";
        }else{
            $this->color = false;
        }

    }

    public function setType($type)
    {
        if(!empty($type))
        {
            $this->type= "'{$type}'";
        }else{
            $this->type = false;
        }

    }

    public function setDashStyle($dStyle)
    {
        if(!empty($dStyle))
        {
            $this->dashStyle= "'{$dStyle}'";
        }else{
            $this->dashStyle = false;
        }

    }


    public function setObjMarker($objMarker)
    {
        if($objMarker instanceof HcSerieMarker)
        {
            $this->objMarker = $objMarker;
        }
        else
        {
            throw new Exception("Error Object Marker must be HcSerieMarker", 1);
        }



    }

   /* public function setMarker($marker)
    {
         if(!empty($marker))
        {
            $this->marker= "marker: {
                    enabled: false
                },";
        }else{
            $this->marker = false;
        }
    }*/


    public function setArrayData(array $array)
    {
         $this->arrayData = $array;
         $this->formatHc();
    }

    private function formatHc()
    {

        $comma_separated = implode(",",$this->arrayData);
        $this->formatSData = "data: [" . $comma_separated . "]";
    }

    public function getFormattedData()
    {
        if(!$this->formatSData)
        {
            $this->formatHc();
        }
        return $this->formatSData;
    }

    public function setMarker($boolean)
    {
        $this->marker = $boolean;
    }




    private function checkMarker()
    {

        $temp = "";
        if($this->marker==FALSE)
        {
            $temp= "";
        }else{

            $temp= "marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }";

        }
        return $temp;
    }


    private function checkColumnDl()
    {
        if($this->cdlabel==FALSE)
        {
            $this->columnDataLabel= "";
        }else{

            $this->columnDataLabel= " dataLabels: {
                    enabled: true,
                    rotation: {$this->rotation},
                    color: '{$this->labelTextColor}',
                    align: 'right',
                    x: 4,
                    y: 15,
                    style: {
                        fontSize: '100%',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 2px black'
                    }
            
                }";

        }
        return $this->columnDataLabel;
    }


    public function setColumnDataLabels($boolean)
    {
        if($boolean == true)
        {
            $this->cdlabel = $boolean;
        }else{
            $this->cdlabel = false;
        }

    }


    private function formattedSeries()
    {
        $temp = "{";
        $arrayFormatted = array();

        if($this->name)
        {
            $arrayFormatted[] =  " name: {$this->name}";
        }

        if($this->color)
        {
            $arrayFormatted[] = " color: {$this->color}";
        }

        if($this->type)
        {
            $arrayFormatted[] = " type: {$this->type}";
        }
        
        if($this->yAxis)
        {
            $arrayFormatted[] = " yAxis: {$this->yAxis}";
        }

        if($this->formatSData)
        {
            $arrayFormatted[] = $this->getFormattedData();
        }

        if($this->cdlabel)
        {
            $arrayFormatted[] = $this->checkColumnDl();
        }

        if($this->dashStyle)
        {
            $arrayFormatted[] = " dashStyle: {$this->dashStyle}";
        }

        if($this->objMarker)
        {
            $arrayFormatted[] = $this->objMarker->getData();
        }

        $implode = implode(",",$arrayFormatted);    
        $temp = $temp  . $implode . "}";

        return $temp;

    }



     public function getData()
    {
        return $this->formattedSeries();
    }

}