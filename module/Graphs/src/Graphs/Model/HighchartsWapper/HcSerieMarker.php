<?php

namespace Graphs\Model\HighchartsWapper;


class HcSerieMarker
{
   protected $lineWidth;
   protected $lineColor;
   protected $fillColor;
   protected $enabled;
   protected $formattedData;

   public function __construct()
   {   
        $this->lineWidth = false;
        $this->lineColor = false;
        $this->fillColor = false;
        $this->enabled = false;   
   }

   public function setLineWidth($lineWidth)
   {
        if(!is_numeric($lineWidth))
        {
            throw new Exception("Error lineWidth must be numeric", 1);
        }
        $this->lineWidth = $lineWidth;
   }

   public function setLineColor($lineColor)
   {
        if(!is_string($lineColor))
        {
            throw new Exception("Error lineCOlor must be string", 1);
        }
        $this->lineColor = $lineColor;
   }

   public function setFillColor($fillColor)
   {
        if(!empty($fillColor))
        {
            throw new Exception("Error FillColor must be string", 1);
        }
        $this->fillColor = $fillColor;
   }

   public function setEnable($boolean)
   {
        if(is_bool($boolean)===false)
        {
            throw new Exception("Error boolean must be Bool", 1);
        }
        $this->enabled = $boolean;
   }


    private function formattedSeries()
    {
        $temp = " marker: { ";
        $arrayFormatted = array();

        if($this->lineWidth)
        {
            $arrayFormatted[] =  " lineWidth: {$this->lineWidth}";
        }

        if($this->lineColor)
        {
            $arrayFormatted[] =  " lineColor: {$this->lineColor}";
        }

        if($this->fillColor)
        {
            $arrayFormatted[] =  " fillColor: '{$this->fillColor}'";
        }



        if($this->enabled===true)
        {
            $arrayFormatted[] =  " enabled: true";
        }
        else
        {
            $arrayFormatted[] =  " enabled: false";
        }


        $implode = implode(",",$arrayFormatted);    
        $temp = $temp  . $implode . "}";

        return $temp;

    }


     public function getData()
    {
        return $this->formattedSeries();
    }

}