<?php

namespace Graphs\Model\HighchartsWapper;


class HcApostropheArrayFormat
{
	protected $formatedArray;

	public function __construct(array $arrayData)
    {
        $this->setFormatedArray($arrayData);
    }

    public function setFormatedArray(array $arrayData)
    {
    	if(empty($arrayData))
    	{
    		throw new Exception('The Array is Empty');
    	}
    	$this->formatedArray = $arrayData;
    }

    private function checkArrayFormmat()
    {
        $tempArray = array();
        foreach($this->formatedArray as $value)
        {
            if (strpos($value,"'") == false) 
            {
                $value = "'{$value}'";
            }

            array_push($tempArray,$value);
        }

        $this->formatedArray = $tempArray;
    }

    public function getFormatedArray()
    {
    	$this->checkArrayFormmat();
    	return $this->formatedArray;
    }
	
}