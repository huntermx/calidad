<?php
namespace Graphs\Model\Dppm;



use Graphs\Model\Dao\InspeccionDimensionalTable;
use Graphs\Model\Dao\InspeccionVisualTable;

Abstract class ClassDppmSetup
{
	protected $objDimData;
	protected $objVisData;
	protected $yearData;


	public function __construct($objDimData=false, $objVisData=false, $year=false)
	{
		if($objDimData)
		{
			$this->setObjDimData($objDimData);
		}

		if($objVisData)
		{
			$this->setObjVisData($objVisData);
		}

		$this->setYearData($year);
	}

	public function setYearData($year)
	{
		if(!is_numeric($year))
		{
			throw new \Exception("year is not llnumeric value $year");
		}
		$this->yearData = $year;
	}

	public function getYearData()
	{
		return $this->yearData;
	}

	public function setObjDimData($obj)
	{
		$var = $obj instanceof InspeccionDimensionalTable;
		if($var == false)
		{
			 throw new \Exception('obj not an instance of InspeccionDimensionalTable');
		}

		$this->objDimData = $obj;
	}

	public function setObjViSData($obj)
	{
		$var = $obj instanceof InspeccionVisualTable;
		if($var == false)
		{
			 throw new Exception('obj not an instance of InspeccionVisualTable');
		}
		$this->objVisData = $obj;
	}




}