<?php
namespace Graphs\Model\Dppm;



use Graphs\Model\Dao\InspeccionDimensionalTable;
use Graphs\Model\Dao\InspeccionVisualTable;

class ClassQtyInspeccionada extends ClassDppmSetup
{
	protected $yearQtyInspect;
	protected $family;

	public function __construct($objDimData=false, $objVisData=false, $year=false, $family=false)
	{
		parent::__construct($objDimData,$objVisData, $year);
		$this->family = $family;
	}

	protected function calcYearData()
	{
		$tempArray = array();

		for($i = 1; $i<=12; $i++)
		{
			$tempArray[$i] = $this->getQtyByMonth($i);
		}
		return $tempArray;
	}

	public function getQtyByMonth($month)
	{
		if($month<10)
		{
			$month = "0{$month}";
		}
		$formatDate = "{$month}-" . $this->getYearData();

		if($this->family ==false || $this->family =="Todas")
		{
			$resultDim = $this->objDimData->getCantidadInspPorMes($formatDate);
			$resultVis = $this->objVisData->getCantidadInspPorMes($formatDate);
		}
		else if($this->family =="HCM")
		{
			$resultDim = $this->objDimData->getCantidadInspPorMesHCM($formatDate);
			$resultVis = $this->objVisData->getCantidadInspPorMesByFamily($formatDate,"HCM");
		}
		else if($this->family =="Proprietary")
		{
			$resultDim = $this->objDimData->getCantidadInspPorMesProprietary($formatDate);
			$resultVis = $this->objVisData->getCantidadInspPorMesByFamily($formatDate,"Proprietary");
		}




		return ($this->extractQtyfromArray($resultDim->current())+ $this->extractQtyfromArrayVis($resultVis->current()));
	}







	protected function extractQtyfromArray($array)
	{
		if(empty($array['cantidadinspeccion']))
		{
			return 0;
		}
		return $array['cantidadinspeccion'];
	}


	protected function extractQtyfromArrayVis($array)
	{
		if(empty($array['cantinspeccionada']))
		{
			return 0;
		}
		return $array['cantinspeccionada'];
	}



	public function getYearQty($year = false)
	{
		if($year)
		{
			$this->setYearData($year);
		}

		return $this->calcYearData();
	}
	
}