<?php
namespace Graphs\Model\Dppm;




use Graphs\Model\Dppm\ClassQtyInspeccionada;
use Graphs\Model\Dppm\ClassQtyRechazada;

class ClassDppm 
{
	protected $objInspected;
	protected $objRejected;
	protected $yearDppm;
	protected $yearInspectedAcum;
	protected $yearRejectedAcum;
	protected $yearDppmAcum;


	public function __construct($objInspected=false, $objRejected=false, $year=false)
	{
		$this->setObjInspected($objInspected);
		$this->setObjRejected($objRejected);
		$this->setYearData($year);
	}

	public function setYearData($year)
	{
		if(!is_numeric($year))
		{
			throw new Exception('year is not numeric value');
		}
		$this->yearData = $year;

	}

	public function getYearData()
	{
		return $this->yearData;
	}

	public function setObjInspected($objInsp)
	{
		if(($objInsp instanceof ClassQtyInspeccionada)==false)
		{
  			throw new Exception('NOT A VALID OBJECT');
		}

		$this->objInspected = $objInsp;
	}	

	public function setObjRejected($objReject)
	{
		if(($objReject instanceof ClassQtyRechazada)==false)
		{
  			throw new Exception('NOT A VALID OBJECT');
		}

		$this->objRejected = $objReject;
	}	


	public function getQtyByMonth($month)
	{
		$resultInspected = $this->objInspected->getQtyByMonth($month);
		$resultRejected = $this->objRejected->getQtyByMonth($month);
		if($resultInspected == 0)
		{
			return 0;
		}
		$dppm = ($resultRejected/$resultInspected) * 1000000;
		return number_format($dppm,2,'.', '');
	}


	protected function calcYearData()
	{
		$tempArray = array();

		for($i = 1; $i<=12; $i++)
		{
			$tempArray[$i] = $this->getQtyByMonth($i);
		}
		return $tempArray;
	}


	private function getQtyYearSum()
	{
		$inspAcum =0;
		$reqchAcum = 0;

		for($i=1; $i<=12; $i++)
		{
			$inspAcum 				=  $inspAcum + $this->objInspected->getQtyByMonth($i);
			$yearInspectedAcum[$i]	=  $inspAcum;

			$reqchAcum 				= $reqchAcum + $this->objRejected->getQtyByMonth($i);
			$yearRejectedAcum[$i]	=  $reqchAcum;

			if($yearInspectedAcum[$i] <= 0)
			{
				$result = 0;
			}
			else
			{
				$result = ($yearRejectedAcum[$i] / $yearInspectedAcum[$i]) * 1000000;
				$result = number_format($result,2,'.', '');
			}
			$yearDppmAcum[$i] = $result;
		}
		return $yearDppmAcum;
	}

	public function getYearAcumm($year = false)
	{
		return $this->getQtyYearSum();
	}


	public function getYearQty($year = false)
	{
		if($year)
		{
			$this->setYearData($year);
		}

		return $this->calcYearData();
	}


}