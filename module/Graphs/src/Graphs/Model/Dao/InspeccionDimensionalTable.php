<?php

namespace Graphs\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\InspeccionDimensional;


class InspeccionDimensionalTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

    public function getRowByRegistroId($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('registro_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            $temp = new InspeccionDimensional();
            $temp->exchangeArray(array());
            return $temp;
        }
        return $row;
    }
   
    
    public function agregar(InspeccionDimensional $inspeccion)
    {
        $data = array(
            'registro_id'               => trim($inspeccion->registro_id),
            'cantidad_inspeccion'       => trim($inspeccion->cantidad_inspeccion),
            'fuera_especificacion'      => trim($inspeccion->fuera_especificacion),
            'cantidad_rechazada'         => trim($inspeccion->cantidad_rechazada),
            'prueba_funcional'          => trim($inspeccion->prueba_funcional),
            'primera_corrida'           => trim($inspeccion->primera_corrida),
            'hora_recibido'             => trim($inspeccion->hora_recibido),
            'hora_calificacion'         => trim($inspeccion->hora_calificacion),
            'tip_sheet'                 => trim($inspeccion->tip_sheet),
            'resultado'                 => trim($inspeccion->resultado),
            'comentarios'               => trim($inspeccion->comentarios),

        );

        $id = (int)$inspeccion->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;

        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function getLastId()
   {
        return $this->last_id;
   }

   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    


    public function getValidYearData($year)
    {
         $sql =" SELECT sum(inspdim.cantidad_inspeccion) as value
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%Y\") = '{$year}'  ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        $var =$result->current();

        if(empty($var['value']))
        {   
            return false;
        }
        return true;
    }


    public function getFirstDataYear()
    {
         $sql ="SELECT DISTINCT DATE_FORMAT(inspreg.fecha_captura, \"%Y\") as year from inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id 
                ORDER BY inspreg.fecha_captura ASC LIMIT 1";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        $var =$result->current();
         if(empty($var['year']))
        {   
            return false;
        }

        return $var['year'];

    }


    public function getCantidadInspPorMesHCM($formatDate)
    {
         
        $sql =" SELECT sum(inspdim.cantidad_inspeccion) as cantidadinspeccion
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id
                                LEFT JOIN partes as prts on inspreg.partes_id = prts.id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}' 
                AND  prts.division = 'HCM' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

    public function getCantidadInspPorMesProprietary($formatDate)
    {
         
        $sql =" SELECT sum(inspdim.cantidad_inspeccion) as cantidadinspeccion
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id
                LEFT JOIN partes as prts on inspreg.partes_id = prts.id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}' 
                AND  prts.division = 'Proprietary' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }
    
    public function getCantidadInspPorMes($formatDate)
    {
         
        $sql =" SELECT sum(inspdim.cantidad_inspeccion) as cantidadinspeccion
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}'  ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

    public function getCantidadRechazadaPorMes($formatDate)
    {
         
        $sql =" SELECT sum(inspdim.fuera_especificacion) as cantidadrechazada
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}'  ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


    public function getCantidadRechazadaPorMesHCM($formatDate)
    {
         
        $sql =" SELECT sum(inspdim.fuera_especificacion) as cantidadrechazada
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id
                LEFT JOIN partes as prts on inspreg.partes_id = prts.id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}'  
                AND  prts.division = 'HCM'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


    public function getCantidadRechazadaPorMesProprietary($formatDate)
    {
         
        $sql =" SELECT sum(inspdim.fuera_especificacion) as cantidadrechazada
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_dimensional as inspdim ON inspreg.id = inspdim.registro_id
                LEFT JOIN partes as prts on inspreg.partes_id = prts.id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}'  
                AND  prts.division = 'Proprietary'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }
    
}