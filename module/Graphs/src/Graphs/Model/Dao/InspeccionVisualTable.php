<?php

namespace Graphs\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Graphs\Model\Entity\InspeccionVisual;


class InspeccionVisualTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;


    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;

    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

   

    public function getRowByRegistroId($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('registro_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            $temp = new InspeccionVisual();
            $temp->exchangeArray(array());
            return $temp;
        }
        return $row;
    }
   
    
    public function agregar(InspeccionVisual $inspeccion)
    {
        $data = array(
            'registro_id'               => trim($inspeccion->registro_id),
            'cantidad_inspeccionada'    => trim($inspeccion->cantidad_inspeccionada),
            'cantidad_liberada'         => trim($inspeccion->cantidad_liberada),
            'cantidad_rechazada'        => trim($inspeccion->cantidad_rechazada),
            'total_defectos'            => trim($inspeccion->total_defectos),
            'comentarios'               => trim($inspeccion->comentarios),
        );

        $id = (int)$inspeccion->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;

        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

   public function getLastId()
   {
        return $this->last_id;
   }

   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
    
    public function getCantidadInspPorMesByFamily($formatDate,$family=false)
    {
        if($family ==false)
        {
            throw new \Exception("family not Found");
        }
         
        $sql =" SELECT sum(inspvis.cantidad_inspeccionada) as cantinspeccionada, sum(inspvis.cantidad_liberada) as cantliberada,
                sum(inspvis.cantidad_inspeccionada)  + sum(inspvis.cantidad_liberada) as cantidadinspeccion
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_visual as inspvis ON inspreg.id = inspvis.registro_id
                LEFT JOIN partes as prts on inspreg.partes_id = prts.id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}' 
                AND  prts.division = '{$family}'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
        
    }


     public function getCantidadInspPorMes($formatDate)
    {
         
        $sql =" SELECT sum(inspvis.cantidad_inspeccionada) as cantinspeccionada, sum(inspvis.cantidad_liberada) as cantliberada,
                sum(inspvis.cantidad_inspeccionada)  + sum(inspvis.cantidad_liberada) as cantidadinspeccion
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_visual as inspvis ON inspreg.id = inspvis.registro_id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }



     public function getCantidadRechazadaPorMes($formatDate)
    {
        $sql = "SELECT sum(inspvisdet.cantidad) as cantidadrechazada
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_visual as inspvis ON inspreg.id = inspvis.registro_id
                LEFT JOIN inspeccion_visual_detalle as inspvisdet ON inspvis.id = inspvisdet.visual_id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

    
     public function getCantidadRechazadaPorMesByFamily($formatDate,$family=false)
    {

        if($family ==false)
        {
            throw new \Exception("family not Found");
        }


        $sql = "SELECT sum(inspvisdet.cantidad) as cantidadrechazada
                FROM inspeccion_registro as inspreg
                LEFT JOIN inspeccion_visual as inspvis ON inspreg.id = inspvis.registro_id
                LEFT JOIN inspeccion_visual_detalle as inspvisdet ON inspvis.id = inspvisdet.visual_id
                LEFT JOIN partes as prts on inspreg.partes_id = prts.id
                WHERE DATE_FORMAT(inspreg.fecha_captura, \"%m-%Y\") = '{$formatDate}'
                AND  prts.division = '{$family}' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


}