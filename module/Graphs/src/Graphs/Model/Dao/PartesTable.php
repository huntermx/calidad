<?php

namespace Graphs\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Graphs\Model\Entity\Partes;

class PartesTable
{

    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();                     
        return $resultSet;
    }

        public function getPartesList()
    {
            $lista = array();
            $resultSet = $this->tableGateway->select();

            foreach ($resultSet as $result)
            {
                $lista[]= array('value' => $result->id , 'label' =>$result->num_parte);
            }
             
            return $lista;
    }



    public function agregar(Partes $partes)
    {
        $data = array(
            'fecha'                => date('Y-m-d H:i:s'),
            'num_parte'            => trim($partes->num_parte),
            'descripcion'          => trim($partes->descripcion),
            'familia'              => trim($partes->familia),
            'division'             => $partes->division,
        );

        $id = (int)$partes->id;

       
        if ($id == 0) {      

            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


    public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    

    public function getAllFamilys()
    {
    
        $sql = "SELECT DISTINCT division FROM partes";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
        
    }
	
}
