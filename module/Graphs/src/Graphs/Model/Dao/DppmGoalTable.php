<?php

namespace Graphs\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Graphs\Model\Entity\DppmGoal;

class DppmGoalTable
{
	protected $tableGateway;
	protected $adapter;

	public function __construct(TableGateway $tableGateway,Adapter $adapter)
	{
		$this->tableGateway = $tableGateway;
		$this->adapter	= $adapter;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	

    public function getDppmGoalForYear($year)
    {
        //$id  = (int) $id;
        $rowset = $this->tableGateway->select(array('anio' => $year));
        $row = $rowset->current();
        if (!$row) {
            $objDppmGoal = new DppmGoal();
            $objDppmGoal->exchangeArray(array('id'=>0,'anio'=>$year, 'dppm_goal' => 0));
           return $objDppmGoal;
        }
        return $row;
    }
   

    public function agregar(DppmGoal $dppm)
    {
        $data = array(
            'anio'           => trim($dppm->anio),
            'dppm_goal'      => trim($dppm->dppm_goal),
        );


        $id = (int)$dppm->id;

       
        if ($id == 0) {      

            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }



      public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }


  
	
}
