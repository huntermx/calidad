<?php

namespace Graphs\Model\Entity;

class DppmGoal
{
	public $id;
	public $anio;
	public $dppm_goal;

	public function exchangeArray($data)
	{
		$this->id           	= (isset($data['id'])) ? $data['id'] : 0;
		$this->anio     		= (isset($data['anio'])) ? $data['anio'] : null;
		$this->dppm_goal     	= (isset($data['dppm_goal'])) ? $data['dppm_goal'] : null;
	}
}