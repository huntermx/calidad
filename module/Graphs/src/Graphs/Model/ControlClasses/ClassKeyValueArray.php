<?php

namespace Graphs\Model\ControlCLasses;


class ClassKeyValueArray
{
	protected $keyValueArray;
	protected $keyArray;
	protected $valueArray;
	

	public function __construct(array $kVArray)
	{
		$this->setKeyValueArray($kVArray);
		$this->keyArray = false;
		$this->valueArray = false;
	}

	public function setKeyValueArray(array $array)
	{
		$this->keyValueArray = $array;
	}

	public function getKeyValueArray()
	{
		return $this->keyValueArray;
	}

	protected function splitData()
	{
		if(!$this->keyValueArray)
		{
			 throw new Exception('There is no a valid Array value');
		}
		$this->keyArray = array();
		$this->valueArray = array();
		foreach($this->keyValueArray as $key=>$value)
		{
			array_push($this->keyArray,$key);
			array_push($this->valueArray,$value);
		}
	}

	public function getKeyArray()
	{
		if(!$this->keyArray)
		{
			$this->splitData();
		}
		return $this->keyArray;
	}

	public function getValueArray()
	{
		if(!$this->valueArray)
		{
			$this->splitData();
		}
		return $this->valueArray;
	}


}