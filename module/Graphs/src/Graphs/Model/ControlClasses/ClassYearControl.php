<?php

namespace Graphs\Model\ControlCLasses;


class ClassYearControl
{
	protected $startYear;
	protected $order;
	protected $objData;
	protected $vectorYear;
	protected $sumValue;


	public function __construct($year=false, $order=false, $sum=false)
	{
		if($year)
		{
			$this->setStartYear($year);
		}

		$this->setOrder($order);
		$this->setSumValue($sum);

	}

	public function setSumValue($sum)
	{
		if($sum==false)
		{
			$this->sumValue =1;
		}
		else
		{
		$this->sumValue = $sum;
		}
	}

	public function setObjData($objData)
	{
		$this->objData = $objData;
	}

	public function setOrder($order)
	{
		if(!$order)
		{
			$this->order =1;
		}
		else
		{
			$this->order = 0;
		}

	}

	public function setStartYear($year)
	{
		if(!is_numeric($year))
		{
			  throw new Exception('NOT A NUMERIC VALUE FOR YEAR');
		}
		$this->startYear = $year;
	}

	private function checkObjData()
	{
		if(!$this->startYear)
		{
			throw new Exception('NOT A NUMERIC VALUE FOR YEAR');
		}

		if(!$this->objData)
		{
				throw new Exception('NOT A VALID OBJECT');
		}

		$tempYear = $this->startYear;
		$i = 0;

		while($this->objData->getValidYearData($tempYear))
		{
			if($i>=100)
			{
				break;

			}
			$this->vectorYear[$tempYear]= $tempYear;

			if($this->order == 1)
			{
				$tempYear = $tempYear + $this->sumValue;
			}else{
				$tempYear = $tempYear - $this->sumValue;
			}
		
			$i++;
		}
	}

	public function getVectorYear()
	{
		if(empty($this->vectorYear))
		{
			$this->checkObjData();
		}

		return $this->vectorYear;
	}


}