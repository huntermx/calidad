<?php

namespace Graphs\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormSelectYmType
{
	
     /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(true)
     * @Annotation\Attributes({"id":"inputDate"})
     * @Annotation\Attributes({"class":"mtz-monthpicker-widgetcontainer"})
     * @Annotation\Filter({"name":"StripTags"})
     * 
     */
    public $selectYearMonth;
    


    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Go!"})
     */
    public $submit;
}