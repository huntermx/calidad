<?php
return array(
    'controllers' => array(
        'invokables' => array(
              'Graphs\Controller\Dppmgraph'             => 'Graphs\Controller\DppmgraphController',
              'Graphs\Controller\RechazosPorPeriodo'    =>  'Graphs\Controller\RechazosPorPeriodoController',
        ),
    ),



    'router' => array(
        'routes' => array(

            'dppmgraph' => array(
                'type'    => 'segment',
                'options' => array(
                'route'    => '[/:lang]/dppmgraph[/:action][/:id]',
                'constraints' => array(
                            'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                    'controller' => 'Graphs\Controller\Dppmgraph',
                    'action'     => 'index',
                    ),
            ),
            ),  


            'reporech' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/reporech',
                'defaults' => array(
                '__NAMESPACE__' => 'Graphs\Controller',
                    'controller' => 'RechazosPorPeriodo',
                    'action' => 'index',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/reporech[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),

            'reporterechazos' => array(
                'type'    => 'segment',
                'options' => array(
                'route'    => '[/:lang]/reporterechazos[/:action][/:id]',
                'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                    'controller' => 'Graphs\Controller\RechazosPorPeriodo',
                    'action'     => 'index',
                    ),
            ),
            ),         

        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'Graphs' => __DIR__ . '/../view',
        ),
    	
    		
    ),

     'view_helpers' => array(
        'invokables'=> array(
            'EmptyToZeroHelper' => 'Graphs\View\Helper\EmptyToZeroHelper'  
        )
    ),  
	
);
