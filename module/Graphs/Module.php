<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Graphs;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\EventManager\StaticEventManager;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\MvcEvent;

use Graphs\Model\Entity\InspeccionDimensional;
use Graphs\Model\Entity\InspeccionVisual;
use Graphs\Model\Entity\DppmGoal;
use Graphs\Model\Entity\Partes;
use Graphs\Model\Entity\InspeccionRegistro;

use Graphs\Model\Dao\PartesTable;
use Graphs\Model\Dao\DppmGoalTable;
use Graphs\Model\Dao\InspeccionVisualTable;
use Graphs\Model\Dao\InspeccionDimensionalTable;
use Graphs\Model\Dao\InspeccionRegistroTable;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
   
    public function onBootstrap($e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
    } 
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    
    
     public function loadConfiguration(MvcEvent $e)
    {
    	$application   = $e->getApplication();
		$sm            = $application->getServiceManager();
		$sharedManager = $application->getEventManager()->getSharedManager();
		$sharedManager->attach('Zend\Mvc\Controller\AbstractActionController','dispatch', 
             function($e) use ($sm) {
		$sm->get('ControllerPluginManager')->get('AuthPlugin')
                   ->doAuthorization($e); //pass to the plugin...    
	    }
        );
    }
    

  public function getServiceConfig()
  {
        return array(
                'factories' => array(
                        'Graphs\Model\Dao\InspeccionDimensionalTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspeccionDimensionalTableGateway');
                            $table = new InspeccionDimensionalTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspeccionDimensionalTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspeccionDimensional());
                            return new TableGateway('inspeccion_dimensional', $dbAdapter, null, $resultSetPrototype);
                        },

                        'Graphs\Model\Dao\InspeccionVisualTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspeccionVisualTableGateway');
                            $table = new InspeccionVisualTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspeccionVisualTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspeccionVisual());
                            return new TableGateway('inspeccion_visual', $dbAdapter, null, $resultSetPrototype);
                        },

                        'Graphs\Model\Dao\DppmGoalTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('DppmGoalTableGateway');
                            $table = new DppmGoalTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'DppmGoalTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new DppmGoal());
                            return new TableGateway('dppm_goal', $dbAdapter, null, $resultSetPrototype);
                        },

                        'Graphs\Model\Dao\PartesTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('PartesTableGateway');
                            $table = new PartesTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'PartesTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Partes());
                            return new TableGateway('partes', $dbAdapter, null, $resultSetPrototype);
                        },

                        'Graphs\Model\Dao\InspeccionRegistroTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspeccionRegistroTableGateway');
                            $table = new InspeccionRegistroTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspeccionRegistroTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspeccionRegistro());
                            return new TableGateway('inspeccion_registro', $dbAdapter, null, $resultSetPrototype);
                        },    



                ),
                );
  }

    
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
    

            ),
        );
    }
    
}
