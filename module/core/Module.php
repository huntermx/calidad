<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace core;

use Zend\ModuleManager\Feature\ViewHelperProviderInterface;    
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\EventManager\StaticEventManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Permissions\Acl\Acl,
    Zend\Permissions\Acl\Role\GenericRole as Role,
    Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Config\Reader\Ini;
use Zend\Session\Container as SessionContainer;

use core\Model\LogManager;
use core\Model\Dao\LogTable;
use core\Model\Dao\UsuariosTable;
use core\Model\Dao\RoleTable;
use core\Model\Dao\RecursoTable;
use core\Model\Dao\PermisosTable;
use core\Model\Dao\MaquinasTable;
use core\Model\Dao\MoldesTable;
use core\Model\Dao\PartesTable;
use core\Model\Dao\FamiliasTable;
use core\Model\Dao\MoldeparteTable;
use core\Model\Dao\LogPaginatorTable;
use core\Model\Dao\DppmGoalTable;

use core\Model\Entity\DppmGoal;
use core\Model\Entity\Moldeparte;
use core\Model\Entity\Familias;
use core\Model\Entity\Partes;
use core\Model\Entity\Usuario;
use core\Model\Entity\Log;
use core\Model\Entity\Rol;
use core\Model\Entity\Recurso;
use core\Model\Entity\Permisos;
use core\Model\Entity\Maquina;
use core\Model\Entity\Moldes;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{

    protected $recursoTable;
    protected $roleTable;
    protected $permisosTable;
    protected $sesscontainer;


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

public function mvcPreDispatch($event) {
       // $di = $event->getTarget()->getLocator();
       //$auth = $di->get('core\Event\Authentication');
      //   return $auth->preDispatch($event);
    }



    public function loadConfiguration(MvcEvent $e)
    {
        $application   = $e->getApplication();
        $sm            = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

       
    }
    
    public function onBootstrap($e)
    {
        $this->initConfig($e);
        $this->initViewRender($e);
        $this->initEnvironment($e);
        $app = $e->getParam('application');    
        $app->getEventManager()->attach('dispatch',array($this, 'initAcl'),100);
    	
 
        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 
                         'dispatch', function($e) 
        {
            $controller      = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config          = $e->getApplication()->getServiceManager()->get('config');
            if (isset($config['module_layouts'][$moduleNamespace])) 
            {
                $controller->layout($config['module_layouts'][$moduleNamespace]);
            }
        }, 100);

        $eventManager        = $e->getApplication()->getEventManager();

    
    //	$eventManager->attach('dispatch', array($this, 'loadConfiguration'), 2);
    //	$moduleRouteListener = new ModuleRouteListener();
   // 	$moduleRouteListener->attach($eventManager);



       
    	/*$e->getApplication()->getEventManager()->getSharedManager()
    	->attach('Zend\Mvc\Controller\AbstractActionController',
    			'dispatch', function($e)
    			{
    				$controller      = $e->getTarget();
    				$controllerClass = get_class($controller);
    				$moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
    				$config          = $e->getApplication()->getServiceManager()->get('config');
    				if (isset($config['module_layouts'][$moduleNamespace]))
    				{
    					$controller->layout($config['module_layouts'][$moduleNamespace]);
    				}
    			}, 100);*/
    
    
    }


    public function initAcl(MvcEvent $e)
    {
        $application    = $e->getApplication();
        $sm             = $application->getServiceManager();
        $acl            = new Acl();
        $rolTable       = $this->getRoleTable($sm);
        $rol            = $rolTable->getAllRol();
        $rolelist       = array();
        $roleherencia   = array();
        //Agrega al usuario Administrador que no tendra restricciones.
        //$acl->addRole(new Role('admin'));
        //Agrega cada rol de la base de datos
        foreach($rol as $ro)
        {
            $found = false;
            array_push($rolelist,$ro['role']);
            $role1 =  strtolower($ro['role']);
            
            if(!empty($ro['herencia']))
            {
                    //Si existe herencia de permisos
                $found = array_search($ro['herencia'],$rolelist);
            $herencia = strtolower($ro['herencia']);
                        //Se necesita validar que el Rol de herencia ya este definido
                if($found)
                {
                            //Si el rol de herencia esta definido, agregarlo
                    $acl->addRole(new Role($role1),$herencia);
                }
                else
                {
                            //Si el rol de herencia no esta definido
                            // se manda a una lista para agregarlos al final
                    $roleherencia[]=array('role'=>$role1,'herencia'=>$herencia);
                }
            }
            else
            {
                    //Si el recurso no tiene herencia agregarlo.
                $acl->addRole(new Role($role1));
            }
        }
        
        // Si existen herencias pendientes de agregar se agregan aqui.
        foreach($roleherencia as $r)
        {
            $acl->addRole(new Role($r['role']),$r['herencia']);
        }
        
        $recursoTable   = $this->getRecursoTable($sm);
        $recurso    = $recursoTable->fetchAll();
        // Se agregan los recursos con el formato modulo:controlador
        foreach($recurso as $r)
        {
            $rec = strtolower($r->modulo) . ':' . strtolower($r->controlador);
            $acl->addResource(New Resource("{$rec}"));
        }
        
        $permisoTable   = $this->getPermisosTable($sm);
        $permisos   = $permisoTable->getPermisos();
        //Aqui se le da permisos en toda la aplicacion al Administrador
        $acl->allow('admin');
        foreach($permisos as $permiso)
        {
            $rec = strtolower($permiso['modulo']) . ':' . strtolower($permiso['controlador']);
            if($permiso['permiso']==='allow')
            {
                $acl->allow($permiso['role'], $rec, array($permiso['accion']));
            }
            else
            {
                $acl->deny($permiso['role'], $rec, array($permiso['accion']));
            }
        }
        
      //El rol por omision es Guest, en caso de estar autenticado
      // se lee del vector de la session.
      $role = (! $this->getSessContainer()->role ) ? 'guest' : $this->getSessContainer()->role;
      $routeMatch       = $e->getRouteMatch();
      $controllerClass  = $routeMatch->getParam('controller');
      $controllerArray  = explode("\\",$controllerClass);
      
      $module           = strtolower($controllerArray[0]);
      $controller       = strtolower($controllerArray[2]);
      $action           = $routeMatch->getParam('action');
      $resourceName     = $module . ':' .$controller;

     
     
      
      
      if(!$acl->isAllowed($role,$resourceName,$action))
      {
           switch ($role) {
                case 'guest':
                      $routeMatch->setParam('controller', 'core\Controller\Authenticate');
                      $routeMatch->setParam('action', 'logout');
                      break;
                default:
                      $routeMatch->setParam('controller', 'core\Controller\Inicio');
                      $routeMatch->setParam('action', 'errorauth');
                     break;
                }
      }
    }


    
 public function init()
    {
        // Attach Event to EventManager
        $events = StaticEventManager::getInstance();
        $events->attach('Zend\Mvc\Controller\ActionController', 'dispatch', array($this, 'mvcPreDispatch'), 100); //@todo - Go directly to User\Event\Authentication
    }
    
  
 public function initConfig($e)
    {
        $application =  $e->getApplication();
        $services    =  $application->getServiceManager();
        
        $services->setFactory('ConfigIni', function($services){
            $reader = new Ini();
            $data   = $reader->fromFile(__DIR__ . '/config/config.ini');
            return $data;
        });
    } 

  public function initViewRender($e)
    {
        $application    = $e->getApplication();
        $sm             = $application->getServiceManager();
        $viewRender     = $sm->get('ViewManager')->getRenderer();
        $config         = $sm->get('ConfigIni');
        
        $viewRender->headMeta()
                   ->setCharset($config['parametros']['view']['charset']);
        $viewRender->doctype($config['parametros']['view']['doctype']);
    }
    
    protected function initEnvironment($e)
    {
        error_reporting(E_ALL | E_STRICT);
        ini_set('display_errors',true);
        $application        = $e->getApplication();
        $config             = $application->getServiceManager()->get('ConfigIni');
        $timeZone           = (string)$config['parametros']['timezone'];
        
        if(empty($timeZone)){
            $timeZone       = "America/Los_Angeles";
        }
        date_default_timezone_set($timeZone);
    }





    public function getServiceConfig()
    {

           return array(
                  'factories' => array(
                         'core\Model\Dao\LogPaginatorTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $table = new LogPaginatorTable($dbAdapter);
                            return $table;
                        },
                        'LogManager' => function($sm) {
                            $instance = new LogManager($sm);
                            return $instance;
                        },
                        'AuthService' => function($sm) {
                            $dbAdapter      = $sm->get('Zend\Db\Adapter\Adapter');
                            $dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter, 'usuarios','usuario','password', 'MD5(?)');
                            $select = $dbTableAuthAdapter->getDbSelect();
                            $select->where('nstatus = 1');
                            $authService = new AuthenticationService();
                            $authService->setAdapter($dbTableAuthAdapter);
                            // $authService->setStorage($sm->get('core\Model\MyAuthStorage'));    
                        return $authService;
                        },
                        'core\Model\Dao\LogTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('LogTableGateway');
                            $table = new logTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'LogTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Log());
                            return new TableGateway('log', $dbAdapter, null, $resultSetPrototype);
                        },
                        'core\Model\Dao\UsuariosTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('UsuariosTableGateway');
                            $table = new UsuariosTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'UsuariosTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Usuario());
                            return new TableGateway('usuarios', $dbAdapter, null, $resultSetPrototype);
                        },
                        'core\Model\MyAuthStorage' => function($sm){
                            return new \core\Model\MyAuthStorage('labodigi');
                        },
                       
                        'core\Model\Dao\RecursoTable' =>  function($sm) {
                            $tableGateway = $sm->get('RecursoTableGateway');
                            $table = new RecursoTable($tableGateway);
                            return $table;
                        },
                        'RecursoTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Recurso());
                            return new TableGateway('usuarios_acl_recursos', $dbAdapter, null, $resultSetPrototype);
                        },
                        'core\Model\Dao\RoleTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('RoleTableGateway');
                            $table = new RoleTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'RoleTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Rol());
                            return new TableGateway('usuarios_acl_roles', $dbAdapter, null, $resultSetPrototype);
                        }, 
                        'core\Model\Dao\PermisosTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('PermisosTableGateway');
                            $table = new PermisosTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'PermisosTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Permisos());
                            return new TableGateway('usuarios_acl_permisos', $dbAdapter, null, $resultSetPrototype);
                        },
                        'core\Model\Dao\MaquinasTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('MaquinasTableGateway');
                            $table = new MaquinasTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'MaquinasTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Maquina());
                            return new TableGateway('maquinas', $dbAdapter, null, $resultSetPrototype);
                        },
                        'core\Model\Dao\MoldesTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('MoldesTableGateway');
                            $table = new MoldesTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'MoldesTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Moldes());
                            return new TableGateway('moldes', $dbAdapter, null, $resultSetPrototype);
                        },

                        'core\Model\Dao\PartesTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('PartesTableGateway');
                            $table = new PartesTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'PartesTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Partes());
                            return new TableGateway('partes', $dbAdapter, null, $resultSetPrototype);
                        },

                        'core\Model\Dao\FamiliasTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('FamiliasTableGateway');
                            $table = new FamiliasTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'FamiliasTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Familias());
                            return new TableGateway('partes_familia', $dbAdapter, null, $resultSetPrototype);
                        },

                        'core\Model\Dao\DppmGoalTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('DppmGoalTableGateway');
                            $table = new DppmGoalTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'DppmGoalTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new DppmGoal());
                            return new TableGateway('dppm_goal', $dbAdapter, null, $resultSetPrototype);
                        },


                        'core\Model\Dao\MoldeparteTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('MoldeparteTableGateway');
                            $table = new MoldeparteTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                        'MoldeparteTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Moldeparte());
                            return new TableGateway('molde_parte', $dbAdapter, null, $resultSetPrototype);
                        },


                        

                ),
            );
    }
    
     public function getViewHelperConfig()
    {
        return array(
          'factories' => array(
              'FamiliasHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('core\Model\Dao\FamiliasTable');
                    return new View\Helper\FamiliasHelper($dao);
                },      

                'MoldepartesHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('core\Model\Dao\MoldeparteTable');
                    return new View\Helper\MoldepartesHelper($dao);
                },      

          ),  
        );
    }

    
    private function getSessContainer()
    {
        if (!$this->sesscontainer) {
            $this->sesscontainer = new SessionContainer('labodigi');
        }
        return $this->sesscontainer;
    }

    public function getRecursoTable($sm)
    {
        if (!$this->recursoTable) {
            $this->recursoTable = $sm->get('core\Model\Dao\RecursoTable');
        }
        return $this->recursoTable;
    }
    
    public function getRoleTable($sm)
    {
        if (!$this->roleTable) {
            $this->roleTable = $sm->get('core\Model\Dao\RoleTable');
        }
        return $this->roleTable;
    }
    
    public function getPermisosTable($sm)
    {
        if (!$this->permisosTable) {
            $this->permisosTable = $sm->get('core\Model\Dao\PermisosTable');
        }
        return $this->permisosTable;
    }    


}