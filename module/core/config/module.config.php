<?php
return array(

 'service_manager' => array(
        'factories' => array(
            'Navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
        ),

    
    'controllers' => array(
        'invokables' => array(
        	'core\Controller\Authenticate'         => 'core\Controller\AuthenticateController',
            'core\Controller\Inicio'                => 'core\Controller\InicioController',
            'core\Controller\Maquinasactivas'       => 'core\Controller\MaquinasactivasController',
            'core\Controller\Usuarios'              => 'core\Controller\UsuariosController',
            'core\Controller\Log'                   => 'core\Controller\LogController',
            'core\Controller\Asyn'                  => 'core\Controller\AsynController',
            'core\Controller\Maquinas'              => 'core\Controller\MaquinasController',
            'core\Controller\Moldes'                => 'core\Controller\MoldesController',
            'core\Controller\Partes'                => 'core\Controller\PartesController',
            'core\Controller\Partesmoldes'          => 'core\Controller\PartesmoldesController',
            'core\Controller\Familias'              => 'core\Controller\FamiliasController',
            'core\Controller\Dppmyeargoal'          => 'core\Controller\DppmyeargoalController',
        ),
    ),





    'router' => array(
        'routes' => array(
        		

'partes2' => array(
       'type'    => 'segment',
               'options' => array(
                    'route'    => '/partes2[/:action][/:id][/page/:page][/partes/:partes]',
                    //'route'    => '/paginator2[/:action][/:id][/order_by/:order_by][/:order]',
                    'constraints' => array(
                        'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                         'page' => '[0-9]+',
                        'partes' => '[0-9]+',
                        
                    ), 
                    'defaults' => array(
                             'controller' => 'core\Controller\Partesmoldes',
                            // 'controller'    => 'RegistroInspeccion',
                             'action'        => 'eliminar',
                                    ),
                             ),
        ),   


         'core' => array(
            'type'    => 'Segment',
            'options' => array(
                'route'    => '/core[/:controller[/:action]]',
                'constraints' => array(
        		
                'controller' =>'[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        	
        		),
        		'defaults' => array(
        		       '__NAMESPACE__' => 'core\Controller',
                      'controller'    => 'Authenticate',
                      'action'        => 'login',
        		),
        ),
        ),


'familias' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/familias[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Familias',
                'action'     => 'index',
                ),
        ),
        ),   



'dppmyeargoal' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/dppmyeargoal[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Dppmyeargoal',
                'action'     => 'index',
                ),
        ),
        ),   


        'logpaginator' => array(
                'type'    => 'segment',
               'options' => array(
                    'route'    => '/logpaginator[/:action][/:id][/page/:page][/order_by/:order_by][/:order]',
                    //'route'    => '/paginator2[/:action][/:id][/order_by/:order_by][/:order]',
                    'constraints' => array(
                        'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                         'page' => '[0-9]+',
                        'order_by' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'order' => 'ASC|DESC',
                    ), 
                    'defaults' => array(
                             'controller' => 'core\Controller\Log',
                            // 'controller'    => 'RegistroInspeccion',
                             'action'        => 'index',
                                    ),
                             ),
         ),


        'core' => array(
            'type'    => 'Segment',
            'options' => array(
                'route'    => '/core[/:controller[/:action]]',
                'constraints' => array(
                
                'controller' =>'[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            
                ),
                'defaults' => array(
                       '__NAMESPACE__' => 'core\Controller',
                      'controller'    => 'Authenticate',
                      'action'        => 'login',
                ),
        ),
        ),

  'usuarios' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/usuarios[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Usuarios',
                'action'     => 'index',
                ),
        ),
        ),      

             
'maquinas' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/maquinas[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Maquinas',
                'action'     => 'index',
                ),
        ),
        ),      


'moldes' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/moldes[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Moldes',
                'action'     => 'index',
                ),
        ),
        ),      

'partes' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/partes[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Partes',
                'action'     => 'index',
                ),
        ),
        ),   



'partesmoldes' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '/partesmoldes[/:action][/:parte][/:id]',
         'constraints' => array(
                
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                'parte' => '[a-zA-Z][a-zA-Z0-9_-]*',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Partesmoldes',
                'action'     => 'index',
                ),
        ),
        ),   






 'asyn' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/asyn[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'core\Controller\Asyn',
                'action'     => 'index',
                ),
        ),
        ),

              
      'log' => array(
        'type'    => 'Literal',
        'options' => array(
        'route'    => '/log',
        'defaults' => array(
                    '__NAMESPACE__' => 'core\Controller',
                'controller'    => 'Log',
                'action'        => 'Index',
                ),
                ),
      'may_terminate' => true,
        'child_routes' => array(
            'default' => array(
                'type'    => 'Segment',
            'options' => array(
            'route'    => '/[:action]',
            'constraints' => array(
            'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            ),
                'defaults' => array(
                ),
                ),
        ),
        ),
        ), 

        'login' => array(
        	 'type'    => 'Literal',
        	 'options' => array(
        	 'route'    => '/login',
        	  'defaults' => array(
        	         '__NAMESPACE__' => 'core\Controller',
        		     'controller'    => 'Authenticate',
        								'action'        => 'login',
        	   ),
        	   ),
        	   'may_terminate' => true,
        			'child_routes' => array(
        			'default' => array(
        					'type'    => 'Segment',
        					'options' => array(
        					'route'    => '/[:action]',
        					'constraints' => array(
        					'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
        					'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
        					),
        					'defaults' => array(
        		),
        		),
        						),
        				),
        		),

        'logout' => array(
            'type'    => 'Literal',
            'options' => array(
            'route'    => '/logout',
                'defaults' => array(
                    '__NAMESPACE__' => 'core\Controller',
                'controller'    => 'Authenticate',
                'action'        => 'logout',
                ),
                ),
        'may_terminate' => true,
            'child_routes' => array(
            'default' => array(
                'type'    => 'Segment',
            'options' => array(
            'route'    => '/[:action]',
            'constraints' => array(
            'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            ),
                'defaults' => array(
                ),
                ),
        ),
        ),
        ),  
        		
        'inicio' => array(
            'type'    => 'Literal',
            'options' => array(
            'route'    => '/inicio',
            'defaults' => array(
                    '__NAMESPACE__' => 'core\Controller',
                    'controller'    => 'Inicio',
                    'action'        => 'index',
                ),
                ),
        'may_terminate' => true,
            'child_routes' => array(
            'default' => array(
                'type'    => 'Segment',
                'options' => array(
                'route'    => '/[:action]',
                'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                ),
                    'defaults' => array(
                ),
                ),
        ),
        ),
        ),

        		
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'core' => __DIR__ . '/../view',
        ),
    	'template_map' => array(
    				'layout/core' => __DIR__ . '/../view/layout/core_layout.phtml',
    				'layout/login' => __DIR__ . '/../view/layout/login_layout.phtml',
                    'layout/setup' => __DIR__ . '/../view/layout/setup_layout.phtml',
                    'layout/error' => __DIR__ . '/../view/layout/error_layout.phtml',
                    'paginator-slide' => __DIR__ . '/../view/layout/slidePaginator.phtml',
                    'paginator-log' => __DIR__ . '/../view/layout/slide_paginator_log.phtml',
    		),
    		
    ),
		'module_layouts' => array(
				'core' => 'layout/core',
				'Inicio' => 'layout/core',
		),
		
	  'view_helpers' => array(
        'invokables'=> array(
            'fechaHelper' => 'core\View\Helper\FechaHelper'  
        )
    ),	
);
