<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;

use core\Model\Log\Log;
use core\Model\Log\LogTable;

class LogController extends AbstractCoreController
{
   protected $logTable;

   public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('core', array(
                        'action' =>  'login'));
        }
        

        $select = new Select();

        $order_by = $this->params()->fromRoute('order_by') ?
                $this->params()->fromRoute('order_by') : 'timestamp';
        $order = $this->params()->fromRoute('order') ?
                $this->params()->fromRoute('order') : Select::ORDER_DESCENDING;
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;



        $albums = $this->getLogPaginatorTable()->fetchAll($select->order($order_by . ' ' . $order));
        $itemsPerPage = 25;

        $albums->current();
        $paginator = new Paginator(new paginatorIterator($albums));
        $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(15);

        $albums2 = $this->getLogPaginatorTable()->fetchAll($select->order($order_by . ' ' . $order));

        $paginator2 = new Paginator(new paginatorIterator($albums2));
        $paginator2->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(15);

        //$sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();


        
       $view = new ViewModel(array(
                    'titulo' => 'Registro Sistema',
                    'order_by' => $order_by,
                    'order' => $order,
                    'page' => $page,
                    'paginator' => $paginator,
                    'paginator2' => $paginator2,
                ));

        $view->setTemplate('core/log/indexpaginator.phtml');
return $view;



        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return new ViewModel(array(
            'system_log' => $this->getLogTable()->fetchAll(),
        ));
       
    }
    
    public function getLogTable()
    {
    	if (!$this->logTable) {
    		$sm = $this->getServiceLocator();
    		$this->logTable = $sm->get('core\Model\Dao\LogTable');
    	}
    	return $this->logTable;
    }
    
}
