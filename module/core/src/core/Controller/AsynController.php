<?php

namespace core\Controller;

use Zend\Mvc\Controller\AbstractActionController;


class AsynController extends AbstractCoreController
{
	protected $DistribuidoresTable;
	protected $RolesTable;
	
	public function getusuariorolesAction()
	{
		$roles = $this->getRoleTable()->fetchAll();
		foreach($roles as $rol){
			$items[]= array($rol->id , $rol->role);
		}

		echo json_encode($items);
		exit;
	}
	
	public function getfamiliasAction()
	{
		$roles = $this->getFamiliasTable()->fetchAll();
		foreach($roles as $rol){
			$items[]= array($rol->id , $rol->familia);
		}

		echo json_encode($items);
		exit;
	}

	public function getmoldesAction()
	{
		$roles = $this->getMoldesTable()->fetchAll();
		foreach($roles as $rol){
			$items[]= array($rol->id , $rol->molde);
		}

		echo json_encode($items);
		exit;
	}
	
	public function getpartesAction()
	{
		$roles = $this->getPartesTable()->fetchAll();
		foreach($roles as $rol){
			$items[]= array($rol->id , $rol->num_parte . ' - ' . $rol->descripcion);
		}

		echo json_encode($items);
		exit;
	}

}