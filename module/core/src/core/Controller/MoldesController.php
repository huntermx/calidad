<?php


namespace core\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;

use core\Model\Entity\Moldes;

class MoldesController extends AbstractCoreController
{
	public function indexAction()
	{
		
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
		
		   
		    $this->layout()->tab1 = "";
        $this->layout()->tab2 = "active";
        $this->layout()->tab3 = "";
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	'titulo' => 'Moldes',
            'moldes' => $this->getMoldesTable()->fetchAll(),
        );
	}


 	public function agregarAction()
    {
        $form       = $this->getMoldesForm();
        $request = $this->getRequest();
                $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        if($request->isPost())
        {
            $moldes = new Moldes();
            $form->setData($request->getPost());

            if($form->isValid())
            {

                $moldes->exchangeArray($form->getData());             
                $jencode    = json_encode($form->getData());
                
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta un molde',
                   'descripcion' => "Molde: {$moldes->molde}",
                   'tabla_afectada' => 'moldes',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $moldes->exchangeArray($form->getData());
			   
			     $this->getMoldesTable()->agregar($moldes);
		       $this->layout()->usuario = $this->session->display_name;
           $this->layout()->lastLogin = $this->session->lastLogin;
           return $this->redirect()->toRoute('moldes',array(
		 		         'action' =>  'index'));
            }
         }else{
              return array('form' => $form,);
         }
         
    }



    public function editarAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      if (!$id || $id == 0) {
        return $this->redirect()->toRoute('moldes', array(
            'action' => 'agregar'));
      }
      
      $moldes = $this->getMoldesTable()->getRegistro($id);
      $form  = $this->getMoldesForm();
      $form->bind($moldes);
      $form->get('submit')->setAttribute('value', 'Edit');
      $request = $this->getRequest();
      
      if($request->isPost()) 
      {
            $form->setData($request->getPost());
            if($form->isValid())
            {
                $jencode    = json_encode($form->getData());
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Edito un Molde',
                   'descripcion' => "Molde: {$moldes->molde} ",
                   'tabla_afectada' => 'moldes',     
                   'detalles_extra' => $jencode,
                );      
                   
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $this->getMoldesTable()->agregar($form->getData());
        return $this->redirect()->toRoute('moldes',array(
              'action' =>  'index'));
            }
      }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
      return array(
          'id' => $id,
          'form' => $form,

      );
      
    }


	public function authAction()
	{
		
 
	}
	
}