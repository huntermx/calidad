<?php


namespace core\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;

use core\Model\Entity\Moldeparte;

class PartesmoldesController extends AbstractCoreController
{
	public function indexAction()
	{
		
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
		

		    $this->layout()->tab1 = "";
        $this->layout()->tab2 = "";
        $this->layout()->tab3 = "";
        $this->layout()->tab4 = "active";
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	'titulo' => 'Partes - Moldes',
          'partes' => $this->getPartesTable()->fetchAll(),

        );
	}


 	


    
    public function agregarmoldeAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      $form = $this->getPartesMoldesForm();

      $request = $this->getRequest();

      if($request->isPost())
      {
          $partesMoldes = new Moldeparte();
          $form->setData($request->getPost());
          $form->get('id_molde')->setValueOptions($this->getMoldesTable()->getMoldesList());
          $form->get('id_parte')->setValueOptions($this->getPartesTable()->getPartesList());

          if($form->isValid())
          {


              $partesMoldes->exchangeArray($form->getData());
              $partesMoldes->id_parte = $id;
              $jencode    = json_encode($form->getData());
                
              $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta una Relacion Molde-Parte',
                   'descripcion' => "Molde: {$partesMoldes->id_molde} , parte: {$partesMoldes->id_parte}    ",
                   'tabla_afectada' => 'molde_parte',     
                   'detalles_extra' => $jencode,
              );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
              // $partes->exchangeArray($form->getData());
                $this->getMoldeParteTable()->agregar($partesMoldes);
                $this->layout()->usuario = $this->session->display_name;
                $this->layout()->lastLogin = $this->session->lastLogin;
                $this->layout()->lastAction = $this->getLastAction();
            return $this->redirect()->toRoute('partesmoldes',array(
                 'action' =>  'agregarmolde',
                 'id' => $id));

          }




      }

        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
       return array(
            'titulo' => 'Partes - Moldes',
            'form' => $form,
            'moldes' =>$this->getMoldeParteTable()->getMoldesByParte_id($id),
            'partes_id' => $id,
        );
    }





    public function editarAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);

      if (!$id || $id == 0) {
        return $this->redirect()->toRoute('partes', array(
            'action' => 'agregar'));
      }
      
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
      $partes = $this->getPartesTable()->getRegistro($id);

      $form  = $this->getPartesForm();
      $form->bind($partes);
      $form->get('submit')->setAttribute('value', 'Edit');
      $request = $this->getRequest();
      
      if($request->isPost()) 
      {
            $form->setData($request->getPost());
            $form->get('familia')->setValueOptions($this->getFamiliasTable()->getFamiliasOptions());


            if($form->isValid())
            {
                $jencode    = json_encode($form->getData());
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Edito una relacion Parte-Molde',
                   'descripcion' => "Parte: {$partes->num_parte} ",
                   'tabla_afectada' => 'partes_familia',     
                   'detalles_extra' => $jencode,
                );      
                   
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $this->getPartesTable()->agregar($form->getData());
             return $this->redirect()->toRoute('partes',array(
                  'action' =>  'index'));
              }
      }
     
      return array(
          'id' => $id,
          'form' => $form,
          'familia_id' => $partes->familia,

      );
      
    }


	 public function eliminarAction()
    {
      $moldespartes_id = (int) $this->params()->fromRoute('id', 0);
      if ($moldespartes_id==0 ) {
            return $this->redirect()->toRoute('partes');
      }


      $partesMoldes = $this->getMoldeParteTable()->getRegistro2($moldespartes_id);
      $partes = $this->getPartesTable()->getRegistro($partesMoldes->id_parte);
      $moldes = $this->getMoldesTable()->getRegistro($partesMoldes->id_molde);




      
      $this->layout()->usuario = $this->session->display_name;
      $this->layout()->lastLogin = $this->session->lastLogin;
      $this->layout()->lastAction = $this->getLastAction();
    
      $request = $this->getRequest();
      if ($request->isPost()) 
      {
        $del = $request->getPost('del', 'No');
      
            if ($del == 'Si') 
            {
                $partes = $this->getMoldeParteTable()->getRegistro2($moldespartes_id);

               // $id = (int) $request->getPost('id');

                $this->getMoldeParteTable()->eliminar($moldespartes_id);
               
               $jencode    = json_encode($partes);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino relacion Partes-Moldes',
                   'descripcion' => "Partes : {$partes->num_parte} , {$moldes->molde}",
                   'tabla_afectada' => 'molde_parte',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               }
      
        return $this->redirect()->toRoute('partesmoldes',array(
              'action' =>  'index'));
      }




      return array(
          'id'    => $moldespartes_id,
          'partes' => $partes,
          'moldes' => $moldes,
         
      );
      
    }
	
}