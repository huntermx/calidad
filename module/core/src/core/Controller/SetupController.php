<?php


namespace core\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;

class SetupController extends AbstractCoreController
{
	public function indexAction()
	{
		
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
		$this->layout('layout/setup');
  		$this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        return array(
            'usuario' => $sesion->display_name,
        );
	}


	public function authAction()
	{
		
 
	}
	
}