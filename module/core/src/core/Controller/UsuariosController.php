<?php

namespace core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Xml;
use Zend\Form\Annotation\AnnotationBuilder;

use core\Model\Entity\Usuario;
use Zend\Session\Container as SessionContainer;


class UsuariosController extends AbstractCoreController
{
    protected $form;
    protected $usuariosTable;
    protected $roleTable;
    protected $session;
    
     public function __construct(){
        $this->session    = new SessionContainer('labodigi');    
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;    
    }
    
    public function init()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('core', array(
                        'action' =>  'login'));
        }
    }
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('core', array(
                        'action' =>  'login'));
        }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return new ViewModel(array(
            'usuarios' => $this->getUsuariosTable()->getValidUsers(),
        ));
        
    }
    
 
    
    public function agregarAction()
    {
        $form       = $this->getUsuariosForm();
        $request = $this->getRequest();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        if($request->isPost())
        {
            $usuario = new Usuario();
            $form->get('rol')->setValueOptions($this->getRoleTable()->getListaRoles());
            $form->setData($request->getPost());

            if($form->isValid())
            {

                $usuario->exchangeArray($form->getData());             
                $jencode    = json_encode($form->getData());
                
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta a un usuario',
                   'descripcion' => "Usuario: {$usuario->usuario} , rol: {$usuario->rol}",
                   'tabla_afectada' => 'usuarios',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $usuario->exchangeArray($form->getData());
			   $this->getUsuariosTable()->agregarUsuarios($usuario);
		
                return $this->redirect()->toRoute('usuarios',array(
		 		'action' =>  'index'));
            }
         }else{
              return array('form' => $form,);
         }
         
    }

    public function editarAction()
    {
    	$id = (int) $this->params()->fromRoute('id', 0);
    	if (!$id || $id == 0) {
    		return $this->redirect()->toRoute('usuarios', array(
    				'action' => 'agregar'));
    	}
    	  $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
    	$usuario = $this->getUsuariosTable()->getUsuario($id);
    	$form  = $this->getUsuariosForm();
    	$form->bind($usuario);
    	$form->get('submit')->setAttribute('value', 'Edit');
    	$request = $this->getRequest();
    	
    	if($request->isPost()) 
    	{
            $form->setData($request->getPost());
            $form->get('rol')->setValueOptions($this->getRoleTable()->getListaRoles());
            if($form->isValid())
            {
                $jencode    = json_encode($form->getData());
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Edito a un usuario',
                   'descripcion' => "Usuario: {$usuario->username} , rol: {$usuario->rol}",
                   'tabla_afectada' => 'usuarios',     
                   'detalles_extra' => $jencode,
                );      
                   
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $this->getUsuariosTable()->agregarUsuarios($form->getData());
    		return $this->redirect()->toRoute('usuarios',array(
    					'action' =>  'index'));
            }
    	}
    	return array(
    			'id' => $id,
    			'form' => $form,
                        'rol' => $usuario->rol,
    	);
    	
    }

    public function eliminarAction()
    {
    	$id = (int) $this->params()->fromRoute('id', 0);
    	if (!$id) {
            return $this->redirect()->toRoute('usuarios');
    	}
    	  $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
    	$request = $this->getRequest();
    	if ($request->isPost()) 
    	{
    		$del = $request->getPost('del', 'No');
    	
            if ($del == 'Si') 
            {
                $usuario = $this->getUsuariosTable()->getUsuario($id);
                $id = (int) $request->getPost('id');
                $this->getUsuariosTable()->eliminarUsuario($id);
                $jencode    = json_encode($usuario);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino a un usuario',
                   'descripcion' => "Usuario: {$usuario->username} , tipo_usuario: {$usuario->tipo_usuario}",
                   'tabla_afectada' => 'usuarios',     
                   'detalles_extra' => $jencode,
                );      
               	$this->getServiceLocator()->get('LogManager')->grabaLog($data);
               }
    	
    		return $this->redirect()->toRoute('usuarios',array(
    					'action' =>  'index'));
    	}

    	return array(
    			'id'    => $id,
    			'usuario' => $this->getUsuariosTable()->getUsuario($id)
    	);
    	
    }
    

    
    
}