<?php


namespace core\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;

use core\Model\Entity\DppmGoal;

class DppmyeargoalController extends AbstractCoreController
{
	public function indexAction()
	{
		
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();

		    $this->layout()->tab1 = "";
        $this->layout()->tab2 = "";
        $this->layout()->tab3 = "";
        $this->layout()->tab4 = "";
        $this->layout()->tab5 = "active";

        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	'titulo' => 'Dppm Year Goal',
            'familia' => $this->getDppmGoalTable()->fetchAll(),
        );
	}


 	public function agregarAction()
    {
        $form         = $this->getDppmGoalForm();
        $request      = $this->getRequest();
        
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        
        if($request->isPost())
        {
            $dppmGoal = new DppmGoal();
            $form->setData($request->getPost());

            if($form->isValid())
            {

                $dppmGoal->exchangeArray($form->getData());             
                $jencode    = json_encode($form->getData());
                
               

                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta un nuevo Dppm Goal',
                   'descripcion' => "Dpp Goal {$dppmGoal->anio}: {$dppmGoal->dppm_goal}",
                   'tabla_afectada' => 'dppm_goal',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               //$moldes->exchangeArray($form->getData());
			   
			     $this->getDppmGoalTable()->agregar($dppmGoal);

		       $this->layout()->usuario = $this->session->display_name;
           $this->layout()->lastLogin = $this->session->lastLogin;

           return $this->redirect()->toRoute('dppmyeargoal',array(
		 		         'action' =>  'index'));
            }
         }else{
              return array('form' => $form,);
         }
         
    }



    public function editarAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      if (!$id || $id == 0) {
        return $this->redirect()->toRoute('dppmyeargoal', array(
            'action' => 'agregar'));
      }
      
      $dppmYearGoalData       = $this->getDppmGoalTable()->getRegistro($id);
      $form                   = $this->getDppmGoalForm();


      $form->bind($dppmYearGoalData);
      $form->get('submit')->setAttribute('value', 'Edit');
      $request = $this->getRequest();
      
      if($request->isPost()) 
      {
            $form->setData($request->getPost());
            if($form->isValid())
            {
                $jencode    = json_encode($form->getData());
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Edito un Dppm Year Goal',
                   'descripcion' => "Year: {$dppmYearGoalData->anio}  Goal: {$dppmYearGoalData->dppm_goal}",
                   'tabla_afectada' => 'dppm_goal',     
                   'detalles_extra' => $jencode,
                );      
                   
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               


               $this->getDppmGoalTable()->agregar($form->getData());

               return $this->redirect()->toRoute('dppmyeargoal',array(
              'action' =>  'index'));
            }
      }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
      return array(
          'id' => $id,
          'form' => $form,

      );
      
    }




  public function eliminarAction()
  {
       $dppm_id = (int) $this->params()->fromRoute('id', 0);
      if ($dppm_id==0 ) {
           return $this->redirect()->toRoute('dppmyeargoal', array(
            'action' => 'index'));
      }
      $dppmYearGoalData       = $this->getDppmGoalTable()->getRegistro($dppm_id);


      
      $this->layout()->usuario = $this->session->display_name;
      $this->layout()->lastLogin = $this->session->lastLogin;
      $this->layout()->lastAction = $this->getLastAction();
    
      $request = $this->getRequest();
      if ($request->isPost()) 
      {
        $del = $request->getPost('del', 'No');

     
      
            if ($del == 'Si') 
            {

              //  $partes = $this->getMoldeParteTable()->getRegistro2($moldespartes_id);

               // $id = (int) $request->getPost('id');

                $this->getDppmGoalTable()->eliminar($dppm_id);
               
               $jencode    = json_encode($dppmYearGoalData);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino Dppm Year Goal',
                   'descripcion' => "Year: {$dppmYearGoalData->anio}  Goal: {$dppmYearGoalData->dppm_goal}",
                   'tabla_afectada' => 'dppm_goal',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               }
      
        return $this->redirect()->toRoute('dppmyeargoal',array(
              'action' =>  'index'));
      }




      return array(
          'id'    => $dppm_id,
          'anio' => $dppmYearGoalData->anio,
          'dppm_goal' => $dppmYearGoalData->dppm_goal,
         
      );


  }




	public function authAction()
	{
		
 
	}
	
}