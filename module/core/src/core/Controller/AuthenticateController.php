<?php


namespace core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use core\Model\Dao\RoleTable;
use Zend\Session\Container as SessionContainer;


class AuthenticateController extends AbstractCoreController
{
	public function loginAction()
	{
		  $this->layout('layout/login');
          $form       = $this->getLoginForm();

  
        
        return array(
            'form'      => $form,
            'messages'  => $this->flashmessenger()->getMessages()
        );
        
	}


	public function authAction()
	{
		$form       = $this->getLoginForm();
        $request    = $this->getRequest();



        if ($request->isPost()){

        	$form->setData($request->getPost());
            if ($form->isValid()){

            	$this->getAuthService()->getAdapter()
	                                       ->setIdentity($request->getPost('username'))
	                                       ->setCredential($request->getPost('password'));        
	       		 $result = $this->getAuthService()->authenticate();

                    //Graba en Tabla sytem_log 
                 $data = array(
                   'user' => $request->getPost('username'),
                   'accion' => 'Ingreso al Sistema',
                 );
                 $this->getServiceLocator()->get('LogManager')->grabaLog($data);

                 foreach($result->getMessages() as $message)
                 {
                    //save message temporary into flashmessenger
                    $this->flashmessenger()->addMessage($message);
                 }
				
                 if ($result->isValid()) {
                    $redirect = 'success';
                    $usuario = $this->getUsuariosTable()->getUsuarioByUsername($request->getPost('username'));
                    
                    $role = $this->getRoleTable()->getRoleId($usuario->rol);

                    $login = $this->getLogTable()->getLastLoginById($request->getPost('username'));
                    $lastLogin = $login->current();

                    $usrAction = $this->getlOgTable()->getLastUserAction($request->getPost('username'));
                    $lastAction = $usrAction->current();
                    

                    $this->getSessContainer()->lastLogin   = $lastLogin['timestamp'];
                    $this->getSessContainer()->lastAction  = $lastAction['accion'];
                    $this->getSessContainer()->role        = $role->role;
                    $this->getSessContainer()->username    = $request->getPost('username');
                    $this->getSessContainer()->display_name= $usuario->nombre 
                            . ' ' . $usuario->apellidos;
                    
                    $this->getAuthService()->setStorage($this->getSessionStorage());
                    $this->getAuthService()->getStorage()->write($request->getPost('username'));
                    
                }else{
                    $data = array(
                        'user' => $request->getPost('username'),
                        'accion' => 'Intento Ingresar al Sistema',
                        'descripcion' => implode(" ",$result->getMessages()),
                    ); 
                        $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                }


                if($redirect === 'success'){
                    return $this->redirect()->toRoute('inicio',array(
                        'action' =>  'index'
                    ));
                }else{
                    return $this->redirect()->toRoute('core', array(
                        'action' =>  'login'
                    ));
                }

               
            }

        	
        }
	}


     public function logoutAction()
    {
        $username = $this->getSessContainer()->username;
        if(!$username){
            $username = 'N/D';
        }
        
        if ($this->getAuthService()->hasIdentity()) {
            $this->getSessionStorage()->forgetMe();
            $this->getAuthService()->clearIdentity();
            $this->getSessionStorage()->deleteMe();
            $this->flashmessenger()->addMessage("You've been logout");           
        }
        $data = array(
                   'user'   =>  $username,
                   'accion' =>  'Salida de Sistema',
               );        
        $this->getServiceLocator()->get('LogManager')->grabaLog($data);
        
        return $this->redirect()->toRoute('core', array(
                        'action' =>  'login'
                    ));
    }

	
}