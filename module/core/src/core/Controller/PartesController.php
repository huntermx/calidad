<?php


namespace core\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;

use core\Model\Entity\Partes;

class PartesController extends AbstractCoreController
{
	public function indexAction()
	{
		
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
		

		    $this->layout()->tab1 = "";
        $this->layout()->tab2 = "";
        $this->layout()->tab3 = "active";
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	'titulo' => 'Partes',
            'partes' => $this->getPartesTable()->fetchAll(),
        );
	}


 	public function agregarAction()
    {
        $form       = $this->getPartesForm();
        $request    = $this->getRequest();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        
        if($request->isPost())
        {
            $partes = new Partes();
            $form->get('familia')->setValueOptions($this->getFamiliasTable()->getFamiliasOptions());
            $form->setData($request->getPost());

            if($form->isValid())
            {

                $partes->exchangeArray($form->getData());             
                $jencode    = json_encode($form->getData());
                
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta una Parte',
                   'descripcion' => "Parte: {$partes->num_parte}",
                   'tabla_afectada' => 'partes_familia',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $partes->exchangeArray($form->getData());
			   
			      $this->getPartesTable()->agregar($partes);
		        $this->layout()->usuario = $this->session->display_name;
            $this->layout()->lastLogin = $this->session->lastLogin;
           return $this->redirect()->toRoute('partes',array(
		 		         'action' =>  'index'));
            }
         }else{
              return array('form' => $form,);
         }
         
    }



    public function editarAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);

      if (!$id || $id == 0) {
        return $this->redirect()->toRoute('partes', array(
            'action' => 'agregar'));
      }
      

      $partes = $this->getPartesTable()->getRegistro($id);

      $form  = $this->getPartesForm();
      $form->bind($partes);
      $form->get('submit')->setAttribute('value', 'Edit');
      $request = $this->getRequest();
      
      if($request->isPost()) 
      {
            $form->setData($request->getPost());
            $form->get('familia')->setValueOptions($this->getFamiliasTable()->getFamiliasOptions());


            if($form->isValid())
            {

                $jencode    = json_encode($form->getData());
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Edito una Parte',
                   'descripcion' => "Parte: {$partes->num_parte} ",
                   'tabla_afectada' => 'partes_familia',     
                   'detalles_extra' => $jencode,
                );      
                   
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $this->getPartesTable()->agregar($form->getData());
        return $this->redirect()->toRoute('partes',array(
              'action' =>  'index'));
            }
      }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
      return array(
          'id' => $id,
          'form' => $form,
          'familia_id' => $partes->familia,

      );
      
    }


	 public function eliminarAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      if (!$id) {
            return $this->redirect()->toRoute('partes');
      }
      
      $request = $this->getRequest();
      if ($request->isPost()) 
      {
        $del = $request->getPost('del', 'No');
      
            if ($del == 'Si') 
            {
                $partes = $this->getPartesTable()->getRegistro($id);

               // $id = (int) $request->getPost('id');

                $this->getPartesTable()->eliminar($id);
               
               $jencode    = json_encode($partes);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino un # Parte',
                   'descripcion' => "Parte Num: {$partes->num_parte} , {$partes->descripcion}",
                   'tabla_afectada' => 'partes',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               }
      
        return $this->redirect()->toRoute('partes',array(
              'action' =>  'index'));
      }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
      return array(
          'id'    => $id,
          'parte' => $this->getPartesTable()->getRegistro($id)
      );
      
    }
	
}