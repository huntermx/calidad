<?php


namespace core\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;

use core\Model\Entity\Maquina;

class MaquinasController extends AbstractCoreController
{
	public function indexAction()
	{
		
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
		
		  
		    $this->layout()->tab1 = "active";
        $this->layout()->tab2 = "";
        $this->layout()->tab3 = "";
         $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();

        return array(
        	'titulo' => 'Máquinas',
            'maquina' => $this->getMaquinasTable()->fetchAll(),
        );
	}


 	public function agregarAction()
    {
        $form       = $this->getMaquinaForm();
        $request = $this->getRequest();
           $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        
        if($request->isPost())
        {
            $maquina = new Maquina();
            $form->setData($request->getPost());

            if($form->isValid())
            {

                $maquina->exchangeArray($form->getData());             
                $jencode    = json_encode($form->getData());
                
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta a un maquina',
                   'descripcion' => "Maquina: {$maquina->maquina}",
                   'tabla_afectada' => 'maquinas',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $maquina->exchangeArray($form->getData());
			   
			     $this->getMaquinasTable()->agregar($maquina);
		       $this->layout()->usuario = $this->session->display_name;
           $this->layout()->lastLogin = $this->session->lastLogin;
                return $this->redirect()->toRoute('maquinas',array(
		 		'action' =>  'index'));
            }
         }else{
              return array('form' => $form,);
         }
         
    }


	public function authAction()
	{
		
 
	}
	
}