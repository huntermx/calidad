<?php

namespace core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container as SessionContainer;
use Zend\Form\Annotation\AnnotationBuilder;

use core\Forms\FormLogin;
use core\Forms\FormMaquina;
use core\Forms\FormUsuarios;
use core\Forms\FormMoldes;
use core\Forms\FormPartes;
use core\Forms\FormPartesMoldes;
use core\Forms\FormFamilias;
use core\Forms\FormDppmGoal;


use core\Model\Dao\RoleTable;
use core\Model\Dao\UsuariosTable;

abstract class AbstractCoreController extends AbstractActionController
{
    protected $flogin;
    protected $authservice;
    protected $usuariosTable;
    protected $roleTable;
    protected $storage;
    protected $sesscontainer;
    protected $formUsuarios;
    protected $maquinasTable;
    protected $formMaquina;
    protected $moldesTable;
    protected $formMoldes;
    protected $partesTable;
    protected $formPartes;
    protected $familiasTable;
    protected $moldeparteTable;
    protected $formPartesMoldes;
    protected $logTable;
    protected $logPaginator;
    protected $formFamilias;
    protected $dppmGoalTable;
    protected $formDppmGoal;

    
    public function __construct()
    {
        $this->session    = new SessionContainer('labodigi');        
    }

    public function getLogPaginatorTable() {
        if (!$this->logPaginator) {
            $sm = $this->getServiceLocator();
            $this->logPaginator = $sm->get('core\Model\Dao\LogPaginatorTable');
        }
        return $this->logPaginator;
    } 


   public function getDppmGoalTable()
    {
        if (!$this->dppmGoalTable) {
            $sm = $this->getServiceLocator();
            $this->dppmGoalTable = $sm->get('core\Model\Dao\DppmGoalTable');
        }
        return $this->dppmGoalTable;
    }

   public function getMoldeParteTable()
    {
        if (!$this->moldeparteTable) {
            $sm = $this->getServiceLocator();
            $this->moldeparteTable = $sm->get('core\Model\Dao\MoldeparteTable');
        }
        return $this->moldeparteTable;
    }

   public function getLogTable()
    {
        if (!$this->logTable) {
            $sm = $this->getServiceLocator();
            $this->logTable = $sm->get('core\Model\Dao\LogTable');
        }
        return $this->logTable;
    }

   public function getUsuariosTable()
    {
        if (!$this->usuariosTable) {
            $sm = $this->getServiceLocator();
            $this->usuariosTable = $sm->get('core\Model\Dao\UsuariosTable');
        }
        return $this->usuariosTable;
    }
    
    public function getPartesTable()
    {
        if (!$this->partesTable) {
            $sm = $this->getServiceLocator();
            $this->partesTable = $sm->get('core\Model\Dao\PartesTable');
        }
        return $this->partesTable;
    }

    public function getFamiliasTable()
    {
        if (!$this->familiasTable) {
            $sm = $this->getServiceLocator();
            $this->familiasTable = $sm->get('core\Model\Dao\FamiliasTable');
        }
        return $this->familiasTable;
    }

    public function getMaquinasTable()
    {
        if (!$this->maquinasTable) {
            $sm = $this->getServiceLocator();
            $this->maquinasTable = $sm->get('core\Model\Dao\MaquinasTable');
        }
        return $this->maquinasTable;
    }

    public function getMoldesTable()
    {
        if (!$this->moldesTable) {
            $sm = $this->getServiceLocator();
            $this->moldesTable = $sm->get('core\Model\Dao\MoldesTable');
        }
        return $this->moldesTable;
    }
    
    public function getConfig()
    {
        if(!$this->config)
        {
            $sm = $this->getServiceLocator();
            $this->config = $sm->get('ConfigIni');
        }
        return $this->config;
    }


    public function getDppmGoalForm()
    {
        if (!$this->formDppmGoal) {
            $temp                       = new FormDppmGoal();
            $builder                    = new AnnotationBuilder();
            $this->formDppmGoal     = $builder->createForm($temp);
        }
        return $this->formDppmGoal;
    }

    public function getFamiliasForm()
    {
        if (!$this->formFamilias) {
            $temp                       = new FormFamilias();
            $builder                    = new AnnotationBuilder();
            $this->formFamilias     = $builder->createForm($temp);
        }
        return $this->formFamilias;
    }

    public function getPartesMoldesForm()
    {
        if (!$this->formPartesMoldes) {
            $temp                       = new FormPartesMoldes();
            $builder                    = new AnnotationBuilder();
            $this->formPartesMoldes     = $builder->createForm($temp);
        }
        return $this->formPartesMoldes;
    }

    public function getPartesForm()
    {
        if (!$this->formPartes) {
            $temp            = new FormPartes();
            $builder         = new AnnotationBuilder();
            $this->formPartes    = $builder->createForm($temp);
        }
        return $this->formPartes;
    }

    public function getLoginForm()
    {
        if (! $this->flogin) {
            $temp            = new FormLogin();
            $builder         = new AnnotationBuilder();
            $this->flogin    = $builder->createForm($temp);
        }
        return $this->flogin;
    }

    public function getMoldesForm()
    {
        if (!$this->formMoldes) {
            $temp            = new FormMoldes();
            $builder         = new AnnotationBuilder();
            $this->formMoldes    = $builder->createForm($temp);
        }
        return $this->formMoldes;
    }

     public function getMaquinaForm()
    {
        if (!$this->formMaquina) {
            $temp            = new FormMaquina();
            $builder         = new AnnotationBuilder();
            $this->formMaquina    = $builder->createForm($temp);
        }
        return $this->formMaquina;
    }

     public function getRoleTable()
    {
        if (!$this->roleTable) {
                $sm = $this->getServiceLocator();
            $this->roleTable = $sm->get('core\Model\Dao\RoleTable');
        }
        return $this->roleTable;
    }


    public function getAuthService()
    {
        if (! $this->authservice) 
        {
            $this->authservice = $this->getServiceLocator()
                                     ->get('AuthService');
        }    
        return $this->authservice;
    } 

     public function getSessionStorage()
    {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()
                                  ->get('core\Model\MyAuthStorage');
        }
        return $this->storage;
    }

    protected function getSessContainer()
    {
        if (!$this->sesscontainer) {
            $this->sesscontainer = new SessionContainer('labodigi');
        }
        return $this->sesscontainer;
    }
    
     public function getUsuariosForm()
    {
        if (!$this->formUsuarios) {
            $temp                   = new FormUsuarios();
            $builder                = new AnnotationBuilder();
            $this->formUsuarios     = $builder->createForm($temp);
        }
        return $this->formUsuarios;
    }


    public function getLastAction()
    {     
         $usrAction = $this->getlOgTable()->getLastUserAction($this->session->username);
         $lastAction = $usrAction->current();
         return $lastAction['accion'];
    
    }



}