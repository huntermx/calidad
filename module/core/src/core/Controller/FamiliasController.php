<?php


namespace core\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;

use core\Model\Entity\Familias;

class FamiliasController extends AbstractCoreController
{
	public function indexAction()
	{
		
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();

		    $this->layout()->tab1 = "";
        $this->layout()->tab2 = "";
        $this->layout()->tab3 = "";
        $this->layout()->tab4 = "";
        $this->layout()->tab5 = "active";

        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	'titulo' => 'Familias',
            'familia' => $this->getFamiliasTable()->fetchAllValid(),
        );
	}


 	public function agregarAction()
    {
        $form         = $this->getFamiliasForm();
        $request      = $this->getRequest();
        
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        if($request->isPost())
        {


          
            $familias = new Familias();
            $form->setData($request->getPost());

            if($form->isValid())
            {

                $familias->exchangeArray($form->getData());             
                $jencode    = json_encode($form->getData());
                
               

                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta una Familia',
                   'descripcion' => "Familia: {$familias->familia}",
                   'tabla_afectada' => 'moldes',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               //$moldes->exchangeArray($form->getData());
			   
			     $this->getFamiliasTable()->agregar($familias);

		       $this->layout()->usuario = $this->session->display_name;
           $this->layout()->lastLogin = $this->session->lastLogin;
           return $this->redirect()->toRoute('familias',array(
		 		         'action' =>  'index'));
            }
         }else{
              return array('form' => $form,);
         }
         
    }



    public function editarAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      if (!$id || $id == 0) {
        return $this->redirect()->toRoute('familias', array(
            'action' => 'agregar'));
      }
      
      $familiaReadData        = $this->getFamiliasTable()->getRegistro($id);
      $form                   = $this->getFamiliasForm();
      $form->bind($familiaReadData);
      $form->get('submit')->setAttribute('value', 'Edit');
      $request = $this->getRequest();
      
      if($request->isPost()) 
      {
            $form->setData($request->getPost());
            if($form->isValid())
            {
                $jencode    = json_encode($form->getData());
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Edito una Familia',
                   'descripcion' => "Molde: {$familiaReadData->familia} ",
                   'tabla_afectada' => 'partes_familia',     
                   'detalles_extra' => $jencode,
                );      
                   
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);


               $this->getFamiliasTable()->agregar($form->getData());
        return $this->redirect()->toRoute('familias',array(
              'action' =>  'index'));
            }
      }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
      return array(
          'id' => $id,
          'form' => $form,

      );
      
    }




  public function eliminarAction()
  {
       $familia_id = (int) $this->params()->fromRoute('id', 0);
      if ($familia_id==0 ) {
           return $this->redirect()->toRoute('familias', array(
            'action' => 'index'));
      }


      $familiaReadData        = $this->getFamiliasTable()->getRegistro($familia_id);


      
      $this->layout()->usuario = $this->session->display_name;
      $this->layout()->lastLogin = $this->session->lastLogin;
      $this->layout()->lastAction = $this->getLastAction();
    
      $request = $this->getRequest();
      if ($request->isPost()) 
      {
        $del = $request->getPost('del', 'No');

     
      
            if ($del == 'Si') 
            {

              //  $partes = $this->getMoldeParteTable()->getRegistro2($moldespartes_id);

               // $id = (int) $request->getPost('id');

                $this->getFamiliasTable()->desabilitar($familia_id);
               
               $jencode    = json_encode($familiaReadData);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino Familia',
                   'descripcion' => "Familia : {$familiaReadData->familia} ",
                   'tabla_afectada' => 'partes_familias',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               }
      
        return $this->redirect()->toRoute('familias',array(
              'action' =>  'index'));
      }




      return array(
          'id'    => $familia_id,
          'familia' => $familiaReadData->familia,
          'descripcion' => $familiaReadData->descripcion,
         
      );


  }




	public function authAction()
	{
		
 
	}
	
}