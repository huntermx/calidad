<?php

namespace core\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormFamilias
{
	/**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $id;
	
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Attributes({"placeholder":"Familia"})
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"form-control"})
     * 
     */
    public $familia;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Descripcion"})
          * @Annotation\Attributes({"class":"form-control"})
     */
    public $descripcion;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Submit"})
     */
    public $submit;
}