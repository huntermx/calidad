<?php

namespace core\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormMaquina
{
	/**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $id;
	
	
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"placeholder":"Nombre Usuario"})
     * @Annotation\Filter({"name":"StripTags"})
     * 
     */
    public $nstatus;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Maquina"})
     */
    public $maquina;
    
    
    
    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Submit"})
     */
    public $submit;
}