<?php

namespace core\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("User")
 * @Annotation\Attributes({"class":"form-horizontal","style":"width:400px;"})
 */
class FormLogin
{
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
          * @Annotation\Attributes({"class":"form-control"})
     */
    public $username;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Password")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"inputPassword"})
          * @Annotation\Attributes({"class":"form-control"})
     */
    public $password;
    

    
    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"value":"Login"})
     * @Annotation\Attributes({"class":"btn btn-primary"})
     */
    public $submit;
}