<?php
namespace core\View\Helper;
use Zend\View\Helper\AbstractHelper;

class FechaHelper extends AbstractHelper
{

	public function __invoke($timestamp,$select=false,$weekday=false)
	{
		$fclass = new ClassFecha($timestamp,true);	

		if($weekday)
		{
			$fclass->setFormatoDsMDA();
		}
		else
		{
			$fclass->setFormatoMDA();
		}
		
		

		$fclass->setTimeFormatAMPM();

		if(!$select){
			return $fclass->getFecha();
		}


		return $fclass->getFechaNTime();
	}
	
}