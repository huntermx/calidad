<?php

namespace core\View\Helper;


class ClassFecha
{
	protected $fecha=false;
	protected $time=false;
	protected $timestamp;
	protected $tempfecha;
	protected $temptime;
	protected $meses_lista=array('01' => 'Ene','02' => 'Feb',
			'03' => 'Mar','04' => 'Abr','05' => 'May','06' => 'Jun','07' => 'Jul',
			'08' => 'Ago','09' => 'Sep','10' => 'Oct','11' => 'Nov','12' => 'Dic');
	
	protected $day_lista=array(0 => 'Domingo',1 => 'Lunes',
			2 => 'Martes',3 => 'Miercoles',4 => 'Jueves',5 => 'Viernes',6 => 'Sabado');


	
	
	public function __construct($timestamp, $selector = false)
	{		
		$this->timestamp = $timestamp;
		$this->validaTimestamp();
	}
	
	private function validaTimestamp()
	{
		if (preg_match('(\d{4}-\d{2}-\d{2})', $this->timestamp) & strlen($this->timestamp)==10) {
			$this->tempfecha = $this->timestamp;
			$this->temptime = false;
		}else if (preg_match('(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})', $this->timestamp)) {
			list($this->tempfecha,$this->time) = explode(' ',$this->timestamp);
			$this->temptime = true;			
		} else {

			$this->tempfecha = true;
		}
	}
	
	public function setFormatoMDA()
	{
		if($this->tempfecha===true){
			$this->fecha='Fecha Invalida';
			return false;
		}		
		list($anio,$mes,$dia) = explode('-',$this->tempfecha);
		$this->fecha = "{$this->meses_lista[$mes]} {$dia} {$anio}";

	}
	
	public function setFormatoDsMDA()
	{
		if($this->tempfecha===true){
			$this->fecha='Fecha Invalida';
			return false;
		}		
		list($anio,$mes,$dia) = explode('-',$this->tempfecha);
		$weekDay = date('w',strtotime($this->tempfecha));

		$this->fecha = "{$this->day_lista[$weekDay]}, {$dia} {$this->meses_lista[$mes]} {$anio}";

	}


	public function setFormatoTime($var=true)
	{
		if($var == true && $this->tempfecha!==true)
		{
			$this->validaTimestamp();
		}else{
			$this->temptime = false;
		}
	}
	
	public function setTimeFormatAMPM()
	{
		if($this->temptime==false || $this->tempfecha===true)
		{
			return false;
		}
		$this->time= date('h:i A', strtotime($this->timestamp));
	}
	
	public function getFecha()
	{
		if($this->temptime)
		{
			$this->fecha= $this->fecha . ' ' . $this->time;
		}
		
		return $this->fecha;
	}

public function getFechaNTime()
	{
		
		
		return $this->fecha;
	}


	
}
