<?php
namespace core\View\Helper;
use Zend\View\Helper\AbstractHelper;

class MoldepartesHelper extends AbstractHelper
{

	private $moldeparteTable;

	function __construct($moldeparteTable)
	{
		$this->moldeparteTable= $moldeparteTable;
	}


	public function __invoke($id)
	{
		
		$moldeparte = $this->moldeparteTable->getMoldes($id);

		return $moldeparte;
		//return $familia->familia;
	}
	
}