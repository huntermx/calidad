<?php
namespace core\View\Helper;
use Zend\View\Helper\AbstractHelper;

class FamiliasHelper extends AbstractHelper
{

	private $familiaTable;

	function __construct($familiaTable)
	{
		$this->familiaTable= $familiaTable;
	}


	public function __invoke($id)
	{
		
		$familia = $this->familiaTable->getFamiliaId($id);

		return $familia->familia;
	}
	
}