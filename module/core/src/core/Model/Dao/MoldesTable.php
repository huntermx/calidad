<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use core\Model\Entity\Moldes;


class MoldesTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(array('nstatus'=>'produccion'));        				
        return $resultSet;
    }

   
    
    public function getMoldesList()
    {
            $lista = array();
            $resultSet = $this->tableGateway->select();

            foreach ($resultSet as $result)
            {
                $lista[]= array('value' => $result->id , 'label' =>$result->molde);
            }
             
            return $lista;
    }

   
    
    public function agregar(Moldes $moldes)
    {
        $data = array(
            'molde'             => trim($moldes->molde),
            'descripcion'       => trim($moldes->descripcion),
            'nstatus'           => trim($moldes->nstatus),
        );

        $id = (int)$moldes->id;

       
        if ($id == 0) {      

            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


    public function eliminarUsuario($id)
    {
    	$data=array('nstatus' =>'3');
        $this->tableGateway->update($data, array('id' => $id));
    }
    
    function parse_timestamp($timestamp, $format = 'd-m-Y')
    {
        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }
    
}