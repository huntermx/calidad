<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use core\Model\Entity\Familias;

class FamiliasTable
{
	protected $tableGateway;
	protected $adapter;

	public function __construct(TableGateway $tableGateway,Adapter $adapter)
	{
		$this->tableGateway = $tableGateway;
		$this->adapter	= $adapter;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	

    public function fetchAllValid()
    {
        $sql = new Sql($this->adapter);
        $select= $sql->select();
        $select->from('partes_familia');

        $where = new Where();
        $where->equalTo('status', 1);
        $select->where($where);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

	public function getFamiliasOptions()
    {
            $lista = array();

            $resultSet = $this->tableGateway->select();

            foreach ($resultSet as $result)
            {
    			$lista[]= array('value' => $result->id , 'label' =>$result->familia);
            }
             
            return $lista;
    }


        public function getFamiliaId($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
        
        public function getListaRoles()
        {
            $lista = array();

            $resultSet = $this->tableGateway->select();

            foreach ($resultSet as $result)
            {
    			$lista[]= array('value' => $result->id , 'label' =>$result->role);
            }
             
            return $lista;
        }

	public function getAllRol()
	{
		$sql = new Sql($this->adapter);
		$select= $sql->select();
		$select->from('usuarios_acl_roles');
		$select->order(array('herencia DESC', 'role DESC'));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();
		return $result;
	}


    public function agregar(Familias $familias)
    {
        $data = array(
            'familia'          => trim($familias->familia),
            'descripcion'      => trim($familias->descripcion),
            'status'           => trim($familias->status),
        );

        $id = (int)$familias->id;

       
        if ($id == 0) {      

            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }



      public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }


     public function desabilitar($id)
    {

        $familiaData = $this->getRegistro($id);
        $familiaData->status =0;

        $this->agregar($familiaData);
        
    }
	
}
