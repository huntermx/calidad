<?php


namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class PermisosTable
{
	protected $tableGateway;
	protected $adapter;

	public function __construct(TableGateway $tableGateway,Adapter $adapter)
	{
		$this->tableGateway = $tableGateway;
		$this->adapter	= $adapter;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getPermisos()
	{
		//$sql = new Sql($this->adapter);
		/*$select= $sql->select();
		$select->from(array('a'=>'usuarios_acl_permisos'));
		$select->join(array('b'=>'usuarios_acl_roles'),'a.roles_id=b.id',array('role'=>'role'));
		$select->join(array('c'=>'usuarios_acl_recursos'),'a.recurso=c.id',array("CONCAT(c.moudulo,':',c.controlador)"));
		*/
		$sq = "SELECT a.accion, a.permiso, b.role, c.modulo, c.controlador
			   FROM usuarios_acl_permisos as a				
				JOIN usuarios_acl_roles as b ON a.roles_id = b.id
				JOIN usuarios_acl_recursos as c ON a.recurso=c.id
				";
				
		$stmt = $this->adapter->query($sq);
		//$statement = $sql->prepareStatementForSqlObject($select);
		$result = $stmt->execute();
		return $result;
	}


}
