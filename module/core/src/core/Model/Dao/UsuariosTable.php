<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use core\Model\Entity\Usuario;


class UsuariosTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(array('nstatus'=>'1'));        				
        return $resultSet;
    }

    public function getValidUsers()
    {
        $sql = new Sql($this->adapter);
        $select= $sql->select();
	    $select->from(array('a'=>'usuarios'));
	    $select->join(array('b'=>'usuarios_acl_roles'),'a.rol=b.id',array('tipousuario'=>'role'));
	    $where = new Where();
        $where->equalTo('a.nstatus', 1);
        $select->where($where);
	    $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
	    return $result;
		
    }
    
    public function getUsuario($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getUsuarioByUsername($username)
    {
        $rowset = $this->tableGateway->select(array('usuario' => $username));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


    public function getUserNameById($id)
    {
        $sql =  "SELECT CONCAT_WS(', ',Nombre,apellidos) as nombrecompleto
                 FROM usuarios
                 WHERE  id = '{$id}'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        
        if(empty($result)){
            return false;
        }

        $usuario = $result->current();

        return $usuario['nombrecompleto'];

    }


    public function getUserIdbyNombre($nombre)
    {
        $nombre  = (string) $nombre;
        $sql =  "SELECT *
                    FROM usuarios
                    WHERE  CONCAT_WS(', ',nombre,apellidos) = '{$nombre}'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        if(empty($result)){
            return false;
        }

        $usuario = $result->current();

        return $usuario['id'];
    }

     public function getUsuarioByNombre($nombre)
    {

        $nombre  = (string) $nombre;
        $sql =  "SELECT *
                    FROM usuarios
                    WHERE  CONCAT_WS(' ',nombre,apellidos) like '%{$nombre}%'
                    ORDER BY CONCAT_WS(' ',nombre,apellidos) DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;


        
      
    }
    
    public function agregarUsuarios(Usuario $usuarios)
    {
        $data = array(
            'timestamp'     => date('Y-m-d H:i:s'),
            'tress_id'      => trim($usuarios->tress_id),
            'nombre'        => trim($usuarios->nombre),
            'apellidos'     => trim($usuarios->apellidos),
            'email'         => trim($usuarios->email),
            'usuario'       => trim($usuarios->usuario),
            'password'      => md5($usuarios->password),
            'nstatus'       => 1,
            'rol'           => trim($usuarios->rol),
        );

        $id = (int)$usuarios->id;

       
        if ($id == 0) {      

            $this->tableGateway->insert($data);
        } else {
            if ($this->getUsuario($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function eliminarUsuario($id)
    {
    	$data=array('nstatus' =>'3');
        $this->tableGateway->update($data, array('id' => $id));
    }
    
    function parse_timestamp($timestamp, $format = 'd-m-Y')
    {
        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }
    
}