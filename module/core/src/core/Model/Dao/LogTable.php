<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use core\Model\Entity\Log;

class LogTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

   
    public function getLastLoginById($usuario)
    {
         $sql =  "SELECT *
                    FROM log
                    WHERE user= '{$usuario}' AND 
                    accion = 'Ingreso al Sistema'
                    ORDER BY id DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

    public function getLastUserAction($usr)
    {
        $user = (string) $usr;
        $sql =  "SELECT *
                    FROM log
                    WHERE user= '{$user}'
                    ORDER BY id DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;

    }


    public function agregarRegistro(Log $registro)
    {
        $data = array(
            'timestamp'         => date('Y-m-d H:i:s'),
            'user'              => trim($registro->user),
            'accion'            => trim($registro->accion),
            'descripcion'       => trim($registro->descripcion),
            'tabla_afectada'    => trim($registro->tabla_afectada),
            'detalles_extra'    => trim($registro->detalles_extra),
         
        );

        $id = (int)$registro->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } 
    }
    
    
    function parse_timestamp($timestamp, $format = 'd-m-Y')
    {
        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }
    
}