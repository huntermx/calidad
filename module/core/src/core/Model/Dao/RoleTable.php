<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class RoleTable
{
	protected $tableGateway;
	protected $adapter;

	public function __construct(TableGateway $tableGateway,Adapter $adapter)
	{
		$this->tableGateway = $tableGateway;
		$this->adapter	= $adapter;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	
    public function getRoleId($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
        
    public function getListaRoles()
    {
            $lista = array();

            $resultSet = $this->tableGateway->select();

            foreach ($resultSet as $result)
            {
    			$lista[]= array('value' => $result->id , 'label' =>$result->role);
            }
             
            return $lista;
    }

	public function getAllRol()
	{
		$sql = new Sql($this->adapter);
		$select= $sql->select();
		$select->from('usuarios_acl_roles');
		$select->order(array('herencia DESC', 'role DESC'));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();
		return $result;
	}

	
}
