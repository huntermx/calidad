<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use core\Model\Entity\Moldeparte;


class MoldeparteTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }


    public function agregar(Moldeparte $mparte)
    {
        $data = array(
            'timestamp'         => date('Y-m-d H:i:s'),
            'id_molde'          => trim($mparte->id_molde),
            'id_parte'          => trim($mparte->id_parte),
        );

        $id = (int)$mparte->id;

       

       
        if ($id == 0) {      

            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function getPartesByMolde($molde_id)
    {

        $sql =  "SELECT p.*
                    FROM molde_parte as mp
                    LEFT JOIN moldes as m ON (m.id=mp.id_molde)
                    LEFT JOIN partes as p ON (p.id=mp.id_parte)
                    WHERE mp.id_molde = {$molde_id}
                    ORDER BY p.num_parte DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;

    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id_parte' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


  public function getRegistro2($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }




    public function getMoldes($id_parte)
    {
        $sql =  "SELECT mp.*, m.molde,p.num_parte
                 FROM molde_parte as mp
                 LEFT JOIN moldes as m ON (m.id=mp.id_molde)
                 LEFT JOIN partes as p ON (p.id=mp.id_parte)
                 WHERE mp.id_parte = {$id_parte}
                 ORDER BY m.molde DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        $string_moldes = "";
        $i =0;

        foreach($result as $re)
        {
            if($i==0)
            {
                $string_moldes =  $re['molde'];
            }else{
                $string_moldes = $string_moldes . " / ". $re['molde'];
            }
            $i++;
        }

        return $string_moldes;

    }


    public function getMoldesByParte_id($partes_id)
    {


          $sql =  "SELECT mp.id AS mparte_id,  m.*
                    FROM molde_parte as mp
                    LEFT JOIN moldes as m ON (m.id=mp.id_molde)
                    LEFT JOIN partes as p ON (p.id=mp.id_parte)
                    WHERE mp.id_parte = {$partes_id}
                    ORDER BY m.molde DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;

    }


    public function eliminar($id)
    {
    	
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
    function parse_timestamp($timestamp, $format = 'd-m-Y')
    {
        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }
    
}