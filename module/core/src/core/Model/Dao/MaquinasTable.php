<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use core\Model\Entity\Maquina;


class MaquinasTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(array('nstatus'=>'1'));        				
        return $resultSet;
    }
    
    public function getMaquinasOptions()
    {
        $lista = array();
        $resultSet = $this->fetchAll();

        foreach ($resultSet as $result)
        {
            $lista[]= array('value' => $result->id , 'label' =>$result->maquina);
        }
             
            return $lista;
    }

   
    public function getNextMachine($id)
    {
        $id  = (int) $id;
        $sql =  "SELECT *,'next'
                 FROM maquinas
                 WHERE `id` > '{$id}'
                 ORDER BY `id`LIMIT 1";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;


        
    }
    
   
    public function agregar(Maquina $maquina)
    {
        $data = array(
            'orden'         => trim($maquina->orden),
            'nstatus'       => trim($maquina->nstatus),
            'maquina'       => trim($maquina->maquina),
        );

        $id = (int)$maquina->id;
        $data['nstatus']     =1;

       
        if ($id == 0) {      
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;
            $maquina->id   = $this->last_id;
            $maquina->orden   = $this->last_id;
            $this->agregar($maquina);
           
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function getLastId()
   {
        return $this->last_id;
   }

    public function eliminarUsuario($id)
    {
    	$data=array('nstatus' =>'3');
        $this->tableGateway->update($data, array('id' => $id));
    }

    
}