<?php

namespace core\Model\Dao;

use Zend\Db\TableGateway\TableGateway;

class RecursoTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }
    
}
