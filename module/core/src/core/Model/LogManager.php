<?php

namespace core\Model;

use Zend\ServiceManager\FactoryInterface;

use Zend\ServiceManager\ServiceLocatorInterface;
use core\Model\Dao\LogTable;
use core\Model\Entity\Log;

class LogManager  {
    
    protected $_log;
    protected $logTable;
    protected $sm;
    
    public function __construct(ServiceLocatorInterface $sm){
        $this->_log = new Log();
        $this->sm  = $sm;
    }
    
    public function grabaLog($data){
        $this->_log->exchangeArray($data);
        $this->geLogTable()->agregarRegistro($this->_log);
    }
    
    private function geLogTable()
    {
    	if (!$this->logTable) {
            $this->logTable = $this->sm->get('core\Model\Dao\LogTable');
    	}
    	return $this->logTable;
    }


    

    
}