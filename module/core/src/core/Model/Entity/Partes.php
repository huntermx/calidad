<?php

namespace core\Model\Entity;

class Partes
{
    public $id;
    public $fecha;
    public $num_parte;
    public $descripcion;
    public $familia;
    public $division;


    public function exchangeArray($data)
    {   
        $this->id           = (isset($data['id'])) ? $data['id'] : 0;
        $this->fecha        = (isset($data['fecha'])) ? $data['fecha'] : null;
        $this->num_parte    = (isset($data['num_parte'])) ? $data['num_parte'] : null;
        $this->descripcion  = (isset($data['descripcion'])) ? $data['descripcion'] : null;
        $this->familia      = (isset($data['familia'])) ? $data['familia'] : null;
        $this->division     = (isset($data['division'])) ? $data['division'] : null;
    }
}