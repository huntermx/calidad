<?php

namespace core\Model\Entity;

class Maquina
{
    public $id;
    public $orden;
    public $nstatus;
    public $maquina;
   

    public function exchangeArray($data)
    {   
        $this->id           = (isset($data['id'])) ? $data['id'] : 0;
        $this->orden        = (isset($data['orden'])) ? $data['orden'] : null;
        $this->nstatus      = (isset($data['nstatus'])) ? $data['nstatus'] : 1;
        $this->maquina      = (isset($data['maquina'])) ? $data['maquina'] : null;
    }
}