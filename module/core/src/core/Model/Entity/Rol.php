<?php

namespace core\Model\Entity;

class Rol
{
	public $id;
	public $role;
	public $herencia;
	 

	public function exchangeArray($data)
	{
		$this->id           = (isset($data['id'])) ? $data['id'] : 0;
		$this->role     	= (isset($data['role'])) ? $data['role'] : null;
		$this->herencia     = (isset($data['herencia'])) ? $data['herencia'] : null;
	}
}