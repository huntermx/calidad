<?php

namespace core\Model\Entity;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class LogPaginator implements InputFilterAwareInterface
{
    public $id;
    public $timestamp;
    public $user;
    public $accion;
    public $descripcion;
    public $tabla_afectada;
    public $detalles_extra;
  
    protected $inputFilter;

    /**
     * Used by ResultSet to pass each database row to the entity
     */
    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->timestamp = (isset($data['timestamp'])) ? $data['timestamp'] : null;
        $this->user  = (isset($data['user'])) ? $data['user'] : null;
        $this->accion     = (isset($data['accion'])) ? $data['accion'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
        $this->tabla_afectada  = (isset($data['tabla_afectada'])) ? $data['tabla_afectada'] : null;
        $this->detalles_extra     = (isset($data['detalles_extra'])) ? $data['detalles_extra'] : null;

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }
}