<?php

namespace core\Model\Entity;

class Recurso
{
    public $id;
    public $modulo;
    public $controlador;
   

    public function exchangeArray($data)
    {   
        $this->id               = (isset($data['id'])) ? $data['id'] : 0;
        $this->modulo        	= (isset($data['modulo'])) ? $data['modulo'] : null;
        $this->controlador      = (isset($data['controlador'])) ? $data['controlador'] : null;
    }
}