<?php

namespace core\Model\Entity;

class Log
{
    public $id;
    public $timestamp;
    public $user;
    public $accion;
    public $descripcion;
    public $tabla_afectada;
    public $detalles_extra;

    public function exchangeArray($data)
    {   
        $this->id               = (isset($data['id'])) ? $data['id'] : 0;
        $this->timestamp        = (isset($data['timestamp'])) ? $data['timestamp'] : null;
        $this->user             = (isset($data['user'])) ? $data['user'] : null;
        $this->accion           = (isset($data['accion'])) ? $data['accion'] : null;
        $this->descripcion      = (isset($data['descripcion'])) ? $data['descripcion'] : null;
        $this->tabla_afectada   = (isset($data['tabla_afectada'])) ? $data['tabla_afectada'] : null;
        $this->detalles_extra   = (isset($data['detalles_extra'])) ? $data['detalles_extra'] : null;
       
    }
}