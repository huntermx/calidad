<?php

namespace core\Model\Entity;

class Moldes
{
    public $id;
    public $molde;
    public $descripcion;
    public $nstatus;
   

    public function exchangeArray($data)
    {   
        $this->id           = (isset($data['id'])) ? $data['id'] : 0;
        $this->molde        = (isset($data['molde'])) ? $data['molde'] : null;
        $this->descripcion  = (isset($data['descripcion'])) ? $data['descripcion'] : null;
        $this->nstatus      = (isset($data['nstatus'])) ? $data['nstatus'] : null;
    }
}