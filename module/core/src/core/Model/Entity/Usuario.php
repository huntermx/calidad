<?php

namespace core\Model\Entity;

class Usuario
{
    public $id;
    public $timestamp;
    public $tress_id;
    public $nombre;
    public $apellidos;
    public $email;
    public $usuario;
    public $password;
    public $nstatus;
    public $rol;

    public function exchangeArray($data)
    {   
        $this->id               = (isset($data['id'])) ? $data['id'] : 0;
        $this->timestamp        = (isset($data['timestamp'])) ? $data['timestamp'] : null;
        $this->tress_id         = (isset($data['tress_id'])) ? $data['tress_id'] : null;
        $this->nombre           = (isset($data['nombre'])) ? $data['nombre'] : null;
        $this->apellidos        = (isset($data['apellidos'])) ? $data['apellidos'] : null;
        $this->email            = (isset($data['email'])) ? $data['email'] : null;
        $this->usuario          = (isset($data['usuario'])) ? $data['usuario'] : null;
        $this->password         = (isset($data['password'])) ? $data['password'] : null;
        $this->nstatus          = (isset($data['nstatus'])) ? $data['nstatus'] : null;
        $this->rol              = (isset($data['rol'])) ? $data['rol'] : null;
    }
}