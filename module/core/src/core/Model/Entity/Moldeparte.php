<?php

namespace core\Model\Entity;

class Moldeparte
{
	public $id;
	public $timestamp;
	public $id_molde;
	public $id_parte;

	public function exchangeArray($data)
	{
		$this->id           	= (isset($data['id'])) ? $data['id'] : 0;
		$this->timestamp     	= (isset($data['timestamp'])) ? $data['timestamp'] : null;
		$this->id_molde     	= (isset($data['id_molde'])) ? $data['id_molde'] : null;
		$this->id_parte     	= (isset($data['id_parte'])) ? $data['id_parte'] : null;
	}
}