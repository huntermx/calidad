<?php

namespace core\Model\Entity;

class Permisos
{
	public $id;
	public $roles_id;
	public $recurso;
	public $accion;
	public $permiso;

	public function exchangeArray($data)
	{
		$this->id           = (isset($data['id'])) ? $data['id'] : 0;
		$this->roles_id     = (isset($data['roles_id'])) ? $data['roles_id'] : null;
		$this->recurso   	= (isset($data['recurso'])) ? $data['recurso'] : null;
		$this->accion   	= (isset($data['accion'])) ? $data['accion'] : null;
		$this->permiso   	= (isset($data['permiso'])) ? $data['permiso'] : null;
	}
}