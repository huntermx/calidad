<?php

namespace core\Model\Entity;

class Familias
{
	public $id;
	public $familia;
	public $descripcion;
	public $status;

	public function exchangeArray($data)
	{
		$this->id           	= (isset($data['id'])) ? $data['id'] : 0;
		$this->familia     		= (isset($data['familia'])) ? $data['familia'] : null;
		$this->descripcion     	= (isset($data['descripcion'])) ? $data['descripcion'] : null;
		$this->status     		= (isset($data['status'])) ? $data['status'] : 1;
	}
}