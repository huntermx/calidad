<?php

//module/Auth/src/SanAuth/Model/MyAuthStorage.php
namespace core\Model;

use Zend\Authentication\Storage;

class MyAuthStorage extends Storage\Session
{
    public function setRememberMe()
    {
        if ($rememberMe == 1) {
            $this->session->getManager()->rememberMe();
        }
    }
    
    public function forgetMe()
    {
        $this->session->getManager()->forgetMe();
    } 
    
    public function deleteMe()
    {
        $this->session->getManager()->destroy();
    }
    
}
