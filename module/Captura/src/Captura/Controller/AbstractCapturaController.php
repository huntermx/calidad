<?php
//http://www.youtube.com/watch?v=hEtI-2aIFtE
//
//
namespace Captura\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container as SessionContainer;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Db\TableGateway\TableGateway;
use Captura\Forms\FormRecorridoMaquinas;
use Captura\Forms\FormMaquinasActivas;
use Captura\Forms\FormInspeccion;
use Captura\Forms\FormInspeccionEditar;
use Captura\Forms\FormInspDimensional;
use Captura\Forms\FormInspVisual;
use Captura\Forms\FormInspDimensionalEdit;
use Captura\Forms\FormDimFueraEspec;
use Captura\Forms\FormVisFueraEspec;

use core\Controller\AbstractCoreController;


abstract class AbstractCapturaController extends AbstractCoreController
{
    protected $maquinasActivasDao;
    protected $formRecorridoMa;
    protected $formMaquinasActivas;
    protected $maquinaDetallesDao;
    protected $registroInspeccionDao;
    protected $registroInspForm;
    protected $registroInspEditarForm;
    protected $registroInspDimForm;
    protected $conceptosTable;
    protected $inspDimensionalTable;
    protected $inspDimDetalleTable;
    protected $formInspVisual;
    protected $defectosVisualTable;
    protected $inspVisualTable;
    protected $inspVisualDetalleTable;
    protected $inspDimEditForm;
    protected $formDimFueraEspec;
    protected $dimfueraEspecTable;
    protected $formVisFueraEspec;
    protected $visFueraEspecTable;
    protected $inspeccionPag;
    protected $inspPaginator;
    protected $burstTestTable;


    
    public function __construct()
    {
        $this->session    = new SessionContainer('labodigi');        
    }


    public function getInspRegPaginator() {
        if (!$this->inspPaginator) {
            $sm = $this->getServiceLocator();
            $this->inspPaginator = $sm->get('Captura\Model\Dao\InspRegPaginatorTable');
        }
        return $this->inspPaginator;
    }

    public function getInpsVisualTable()
    {
        if (!$this->inspVisualTable) {
            $sm = $this->getServiceLocator();
            $this->inspVisualTable = $sm->get('Captura\Model\Dao\InspVisualTable');
        }
        return $this->inspVisualTable;
    }

    public function getDimFueraEspecTable()
    {
        if (!$this->dimfueraEspecTable) {
            $sm = $this->getServiceLocator();
            $this->dimfueraEspecTable = $sm->get('Captura\Model\Dao\DimFueraEspecTable');
        }
        return $this->dimfueraEspecTable;
    }

    public function getVisFueraEspecTable()
    {
        if (!$this->visFueraEspecTable) {
            $sm = $this->getServiceLocator();
            $this->visFueraEspecTable = $sm->get('Captura\Model\Dao\VisFueraEspecTable');
        }
        return $this->visFueraEspecTable;
    }

    public function getInpsVisualDetalleTable()
    {
        if (!$this->inspVisualDetalleTable) {
            $sm = $this->getServiceLocator();
            $this->inspVisualDetalleTable = $sm->get('Captura\Model\Dao\InspVisualDetalleTable');
        }
        return $this->inspVisualDetalleTable;
    }

    public function getInpsDimDetalleTable()
    {
        if (!$this->inspDimDetalleTable) {
            $sm = $this->getServiceLocator();
            $this->inspDimDetalleTable = $sm->get('Captura\Model\Dao\InspDimDetalleTable');
        }
        return $this->inspDimDetalleTable;
    }

    public function getDefectoVisualTable()
    {
        if (!$this->defectosVisualTable) {
            $sm = $this->getServiceLocator();
            $this->defectosVisualTable = $sm->get('Captura\Model\Dao\DefectosVisualesTable');
        }
        return $this->defectosVisualTable;
    }

    public function getInpsDimTable()
    {
        if (!$this->inspDimensionalTable) {
            $sm = $this->getServiceLocator();
            $this->inspDimensionalTable = $sm->get('Captura\Model\Dao\InspDimTable');
        }
        return $this->inspDimensionalTable;
    }
  
    public function getMaquinasActivasTable()
    {
        if (!$this->maquinasActivasDao) {
            $sm = $this->getServiceLocator();
            $this->maquinasActivasDao = $sm->get('Captura\Model\Dao\MaquinasactivasTable');
        }
        return $this->maquinasActivasDao;
    }

    public function getConceptosTable()
    {
        if (!$this->conceptosTable) {
            $sm = $this->getServiceLocator();
            $this->conceptosTable = $sm->get('Captura\Model\Dao\PruebaConceptosTable');
        }
        return $this->conceptosTable;
    }

    public function getDimFueraEspecForm()
    {
        if (!$this->formDimFueraEspec) {
            $temp            = new FormDimFueraEspec();
            $builder         = new AnnotationBuilder();
            $this->formDimFueraEspec    = $builder->createForm($temp);
        }
        return $this->formDimFueraEspec;
    }

    public function getVisFueraEspecForm()
    {
        if (!$this->formVisFueraEspec) {
            $temp            = new FormVisFueraEspec();
            $builder         = new AnnotationBuilder();
            $this->formVisFueraEspec    = $builder->createForm($temp);
        }
        return $this->formVisFueraEspec;
    }


    public function getInspVisualForm()
    {
        if (!$this->formInspVisual) {
            $temp            = new FormInspVisual();
            $builder         = new AnnotationBuilder();
            $this->formInspVisual    = $builder->createForm($temp);
        }
        return $this->formInspVisual;
    }

    public function getRegistroInsDimForm()
    {
        if (!$this->registroInspDimForm) {
            $temp            = new FormInspDimensional();
            $builder         = new AnnotationBuilder();
            $this->registroInspDimForm    = $builder->createForm($temp);
        }
        return $this->registroInspDimForm;
    }


    public function getInspDimEditForm()
    {
        if (!$this->inspDimEditForm) {
            $temp            = new FormInspDimensionalEdit();
            $builder         = new AnnotationBuilder();
            $this->inspDimEditForm    = $builder->createForm($temp);
        }
        return $this->inspDimEditForm;
    }



    public function getMaquinasActivasForm()
    {
        if (!$this->formRecorridoMa) {
            $temp            = new FormRecorridoMaquinas();
            $builder         = new AnnotationBuilder();
            $this->formRecorridoMa    = $builder->createForm($temp);
        }
        return $this->formRecorridoMa;
    }

    public function getMaquinaForm()
    {
        if (!$this->formMaquinasActivas) {
            $temp            = new FormMaquinasActivas();
            $builder         = new AnnotationBuilder();
            $this->formMaquinasActivas    = $builder->createForm($temp);
        }
        return $this->formMaquinasActivas;
    }

    public function getMaquinasDetallesTable()
    {
        if (!$this->maquinaDetallesDao) {
            $sm = $this->getServiceLocator();
            $this->maquinaDetallesDao = $sm->get('Captura\Model\Dao\MaquinasdetalleTable');
        }
        return $this->maquinaDetallesDao;
    }

    public function getRegistroInspeccionTable()
    {
        if (!$this->registroInspeccionDao) {
            $sm = $this->getServiceLocator();
            $this->registroInspeccionDao = $sm->get('Captura\Model\Dao\InspeccionRegistroTable');
        }
        return $this->registroInspeccionDao;
    }

    public function getDimBurstTestTable()
    {
        if (!$this->burstTestTable) {
            $sm = $this->getServiceLocator();
            $this->burstTestTable = $sm->get('Captura\Model\Dao\DimBurstTestTable');
        }
        return $this->burstTestTable;
    }

    public function getRegistroInspForm()
    {
        if (!$this->registroInspForm) {
            $temp            = new FormInspeccion();
            $builder         = new AnnotationBuilder();
            $this->registroInspForm    = $builder->createForm($temp);
        }
        return $this->registroInspForm;
    }

    public function getRegistroInspEditarForm()
    {
        if (!$this->registroInspForm) {
            $temp            = new FormInspeccionEditar();
            $builder         = new AnnotationBuilder();
            $this->registroInspEditarForm    = $builder->createForm($temp);
        }
        return $this->registroInspEditarForm;
    }



    public function getSplitFormDim($objDim,&$form)
    {
        $horaRecibido = $objDim->hora_recibido;
        list($horas,$minutos,$segundos) = explode(':',$horaRecibido);
        $form->get('horas')->setValue($horas);
        $form->get('minutos')->setValue($minutos);
        $form->get('segundos')->setValue($segundos);


        $horaCalificacion = $objDim->hora_calificacion;
        list($horas2,$minutos2,$segundos2) = explode(':',$horaCalificacion);
        $form->get('horas_cal')->setValue($horas2);
        $form->get('minutos_cal')->setValue($minutos2);
        $form->get('segundos_cal')->setValue($segundos2);

    }


     public function getInspeccionReg()
        {
                if (!$this->inspeccionPag) {
                        $this->inspeccionPag = new TableGateway(
                                'inspeccion_registro', 
                                $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')
//                                new \Zend\Db\TableGateway\Feature\RowGatewayFeature('usr_id') // Zend\Db\RowGateway\RowGateway Object
//                                ResultSetPrototype
                        );
                }
                return $this->inspeccionPag;
        }


}