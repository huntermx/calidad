<?php

namespace Captura\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\Input;


use Captura\Model\Entity\VisFueraEspec;



class InspVisualController extends AbstractCapturaController
{

	public function indexAction()
	{		
         $sesion = new SessionContainer('labodigi');
         $this->flashmessenger()->clearCurrentMessages();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	     'titulo' => 'Registro Inspeccion',
               'registro' => $this->getRegistroInspeccionTable()->fetchAll(),
        );
	}


    public function regfueraespecAction()
    {
        $visual_id =  $this->params()->fromRoute('id',0); 
        if (!$visual_id || $visual_id == 0) {
                  return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'index'));
        }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();

        $obj_vis = $this->getInpsVisualTable()->getRowByRegistroId($visual_id);

        $form = $this->getVisFueraEspecForm();
        $form->get('visual_id')->setValue($obj_vis->id);    
        $request    = $this->getRequest();

        if($request->isPost())
        {
             $post = $request->getPost();
             $form->setData($post);

             $objFueraEspec = new VisFueraEspec();
             
            if($form->isValid())
            {
                $array_post = $form->getData();
                $objFueraEspec->exchangeArray($form->getData());  
                $jencode    = json_encode($form->getData());

                $this->getVisFueraEspecTable()->agregar($objFueraEspec);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta Registro Visual Fuera Especificacion',
                   'descripcion' => "Registro Visual: {$visual_id}",
                   'tabla_afectada' => 'visual_fuera_especificacion',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
             
                if($array_post['submit2']){
                      return $this->redirect()->toRoute('reginspeccion',array(
                                                        'action' =>  'index'));                
                }
    
                if($array_post['submit']){
                      return $this->redirect()->toRoute('inspvisual',array(
                                                        'controller' => 'InspVisual',
                                                        'action'     => 'regfueraespec',
                                                        'id'     => $visual_id,));                
                }
             }
        }
        return array(
                 'titulo' => 'Registro Visual Fuera Especificacion',
                 'id'     => $visual_id,
                 'registro' => $this->getVisFueraEspecTable()->getRowsByVisual_Id($obj_vis->id),
                 'form' => $form,
        );
    }
	


     public function eliminarfueraespecAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
                 return $this->redirect()->toRoute('reginspeccion',array(
                                                        'action' =>  'index'));         
        }

        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
           $this->layout()->lastAction = $this->getLastAction();
        $request = $this->getRequest();

       if ($request->isPost()) 
       {
            $del = $request->getPost('del', 'No');
            if ($del == 'Si') 
            {
                $fueraEspec = $this->getVisFueraEspecTable()->getRegistro($id);
                 $this->getVisFueraEspecTable()->eliminar($id);
               
                $jencode    = json_encode($fueraEspec);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino un Registro Visual Fuera Especificacion',
                   'descripcion' => "Registro Visual: {$fueraEspec->visual_id} / Dim Fuera Espec: {$fueraEspec->id}",
                   'tabla_afectada' => 'visual_fuera_especificacion',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);

            }
                return $this->redirect()->toRoute('inspvisual',array(
                                                        'controller' => 'InspVisual',
                                                        'action'     => 'regfueraespec',
                                                        'id'     => $fueraEspec->visual_id,));        

       }

         return array(
          'id'    => $id,
          'rowfueraespec' => $this->getVisFueraEspecTable()->getRegistro($id),
        
      );
    }
	
}