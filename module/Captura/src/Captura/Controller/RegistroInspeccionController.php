<?php
namespace Captura\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\View\Model\ViewModel;


use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\Input;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Db\Sql\Select;
use Captura\Model\Entity\Maquinasactivas;
use Captura\Model\Entity\Maquinasdetalle;
use Captura\Model\Entity\InspeccionRegistro;
use Captura\Model\Entity\InspeccionDimensional;
use Captura\Model\Entity\InspDimDetalle;
use Captura\Model\Entity\InspeccionVisual;
use Captura\Model\Entity\InspVisualDetalle;



use Captura\Model\Classes\PostDimensional;



class RegistroInspeccionController extends AbstractCapturaController
{

	public function indexAction()
	{		
      $select = new Select();
      $order_by = $this->params()->fromRoute('order_by') ?
                $this->params()->fromRoute('order_by') : 'id';
      $order = $this->params()->fromRoute('order') ?
                $this->params()->fromRoute('order') : Select::ORDER_DESCENDING;
      $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;

      $albums = $this->getInspRegPaginator()->fetchAll($select->order($order_by . ' ' . $order));
      $itemsPerPage = 25;
      $albums->current();
      
        $paginator = new Paginator(new paginatorIterator($albums));
        $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(12);

        $albums2 = $this->getInspRegPaginator()->fetchAll($select->order($order_by . ' ' . $order));

        $paginator2 = new Paginator(new paginatorIterator($albums2));
        $paginator2->setCurrentPageNumber($page)
                ->setItemCountPerPage($itemsPerPage)
                ->setPageRange(12);

        //$sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();


        
       $view = new ViewModel(array(
                    'titulo' => 'Registro Inspección',
                    'order_by' => $order_by,
                    'order' => $order,
                    'page' => $page,
                    'paginator' => $paginator,
                    'paginator2' => $paginator2,
                ));

        $view->setTemplate('captura/registro-inspeccion/indexpaginator.phtml');
        return $view;

	}


	public function agregarAction()
  {
    	 $form       = $this->getRegistroInspForm();
    	 $request 	 = $this->getRequest();
       $this->layout()->usuario = $this->session->display_name;
       $this->layout()->lastLogin = $this->session->lastLogin;
       $this->layout()->lastAction = $this->getLastAction();
       $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
       $url = $renderer->basePath('/async/getusuariobyname');
       $form->get('capturista')->setAttribute('onkeyup', "searchSuggest2('{$url}')");
       $form->get('inspector_dimensional')->setAttribute('onkeyup', "searchSuggest('{$url}')");
       $form->get('inspector_visual')->setAttribute('onkeyup', "searchSuggest3('{$url}')");
       $flagDuplicate = 0;


        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);
            $registroInspeccion = new InspeccionRegistro();
            $form->get('capturista')->setValue($this->getUsuariosTable()->getUserIdbyNombre($post->capturista));
            $form->get('inspector_dimensional')->setValue($this->getUsuariosTable()->getUserIdbyNombre($post->inspector_dimensional));
            $form->get('inspector_visual')->setValue($this->getUsuariosTable()->getUserIdbyNombre($post->inspector_visual));
            $form->get('fecha_captura_turno')->setValueOptions($this->getMaquinasActivasTable()->getListaMactivas());
            $form->get('maquina_id')->setValueOptions($this->getMaquinasTable()->getMaquinasOptions());
            $form->get('moldes_id')->setValueOptions($this->getMoldesTable()->getMoldesList());
            $form->get('partes_id')->setValueOptions($this->getPartesTable()->getPartesList());
            
            $formValid    = $form->isValid();
            $data2        = $this->getVectorInspeccion($form->getData());    
            $result       = $this->getRegistroInspeccionTable()->checkDuplicateByArray($data2);
            $testEmpty    = $result->current();

            
            if(empty($testEmpty)==false)
            {
                $flagDuplicate = 1;
             
            }

            if($formValid  & $form->get('capturista')->getValue() != false 
                                & $form->get('inspector_dimensional')->getValue() != false
                                & $form->get('inspector_visual')->getValue() != false
                                & $form->get('partes_id')->getValue() != 0 
                                & $form->get('maquina_id')->getValue() != 0
                                & $flagDuplicate ==0)
            {
                
                $jencode    = json_encode($data2);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta un Registro Inspeccion',
                   'descripcion' => "fecha: {$data2['fecha_captura']} , turno: {$data2['turno']}",
                   'tabla_afectada' => 'inspeccion_registro',     
                   'detalles_extra' => $jencode,
                );

               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $registroInspeccion->exchangeArray($data2);
               $this->getRegistroInspeccionTable()->agregar($registroInspeccion);
    
              return $this->redirect()->toRoute('reginspeccion',array(
                  'action' =>  'index'));

            }
            else if($flagDuplicate==1)
            {
               return $this->redirect()->toRoute('reginspeccion',array(
                  'action' =>  'index'));
            }
            else
            {
               $form->setData($post);
            }
        }

    	 return array(
    	 		'titulo' => 'Agrega Inspección',
    	 		'form' => $form,
    	 	);

    }

    public function editarAction()
    {
          $id = (int) $this->params()->fromRoute('id', 0); 
          if (!$id || $id == 0) {
                  return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'agregar'));
          }
          //
          $registroInspeccion = $this->getRegistroInspeccionTable()->getRegistro($id);
          //
          $request    = $this->getRequest();
          $this->layout()->usuario = $this->session->display_name;
          $this->layout()->lastLogin = $this->session->lastLogin;
          $this->layout()->lastAction = $this->getLastAction();
          
          //Get Form
          $form  = $this->getRegistroInspEditarForm();
          $post = $request->getPost();

          if($request->isPost())
          {

             $form->bind($registroInspeccion);
             $form->setData($post);
             $this->checkPostForm($form,$registroInspeccion);

             $form->get('fecha_captura_turno')->setValueOptions($this->getMaquinasActivasTable()->getListaMactivas());
             $form->get('maquina_id')->setValueOptions($this->getMaquinasTable()->getMaquinasOptions());
             $form->get('moldes_id')->setValueOptions($this->getMoldesTable()->getMoldesList());
             $form->get('partes_id')->setValueOptions($this->getPartesTable()->getPartesList());

             
             if($form->isValid()  & $form->get('partes_id')->getValue() != 0 
                                  & $form->get('maquina_id')->getValue() != 0)
             {
           
                $data2 = $this->getObjetoInspeccion($form);
                $jencode    = json_encode($data2);
                
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Edito un Registro Inspeccion',
                   'descripcion' => "fecha: {$data2['fecha_captura']} , turno: {$data2['turno']}",
                   'tabla_afectada' => 'inspeccion_registro',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               $registroInspeccion->exchangeArray($data2);
               $this->getRegistroInspeccionTable()->agregar($registroInspeccion);
    
              return $this->redirect()->toRoute('reginspeccion',array(
                  'action' =>  'index'));
             }
             else
             {
                $registroInspeccion = $this->getRegistroInspeccionTable()->getRegistro($id);
                $form->get('inspector_visual')->setValue('');
                $form->get('inspector_dimensional')->setValue('');
                $form->get('capturista')->setValue('');


                if($this->getCapturistaEdit($post['capturista'], $registroInspeccion, $capturista))
                {
                    $form->get('capturista')->setValue($capturista);
                }


                if($this->getInspDimEdit($post['inspector_dimensional'], $registroInspeccion, $insp_dim))
                {
                    $form->get('inspector_dimensional')->setValue($insp_dim);
                }

                if($this->getInspVisEdit($post['inspector_visual'], $registroInspeccion, $insp_visual))
                {
                    $form->get('inspector_visual')->setValue($insp_visual);
                }

               
              //  print_r($post);
                $form->get('inspector_dimensional')->setAttribute("placeholder","${insp_dim}");
                $form->get('inspector_visual')->setAttribute("placeholder","${insp_visual}");
                $form->get('capturista')->setAttribute("placeholder","${capturista}");


                $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
                $url = $renderer->basePath('/async/getusuariobyname');
                $form->get('capturista')->setAttribute('onkeyup', "searchSuggest2('{$url}')");
                $form->get('inspector_dimensional')->setAttribute('onkeyup', "searchSuggest('{$url}')","placeholder","${insp_dim}");
                $form->get('inspector_visual')->setAttribute('onkeyup', "searchSuggest3('{$url}')");

                
             }




          }
          else
          {
           
              $form->bind($registroInspeccion);
              $form->get('capturista')->setValue('');
              $form->get('inspector_dimensional')->setValue('');
              $form->get('inspector_visual')->setValue('');
              
              $capturista = $this->getUsuariosTable()->getUserNameById($registroInspeccion->capturista);
              $insp_dim = $this->getUsuariosTable()->getUserNameById($registroInspeccion->inspector_dimensional);
              $insp_visual = $this->getUsuariosTable()->getUserNameById($registroInspeccion->inspector_visual);
          
              $form->get('inspector_dimensional')->setAttribute("placeholder","${insp_dim}");
              $form->get('inspector_visual')->setAttribute("placeholder","${insp_visual}");
              $form->get('capturista')->setAttribute("placeholder","${capturista}");

         // $form->get('capturista')->setAttribute("placeholder","${capturista}");

          $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
          $url = $renderer->basePath('/async/getusuariobyname');
          $form->get('capturista')->setAttribute('onkeyup', "searchSuggest2('{$url}')");
          $form->get('inspector_dimensional')->setAttribute('onkeyup', "searchSuggest('{$url}')","placeholder","${insp_dim}");
          $form->get('inspector_visual')->setAttribute('onkeyup', "searchSuggest3('{$url}')");

          }



          return array(
            'titulo' => 'Edita Inspeccion',
            'reg_id' => $registroInspeccion->recorrido_id,
            'maquina_id' => $registroInspeccion->maquina_id,
            'moldes_id' => $registroInspeccion->moldes_id,
            'partes_id' => $registroInspeccion->partes_id,
            'id' => $id,
            'form' => $form,
        );
    }



    private function getCapturistaEdit($postCapturista = false, $objRegistroInspeccion, &$capturista)
    {

        if(empty($postCapturista))
        {

            $capturista = $this->getUsuariosTable()->getUserNameById($objRegistroInspeccion->capturista);
        }
        else
        {
            $post_id = $this->getUsuariosTable()->getUserIdbyNombre($postCapturista);

            if($post_id == false || $post_id == $objRegistroInspeccion->capturista)
            {
                $this->getCapturistaEdit(false,$objRegistroInspeccion, $capturista);
                return false;
            }
            else
            { 
               $capturista = $postCapturista;
                return true;
            }


            
        }

      //  return $capturista;
         //$capturista = $this->getUsuariosTable()->getUserNameById($registroInspeccion->capturista);
    }

    private function getInspDimEdit($postDim = false, $objRegistroInspeccion, &$insp_dim)
    {
        if(empty($postDim) || $postDim == false)
        {
            $insp_dim = $this->getUsuariosTable()->getUserNameById($objRegistroInspeccion->inspector_dimensional);
        }
        else
        {
            $post_id = $this->getUsuariosTable()->getUserIdbyNombre($postDim);
            if($post_id == false || $post_id == $objRegistroInspeccion->inspector_dimensional)
            {
                $this->getInspDimEdit(false,$objRegistroInspeccion, $insp_dim);
                return false;
            }
            else
            { 
               $insp_dim = $postDim;
                return true;
            }
         
        }
        
         //$capturista = $this->getUsuariosTable()->getUserNameById($registroInspeccion->capturista);
    }

    private function getInspVisEdit($postVis = false, $objRegistroInspeccion, &$insp_visual)
    {
        if(empty($postVis))
        {
            $insp_visual = $this->getUsuariosTable()->getUserNameById($objRegistroInspeccion->inspector_visual);
        }
        else
        {
            $post_id = $this->getUsuariosTable()->getUserIdbyNombre($postVis);

            if($post_id == false || $post_id == $objRegistroInspeccion->inspector_visual)
            {
                $this->getInspVisEdit(false,$objRegistroInspeccion, $insp_visual);
                return false;
            }
            else
            { 
               $insp_visual = $postVis;
                return true;
            }
        }

        
         //$capturista = $this->getUsuariosTable()->getUserNameById($registroInspeccion->capturista);
    }


    public function editavisAction()
    {
        $id_insp = (int) $this->params()->fromRoute('id_insp', 0); 
          if (!$id_insp || $id_insp == 0) {
                  return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'index'));
          }
          $this->layout()->usuario = $this->session->display_name;
          $this->layout()->lastLogin = $this->session->lastLogin;
          $this->layout()->lastAction = $this->getLastAction();

          $inspVis = $this->getInpsVisualTable()->getRowByRegistroId($id_insp);

          $form             = $this->getInspVisualForm();  
          $dvisuales        = $this->getDefectoVisualTable()->fetchAll();
          $request          = $this->getRequest();
          $form->bind($inspVis);   

         while($row=$dvisuales->current())
         {
              $test = new Element("cantidad{$row->id}");
              $test->setAttributes(array(
                      'type'  => 'text',
                      'class'  => 'form-control'
              ));

              $objInspVis     =  $this->getInpsVisualDetalleTable()->getDetalleById($row->id,$inspVis->id);
              $arrayObjeto    =  $objInspVis->current();
              if(!empty($arrayObjeto))
              {
                $test->setValue($arrayObjeto['cantidad']);
              }
              $form->add($test);
         }


         $post = $request->getPost();
         if($request->isPost())
         {
             $form->setData($post);
             if($form->isValid())
             {               
                  $objVisual=$this->getObjInspVisual($post,$id_insp);  // Crea Objeto Visual Inspeccion
                  $data2 = $objVisual->getPropertyArray();
                  $jencode    = json_encode($data2);
                  $now = date('Y-m-d G:i:s');
                  
                  $data =array(
                    'user' => $this->session->username,
                    'accion' => 'Edito Inspeccion Visual',
                    'descripcion' => "fecha: {$now} , Inspeccion VI: {$id_insp}",
                    'tabla_afectada' => 'inspeccion_visual',     
                    'detalles_extra' => $jencode,
                  );
                
                  $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                  $this->getInpsVisualTable()->agregar($objVisual);
                 
                  $dvisual        = $this->getDefectoVisualTable()->fetchAll();
                  while($row=$dvisual->current())
                  {
                      $objDetalles= $this->getObjVisualDetalles($objVisual->id,$row,$post);

                      if(!empty($objDetalles->cantidad)){
                          
                          $this->getInpsVisualDetalleTable()->agregar($objDetalles);
                          //Graba los datos en el Registro
                          $data2 = $objDetalles->getPropertyArray();
                          $jencode    = json_encode($data2);
                          $now = date('Y-m-d G:i:s');
                
                          $data =array(
                                  'user' => $this->session->username,
                                  'accion' => 'Edito Inspeccion Visual Detalle',
                                  'descripcion' => "fecha: {$now} , Inspeccion Visual Id: {$objVisual->id}",
                                  'tabla_afectada' => 'inspeccion_visual_detalle',     
                                  'detalles_extra' => $jencode,
                          );
                          $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                    }    
                }
                return $this->redirect()->toRoute('reginspeccion',array(
                                                  'action' =>  'index'));      


             }
         }

        return array(
            'titulo' => 'Editar Inspeccion Visual',
            'insp_id' => $id_insp,
            'datos_ri' => $this->getRegistroInspeccionTable()->getRegistro($id_insp),
            'sql_defectos' =>  $this->getDefectoVisualTable()->getAllCodigosDescripcion(),
            'form' => $form,
        );
    }


    public function agregavisAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0); 
        if (!$id || $id == 0) {
              return $this->redirect()->toRoute('reginspeccion', array(
                                                'action' => 'index'));
        }


      

        $form             = $this->getInspVisualForm();  
        $dvisuales        = $this->getDefectoVisualTable()->fetchAll();
        $request          = $this->getRequest();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        
        while($row=$dvisuales->current())
        {
           // @Annotation\Attributes({"class":"form-control"})
            $test = new Element("cantidad{$row->id}");
            $test->setAttributes(array(
                      'type'  => 'text',
                      'class'  => 'form-control'
              ));
            $form->add($test);
        }
        
        $post = $request->getPost();
        if($request->isPost())
        {
             $form->setData($post);
             if($form->isValid())
             {               
                
                $objVisual=$this->getObjInspVisual($post,$id);  // Crea Objeto Visual Inspeccion

                $data2 = $objVisual->getPropertyArray();
                $jencode    = json_encode($data2);
                $now = date('Y-m-d G:i:s');
                
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta Inspeccion Visual',
                   'descripcion' => "fecha: {$now} , Inspeccion Id: {$id}",
                   'tabla_afectada' => 'inspeccion_visual',     
                   'detalles_extra' => $jencode,
                );
                
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);



                $this->getInpsVisualTable()->agregar($objVisual);
                $last_id =$this->getInpsVisualTable()->getLastId();


                $dvisual        = $this->getDefectoVisualTable()->fetchAll();
                while($row=$dvisual->current())
                {
                    $objDetalles= $this->getObjVisualDetalles($last_id,$row,$post);

                     if(!empty($objDetalles->cantidad)){
                        $this->getInpsVisualDetalleTable()->agregar($objDetalles);
                        //Graba los datos en el Registro
                        $data2 = $objDetalles->getPropertyArray();
                        $jencode    = json_encode($data2);
                        $now = date('Y-m-d G:i:s');
                
                        $data =array(
                                'user' => $this->session->username,
                                'accion' => 'Dio de Alta Inspeccion Visual Detalle',
                                'descripcion' => "fecha: {$now} , Inspeccion Visual Id: {$last_id}",
                                'tabla_afectada' => 'inspeccion_visual_detalle',     
                                'detalles_extra' => $jencode,
                        );
                         $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                    }    
                }

                return $this->redirect()->toRoute('reginspeccion',array(
                                                  'action' =>  'index'));              
             }

        }

        return array(
            'titulo' => 'Agregar Inspección Visual',
            'insp_id' => $id,
             'datos_ri' => $this->getRegistroInspeccionTable()->getRegistro($id),
            'sql_defectos' =>  $this->getDefectoVisualTable()->getAllCodigosDescripcion(),
            'form' => $form,
        );
    }

   

    public function editardimAction()
    {
          $id_insp = (int) $this->params()->fromRoute('id_insp', 0); 
          if (!$id_insp || $id_insp == 0) {
                  return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'index'));
          }
          $this->layout()->usuario = $this->session->display_name;
          $this->layout()->lastLogin = $this->session->lastLogin;
           $this->layout()->lastAction = $this->getLastAction();
          /// Objeto con la info de la tabla;
          $inspDim = $this->getInpsDimTable()->getRowByRegistroId($id_insp);

          $form = $this->getInspDimEditForm(); 
          $form->bind($inspDim);   

          $conceptos = $this->getConceptosTable()->fetchAll();
          $request    = $this->getRequest();
          $this->getSplitFormDim($inspDim,$form);


    

          while($row=$conceptos->current())
          {
              $test = new Element($row->form_name_muestra);
              $test->setAttributes(array(
                      'type'  => 'text',
                         'class'  => 'form-control'
              ));
              
              $test2 = new Element($row->form_name_rechazado);
              $test2->setAttributes(array(
                      'type'  => 'text',
                         'class'  => 'form-control'
              ));
              // Se busca el ID de Inspeccion Dimensional
              //  $objInspDim = $this->getInpsDimDetalleTable()->getRegistro($row->id);
              $objInspDim = $this->getInpsDimDetalleTable()->getDetalleById($row->id,$inspDim->id);
              $arrayObjeto = $objInspDim->current();

              if(!empty($arrayObjeto))
              {
                  $test->setValue($arrayObjeto['tamano_muestra']);
                  $test2->setValue($arrayObjeto['rechazado']);
              }

              $form->add($test);
              $form->add($test2);
          }

          //Burst Test


       //   print_r($id_insp);
        //  exit;
           for($i=1; $i<=8; $i++)
          {
              $cavidad = 'cavidad' . $i;
              $medida = 'medida' . $i;
              

              $burstestCav = new Element($cavidad);
              $burstestCav->setAttributes(array(
                      'type'  => 'text',
                         'class'  => 'form-control'
              ));
              

              $burstestMed = new Element($medida);
              $burstestMed->setAttributes(array(
                      'type'  => 'text',
                          'class'  => 'form-control'
              ));

              $objBurst =$this->getDimBurstTestTable()->getFormRow($inspDim->id,$i);
              
              $burstestCav->setValue($objBurst['cavidad']);
              $burstestMed->setValue($objBurst['medida']);
             

              $form->add($burstestCav);
              $form->add($burstestMed);
          }




          $post = $request->getPost();

          if($request->isPost())
          {
              $form->setData($post);
              if($form->isValid())
              {               
           
                  $objDim =$this->getObjInspDim($post,$id_insp);
                  $this->getInpsDimTable()->agregar($objDim);

                  //////////////////////////////////////////
                  $dataDim = $objDim->getPropertyArray();
                  $jencode    = json_encode($dataDim);
                  $now = date('Y-m-d G:i:s');
                
                  $data =array(
                    'user' => $this->session->username,
                    'accion' => 'Edito Inspeccion Dimensional',
                    'descripcion' => "fecha: {$now} , Inspeccion Id: {$id_insp}",
                    'tabla_afectada' => 'inspeccion_dimensional',     
                    'detalles_extra' => $jencode,
                  );
                  $this->getServiceLocator()->get('LogManager')->grabaLog($data);

                 ///////////////////////////////////////////////////////
                                
                $objPost = new PostDimensional($post,$inspDim->id);
                $objPost->setObjBurst($this->getDimBurstTestTable());


                foreach($objPost->getInsertArray() as $obj)
                {
                  $this->getDimBurstTestTable()->agregar($obj);
                }

                ////////////////////////////////////////////////////




                  $concepto = $this->getConceptosTable()->fetchAll();
 
                  while($row=$concepto->current())
                  {

                      $objDetalles=$this->getObjDimDetalles($dataDim['id'],$row,$post);

                      if(!empty($objDetalles->tamano_muestra) & !empty($objDetalles->tamano_muestra) 
                          &  $objDetalles->dimensional_id != 0){
                          
                          $this->getInpsDimDetalleTable()->agregar($objDetalles);
                          //////////////////////////////////////////
                          $data2 = $objDetalles->getPropertyArray();
                          $jencode    = json_encode($data2);
                          $now = date('Y-m-d G:i:s');
                
                          $data =array(
                                'user' => $this->session->username,
                                'accion' => 'Edito Inspeccion Dimensional Detalle',
                                'descripcion' => "fecha: {$now} , Inspeccion Registro Id: {$dataDim['id']}",
                                'tabla_afectada' => 'inspeccion_dimensional_detalle',     
                                'detalles_extra' => $jencode,
                            );
                          $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                          /////////////////////////////////////////

                    }    
                }

              return $this->redirect()->toRoute('reginspeccion',array(
                'action' =>  'index'));                
              }
          }



        return array(
            'titulo' => 'Editar Inspeccion Dimensional',
            'insp_id' => $id_insp,
            'datos_ri' => $this->getRegistroInspeccionTable()->getRegistro($id_insp),
            'sql_conceptos' =>   $this->getConceptosTable()->fetchAll(),
            'form' => $form,
        );
    }


    public function agregadimAction()
    {
          $id =  $this->params()->fromRoute('id',0); 
          if (!$id || $id == 0) {
                  return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'index'));
          }

          $this->layout()->usuario = $this->session->display_name;
          $this->layout()->lastLogin = $this->session->lastLogin;
          $this->layout()->lastAction = $this->getLastAction();
          $form = $this->getRegistroInsDimForm();    
          $conceptos = $this->getConceptosTable()->fetchAll();


          $request    = $this->getRequest();
          while($row=$conceptos->current())
          {

              $test = new Element($row->form_name_muestra);
              $test->setAttributes(array(
                      'type'  => 'text',
                      'class'  => 'form-control'
              ));
              $form->add($test);

              $test2 = new Element($row->form_name_rechazado);
              $test2->setAttributes(array(
                      'type'  => 'text',
                      'class'  => 'form-control'
              ));
              $form->add($test2);
          }


          for($i=1; $i<=8; $i++)
          {
              $cavidad = 'cavidad' . $i;
              $medida = 'medida' . $i;
              

              $burstestCav = new Element($cavidad);
              $burstestCav->setAttributes(array(
                      'type'  => 'text',
                      'class'  => 'form-control'
              ));
              $form->add($burstestCav);

              $burstestMed = new Element($medida);
              $burstestMed->setAttributes(array(
                      'type'  => 'text',
                     'class'  => 'form-control'
              ));
              $form->add($burstestMed);
          }


          

          $post = $request->getPost();




          if($request->isPost())
          {
            
             $form->setData($post);
             if($form->isValid())
             {               
                $objDim =$this->getObjInspDim($post,$id);
                $this->getInpsDimTable()->agregar($objDim);

                //////////////////////////////////////////
                $data2 = $objDim->getPropertyArray();
                $jencode    = json_encode($data2);
                $now = date('Y-m-d G:i:s');
                
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta Inspeccion Dimensional',
                   'descripcion' => "fecha: {$now} , Inspeccion Id: {$id}",
                   'tabla_afectada' => 'inspeccion_dimensional',     
                   'detalles_extra' => $jencode,
                );
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                $last_id =$this->getInpsDimTable()->getLastId();
               ///////////////////////////////////////////////////////
                                
                $objPost = new PostDimensional($post,$last_id);
                foreach($objPost->getInsertArray() as $obj)
                {
                  $this->getDimBurstTestTable()->agregar($obj);
                }

                ////////////////////////////////////////////////////
                $concepto = $this->getConceptosTable()->fetchAll();
                while($row=$concepto->current())
                {
                    $objDetalles=$this->getObjDimDetalles($last_id,$row,$post);
                    if(!empty($objDetalles->tamano_muestra) & !empty($objDetalles->tamano_muestra)){
                        $this->getInpsDimDetalleTable()->agregar($objDetalles);
                        
                        //////////////////////////////////////////
                        $data2 = $objDetalles->getPropertyArray();
                        $jencode    = json_encode($data2);
                        $now = date('Y-m-d G:i:s');
                
                        $data =array(
                                'user' => $this->session->username,
                                'accion' => 'Dio de Alta Inspeccion Dimensional Detalle',
                                'descripcion' => "fecha: {$now} , Inspeccion Registro Id: {$last_id}",
                                'tabla_afectada' => 'inspeccion_dimensional_detalle',     
                                'detalles_extra' => $jencode,
                          );
                        $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                        /////////////////////////////////////////

                    }    
                }

                return $this->redirect()->toRoute('inspdimensional',array(
                                                  'controller' => 'InspDimensional',
                                                  'action'  =>  'regfueraespec',
                                                  'id'      => $last_id ));                
             }

          }


          return array(
            'titulo' => 'Inspección Dimensional',
            'insp_id' => $id,
            'datos_ri' => $this->getRegistroInspeccionTable()->getRegistro($id),
            'sql_conceptos' =>   $this->getConceptosTable()->fetchAll(),
            'form' => $form,
        );

    }


     public function eliminarAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
             return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'index'));
        }
      
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        $request = $this->getRequest();
        
        if ($request->isPost()) 
        {
            $del = $request->getPost('del', 'No');
      
            if ($del == 'Si') 
            {

                // borrar dimensional
                $recorrido      =  $this->getRegistroInspeccionTable()->getRegistro($id);
                $dim_recorrido  =  $this->getInpsDimTable()->getRowByRegistroId($id);

              
                

                if($dim_recorrido->id!=0)
                {
                  
                    $this->getInpsDimDetalleTable()->eliminarByDimId($dim_recorrido->id);
                      $this->getDimFueraEspecTable()->eliminarByDimId($dim_recorrido->id);

                }

                $this->getInpsDimTable()->eliminar($dim_recorrido->id);






                $visual_recorrido  =  $this->getInpsVisualTable()->getRowByRegistroId($id);


                if($visual_recorrido->id!=0)
                {
                      
                      $this->getInpsVisualDetalleTable()->eliminarByVisId($visual_recorrido->id);
                      $this->getVisFueraEspecTable()->eliminarByVisId($visual_recorrido->id);

                }


                $this->getInpsVisualTable()->eliminar($visual_recorrido->id);

                $this->getRegistroInspeccionTable()->eliminar($id);



               
                $jencode    = json_encode($dim_recorrido);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino un Registro Inspeccion',
                   'descripcion' => "Registro/inspeccion: {$dim_recorrido->id} / {$dim_recorrido->turno}",
                   'tabla_afectada' => 'inspeccion_registro',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                
            }
      
        return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'index'));
      }

      $recorrido  = $this->getRegistroInspeccionTable()->getRegistro($id);
    


      return array(

          'id'    => $id,
          'recorrido' => $recorrido,
        
      );
      
    }





    protected function getObjInspDim($post,$id)
    {
         $dimensional = new InspeccionDimensional();
         if(array_key_exists('id', $post))
         {
              $dimensional->id                     = $post['id'];
         }
         $dimensional->registro_id            = $id;
         $dimensional->cantidad_inspeccion    = $post['cantidad_inspeccion'];
         $dimensional->fuera_especificacion   = $post['fuera_especificacion'];
         $dimensional->cantidad_rechazada      = $post['cantidad_rechazada'];
         
         if($post['prueba_funcional']==1){
            $dimensional->prueba_funcional = 'Aplica';
         }else{
            $dimensional->prueba_funcional = 'No Aplica';
         }

         if($post['primera_corrida']==1){
            $dimensional->primera_corrida = 'Si';
         }else{
            $dimensional->primera_corrida = 'No';
         }

         $dimensional->hora_recibido        = $post['horas'] . ':' . $post['minutos']  . ':' .$post['segundos'];
         $dimensional->hora_calificacion    = $post['horas_cal'] . ':' . $post['minutos_cal']  . ':' .$post['segundos_cal'];
         $dimensional->tip_sheet            = $post['tip_sheet'];
         $dimensional->resultado            = $post['resultado'];
         $dimensional->comentarios          = $post['comentarios'];
        
         return $dimensional;
    }


    protected function getObjDimDetalles($dim_id,$obj,$post)
    {
        $inspDetalle = new InspDimDetalle();
        
        $objInspDim = $this->getInpsDimDetalleTable()->getDetalleById($obj->id,$dim_id);
        $arrayObjeto = $objInspDim->current();

         $data =array(
            'id' => $arrayObjeto['id'],
            'dimensional_id' => $dim_id,
            'pconceptos_id' => $obj->id,
            'tamano_muestra' => $post[$obj->form_name_muestra],
            'rechazado' => $post[$obj->form_name_rechazado],
          );

         $inspDetalle->exchangeArray($data);
        return $inspDetalle;
    }


    protected function getVectorInspeccion($formInspeccion)
    {
        $recorrido=$this->getMaquinasActivasTable()->getRegistro($formInspeccion['fecha_captura_turno']);
        //
        return $data = array(
              'recorrido_id' => $formInspeccion['fecha_captura_turno'],
              'fecha_captura' => $recorrido->fecha,
              'turno'         => $recorrido->turno,
              'numero_recorrido' => $formInspeccion['numero_recorrido'],
              'capturista'       => $this->getUsuariosTable()->getUserIdbyNombre($formInspeccion['capturista']),
              'inspector_dimensional'  => $this->getUsuariosTable()->getUserIdbyNombre($formInspeccion['inspector_dimensional']),
              'inspector_visual'       => $this->getUsuariosTable()->getUserIdbyNombre($formInspeccion['inspector_visual']),
              'maquina_id'             => $formInspeccion['maquina_id'],
              'moldes_id'              => $formInspeccion['moldes_id'],
              'partes_id'              => $formInspeccion['partes_id'],
              'cambio_turno'           => $formInspeccion['cambio_turno'],
          );
    }

    protected function getObjetoInspeccion($form)
    {
         $formInspeccion = $form->getData();
         

         $recorrido=$this->getMaquinasActivasTable()->getRegistro($formInspeccion->fecha_captura_turno);
         return array(
                        'id'  => $formInspeccion->id,
                        'recorrido_id' => $formInspeccion->fecha_captura_turno,
                        'fecha_captura' => $recorrido->fecha,
                        'turno' => $recorrido->turno,
                        'numero_recorrido' =>  $formInspeccion->numero_recorrido,
                        'capturista' =>  $form->get('capturista')->getValue(),
                        'inspector_dimensional' =>   $form->get('inspector_dimensional')->getValue(),
                        'inspector_visual' => $form->get('inspector_visual')->getValue(),
                        'maquina_id' => $formInspeccion->maquina_id,
                        'moldes_id' => $formInspeccion->moldes_id,
                        'partes_id' => $formInspeccion->partes_id,
                        'cambio_turno'    => $formInspeccion->cambio_turno,
          );
    }


 protected function getObjVisualDetalles($vis_id,$obj,$post)
    {
          $inspDetalle = new InspVisualDetalle();
          $visualDetalle = $this->getInpsVisualDetalleTable()->getDetalleById($obj->id,$vis_id);
          $arrayVisual = $visualDetalle->current();

          if(empty($arrayVisual))
          {
             $arrayVisual = array();
          }

          $inspDetalle->exchangeArray(array(
            'visual_id' => $vis_id,
            'defecto_id' => $obj->id,
            'cantidad' => $post["cantidad{$obj->id}"],
          ));
 
          if(array_key_exists('id', $arrayVisual))
          {
              $inspDetalle->id                     = $arrayVisual['id'];
          }

          return $inspDetalle;
    }

    protected function getObjInspVisual($post,$id)
    {
         $visual = new InspeccionVisual();
         $visual->exchangeArray(array(
        
            'registro_id'             => $id,
            'cantidad_inspeccionada'  => $post['cantidad_inspeccionada'],
            'cantidad_liberada'       => $post['cantidad_liberada'],
            'cantidad_rechazada'      => $post['cantidad_rechazada'],
            'total_defectos'          => $post['total_defectos'],
            'comentarios'             => $post['comentarios'],
          ));
          if(array_key_exists('id', $post))
          {
              $visual->id                     = $post['id'];
          }
         return $visual;
    }


    protected function checkPostForm(&$form,$obj_inspeccion)
    {

        $formcapturista = $form->get('capturista')->getValue();
        $forminsp_dim   = $form->get('inspector_dimensional')->getValue();
        $forminsp_vis   = $form->get('inspector_visual')->getValue();

        if(empty($formcapturista))
        {   // Si el campo de capturista esta vacio
            $form->get('capturista')->setValue($obj_inspeccion->capturista);

        }else{
          // Si el campo no esta vacio
            $usuario = $this->getUsuariosTable()->getUserIdbyNombre($form->get('capturista')->getValue());
            
            if($usuario)
            {
                $form->get('capturista')->setValue($usuario);

            }else{
                $form->get('capturista')->setValue('');
            }
        }

        $usuario = false;

        if(empty($forminsp_dim))
        {   // Si el campo de capturista esta vacio
            $form->get('inspector_dimensional')->setValue($obj_inspeccion->inspector_dimensional);

        }else{
          // Si el campo no esta vacio
            $usuario = $this->getUsuariosTable()->getUserIdbyNombre($form->get('inspector_dimensional')->getValue());
            
            if($usuario)
            {
                $form->get('inspector_dimensional')->setValue($usuario);

            }else{
                 $form->get('inspector_dimensional')->setValue('');
            }
        }

        $usuario = false;

        if(empty($forminsp_vis))
        {   // Si el campo de capturista esta vacio
            $form->get('inspector_visual')->setValue($obj_inspeccion->inspector_visual);

        }else{
          // Si el campo no esta vacio
            $usuario = $this->getUsuariosTable()->getUserIdbyNombre($form->get('inspector_visual')->getValue());
            
            if($usuario)
            {
                $form->get('inspector_visual')->setValue($usuario);

            }else{

                $form->get('inspector_visual')->setValue('');
            }
        }
    }



   
	
}