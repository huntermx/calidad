<?php

namespace Captura\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;
use Captura\Model\Entity\Maquinasactivas;
use Captura\Model\Entity\Maquinasdetalle;


class MaquinasactivasController extends AbstractCapturaController
{
  
	public function indexAction()
	{
        $sesion = new SessionContainer('labodigi');
        $this->flashmessenger()->clearCurrentMessages();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	 'titulo' => 'Máquinas Activas',
           'maquina' => $this->getMaquinasActivasTable()->fetchAll(),
        );
	}


	public function agregarAction()
    {
    	 $form       = $this->getMaquinasActivasForm();
    	 $request 	 = $this->getRequest();
         $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        $this->layout()->lastAction = $this->getLastAction();

    	if($request->isPost())
        {
            $maquina = new Maquinasactivas();
            $form->setData($request->getPost());

            if($form->isValid())
            {
            	   $maquina->exchangeArray($form->getData()); 
                 $valida =$this->getMaquinasActivasTable()->CheckFechaTurno($maquina->fecha, $maquina->turno);
                 
                 if(!$valida)
                 {
                      $jencode    = json_encode($form->getData());
                      $data =array(
                            'user' => $this->session->username,
                            'accion' => 'Dio de Alta una Capura',
                            'descripcion' => "Captura: {$maquina->fecha} / {$maquina->turno}",
                            'tabla_afectada' => 'maquinas_activas',     
                            'detalles_extra' => $jencode,
                      );
                
                      $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                      $maquina->exchangeArray($form->getData());
                      $this->getMaquinasActivasTable()->agregar($maquina);
                      $lastId = $this->getMaquinasActivasTable()->getLastId();
                 }

            	 return $this->redirect()->toRoute('maquinasactivas',array(
                          'action' =>  'agregarmaquinas',
                           'id' => $lastId));
            }
		    }else{

    	       return array(
    	 		      'titulo' => 'Agregar Captura',
    	 		      'form' => $form,
    	 	     );
    	}


    }


	public function agregarmaquinasAction()
  {

    	$id = (int) $this->params()->fromRoute('id', 0);
    	if (!$id || $id == 0) {
        		return $this->redirect()->toRoute('maquinasactivas', array(
            	'action' => 'index'));
      	}
       $this->layout()->usuario = $this->session->display_name;
       $this->layout()->lastLogin = $this->session->lastLogin;
       $this->layout()->lastAction = $this->getLastAction();
    	 $form       = $this->getMaquinaForm();
		   $form->get('maquinas_activas_id')->setValue($id);
		   $request 	 = $this->getRequest();


       
       $lastId=$this->getMaquinasDetallesTable()->getLastMachine($id);
       $arrayLast = $lastId->current();
       $nextId=$this->getMaquinasTable()->getNextMachine($arrayLast['maquina_id']);
       $arrayNext = $nextId->current();
       $nextMachineId = $arrayNext['id'];
       


	     if($request->isPost())
        {
        	
        	$maquina = new Maquinasdetalle();
        
        	$form->get('maquina_id')->setValueOptions($this->getMaquinasTable()->getMaquinasOptions());
        	 $form->setData($request->getPost());
        	if($form->isValid())
            {
            	 $maquina->exchangeArray($form->getData());             
                 $jencode     = json_encode($form->getData());

                 $temp        = $this->getMaquinasDetallesTable()->getMaquinaByMaquinaId($maquina->maquina_id,$maquina->maquinas_activas_id);
                 $arrayTemp   = $temp->current();

                 print_r($arrayTemp);

                 if(empty($arrayTemp) & $maquina->maquina_id!=0)
                 {
                   
                      $data =array(
                                  'user' => $this->session->username,
                                  'accion' => 'Dio de Alta una Maquina a una Captura',
                                  'descripcion' => "Captura: {$maquina->fecha} / {$maquina->turno}",
                                  'tabla_afectada' => 'maquinas_detalle',     
                                  'detalles_extra' => $jencode,
                      );
                
                      $this->getServiceLocator()->get('LogManager')->grabaLog($data);
                      $maquina->exchangeArray($form->getData());
                      $this->getMaquinasDetallesTable()->agregar($maquina);
                 }

			   	       return $this->redirect()->toRoute('maquinasactivas',array(
		 											'action' =>  'agregarmaquinas',
		 											 'id' => $id));

            }

        }else{	


			return array(
    	 			'titulo' => 'Agrega Maquina a Captura',
    	 			'form' => $form,
            'nextSelect' => $nextMachineId,
    	 			'id' => $id,
    	 			'mdetalle' => $this->getMaquinasDetallesTable()->getAllMaquinasByRecorrido($id),
    	 	);
		}

    }


    public function eliminarAction()
    {
      $id = (int) $this->params()->fromRoute('id', 0);
      if (!$id) {
            return $this->redirect()->toRoute('maquinasactivas');
      }
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
      $request = $this->getRequest();
      if ($request->isPost()) 
      {
        $del = $request->getPost('del', 'No');
      
            if ($del == 'Si') 
            {

            	$maquinaDetalles = $this->getMaquinasDetallesTable()->getRegistro($id);
            

               // $id = (int) $request->getPost('id');

                $this->getMaquinasDetallesTable()->eliminar($id);
               
               $jencode    = json_encode($maquinaDetalles);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino una maquina activa de una Captura',
                   'descripcion' => "Recorrido/Maquina: {$maquinaDetalles->maquinas_activas_id} / {$maquinaDetalles->maquina_id}",
                   'tabla_afectada' => 'maquinas_detalle',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);
               }
      
        return $this->redirect()->toRoute('maquinasactivas',array(
              'action' =>  'index'));
      }

      $maquinaDetalle = $this->getMaquinasDetallesTable()->getRegistro($id);
      $maquina = $this->getMaquinasTable()->getRegistro($maquinaDetalle->maquina_id);


      return array(
          'id'    => $id,
          'maquina' => $maquina->maquina,
        
      );
      
    }


	
}