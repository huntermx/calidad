<?php

namespace Captura\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use core\View\Helper\ClassFecha;

class AsyncController extends AbstractCapturaController
{
	protected $DistribuidoresTable;
	protected $RolesTable;
	
	public function getmaquinasAction()
	{
		$recorrido = (int) $this->params()->fromRoute('id', 0);  


		$roles = $this->getMaquinasTable()->fetchAll();
		foreach($roles as $rol){
			$items[]= array($rol->id , $rol->maquina);
		}
		echo json_encode($items);
		exit;
	}

	public function getmaquinasNotAction()
	{
		$maquinas_activas_id = (int) $this->params()->fromRoute('id', 0);  
		$roles = $this->getMaquinasTable()->fetchAll();
		$test=$this->getMaquinasDetallesTable()->getMaquinaByMaquinaId(1,$maquinas_activas_id);

		foreach($roles as $rol){
			$test=$this->getMaquinasDetallesTable()->getMaquinaByMaquinaId($rol->id,$maquinas_activas_id);
			$temp = $test->current();
			if(empty($temp))
			{
				$items[]= array($rol->id , $rol->maquina);
			}
			
		}
		echo json_encode($items);
		exit;
	}



	
	public function getrecorridos2Action()
	{
		$recorridos= $this->getMaquinasActivasTable()->getListaCapturaMaquina();
		foreach($recorridos as $recorrido)
		{
			
			$maquinasTotal= $this->getMaquinasDetallesTable()->getMaquinasNumberByRecorrido($recorrido['id']);
			$regCapTotal = $this->getRegistroInspeccionTable()->getInspRegistroNumById($recorrido['id']);
			$totalMaqReg = $regCapTotal->count();

			if($maquinasTotal!=$totalMaqReg)
			{

				$fclass = new ClassFecha($recorrido['fecha'],true);	
				$fclass->setFormatoMDA();
				$fclass->setFormatoDsMDA();

				$temp = $fclass->getFecha(). " - ". $recorrido['turno']  ;
				$items[]= array($recorrido['id'] , $temp);

			}

			
		}
		echo json_encode($items);
		exit;
	}





	// Registro iNspeccion - Agrega Fecha
	public function getrecorridosAction()
	{
		$recorridos = $this->getMaquinasActivasTable()->fetchAll();

		foreach($recorridos as $recorrido)
		{
			$fclass = new ClassFecha($recorrido->fecha,true);	
			$fclass->setFormatoMDA();
			$fclass->setFormatoDsMDA();

			$temp = $fclass->getFecha(). " - ". $recorrido->turno;
			$items[]= array($recorrido->id , $temp);
		}
		echo json_encode($items);
		exit;
	}


	public function getmaquinasleftAction()
	{
		$recorrido = (int) $this->params()->fromRoute('id', 0);  

		$maquinas_activas = $this->getMaquinasDetallesTable()->getAllMaquinasByRecorrido($recorrido);
		$items = false;

		foreach($maquinas_activas as $maquinas)
		{
			$temp = $this->getMaquinasTable()->getRegistro($maquinas->maquina_id);

			$objRegistro= $this->getRegistroInspeccionTable()->getRowByRecMaquina($recorrido,$maquinas->maquina_id);
			$arrayRow = $objRegistro->current();
			if(empty($arrayRow))
			{
				$items[]= array($maquinas->maquina_id , $temp->maquina);
			}
			
		}


		if($items==false)
		{
			$items[] = array("","");
		}


		echo json_encode($items);
		exit;

	}

	//getRecorridosmaquina
	public function getrecorridosmaquinaAction()
	{
		$recorrido = (int) $this->params()->fromRoute('id', 0);  
	
		//
		$maquinas_activas = $this->getMaquinasDetallesTable()->getAllMaquinasByRecorrido($recorrido);

		foreach($maquinas_activas as $maquinas)
		{
			$temp = $this->getMaquinasTable()->getRegistro($maquinas->maquina_id);

			//$objRegistro= $this->getRegistroInspeccionTable()->getRowByRecMaquina($recorrido,$maquinas->maquina_id);
		///	$arrayRow = $objRegistro->current();
	//		if(empty($arrayRow))
	//		{
				$items[]= array($maquinas->maquina_id , $temp->maquina);
	//		}
			
		}
		echo json_encode($items);
		exit;
	}

	public function getmoldespartesAction()
	{
		$molde = (int) $this->params()->fromRoute('id', 0);  
		$partes = $this->getMoldeParteTable()->getPartesByMolde($molde);

		foreach($partes as $parte)
		{
		
			$temp =  $parte['num_parte'] . " - ". $parte['descripcion'];
			$items[]= array($parte['id'] , $temp);
		}
		echo json_encode($items);
		exit;
	}

	public function getusuariobynameAction()
	{
		$nombre = (string) $this->params()->fromRoute('id', 0);  
		$usuarios = $this->getUsuariosTable()->getUsuarioByNombre($nombre);

 		$list = '';
		foreach($usuarios as $usuario)
		{
		    $list = $list .$usuario['nombre'] . ", " . $usuario['apellidos'] . "\n";
			
		}

                return $this->getResponse()->setContent($list);
		
	}

}