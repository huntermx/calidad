<?php

namespace Captura\Controller;

use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\Input;


use Captura\Model\Entity\DimFueraEspec;



class InspDimensionalController extends AbstractCapturaController
{

	public function indexAction()
	{		
         $sesion = new SessionContainer('labodigi');
         $this->flashmessenger()->clearCurrentMessages();
        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();
        return array(
        	     'titulo' => 'Registro Inspeccion',
               'registro' => $this->getRegistroInspeccionTable()->fetchAll(),
        );
	}


    public function regfueraespecAction()
    {
        $dimensional_id =  $this->params()->fromRoute('id',0); 
        if (!$dimensional_id || $dimensional_id == 0) {
                  return $this->redirect()->toRoute('reginspeccion', array(
                                                    'action' => 'index'));
        }

        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $this->layout()->lastAction = $this->getLastAction();

        $obj_dim = $this->getInpsDimTable()->getRowByRegistroId($dimensional_id);



        $form = $this->getDimFueraEspecForm();
        $form->get('dimensional_id')->setValue($obj_dim->id);    

        $request    = $this->getRequest();

        if($request->isPost())
        {
             $post = $request->getPost();
             $form->setData($post);

             $objFueraEspec = new DimFueraEspec();
             
            if($form->isValid())
            {
                $array_post = $form->getData();
                $objFueraEspec->exchangeArray($form->getData());  
                $jencode    = json_encode($form->getData());

                $this->getDimFueraEspecTable()->agregar($objFueraEspec);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Dio de Alta Registro Fuera Especificacion',
                   'descripcion' => "Registro Dim: {$dimensional_id}",
                   'tabla_afectada' => 'dimensional_fuera_especificacion',     
                   'detalles_extra' => $jencode,
                );
                
               $this->getServiceLocator()->get('LogManager')->grabaLog($data);
             
                if($array_post['submit2']){
                      return $this->redirect()->toRoute('reginspeccion',array(
                                                        'action' =>  'index'));                
                }
    
                if($array_post['submit']){
                      return $this->redirect()->toRoute('inspdimensional',array(
                                                        'controller' => 'InspDimensional',
                                                        'action'     => 'regfueraespec',
                                                        'id'     => $dimensional_id,));                
                }
             }
        }
        return array(
                 'titulo' => 'Registro Fuera Especificacion',
                 'id'     => $dimensional_id,
                 'registro' => $this->getDimFueraEspecTable()->getRowsByDimensional_Id($obj_dim->id),
                 'form' => $form,

        );
    }
	


     public function eliminarfueraespecAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
                 return $this->redirect()->toRoute('reginspeccion',array(
                                                        'action' =>  'index'));         
        }

        $this->layout()->usuario = $this->session->display_name;
        $this->layout()->lastLogin = $this->session->lastLogin;
        $request = $this->getRequest();

       if ($request->isPost()) 
       {
            $del = $request->getPost('del', 'No');
            if ($del == 'Si') 
            {
                $fueraEspec = $this->getDimFueraEspecTable()->getRegistro($id);
                 $this->getDimFueraEspecTable()->eliminar($id);
               
                $jencode    = json_encode($fueraEspec);
                $data =array(
                   'user' => $this->session->username,
                   'accion' => 'Elimino un Registro Fuera Especificacion',
                   'descripcion' => "Registro Dimensional: {$fueraEspec->dimensional_id} / Dim FUera Espec: {$fueraEspec->id}",
                   'tabla_afectada' => 'dimensional_fuera_especificacion',     
                   'detalles_extra' => $jencode,
                );      
                $this->getServiceLocator()->get('LogManager')->grabaLog($data);

            }
                return $this->redirect()->toRoute('inspdimensional',array(
                                                        'controller' => 'InspDimensional',
                                                        'action'     => 'regfueraespec',
                                                        'id'     => $fueraEspec->dimensional_id,));        

       }

         return array(
          'id'    => $id,
          'rowfueraespec' => $this->getDimFueraEspecTable()->getRegistro($id),
        
      );
    }
	
}