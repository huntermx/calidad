<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\DimFueraEspec;


class DimFueraEspecTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

   
   public function getRowsByDimensional_Id($dim_id)
   {
        $dim_id  = (int) $dim_id;
        $rowset = $this->tableGateway->select(array('dimensional_id' => $dim_id));
        return $rowset;
   }
   
    
    public function agregar(DimFueraEspec $inspeccion)
    {
        $data = array(
            'dimensional_id'            => trim($inspeccion->dimensional_id),
            'cantidad'                  => trim($inspeccion->cantidad),
            'cavidad'                   => trim($inspeccion->cavidad),
            'dimension'                 => trim($inspeccion->dimension),
            'zona_dibujo'               => trim($inspeccion->zona_dibujo),
        );

        $id = (int)$inspeccion->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;

        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function getLastId()
   {
        return $this->last_id;
   }

   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
     public function eliminarByDimId($id)
    {
        $id  = (int) $id;
        $this->tableGateway->delete(array('dimensional_id' => $id));
        
    }
    
}