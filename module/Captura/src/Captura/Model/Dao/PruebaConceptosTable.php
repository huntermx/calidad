<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\Maquinasdetalle;


class PruebaConceptosTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

   
    
     public function getAllMaquinasByRecorrido($recorrido_id)
    {

        $id  = (int) $recorrido_id;
        $rowset = $this->tableGateway->select(array('maquinas_activas_id' => $id));
       
        return $rowset;
    }

    public function getMaquinasNumberByRecorrido($recorrido_id)
    {
        $id  = (int) $recorrido_id;
        $rowset = $this->tableGateway->select(array('maquinas_activas_id' => $id));
        $count = $rowset->count();
        return $count;
       
    }



   
    
    public function agregar(Maquinasdetalle $maquina)
    {
        $data = array(
            'timestamp'                 => date('Y-m-d H:i:s'),
            'maquinas_activas_id'       => trim($maquina->maquinas_activas_id),
            'maquina_id'                => trim($maquina->maquina_id),
        );

        $id = (int)$maquina->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
    function parse_timestamp($timestamp, $format = 'd-m-Y')
    {
        $formatted_timestamp = date($format, strtotime($timestamp));
        return $formatted_timestamp;
    }
    
}