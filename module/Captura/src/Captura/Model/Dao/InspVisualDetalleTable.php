<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\InspVisualDetalle;


class InspVisualDetalleTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;
    protected $bdtable;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
        $this->bdtable      = 'inspeccion_visual_detalle';
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }
   
    public function getDetalleById($defecto_id,$insp_detalle_id)
    {
        $sql = new Sql($this->adapter);
        $select= $sql->select();
        $select->from($this->bdtable);
        $where = new Where();
        $where->equalTo('defecto_id', $defecto_id);
        $where->equalTo('visual_id', $insp_detalle_id);
        $select->where($where);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }
    
    public function agregar(InspVisualDetalle $inspeccion)
    {
        $data = array(
            'visual_id'           => trim($inspeccion->visual_id),
            'defecto_id'          => trim($inspeccion->defecto_id),
            'cantidad'            => trim($inspeccion->cantidad),
            'comentarios'         => trim($inspeccion->comentarios),
        );

        $id = (int)$inspeccion->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;

        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function getLastId()
   {
        return $this->last_id;
   }

   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
        public function eliminarByVisId($id)
    {
        $id  = (int) $id;
        $this->tableGateway->delete(array('visual_id' => $id));
        
    }


    public function getSumDefectos($registro_id)
    {

        $sql =  "SELECT sum(inspvisdet.cantidad) as cantidad
                 FROM inspeccion_registro as ireg
                 LEFT JOIN inspeccion_visual as ivis ON ireg.id = ivis.registro_id
                 LEFT JOIN inspeccion_visual_detalle as inspvisdet ON ivis.id = inspvisdet.visual_id
                 WHERE ireg.id = '{$registro_id}' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }
    
    
}