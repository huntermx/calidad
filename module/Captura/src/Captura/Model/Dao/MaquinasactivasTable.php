<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\Maquinasactivas;


class MaquinasactivasTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;
    protected $bdtable;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
        $this->bdtable      = 'maquinas_activas';

    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

    public function getListaMactivas()
    {
            $lista = array();
            $resultSet = $this->tableGateway->select();

            foreach ($resultSet as $result)
            {
                $temp = $result->fecha. "-". $result->turno;
                $lista[]= array('value' => $result->id , 'label' =>$temp);
            }        
            return $lista;
    }
    

    public function getListaCapturaMaquina()
    {

       // $date = date('Y-m-d', strtotime('-4 week'));
        $sql =  "SELECT DISTINCT ma.id, ma.fecha,ma.turno 
                 FROM maquinas_activas as ma
                 RIGHT JOIN maquinas_detalle as md on ma.id=md.maquinas_activas_id
                 WHERE ma.fecha 
                 BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 4 WEEK) AND CURRENT_DATE()
                 ORDER by ma.id DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }
   
 public function CheckFechaTurno($fecha,$turno)
    {
        $sql =  "SELECT * 
                 FROM maquinas_activas 
                 WHERE fecha='{$fecha}' AND turno='{$turno}'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();

        $row = $result->current();

        if(!empty($row))
        {
            return TRUE;

        }else{
            return FALSE;
        }

    }


    
    public function agregar(Maquinasactivas $maquina)
    {
        $data = array(
            'timestamp'     => date('Y-m-d H:i:s'),
            'fecha'         => trim($maquina->fecha),
            'turno'         => trim($maquina->turno),
            'comentarios'   => trim($maquina->comentarios),
        );

        $id = (int)$maquina->id;


       
        if ($id == 0) 
        {      
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


    public function eliminarUsuario($id)
    {
    	$data=array('nstatus' =>'3');
        $this->tableGateway->update($data, array('id' => $id));
    }
    
   
   public function getLastId()
   {
        return $this->last_id;
   }

    
}