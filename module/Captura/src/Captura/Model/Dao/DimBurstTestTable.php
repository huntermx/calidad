<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\DimBurstTest;


class DimBurstTestTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;
    protected $bdtable;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
        $this->bdtable      = "dimensional_burst_test";

    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

   public function getFormRow($dim_id,$order_num)
   {
        $array_result = array(
                'cavidad' => null,
                'medida' => null,
            );

        $result=$this->getIdFromDim($dim_id,$order_num);
       /* $sql = new Sql($this->adapter);
        $select= $sql->select();
        $select->from($this->bdtable);
        $where = new Where();
        $where->equalTo('dimensional_id', $dim_id);
        $where->equalTo('order_number', $order_num);
        $select->where($where);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();*/
        $arrayObjeto = $result->current();

        if(!empty($arrayObjeto))
        {
            $array_result['cavidad'] =$arrayObjeto['cavidad'];
            $array_result['medida'] = $arrayObjeto['medida'];
        }


        return $array_result;
   }



   public function getIdFromDim($dimensional_id,$order_number)
   {
        $sql = new Sql($this->adapter);
        $select= $sql->select();
        $select->from($this->bdtable);
        $where = new Where();
        $where->equalTo('dimensional_id', $dimensional_id);
        $where->equalTo('order_number', $order_number);
        $select->where($where);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
   }
   
    
    public function agregar(DimBurstTest $inspeccion)
    {
        $data = array(
            'dimensional_id'            => trim($inspeccion->dimensional_id),
            'order_number'              => trim($inspeccion->order_number),
            'cavidad'                   => trim($inspeccion->cavidad),
            'medida'                    => trim($inspeccion->medida),

        );

        $id = (int)$inspeccion->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;

        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function getLastId()
   {
        return $this->last_id;
   }

   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
     public function eliminarByDimId($id)
    {
        $id  = (int) $id;
        $this->tableGateway->delete(array('dimensional_id' => $id));
    }
    
    public function eliminarById($id)
    {
        $id  = (int) $id;
        $this->tableGateway->delete(array('id' => $id));

    }
}