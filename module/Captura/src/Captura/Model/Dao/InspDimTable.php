<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\InspeccionDimensional;


class InspDimTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

    public function getRowByRegistroId($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('registro_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            $temp = new InspeccionDimensional();
            $temp->exchangeArray(array());
            return $temp;
        }
        return $row;
    }
   
    
    public function agregar(InspeccionDimensional $inspeccion)
    {
        $data = array(
            'registro_id'               => trim($inspeccion->registro_id),
            'cantidad_inspeccion'       => trim($inspeccion->cantidad_inspeccion),
            'fuera_especificacion'      => trim($inspeccion->fuera_especificacion),
            'cantidad_rechazada'         => trim($inspeccion->cantidad_rechazada),
            'prueba_funcional'          => trim($inspeccion->prueba_funcional),
            'primera_corrida'           => trim($inspeccion->primera_corrida),
            'hora_recibido'             => trim($inspeccion->hora_recibido),
            'hora_calificacion'         => trim($inspeccion->hora_calificacion),
            'tip_sheet'                 => trim($inspeccion->tip_sheet),
            'resultado'                 => trim($inspeccion->resultado),
            'comentarios'               => trim($inspeccion->comentarios),

        );

        $id = (int)$inspeccion->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;

        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function getLastId()
   {
        return $this->last_id;
   }

   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
    
    public function getSumFueraEspec($reg_id)
    {
        $sql =  "SELECT idim.fuera_especificacion as fuera_especificacion
                FROM inspeccion_registro as ireg
                LEFT JOIN inspeccion_dimensional as idim ON ireg.id = idim.registro_id
                WHERE  ireg.id='{$reg_id}' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    
    }
    
}