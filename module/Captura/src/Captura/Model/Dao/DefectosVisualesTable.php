<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\DefectosVisuales;


class DefectosVisualesTable
{
    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }

   
    public function getAllCodigosDescripcion()
    {
        $resultSet = $this->tableGateway->select();  
        $list = array();
        while($row=$resultSet->current())
        {
            $temp = $this->chckEmpty($row->codigo);
            $temp2 = $this->chckEmpty($row->descripcion);
            if($temp){
                $temp = $temp . "-";
            }
            $lista[] = array("id" => $row->id,
                          "text" => $temp . $temp2);
        }


        return $lista;
    }
    

    protected function chckEmpty($var)
    {
        if(empty($var)){
            return false;
        }
        return $var;
    }

    
    public function agregar(DefectosVisuales $maquina)
    {
        $data = array(
            'timestamp'                 => date('Y-m-d H:i:s'),
            'maquinas_activas_id'       => trim($maquina->maquinas_activas_id),
            'maquina_id'                => trim($maquina->maquina_id),
        );

        $id = (int)$maquina->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
   
    
}