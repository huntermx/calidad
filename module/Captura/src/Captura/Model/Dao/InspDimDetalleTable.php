<?php

namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

use Captura\Model\Entity\InspDimDetalle;


class InspDimDetalleTable
{
    protected $tableGateway;
    protected $adapter;
    protected $last_id= false;
    protected $bdtable;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
        $this->bdtable      = 'inspeccion_dimensional_detalle';
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();        				
        return $resultSet;
    }
   

    public function getDetalleById($pconceptos_id,$insp_detalle_id)
   {
        $sql = new Sql($this->adapter);
        $select= $sql->select();
        $select->from($this->bdtable);
        $where = new Where();
        $where->equalTo('pconceptos_id', $pconceptos_id);
        $where->equalTo('dimensional_id', $insp_detalle_id);
        $select->where($where);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
   }
    
    public function agregar(InspDimDetalle $inspeccion)
    {
        $data = array(
            'dimensional_id'            => trim($inspeccion->dimensional_id),
            'pconceptos_id'             => trim($inspeccion->pconceptos_id),
            'tamano_muestra'            => trim($inspeccion->tamano_muestra),
            'rechazado'                 => trim($inspeccion->rechazado),
            'comentarios'               => trim($inspeccion->comentarios),
        );

        $id = (int)$inspeccion->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
            $this->last_id = $this->tableGateway->lastInsertValue;

        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function getLastId()
   {
        return $this->last_id;
   }

   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
     

     public function eliminarByDimId($id)
    {
        $id  = (int) $id;
        $this->tableGateway->delete(array('dimensional_id' => $id));
        
    }
    
}