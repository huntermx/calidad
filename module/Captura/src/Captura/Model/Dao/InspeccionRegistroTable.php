<?php
// http://bigemployee.com/zend-framework-2-pagination-sorting-tutorial/
namespace Captura\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;
use Captura\Model\Entity\InspeccionRegistro;


class InspeccionRegistroTable extends AbstractTableGateway
{
    protected $tableGateway;
    protected $adapter;
    protected $bdtable;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter      = $adapter;
        $this->bdtable      = 'inspeccion_registro';
    }

 
   
    public function fetchAll(Select $select = null) 
    {
        if (null === $select)
            $select = new Select();
        $select->from($this->bdtable);
        $resultSet = $this->selectWith($select);
        $resultSet->buffer();
        return $resultSet;
    }



    public function getRowByRecMaquina($req_id,$maquina_id)
    {
        $sql = new Sql($this->adapter);
        $select= $sql->select();
        $select->from($this->bdtable);
        $where = new Where();
        $where->equalTo('recorrido_id', $req_id);
        $where->equalTo('maquina_id', $maquina_id);
        $where->equalTo('cambio_turno', 0);
        $select->where($where);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
   

    public function getInspRegistroNumById($mactiva_id)
    {
        $sql =  "SELECT DISTINCT ma.maquina_id 
                 FROM inspeccion_registro AS ir
                 JOIN maquinas_detalle as ma on ir.maquina_id=ma.maquina_id
                 WHERE ir.recorrido_id='{$mactiva_id}' AND ir.cambio_turno='0'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }
   
    
    public function agregar(InspeccionRegistro $inspeccionRegistro)
    {
        $data = array(
            'timestamp'                 => date('Y-m-d H:i:s'),
            'recorrido_id'              => trim($inspeccionRegistro->recorrido_id),
            'fecha_captura'             => trim($inspeccionRegistro->fecha_captura),
            'turno'                     => trim($inspeccionRegistro->turno),
            'numero_recorrido'          => trim($inspeccionRegistro->numero_recorrido),
            'capturista'                => trim($inspeccionRegistro->capturista),
            'inspector_dimensional'     => trim($inspeccionRegistro->inspector_dimensional),
            'inspector_visual'          => trim($inspeccionRegistro->inspector_visual),
            'maquina_id'                => trim($inspeccionRegistro->maquina_id),
            'moldes_id'                 => trim($inspeccionRegistro->moldes_id),
            'partes_id'                 => trim($inspeccionRegistro->partes_id),
            'cambio_turno'              => trim($inspeccionRegistro->cambio_turno),

        );

        $id = (int)$inspeccionRegistro->id;
       
        if ($id == 0) {      
       
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }



    public function checkDuplicateByArray(array $arrayInspeccion)
    {

        $sql =  "SELECT * 
                 FROM inspeccion_registro 
                 WHERE recorrido_id = '{$arrayInspeccion['recorrido_id']}'
                 AND fecha_captura = '{$arrayInspeccion['fecha_captura']}'
                 AND turno = '{$arrayInspeccion['turno']}'
                 AND numero_recorrido = '{$arrayInspeccion['numero_recorrido']}'
                 AND capturista = '{$arrayInspeccion['capturista']}'
                 AND inspector_dimensional = '{$arrayInspeccion['inspector_dimensional']}'
                 AND inspector_visual = '{$arrayInspeccion['inspector_visual']}'
                 AND maquina_id = '{$arrayInspeccion['maquina_id']}'
                 AND moldes_id = '{$arrayInspeccion['moldes_id']}'
                 AND partes_id = '{$arrayInspeccion['partes_id']}'
                 AND cambio_turno = '{$arrayInspeccion['cambio_turno']}'";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;


    }


    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


   public function eliminar($id)
    {
        
        $this->tableGateway->delete(array('id' => $id));
        
    }
    
    
    
}