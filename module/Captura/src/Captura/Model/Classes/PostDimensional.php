<?php

namespace Captura\Model\Classes;


use Captura\Model\Entity\DimBurstTest;

class PostDimensional
{
	
	protected $postArray;
	protected $insertArray;
	protected $dimensional_id;
	protected $table_id;
	protected $objBurstTest;

	public function __construct($post,$dim_id=false,$id=false)
	{
		$this->setPost($post);
		$this->setDimensional($dim_id);
		$this->setTableId($id);
		$this->objBurstTest = false;
	}

	public function setPost($post)
	{
		if(empty($post))
		{
			throw new Exception('No Post Values.');
		}

		$this->postArray = $post;
		$this->insertArray = false;
	}

	public function setObjBurst($obj)
	{
		$this->objBurstTest = $obj;
	}

	public function setTableId($id)
	{

		if($id==false)
		{
			$this->table_id = 0;
		}else{
			$this->table_id = $id;
		}

	}

	public function setDimensional($dim_id)
	{
		if(empty($dim_id))
		{
			throw new Exception('No Post Values.');
		}

		$this->dimensional_id = $dim_id;

	}


	protected function splitDimBurst()
	{
		for($i=1; $i<=8; $i++)
		{
			$cavidad = "cavidad{$i}";
			$medida = "medida{$i}";

			if($this->objBurstTest)
			{
				$this->getId($i);
			}


			if(!empty($this->postArray[$cavidad]) || !empty($this->postArray[$medida]) )
			{
				$this->setInsertArray($this->setEntityBurstTest($this->postArray[$cavidad],$this->postArray[$medida],$i));
			}

			if(empty($this->postArray[$cavidad]) && empty($this->postArray[$medida]) & $this->table_id!=0)
			{
				echo "BORRAR";
				$this->objBurstTest->eliminarById($this->table_id);
			}

		}
		//return $this->insertArray;
	}


	protected function getId($cont_row)
	{
		 $result =$this->objBurstTest->getIdFromDim($this->dimensional_id,$cont_row);
		 $arrayRow = $result->current();
		 if(!empty($arrayRow))
		 {
		 	$this->setTableId($arrayRow['id']);
		 }else{
		 	$this->setTableId(false);
		 }

	}




	protected function setEntityBurstTest($cavidad,$medida,$cont)
	{
		$tempBurst = new DimBurstTest();
		$tempBurst->exchangeArray(array(
				'id'	=> $this->table_id,
				'dimensional_id' => $this->dimensional_id,
				'order_number'	=> $cont,
				'cavidad'	=> $cavidad,
				'medida'	=> $medida,
 			));
		return $tempBurst;
	}


	protected function setInsertArray(DimBurstTest $obj)
	{
		$this->insertArray[] = $obj;
	}

	
	public function getInsertArray()
	{
		if(!$this->insertArray)
		{
			$this->splitDimBurst();
		}

		return $this->insertArray;
	}



}
