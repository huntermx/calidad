<?php

namespace Captura\Model\Entity;

class InspVisualDetalle
{
    public $id;
    public $visual_id;
    public $defecto_id;    
    public $cantidad;
    public $comentarios;
   

    public function exchangeArray($data)
    {   

        $this->id                   = (isset($data['id'])) ? $data['id'] : 0;
        $this->visual_id            = (isset($data['visual_id'])) ? $data['visual_id'] : null;
        $this->defecto_id           = (isset($data['defecto_id'])) ? $data['defecto_id'] : null;
        $this->cantidad             = (isset($data['cantidad'])) ? $data['cantidad'] : null;
        $this->comentarios          = (isset($data['comentarios'])) ? $data['comentarios'] : null;
    }

    public function getPropertyArray()
    {
        return array(
                    'id'                => $this->id,
                    'visual_id'         => $this->visual_id,
                    'defecto_id'        => $this->defecto_id,
                    'cantidad'          => $this->cantidad,
                    'comentarios'       => $this->comentarios,
            );
    }

}