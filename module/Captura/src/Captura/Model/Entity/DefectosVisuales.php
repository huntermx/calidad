<?php

namespace Captura\Model\Entity;

class DefectosVisuales
{
    public $id;
    public $codigo;
    public $descripcion;


    public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? $data['id'] : 0;
        $this->codigo                   = (isset($data['codigo'])) ? $data['codigo'] : null;
        $this->descripcion        = (isset($data['descripcion'])) ? $data['descripcion'] : null;

    }
}