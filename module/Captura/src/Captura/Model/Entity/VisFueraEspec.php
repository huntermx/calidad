<?php

namespace Captura\Model\Entity;

class VisFueraEspec
{
    public $id;
    public $visual_id;
    public $cantidad;    
    public $cavidad;
    public $dimension;
    public $zona_dibujo;
   

    public function exchangeArray($data)
    {   

        $this->id                        = (isset($data['id'])) ? $data['id'] : 0;
        $this->visual_id                 = (isset($data['visual_id'])) ? $data['visual_id'] : null;
        $this->cantidad                  = (isset($data['cantidad'])) ? $data['cantidad'] : null;
        $this->cavidad                   = (isset($data['cavidad'])) ? $data['cavidad'] : null;
        $this->dimension                 = (isset($data['dimension'])) ? $data['dimension'] : null;
        $this->zona_dibujo               = (isset($data['zona_dibujo'])) ? $data['zona_dibujo'] : null;
    }

    public function getPropertyArray()
    {
        return array(
                    'id'                        => $this->id,
                    'visual_id'                 => $this->visual_id,
                    'cantidad'                  => $this->cantidad,
                    'cavidad'                   => $this->cavidad,
                    'dimension'                 => $this->dimension,
                    'zona_dibujo'               => $this->zona_dibujo,                                        
            );
    }
}