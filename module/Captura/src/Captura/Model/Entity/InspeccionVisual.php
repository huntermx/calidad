<?php

namespace Captura\Model\Entity;

class InspeccionVisual
{
    public $id;
    public $registro_id;
    public $cantidad_inspeccionada;    
    public $cantidad_liberada;
    public $total_defectos;
    public $comentarios;

    public function exchangeArray($data)
    {   
        $this->id                        = (isset($data['id'])) ? $data['id'] : 0;
        $this->registro_id               = (isset($data['registro_id'])) ? $data['registro_id'] : null;
        $this->cantidad_inspeccionada    = (isset($data['cantidad_inspeccionada'])) ? $data['cantidad_inspeccionada'] : null;
        $this->cantidad_liberada         = (isset($data['cantidad_liberada'])) ? $data['cantidad_liberada'] : null;
        $this->cantidad_rechazada        = (isset($data['cantidad_rechazada'])) ? $data['cantidad_rechazada'] : null;
        $this->total_defectos            = (isset($data['total_defectos'])) ? $data['total_defectos'] : null;
        $this->comentarios               = (isset($data['comentarios'])) ? $data['comentarios'] : null;
    }

    public function getPropertyArray()
    {
        return array(
                    'id'              => $this->id,
                    'registro_id'     => $this->registro_id,
                    'cantidad_inspeccionada' => $this->cantidad_inspeccionada,
                    'cantidad_liberada'  => $this->cantidad_liberada,
                    'cantidad_rechazada'  => $this->cantidad_rechazada,
                    'total_defectos'  => $this->total_defectos,
                    'comentarios'  => $this->comentarios,
            );
    }
}