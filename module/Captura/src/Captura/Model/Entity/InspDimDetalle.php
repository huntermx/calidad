<?php

namespace Captura\Model\Entity;

class InspDimDetalle
{
    public $id;
    public $dimensional_id;
    public $pconceptos_id;    
    public $tamano_muestra;
    public $rechazado;
    public $comentarios;
   

    public function exchangeArray($data)
    {   

        $this->id                        = (isset($data['id'])) ? $data['id'] : 0;
        $this->dimensional_id            = (isset($data['dimensional_id'])) ? $data['dimensional_id'] : null;
        $this->pconceptos_id             = (isset($data['pconceptos_id'])) ? $data['pconceptos_id'] : null;
        $this->tamano_muestra            = (isset($data['tamano_muestra'])) ? $data['tamano_muestra'] : null;
        $this->rechazado                 = (isset($data['rechazado'])) ? $data['rechazado'] : null;
        $this->comentarios               = (isset($data['comentarios'])) ? $data['comentarios'] : null;
    }

    public function getPropertyArray()
    {
        return array(
                    'id'                        => $this->id,
                    'dimensional_id'            => $this->dimensional_id,
                    'pconceptos_id'             => $this->pconceptos_id,
                    'tamano_muestra'            => $this->tamano_muestra,
                    'rechazado'                 => $this->rechazado,
                    'comentarios'               => $this->comentarios,                                        
            );
    }
}