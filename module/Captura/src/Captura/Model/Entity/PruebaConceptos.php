<?php

namespace Captura\Model\Entity;

class PruebaConceptos
{
    public $id;
    public $prueba;
    public $form_name_muestra;
    public $form_name_rechazado;
    public $nstatus;

    public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? $data['id'] : 0;
        $this->prueba                   = (isset($data['prueba'])) ? $data['prueba'] : null;
        $this->form_name_muestra        = (isset($data['form_name_muestra'])) ? $data['form_name_muestra'] : null;
        $this->form_name_rechazado      = (isset($data['form_name_rechazado'])) ? $data['form_name_rechazado'] : null;
        $this->nstatus                  = (isset($data['nstatus'])) ? $data['nstatus'] : null;
        
    }
}