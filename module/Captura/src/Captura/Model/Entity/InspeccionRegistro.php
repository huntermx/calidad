<?php

namespace Captura\Model\Entity;

class InspeccionRegistro
{
    public $id;
    public $timestamp;
    public $recorrido_id;    
    public $fecha_captura;
    public $turno;
    public $numero_recorrido;
    public $capturista;
    public $inspector_dimensional;
    public $inspector_visual;
    public $maquina_id;
    public $moldes_id;
    public $partes_id;
    public $cambio_turno;

    public function exchangeArray($data)
    {   

        $this->id                       = (isset($data['id'])) ? $data['id'] : 0;
        $this->timestamp                = (isset($data['timestamp'])) ? $data['timestamp'] : null;
        $this->recorrido_id             = (isset($data['recorrido_id'])) ? $data['recorrido_id'] : null;
        $this->fecha_captura            = (isset($data['fecha_captura'])) ? $data['fecha_captura'] : null;
        $this->turno                    = (isset($data['turno'])) ? $data['turno'] : null;
        $this->numero_recorrido         = (isset($data['numero_recorrido'])) ? $data['numero_recorrido'] : null;
        $this->capturista               = (isset($data['capturista'])) ? $data['capturista'] : null;
        $this->inspector_dimensional    = (isset($data['inspector_dimensional'])) ? $data['inspector_dimensional'] : null;
        $this->inspector_visual         = (isset($data['inspector_visual'])) ? $data['inspector_visual'] : null;
        $this->maquina_id               = (isset($data['maquina_id'])) ? $data['maquina_id'] : null;
        $this->moldes_id                = (isset($data['moldes_id'])) ? $data['moldes_id'] : null;
        $this->partes_id                = (isset($data['partes_id'])) ? $data['partes_id'] : null;
        $this->cambio_turno             = (isset($data['cambio_turno'])) ? $data['cambio_turno'] : null;        
    }
}