<?php

namespace Captura\Model\Entity;

class Maquinasdetalle
{
    public $id;
    public $timestamp;
    public $maquinas_activas_id;
    public $maquina_id;


    public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? $data['id'] : 0;
        $this->timestamp                = (isset($data['timestamp'])) ? $data['timestamp'] : null;
        $this->maquinas_activas_id      = (isset($data['maquinas_activas_id'])) ? $data['maquinas_activas_id'] : null;
        $this->maquina_id               = (isset($data['maquina_id'])) ? $data['maquina_id'] : null;
    }
}