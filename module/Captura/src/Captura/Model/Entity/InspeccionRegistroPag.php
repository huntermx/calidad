<?php

namespace Captura\Model\Entity;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InspeccionRegistroPag implements InputFilterAwareInterface
{
    public $id;
    public $timestamp;
    public $recorrido_id;
    public $fecha_captura;
    public $turno;
    public $numero_recorrido;
    public $capturista;
    public $inspector_dimensional;
    public $inspector_visual;
    public $maquina_id;
    public $moldes_id;
    public $partes_id;

    protected $inputFilter;

    /**
     * Used by ResultSet to pass each database row to the entity
     */
    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->timestamp = (isset($data['timestamp'])) ? $data['timestamp'] : null;
        $this->recorrido_id  = (isset($data['recorrido_id'])) ? $data['recorrido_id'] : null;
        $this->fecha_captura     = (isset($data['fecha_captura'])) ? $data['fecha_captura'] : null;
        $this->turno = (isset($data['turno'])) ? $data['turno'] : null;
        $this->numero_recorrido  = (isset($data['numero_recorrido'])) ? $data['numero_recorrido'] : null;
        $this->capturista     = (isset($data['capturista'])) ? $data['capturista'] : null;
        $this->inspector_dimensional = (isset($data['inspector_dimensional'])) ? $data['inspector_dimensional'] : null;
        $this->inspector_visual  = (isset($data['inspector_visual'])) ? $data['inspector_visual'] : null;
        $this->maquina_id     = (isset($data['maquina_id'])) ? $data['maquina_id'] : null;
        $this->moldes_id = (isset($data['moldes_id'])) ? $data['moldes_id'] : null;
        $this->partes_id  = (isset($data['partes_id'])) ? $data['partes_id'] : null;



    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'artist',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
}