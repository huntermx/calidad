<?php

namespace Captura\Model\Entity;

class DimFueraEspec
{
    public $id;
    public $dimensional_id;
    public $cantidad;    
    public $cavidad;
    public $dimension;
    public $zona_dibujo;
   

    public function exchangeArray($data)
    {   

        $this->id                        = (isset($data['id'])) ? $data['id'] : 0;
        $this->dimensional_id            = (isset($data['dimensional_id'])) ? $data['dimensional_id'] : null;
        $this->cantidad                  = (isset($data['cantidad'])) ? $data['cantidad'] : null;
        $this->cavidad                   = (isset($data['cavidad'])) ? $data['cavidad'] : null;
        $this->dimension                 = (isset($data['dimension'])) ? $data['dimension'] : null;
        $this->zona_dibujo               = (isset($data['zona_dibujo'])) ? $data['zona_dibujo'] : null;
    }

    public function getPropertyArray()
    {
        return array(
                    'id'                        => $this->id,
                    'dimensional_id'            => $this->dimensional_id,
                    'cantidad'                  => $this->cantidad,
                    'cavidad'                   => $this->cavidad,
                    'dimension'                 => $this->dimension,
                    'zona_dibujo'               => $this->zona_dibujo,                                        
            );
    }
}