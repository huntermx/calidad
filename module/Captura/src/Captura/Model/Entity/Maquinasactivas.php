<?php

namespace Captura\Model\Entity;

class Maquinasactivas
{
    public $id;
    public $timestamp;
    public $fecha;
    public $turno;
    public $comentarios;
  


    public function exchangeArray($data)
    {   
        $this->id           = (isset($data['id'])) ? $data['id'] : 0;
        $this->timestamp    = (isset($data['timestamp'])) ? $data['timestamp'] : null;
        $this->fecha        = (isset($data['fecha'])) ? $data['fecha'] : null;
        $this->turno        = (isset($data['turno'])) ? $data['turno'] : null;
        $this->comentarios  = (isset($data['comentarios'])) ? $data['comentarios'] : null;
    }
}