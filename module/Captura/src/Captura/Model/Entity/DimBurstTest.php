<?php

namespace Captura\Model\Entity;

class DimBurstTest
{
    public $id;
    public $dimensional_id;
    public $order_number;    
    public $cavidad;
    public $medida;
   

    public function exchangeArray($data)
    {   
        $this->id                       = (isset($data['id'])) ? $data['id'] : 0;
        $this->dimensional_id           = (isset($data['dimensional_id'])) ? $data['dimensional_id'] : null;
        $this->order_number             = (isset($data['order_number'])) ? $data['order_number'] : null;
        $this->cavidad                  = (isset($data['cavidad'])) ? $data['cavidad'] : null;
        $this->medida                   = (isset($data['medida'])) ? $data['medida'] : null;
    }
}