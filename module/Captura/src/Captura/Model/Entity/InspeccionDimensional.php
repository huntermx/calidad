<?php

namespace Captura\Model\Entity;

class InspeccionDimensional
{
    public $id;
    public $registro_id;
    public $cantidad_inspeccion;    
    public $fuera_especificacion;
    public $cantidad_rechazada;
    public $prueba_funcional;
    public $primera_corrida;
    public $hora_recibido;
    public $hora_calificacion;
    public $tip_sheet;
    public $resultado;
    public $comentarios;

    public function exchangeArray($data)
    {   

        $this->id                        = (isset($data['id'])) ? $data['id'] : 0;
        $this->registro_id               = (isset($data['registro_id'])) ? $data['registro_id'] : null;
        $this->cantidad_inspeccion       = (isset($data['cantidad_inspeccion'])) ? $data['cantidad_inspeccion'] : null;
        $this->fuera_especificacion      = (isset($data['fuera_especificacion'])) ? $data['fuera_especificacion'] : null;
        $this->cantidad_rechazada        = (isset($data['cantidad_rechazada'])) ? $data['cantidad_rechazada'] : null;
        $this->prueba_funcional          = (isset($data['prueba_funcional'])) ? $data['prueba_funcional'] : null;
        $this->primera_corrida           = (isset($data['primera_corrida'])) ? $data['primera_corrida'] : null;
        $this->hora_recibido             = (isset($data['hora_recibido'])) ? $data['hora_recibido'] : null;
        $this->hora_calificacion         = (isset($data['hora_calificacion'])) ? $data['hora_calificacion'] : null;
        $this->tip_sheet                 = (isset($data['tip_sheet'])) ? $data['tip_sheet'] : null;
        $this->resultado                 = (isset($data['resultado'])) ? $data['resultado'] : null;
        $this->comentarios               = (isset($data['comentarios'])) ? $data['comentarios'] : null;
    }

    public function getPropertyArray()
    {
        return array(
                    'id'                        => $this->id,
                    'registro_id'               => $this->registro_id,
                    'cantidad_inspeccion'       => $this->cantidad_inspeccion,
                    'fuera_especificacion'      => $this->fuera_especificacion,
                    'cantidad_rechazada'        => $this->cantidad_rechazada,
                    'prueba_funcional'          => $this->prueba_funcional,
                    'primera_corrida'           => $this->primera_corrida,
                    'hora_recibido'             => $this->hora_recibido,
                    'hora_calificacion'         => $this->hora_calificacion,
                    'tip_sheet'                 => $this->tip_sheet,
                    'resultado'                 => $this->resultado,
                    'comentarios'               => $this->comentarios,                                        
            );
    }
}