<?php

namespace Captura\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormInspeccion
{
	/**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $id;
	
    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Attributes({"id":"fechacaptura"})
     * @Annotation\Required({"required":"true"})
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"form-control"})
     * 
     */
    public $fecha_captura_turno;
    
    
    
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"form-control"})
     * @Annotation\Attributes({"placeholder":"Numero Recorrido"})
     */
    public $numero_recorrido;

   /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"txtSearch2"})  
     * @Annotation\Attributes({"class":"textbox"})   
     * @Annotation\Attributes({"autocomplete":"off"})   
     * @Annotation\Attributes({"onkeyup":"searchSuggest2('/async/getusuariobyname');"})  
     * @Annotation\Attributes({"alt":"Search Criteria"})   
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $capturista;

       /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"txtSearch"})  
     * @Annotation\Attributes({"class":"textbox"})   
     * @Annotation\Attributes({"autocomplete":"off"})   
     * @Annotation\Attributes({"alt":"Search Criteria"})   
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $inspector_dimensional;

        /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"txtSearch3"})  
     * @Annotation\Attributes({"class":"textbox"})   
     * @Annotation\Attributes({"autocomplete":"off"})   
     * @Annotation\Attributes({"onkeyup":"searchSuggest('/async/getusuariobyname');"})  
     * @Annotation\Attributes({"alt":"Search Criteria"})   
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $inspector_visual;

    /**
     * @Annotation\Type("Zend\Form\Element\Checkbox")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"options":{"checked_value":"1",
     * "unchecked_value":"0"}})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $cambio_turno;


    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"maquinas"})
     * @Annotation\Attributes({"placeholder":"Capturista"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $maquina_id;

    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"moldes"}) 
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $moldes_id;

    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"partes"})
     * @Annotation\Attributes({"placeholder":"Capturista"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $partes_id;


    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
      * @Annotation\Attributes({"id":"submit"})
     * @Annotation\Attributes({"value":"Submit"})
     */
    public $submit;
}