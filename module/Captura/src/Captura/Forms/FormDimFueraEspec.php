<?php

namespace Captura\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormDimFueraEspec
{
	/**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $id;

     /**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $dimensional_id;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Cantidad"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $cantidad;

     /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Cavidad"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $cavidad;

     /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Dimension"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $dimension;

    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Zona Dibujo"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $zona_dibujo;


      /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"value":"Guardar Agregar Otro"})
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Required(false)
     */
    public $submit;
    
    
     /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"value":"Guardar Finalizar"})
     * @Annotation\Attributes({"class":"btn btn-primary"})
      * @Annotation\Required(false)
     */
    public $submit2;

}