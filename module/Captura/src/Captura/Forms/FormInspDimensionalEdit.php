<?php

namespace Captura\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormInspDimensionalEdit
{
	/**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $id;
	
   /**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $registro_id;
    
     /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Cantidad Inspeccionada"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $cantidad_inspeccion;

     /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Cantidad fuera especificacion"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $fuera_especificacion;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Cantidad Rechazada"})
     * @Annotation\Attributes({"class":"form-control"})
     */
     public $cantidad_rechazada;


        /**
     * @Annotation\Type("Zend\Form\Element\Checkbox")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"checkMeOut"})  
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $primera_corrida;

/**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"span2"})
     * @Annotation\Attributes({"id":"horas"})  
     * @Annotation\Attributes({"options":{"0":"00",
     * "1":"01","2":"02","3":"03","4":"04","5":"05","6":"06","7":"07",
     * "8":"08","9":"09","10":"10","11":"11","12":"12","13":"13","14":"14","15":"15",
     * "16":"16","17":"17","18":"18","19":"19","20":"20","21":"21","22":"22","23":"23"}})
     */
    public $horas;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"span2"})
     * @Annotation\Attributes({"id":"minutos"})  
     * @Annotation\Attributes({"options":{"0":"00",
     * "1":"01","2":"02","3":"03","4":"04","5":"05","6":"06","7":"07",
     * "8":"08","9":"09","10":"10","11":"11","12":"12","13":"13","14":"14","15":"15",
     * "16":"16","17":"17","18":"18","19":"19","20":"20","21":"21","22":"22","23":"23",
     * "24":"24","25":"25","26":"26","27":"27","28":"28","29":"29","30":"30","31":"31",
     * "32":"32","33":"33","34":"34","35":"35","36":"36","37":"37","38":"38","39":"39",
     * "40":"40","41":"41","42":"42","43":"43","44":"44","45":"45","46":"46","47":"47",
     * "48":"48","49":"49","50":"50","51":"51","52":"52","53":"53","54":"54","55":"55",
     * "56":"56","57":"57","58":"58","59":"59"}})
     */
    public $minutos;
 
    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"span2"})
     * @Annotation\Attributes({"id":"segundos"})    
     * @Annotation\Attributes({"options":{"0":"00",
     * "1":"01","2":"02","3":"03","4":"04","5":"05","6":"06","7":"07",
     * "8":"08","9":"09","10":"10","11":"11","12":"12","13":"13","14":"14","15":"15",
     * "16":"16","17":"17","18":"18","19":"19","20":"20","21":"21","22":"22","23":"23",
     * "24":"24","25":"25","26":"26","27":"27","28":"28","29":"29","30":"30","31":"31",
     * "32":"32","33":"33","34":"34","35":"35","36":"36","37":"37","38":"38","39":"39",
     * "40":"40","41":"41","42":"42","43":"43","44":"44","45":"45","46":"46","47":"47",
     * "48":"48","49":"49","50":"50","51":"51","52":"52","53":"53","54":"54","55":"55",
     * "56":"56","57":"57","58":"58","59":"59"}})
     */
    public $segundos;

/**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"span2"})
     * @Annotation\Attributes({"id":"horas_cal"})  
     * @Annotation\Attributes({"options":{"0":"00",
     * "1":"01","2":"02","3":"03","4":"04","5":"05","6":"06","7":"07",
     * "8":"08","9":"09","10":"10","11":"11","12":"12","13":"13","14":"14","15":"15",
     * "16":"16","17":"17","18":"18","19":"19","20":"20","21":"21","22":"22","23":"23"}})
     */
    public $horas_cal;
    
    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"span2"})
     * @Annotation\Attributes({"id":"minutos_cal"})  
     * @Annotation\Attributes({"options":{"0":"00",
     * "1":"01","2":"02","3":"03","4":"04","5":"05","6":"06","7":"07",
     * "8":"08","9":"09","10":"10","11":"11","12":"12","13":"13","14":"14","15":"15",
     * "16":"16","17":"17","18":"18","19":"19","20":"20","21":"21","22":"22","23":"23",
     * "24":"24","25":"25","26":"26","27":"27","28":"28","29":"29","30":"30","31":"31",
     * "32":"32","33":"33","34":"34","35":"35","36":"36","37":"37","38":"38","39":"39",
     * "40":"40","41":"41","42":"42","43":"43","44":"44","45":"45","46":"46","47":"47",
     * "48":"48","49":"49","50":"50","51":"51","52":"52","53":"53","54":"54","55":"55",
     * "56":"56","57":"57","58":"58","59":"59"}})
     */
    public $minutos_cal;
 
    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"span2"})
     * @Annotation\Attributes({"id":"segundos_cal"})    
     * @Annotation\Attributes({"options":{"0":"00",
     * "1":"01","2":"02","3":"03","4":"04","5":"05","6":"06","7":"07",
     * "8":"08","9":"09","10":"10","11":"11","12":"12","13":"13","14":"14","15":"15",
     * "16":"16","17":"17","18":"18","19":"19","20":"20","21":"21","22":"22","23":"23",
     * "24":"24","25":"25","26":"26","27":"27","28":"28","29":"29","30":"30","31":"31",
     * "32":"32","33":"33","34":"34","35":"35","36":"36","37":"37","38":"38","39":"39",
     * "40":"40","41":"41","42":"42","43":"43","44":"44","45":"45","46":"46","47":"47",
     * "48":"48","49":"49","50":"50","51":"51","52":"52","53":"53","54":"54","55":"55",
     * "56":"56","57":"57","58":"58","59":"59"}})
     */
    public $segundos_cal;

     /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"id":"tip_sheet"})    
     * @Annotation\Attributes({"placeholder":"Tip Sheet"})
         * @Annotation\Attributes({"class":"form-control"})
     */
    public $tip_sheet;

    /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"resultado"})    
     * @Annotation\Attributes({"options":{"Mal":"Mal",
     * "Bien":"Bien","No Aplica":"No Aplica"}})
         * @Annotation\Attributes({"class":"form-control"})
     */
    public $resultado;


     /**
     * @Annotation\Type("Zend\Form\Element\Checkbox")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"checkMeOut2"})  
         * @Annotation\Attributes({"class":"form-control"})
     */
     public $prueba_funcional;


       /**
     * @Annotation\Type("Zend\Form\Element\Textarea")
     * @Annotation\Required(false)
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Comentarios"})
         * @Annotation\Attributes({"class":"form-control"})
     */
    public $comentarios;

    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Submit"})
     */
    public $submit;
}