<?php

namespace Captura\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormMaquinasActivas
{
	/**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $id;
	
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"recorrido"})
     * @Annotation\Attributes({"readonly":"readonly"})
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"form-control"})
     * 
     */
    public $maquinas_activas_id;
    
     /**
     * @Annotation\Type("Zend\Form\Element\Select")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"maquina_id"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $maquina_id;
    

    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Submit"})
     */
    public $submit;
}