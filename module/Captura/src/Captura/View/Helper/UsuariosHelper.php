<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class UsuariosHelper extends AbstractHelper
{

	private $usuariosTable;

	function __construct($usuariosTable)
	{
		$this->usuariosTable= $usuariosTable;
	}


	public function __invoke($id)
	{
		if(empty($id))
		{
			return "N/D";
		}
		$usuario = $this->usuariosTable->getUsuario($id);

		$nombre = $usuario->nombre .", " . $usuario->apellidos;
		return $nombre;

	}
	
}