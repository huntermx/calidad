<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class PartesNombreHelper extends AbstractHelper
{

	private $parteTable;

	function __construct($parteTable)
	{
		$this->parteTable= $parteTable;
	}


	public function __invoke($id)
	{
		
		$parte = $this->parteTable->getRegistro($id);

		

		return $parte->num_parte;
		//return $familia->familia;
	}
	
}