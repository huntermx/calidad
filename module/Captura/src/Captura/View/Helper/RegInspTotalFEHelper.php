<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class RegInspTotalFEHelper extends AbstractHelper
{

	private $inspDimensional;
	private $inspVisual;

	function __construct($inspDimensional,$inspVisual)
	{
		$this->inspDimensional= $inspDimensional;
		$this->inspVisual = $inspVisual;
	}


	public function __invoke($id)
	{
		
		$inspeccionDim = $this->inspDimensional->getRowByRegistroId($id);
		$inspeccionVis = $this->inspVisual->getRowByRegistroId($id);

		$total = $inspeccionDim->cantidad_rechazada + $inspeccionVis->cantidad_rechazada;

		if($id==0)
		{
			return false;
		}



		return $total;
		//return $familia->familia;
	}
	
}