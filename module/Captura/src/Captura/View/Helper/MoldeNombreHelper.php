<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class MoldeNombreHelper extends AbstractHelper
{

	private $moldeTable;

	function __construct($moldeTable)
	{
		$this->moldeTable= $moldeTable;
	}


	public function __invoke($id)
	{
		
		$molde = $this->moldeTable->getRegistro($id);

		

		return $molde->molde;
		//return $familia->familia;
	}
	
}