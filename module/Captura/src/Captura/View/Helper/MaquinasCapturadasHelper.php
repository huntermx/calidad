<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class MaquinasCapturadasHelper extends AbstractHelper
{

	private $maquinasCapturadas;

	function __construct($maquinas_captura)
	{
		$this->maquinasCapturadas= $maquinas_captura;
	}


	public function __invoke($id)
	{
		
		$maquinasDetalle = $this->maquinasCapturadas->getMaquinasNumberByRecorrido($id);

		if(empty($maquinasDetalle))
		{
			return 0;
		}

		return $maquinasDetalle;
		//return $familia->familia;
	}
	
}