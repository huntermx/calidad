<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class SumFueraEspecDimHelper extends AbstractHelper
{

	private $inspDim;

	function __construct($inspVisual)
	{
		$this->inspDim= $inspVisual;
	}


	public function __invoke($id)
	{
		$inspeccion = $this->inspDim->getSumFueraEspec($id);
		$resultArray=$inspeccion->current();

		if(empty($resultArray['fuera_especificacion']))
		{
			return 0;
		}


		return $resultArray['fuera_especificacion'];
		//return $familia->familia;
	}
	
}