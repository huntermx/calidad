<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class SumDefectosVisualesHelper extends AbstractHelper
{

	private $inspVisual;

	function __construct($inspVisual)
	{
		$this->inspVisual= $inspVisual;
	}


	public function __invoke($id)
	{
		$inspeccion = $this->inspVisual->getSumDefectos($id);
		$resultArray=$inspeccion->current();

		if(empty($resultArray['cantidad']))
		{
			return 0;
		}


		return $resultArray['cantidad'];
		//return $familia->familia;
	}
	
}