<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class InspVisualHelper extends AbstractHelper
{

	private $inspVisual;

	function __construct($inspVisual)
	{
		$this->inspVisual= $inspVisual;
	}


	public function __invoke($id)
	{
		
		$inspeccion = $this->inspVisual->getRowByRegistroId($id);

		if($inspeccion->id==0)
		{
			return false;
		}



		return true;
		//return $familia->familia;
	}
	
}