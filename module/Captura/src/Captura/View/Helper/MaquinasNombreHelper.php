<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class MaquinasNombreHelper extends AbstractHelper
{

	private $maquinasTable;

	function __construct($maquinasTable)
	{
		$this->maquinasTable= $maquinasTable;
	}


	public function __invoke($id)
	{
		
		$maquinasDetalle = $this->maquinasTable->getRegistro($id);

		

		return $maquinasDetalle->maquina;
		//return $familia->familia;
	}
	
}