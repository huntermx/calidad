<?php
namespace Captura\View\Helper;
use Zend\View\Helper\AbstractHelper;

class InspDimensionalHelper extends AbstractHelper
{

	private $inspDimensional;

	function __construct($inspDimensional)
	{
		$this->inspDimensional= $inspDimensional;
	}


	public function __invoke($id)
	{
		
		$inspeccion = $this->inspDimensional->getRowByRegistroId($id);

		if($inspeccion->id==0)
		{
			return false;
		}



		return true;
		//return $familia->familia;
	}
	
}