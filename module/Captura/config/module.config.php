<?php
return array(



    
    'controllers' => array(
        'invokables' => array(
            'Captura\Controller\RegistroInspeccion'         => 'Captura\Controller\RegistroInspeccionController',
            'Captura\Controller\Maquinasactivas'            => 'Captura\Controller\MaquinasactivasController',
            'Captura\Controller\Async'                      => 'Captura\Controller\AsyncController',
            'Captura\Controller\InspDimensional'            => 'Captura\Controller\InspDimensionalController',
            'Captura\Controller\InspVisual'                 => 'Captura\Controller\InspVisualController',
            
        ),
    ),





    'router' => array(
        'routes' => array(
       

        'paginator' => array(
                'type'    => 'Segment',
                'options' => array(
                'route'    => '/:controller/[page/:page]',
                              'constraints' => array(
                              'page' => '[0-9]*',
                              ),
                'defaults' => array(
                             '__NAMESPACE__' => 'Captura\Controller',
                             'controller'    => 'RegistroInspeccion',
                             'action'        => 'index',
                                    ),
                             ),
         ),


 'paginator2' => array(
                'type'    => 'segment',
               'options' => array(
                    'route'    => '/paginator2[/:action][/:id][/page/:page][/order_by/:order_by][/:order]',
                    //'route'    => '/paginator2[/:action][/:id][/order_by/:order_by][/:order]',
                    'constraints' => array(
                        'action' => '(?!\bpage\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                         'page' => '[0-9]+',
                        'order_by' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'order' => 'ASC|DESC',
                    ), 
                    'defaults' => array(
                             'controller' => 'Captura\Controller\RegistroInspeccion',
                            // 'controller'    => 'RegistroInspeccion',
                             'action'        => 'index',
                                    ),
                             ),
         ),


        'reginspeccion' => array(
                'type'    => 'segment',
                'options' => array(
                'route'    => '[/:lang]/reginspeccion[/:action][/:id]',
                'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                    'controller' => 'Captura\Controller\RegistroInspeccion',
                    'action'     => 'index',
                    ),
        ),
        ),   		

        'inspdimensional' => array(
                'type'    => 'segment',
                'options' => array(
                'route'    => '[/:lang]/inspdimensional[/:action][/:id]',
                'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                    'controller' => 'Captura\Controller\InspDimensional',
                    'action'     => 'index',
                    ),
        ),
        ),          

        'inspvisual' => array(
                'type'    => 'segment',
                'options' => array(
                'route'    => '[/:lang]/inspvisual[/:action][/:id]',
                'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                    'controller' => 'Captura\Controller\InspVisual',
                    'action'     => 'index',
                    ),
        ),
        ),  

        'reginspeccionvedit' => array(
                'type'    => 'segment',
                'options' => array(
                'route'    => '[/:lang]/reginspeccionvedit[/:action][/:id_insp]',
                'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id_insp'     => '[0-9]+',
                    ),
                    'defaults' => array(
                    'controller' => 'Captura\Controller\RegistroInspeccion',
                    'action'     => 'index',
                    ),
        ),
        ),          



        'reginspeccionedit' => array(
                'type'    => 'segment',
                'options' => array(
                'route'    => '[/:lang]/reginspeccionedit[/:action][/:id_insp]',
                'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id_insp'     => '[0-9]+',
                    ),
                    'defaults' => array(
                    'controller' => 'Captura\Controller\RegistroInspeccion',
                    'action'     => 'index',
                    ),
        ),
        ),          


        'maquinasactivas' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/maquinasactivas[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'Captura\Controller\Maquinasactivas',
                'action'     => 'index',
                ),
        ),
        ),  

 'async' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/async[/:action][/:id]',
         'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                ),
                'defaults' => array(
                'controller' => 'Captura\Controller\Async',
                'action'     => 'index',
                ),
        ),
        ),

              
 
        		
       
        		
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Captura' => __DIR__ . '/../view',
        ),
    	
    		
    ),

       

	
	
);
