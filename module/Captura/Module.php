<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Captura;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\EventManager\StaticEventManager;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\MvcEvent;


use Captura\Model\Dao\InspRegPaginatorTable;
use Captura\Model\Dao\MaquinasactivasTable;
use Captura\Model\Dao\MaquinasdetalleTable;
use Captura\Model\Dao\InspeccionRegistroTable;
use Captura\Model\Dao\PruebaConceptosTable;
use Captura\Model\Dao\InspDimTable;
use Captura\Model\Dao\InspDimDetalleTable;
use Captura\Model\Dao\DefectosVisualesTable;
use Captura\Model\Dao\InspVisualTable;
use Captura\Model\Dao\InspVisualDetalleTable;
use Captura\Model\Dao\DimFueraEspecTable;
use Captura\Model\Dao\VisFueraEspecTable;
use Captura\Model\Dao\DimBurstTestTable;

use Captura\Model\Entity\DimBurstTest;
use Captura\Model\Entity\VisFueraEspec;
use Captura\Model\Entity\DimFueraEspec;
use Captura\Model\Entity\InspVisualDetalle;
use Captura\Model\Entity\InspeccionVisual;
use Captura\Model\Entity\DefectosVisuales;
use Captura\Model\Entity\InspDimDetalle;
use Captura\Model\Entity\InspeccionDimensional;
use Captura\Model\Entity\PruebaConceptos;
use Captura\Model\Entity\InspeccionRegistro;
use Captura\Model\Entity\Maquinasactivas;
use Captura\Model\Entity\Maquinasdetalle;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
   
    public function onBootstrap($e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
    } 
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    
    
     public function loadConfiguration(MvcEvent $e)
    {
    	$application   = $e->getApplication();
		$sm            = $application->getServiceManager();
		$sharedManager = $application->getEventManager()->getSharedManager();
		$sharedManager->attach('Zend\Mvc\Controller\AbstractActionController','dispatch', 
             function($e) use ($sm) {
		$sm->get('ControllerPluginManager')->get('AuthPlugin')
                   ->doAuthorization($e); //pass to the plugin...    
	    }
        );
    }
    

  public function getServiceConfig()
  {
        return array(
                'factories' => array(
                        'Captura\Model\Dao\InspRegPaginatorTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $table = new InspRegPaginatorTable($dbAdapter);
                            return $table;
                        },
                        'Captura\Model\Dao\MaquinasactivasTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('MaquinasactivasTableGateway');
                            $table = new MaquinasactivasTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'MaquinasactivasTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Maquinasactivas());
                            return new TableGateway('maquinas_activas', $dbAdapter, null, $resultSetPrototype);
                        },
                         'Captura\Model\Dao\MaquinasdetalleTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('MaquinasdetalleTableGateway');
                            $table = new MaquinasdetalleTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'MaquinasdetalleTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new Maquinasdetalle());
                            return new TableGateway('maquinas_detalle', $dbAdapter, null, $resultSetPrototype);
                        },
                        'Captura\Model\Dao\InspeccionRegistroTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspeccionRegistroTableGateway');
                            $table = new InspeccionRegistroTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspeccionRegistroTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspeccionRegistro());
                            return new TableGateway('inspeccion_registro', $dbAdapter, null, $resultSetPrototype);
                        },
                         'Captura\Model\Dao\PruebaConceptosTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('PruebaConceptosTableGateway');
                            $table = new PruebaConceptosTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'PruebaConceptosTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new PruebaConceptos());
                            return new TableGateway('prueba_conceptos', $dbAdapter, null, $resultSetPrototype);
                        },
                         'Captura\Model\Dao\InspDimTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspDimTableGateway');
                            $table = new InspDimTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspDimTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspeccionDimensional());
                            return new TableGateway('inspeccion_dimensional', $dbAdapter, null, $resultSetPrototype);
                        },
                         'Captura\Model\Dao\InspDimDetalleTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspDimDetalleTableGateway');
                            $table = new InspDimDetalleTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspDimDetalleTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspDimDetalle());
                            return new TableGateway('inspeccion_dimensional_detalle', $dbAdapter, null, $resultSetPrototype);
                        },

                         'Captura\Model\Dao\DefectosVisualesTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('DefectosVisualesTableGateway');
                            $table = new DefectosVisualesTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'DefectosVisualesTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new DefectosVisuales());
                            return new TableGateway('defectos_visuales', $dbAdapter, null, $resultSetPrototype);
                        },                        
                         'Captura\Model\Dao\InspVisualTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspVisualTableGateway');
                            $table = new InspVisualTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspVisualTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspeccionVisual());
                            return new TableGateway('inspeccion_visual', $dbAdapter, null, $resultSetPrototype);
                        },       
                        'Captura\Model\Dao\InspVisualDetalleTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('InspVisualDetalleTableGateway');
                            $table = new InspVisualDetalleTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'InspVisualDetalleTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new InspVisualDetalle());
                            return new TableGateway('inspeccion_visual_detalle', $dbAdapter, null, $resultSetPrototype);
                        },

                        'Captura\Model\Dao\DimFueraEspecTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('DimFueraEspecTableGateway');
                            $table = new DimFueraEspecTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'DimFueraEspecTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new DimFueraEspec());
                            return new TableGateway('dimensional_fuera_especificacion', $dbAdapter, null, $resultSetPrototype);
                        },

                         'Captura\Model\Dao\VisFueraEspecTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('VisFueraEspecTableGateway');
                            $table = new VisFueraEspecTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'VisFueraEspecTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new VisFueraEspec());
                            return new TableGateway('visual_fuera_especificacion', $dbAdapter, null, $resultSetPrototype);
                        },
//New
                        'Captura\Model\Dao\DimBurstTestTable' =>  function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $tableGateway = $sm->get('DimBurstTestTableGateway');
                            $table = new DimBurstTestTable($tableGateway,$dbAdapter);
                            return $table;
                        },
                         'DimBurstTestTableGateway' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $resultSetPrototype = new ResultSet();
                            $resultSetPrototype->setArrayObjectPrototype(new DimBurstTest());
                            return new TableGateway('dimensional_burst_test', $dbAdapter, null, $resultSetPrototype);
                        },

                ),
                );
  }

    
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
               'MaquinasCapturadasHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('Captura\Model\Dao\MaquinasdetalleTable');
                    return new View\Helper\MaquinasCapturadasHelper($dao);
                },   
                'MaquinasNombreHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('core\Model\Dao\MaquinasTable');
                    return new View\Helper\MaquinasNombreHelper($dao);
                },  
                'UsuariosHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('core\Model\Dao\UsuariosTable');
                    return new View\Helper\UsuariosHelper($dao);
                },  
                'MoldeNombreHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('core\Model\Dao\MoldesTable');
                    return new View\Helper\MoldeNombreHelper($dao);
                },  
                'PartesNombreHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('core\Model\Dao\PartesTable');
                    return new View\Helper\PartesNombreHelper($dao);
                },  

                'InspDimensionalHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('Captura\Model\Dao\InspDimTable');
                    return new View\Helper\InspDimensionalHelper($dao);
                },  

                'InspVisualHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('Captura\Model\Dao\InspVisualTable');
                    return new View\Helper\InspVisualHelper($dao);
                },  
                'RegInspTotalFEHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('Captura\Model\Dao\InspVisualTable');
                    $dao2 = $locator->get('Captura\Model\Dao\InspDimTable');
                    return new View\Helper\RegInspTotalFEHelper($dao,$dao2);
                },  
                 'SumDefectosVisualesHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('Captura\Model\Dao\InspVisualDetalleTable');
                    return new View\Helper\SumDefectosVisualesHelper($dao);
                },  
                  'SumFueraEspecDimHelper' => function($sm){
                    $locator = $sm->getServiceLocator();
                    $dao = $locator->get('Captura\Model\Dao\InspDimTable');
                    return new View\Helper\SumFueraEspecDimHelper($dao);
                },  

            ),
        );
    }
    
}
