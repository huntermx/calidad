/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Gets the browser specific XmlHttpRequest Object
function getXmlHttpRequestObject() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else if(window.ActiveXObject) {
		return new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		alert("Your Browser Sucks!\nIt's about time to upgrade don't you think?");
	}
}

//Our XmlHttpRequest object to get the auto suggest
var searchReq = getXmlHttpRequestObject();

//Called from keyup on the search textbox.
//Starts the AJAX request.
function searchSuggest2(path) {
	if (searchReq.readyState == 4 || searchReq.readyState == 0) {
		var str = escape(document.getElementById('txtSearch2').value);
                var segment = '/';
                if(! str){
                    segment = '';
                }
                
		searchReq.open("GET", path + segment  + str, true);
		searchReq.onreadystatechange = handleSearchSuggest2; 
		searchReq.send(null);
	}		
}


//Called when the AJAX response is returned.
function handleSearchSuggest2() {
	if (searchReq.readyState == 4) {
		var ss = document.getElementById('search2_suggest');
		ss.innerHTML = '';
		var str = searchReq.responseText.split("\n");
		for(i=0; i < str.length - 1; i++) {
			//Build our element string.  This is cleaner using the DOM, but
			//IE doesn't support dynamically added attributes.
			var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
			suggest += 'onmouseout="javascript:suggestOut(this);" ';
			suggest += 'onclick="javascript:setSearch2(this.innerHTML);" ';
			suggest += 'class="suggest_link">' + str[i] + '</div>';
			ss.innerHTML += suggest;
		}
	}
}

function setSearch2(value) {

        var srt=value.split("-"); 
	document.getElementById('txtSearch2').value = trim(srt[0]);
	document.getElementById('search2_suggest').innerHTML = '';
}

function suggestOut2(div_value) {
	div_value.className = 'suggest_link';
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Called from keyup on the search textbox.
//Starts the AJAX request.
function searchSuggest3(path) {
	if (searchReq.readyState == 4 || searchReq.readyState == 0) {
		var str = escape(document.getElementById('txtSearch3').value);
                var segment = '/';
                if(! str){
                    segment = '';
                }
                
		searchReq.open("GET", path + segment  + str, true);
		searchReq.onreadystatechange = handleSearchSuggest3; 
		searchReq.send(null);
	}		
}


//Called when the AJAX response is returned.
function handleSearchSuggest3() {
	if (searchReq.readyState == 4) {
		var ss = document.getElementById('search3_suggest')
		ss.innerHTML = '';
		var str = searchReq.responseText.split("\n");
		for(i=0; i < str.length - 1; i++) {
			//Build our element string.  This is cleaner using the DOM, but
			//IE doesn't support dynamically added attributes.
			var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
			suggest += 'onmouseout="javascript:suggestOut3(this);" ';
			suggest += 'onclick="javascript:setSearch3(this.innerHTML);" ';
			suggest += 'class="suggest_link">' + str[i] + '</div>';
			ss.innerHTML += suggest;
		}
	}
}

function setSearch3(value) {
    var srt=value.split("-"); 
	document.getElementById('txtSearch3').value = trim(srt[0]);
	document.getElementById('search3_suggest').innerHTML = '';
}

function suggestOut3(div_value) {
	div_value.className = 'suggest_link';
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

function searchSuggest(path) {
	if (searchReq.readyState == 4 || searchReq.readyState == 0) {
		var str = escape(document.getElementById('txtSearch').value);
                var segment = '/';
                if(! str){
                    segment = '';
                }
                
		searchReq.open("GET", path + segment  + str, true);
		searchReq.onreadystatechange = handleSearchSuggest; 
		searchReq.send(null);
	}		
}


//Called when the AJAX response is returned.
function handleSearchSuggest() {
	if (searchReq.readyState == 4) {
		var ss = document.getElementById('search_suggest')
		ss.innerHTML = '';
		var str = searchReq.responseText.split("\n");
		for(i=0; i < str.length - 1; i++) {
			//Build our element string.  This is cleaner using the DOM, but
			//IE doesn't support dynamically added attributes.
			var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
			suggest += 'onmouseout="javascript:suggestOut(this);" ';
			suggest += 'onclick="javascript:setSearch(this.innerHTML);" ';
			suggest += 'class="suggest_link">' + str[i] + '</div>';
			ss.innerHTML += suggest;
		}
	}
}


//Mouse over function
function suggestOver(div_value) {
	div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
	div_value.className = 'suggest_link';
}

//Click function
function setSearch(value) {

        var srt=value.split("-"); 
	document.getElementById('txtSearch').value = trim(srt[0]);
	document.getElementById('search_suggest').innerHTML = '';
}

function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
}


function searchCodigo(path) {
	if (searchReq.readyState == 4 || searchReq.readyState == 0) {
		var str = escape(document.getElementById('txtSearch').value);
                var segment = '/';
                if(! str){
                    segment = '';
                }
                
		searchReq.open("GET", path + segment  + str, true);
		searchReq.onreadystatechange = handleCodigo; 
		searchReq.send(null);
	}		
}


function handleCodigo() {
	if (searchReq.readyState == 4) {
		var ss = document.getElementById('search_suggest')
		ss.innerHTML = '';
		var str = searchReq.responseText.split("\n");
		for(i=0; i < str.length - 1; i++) {
			//Build our element string.  This is cleaner using the DOM, but
			//IE doesn't support dynamically added attributes.
			var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
			suggest += 'onmouseout="javascript:suggestOut(this);" ';
			suggest += 'onclick="javascript:setSearchCodigo(this.innerHTML);" ';
			suggest += 'class="suggest_link">' + str[i] + '</div>';
			ss.innerHTML += suggest;
		}
	}
}


//Click function
function setSearchCodigo(value) {

        var srt=value.split("-"); 
	document.getElementById('txtSearch').value = trim(srt[1]);
        document.getElementById('studio_id').value = trim(srt[0]);
	document.getElementById('search_suggest').innerHTML = '';
}


//

function searchCpl(path) {
	if (searchReq.readyState == 4 || searchReq.readyState == 0) {
		var str = escape(document.getElementById('txtcpl').value);
                var segment = '/';
                if(! str){
                    segment = '';
                }
                
		searchReq.open("GET", path + segment  + str, true);
		searchReq.onreadystatechange = handleSearchCpl; 
		searchReq.send(null);
	}		
}

function handleSearchCpl() {
	if (searchReq.readyState == 4) {
		var ss = document.getElementById('search_suggest')
		ss.innerHTML = '';
		var str = searchReq.responseText.split("\n");
		for(i=0; i < str.length - 1; i++) {
			//Build our element string.  This is cleaner using the DOM, but
			//IE doesn't support dynamically added attributes.
			var suggest = '<div onmouseover="javascript:suggestOver(this);" ';
			suggest += 'onmouseout="javascript:suggestOut(this);" ';
			suggest += 'onclick="javascript:setCpl(this.innerHTML);" ';
			suggest += 'class="suggest_link">' + str[i] + '</div>';
			ss.innerHTML += suggest;
		}
	}
}

function suggestOver(div_value) {
	div_value.className = 'suggest_link_over';
}
//Mouse out function
function suggestOut(div_value) {
	div_value.className = 'suggest_link';
}


function setCpl(value) {

        var srt=value.split("-"); 
	document.getElementById('txtcpl').value = trim(srt[1]);
        document.getElementById('txtString').value = trim(srt[0]);
	document.getElementById('search_suggest').innerHTML = '';
}

function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
}