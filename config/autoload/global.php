<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    // ...

	 'module_layouts' => array(
    	'core'      => 'layout/core',
        'Captura'   => 'layout/core',
        'Graphs'    => 'layout/core',
        
    ),


	 
	   'navigation' => array(
        // The DefaultNavigationFactory we configured in (1) uses 'default' as the sitemap key
        'default' => array(
            // And finally, here is where we define our page hierarchy
            
            'logout' => array(
                'label' => 'Salir',
                'route' => 'logout',
            ),
            'system_log' => array(
                'label' => 'Registro',
                'route' => 'log',
            ),
            'home' => array(
                'label' => 'Inicio',
                'route' => 'inicio',
            ),
            
            'usuarios' => array(
                'label' => 'Usuarios',
                'route' => 'usuarios',
            ),

            'setup' => array(
                'label' => 'Configuración',
                'route' => 'maquinas',
            ),

            'maquinasactivas' => array(
                'label' => 'Capturas',
                'route' => 'maquinasactivas',
            ),

            'reginspeccion' => array(
                'label' => 'Registro Inspección',
                'route' => 'reginspeccion',
            ),

            'dppm' => array(
                'label' => 'Grafica Dppm',
                'route' => 'dppmgraph',
            ),
            
           
        ),
        
         
    ),

 'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
                    => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),


	'db' => array(
				'driver'         => 'Pdo',
				'dsn'            => 'mysql:dbname=huntercalidad;host=localhost',
				'username'	 	 => 'root',
				'password'		 =>  'hunter',
				'driver_options' => array(
				 PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
				),
	),
	
		
		
);
