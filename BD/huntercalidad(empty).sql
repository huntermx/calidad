/*
Navicat MySQL Data Transfer

Source Server         : MXSERVER
Source Server Version : 50532
Source Host           : 10.20.0.120:3306
Source Database       : huntercalidad

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2013-08-23 17:07:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for defectos_visuales
-- ----------------------------
DROP TABLE IF EXISTS `defectos_visuales`;
CREATE TABLE `defectos_visuales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of defectos_visuales
-- ----------------------------
INSERT INTO `defectos_visuales` VALUES ('1', 'MBK', 'Molding: Broken/Damaged');
INSERT INTO `defectos_visuales` VALUES ('2', 'MBR', 'Molding: Burr');
INSERT INTO `defectos_visuales` VALUES ('3', 'MDI', 'Molding: Dimension');
INSERT INTO `defectos_visuales` VALUES ('4', 'MDN', 'Molding: Dent');
INSERT INTO `defectos_visuales` VALUES ('5', 'MDS', 'Molding: Double Shot');
INSERT INTO `defectos_visuales` VALUES ('6', 'MDT', 'Molding: Damaged threads');
INSERT INTO `defectos_visuales` VALUES ('7', 'MEP', 'Molding: Ejector pin marks');
INSERT INTO `defectos_visuales` VALUES ('8', 'MFL', 'Molding: Flash');
INSERT INTO `defectos_visuales` VALUES ('9', 'MGR', 'Molding: Gate Remnant');
INSERT INTO `defectos_visuales` VALUES ('10', 'MHM', 'Handling: Mixed Parts');
INSERT INTO `defectos_visuales` VALUES ('11', 'MHS', 'Handling: Spilled');
INSERT INTO `defectos_visuales` VALUES ('12', 'MKL', 'Molding: Knit Line');
INSERT INTO `defectos_visuales` VALUES ('13', 'MMM', 'Molding: Mismatch');
INSERT INTO `defectos_visuales` VALUES ('14', 'MMR', 'Molding: Machine rejects');
INSERT INTO `defectos_visuales` VALUES ('15', 'MOS', 'Molding: Oil stained');
INSERT INTO `defectos_visuales` VALUES ('16', 'MQB', 'Quality burst failed low');
INSERT INTO `defectos_visuales` VALUES ('17', 'MQC', 'Quality compression failure');
INSERT INTO `defectos_visuales` VALUES ('18', 'MQD', 'Quality destructive test');
INSERT INTO `defectos_visuales` VALUES ('19', 'MQP', 'Quality ull failure');
INSERT INTO `defectos_visuales` VALUES ('20', 'MRC', 'Resin: Contamination');
INSERT INTO `defectos_visuales` VALUES ('21', 'MRO', 'Resin: Overheat / Overdry');
INSERT INTO `defectos_visuales` VALUES ('22', 'MRR', 'Resin: Recipe Error');
INSERT INTO `defectos_visuales` VALUES ('23', 'MSC', 'Molding: Scratch');
INSERT INTO `defectos_visuales` VALUES ('24', 'MSH', 'Molding: Short Shot');
INSERT INTO `defectos_visuales` VALUES ('25', 'MSI', 'Molding: Sink');
INSERT INTO `defectos_visuales` VALUES ('26', 'MSP', 'Molding: Splay');
INSERT INTO `defectos_visuales` VALUES ('27', 'MSS', 'Molding: Set-up Scrap');
INSERT INTO `defectos_visuales` VALUES ('28', 'MSU', 'Molding: Start-up Scrap');
INSERT INTO `defectos_visuales` VALUES ('29', 'MVD', 'Molding: Void');
INSERT INTO `defectos_visuales` VALUES ('30', 'MWP', 'Molding: Warping');
INSERT INTO `defectos_visuales` VALUES ('31', null, 'Bubbles');
INSERT INTO `defectos_visuales` VALUES ('32', null, 'Tonality Variation');

-- ----------------------------
-- Table structure for dimensional_fuera_especificacion
-- ----------------------------
DROP TABLE IF EXISTS `dimensional_fuera_especificacion`;
CREATE TABLE `dimensional_fuera_especificacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dimensional_id` int(11) DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `cavidad` varchar(15) DEFAULT NULL,
  `dimension` double DEFAULT NULL,
  `zona_dibujo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dimensional_fuera_especificacion
-- ----------------------------

-- ----------------------------
-- Table structure for dimensional_pruebas
-- ----------------------------
DROP TABLE IF EXISTS `dimensional_pruebas`;
CREATE TABLE `dimensional_pruebas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(12) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dimensional_pruebas
-- ----------------------------

-- ----------------------------
-- Table structure for inspeccion_dimensional
-- ----------------------------
DROP TABLE IF EXISTS `inspeccion_dimensional`;
CREATE TABLE `inspeccion_dimensional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registro_id` int(11) DEFAULT NULL,
  `cantidad_inspeccion` double DEFAULT NULL,
  `fuera_especificacion` double DEFAULT NULL,
  `cantidad_rechazada` double DEFAULT NULL,
  `prueba_funcional` enum('Aplica','No Aplica') DEFAULT NULL,
  `primera_corrida` enum('No','Si') DEFAULT NULL,
  `hora_recibido` time DEFAULT NULL,
  `hora_calificacion` time DEFAULT NULL,
  `tip_sheet` varchar(120) DEFAULT NULL,
  `resultado` enum('Mal','Bien') DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inspeccion_dimensional
-- ----------------------------

-- ----------------------------
-- Table structure for inspeccion_dimensional_detalle
-- ----------------------------
DROP TABLE IF EXISTS `inspeccion_dimensional_detalle`;
CREATE TABLE `inspeccion_dimensional_detalle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dimensional_id` int(11) DEFAULT NULL,
  `pconceptos_id` int(11) DEFAULT NULL,
  `tamano_muestra` double DEFAULT NULL,
  `rechazado` double DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inspeccion_dimensional_detalle
-- ----------------------------

-- ----------------------------
-- Table structure for inspeccion_registro
-- ----------------------------
DROP TABLE IF EXISTS `inspeccion_registro`;
CREATE TABLE `inspeccion_registro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `recorrido_id` int(11) DEFAULT NULL,
  `fecha_captura` date DEFAULT NULL,
  `turno` enum('Turno Especial','Turno 3','Turno 2','Turno 1') DEFAULT NULL,
  `numero_recorrido` int(11) DEFAULT NULL,
  `capturista` int(11) DEFAULT NULL,
  `inspector_dimensional` int(11) DEFAULT NULL,
  `inspector_visual` int(11) DEFAULT NULL,
  `maquina_id` int(11) DEFAULT NULL,
  `moldes_id` int(11) DEFAULT NULL,
  `partes_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inspeccion_registro
-- ----------------------------

-- ----------------------------
-- Table structure for inspeccion_visual
-- ----------------------------
DROP TABLE IF EXISTS `inspeccion_visual`;
CREATE TABLE `inspeccion_visual` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `registro_id` int(11) DEFAULT NULL,
  `cantidad_inspeccionada` double DEFAULT NULL,
  `cantidad_rechazada` double DEFAULT NULL,
  `cantidad_liberada` double DEFAULT NULL,
  `total_defectos` double DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inspeccion_visual
-- ----------------------------

-- ----------------------------
-- Table structure for inspeccion_visual_detalle
-- ----------------------------
DROP TABLE IF EXISTS `inspeccion_visual_detalle`;
CREATE TABLE `inspeccion_visual_detalle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visual_id` int(11) DEFAULT NULL,
  `defecto_id` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of inspeccion_visual_detalle
-- ----------------------------

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(25) DEFAULT NULL,
  `accion` varchar(150) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `tabla_afectada` varchar(35) DEFAULT NULL,
  `detalles_extra` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('1', '2013-08-23 17:06:36', 'dgomez', 'Ingreso al Sistema', '', '', '');
INSERT INTO `log` VALUES ('2', '2013-08-23 17:06:45', 'dgomez', 'Salida de Sistema', '', '', '');

-- ----------------------------
-- Table structure for maquinas
-- ----------------------------
DROP TABLE IF EXISTS `maquinas`;
CREATE TABLE `maquinas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orden` int(11) DEFAULT NULL,
  `nstatus` enum('0','1') DEFAULT NULL,
  `maquina` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of maquinas
-- ----------------------------
INSERT INTO `maquinas` VALUES ('1', '1', '1', '301');
INSERT INTO `maquinas` VALUES ('2', '2', '1', '302');
INSERT INTO `maquinas` VALUES ('3', '3', '1', '303');
INSERT INTO `maquinas` VALUES ('4', '4', '1', '304');
INSERT INTO `maquinas` VALUES ('5', '5', '1', '305');
INSERT INTO `maquinas` VALUES ('6', '6', '1', '401');
INSERT INTO `maquinas` VALUES ('7', '7', '1', '402');
INSERT INTO `maquinas` VALUES ('8', '8', '1', '306');
INSERT INTO `maquinas` VALUES ('9', '9', '1', '201');
INSERT INTO `maquinas` VALUES ('10', '10', '1', '202');
INSERT INTO `maquinas` VALUES ('11', '11', '1', '203');
INSERT INTO `maquinas` VALUES ('12', '12', '1', '204');
INSERT INTO `maquinas` VALUES ('13', '13', '1', '205');
INSERT INTO `maquinas` VALUES ('14', '14', '1', '309');
INSERT INTO `maquinas` VALUES ('15', '15', '1', '404');
INSERT INTO `maquinas` VALUES ('16', '16', '1', '403');
INSERT INTO `maquinas` VALUES ('17', '17', '1', '307');
INSERT INTO `maquinas` VALUES ('18', '18', '1', '56');
INSERT INTO `maquinas` VALUES ('19', '19', '1', '55');
INSERT INTO `maquinas` VALUES ('20', '20', '1', '109');
INSERT INTO `maquinas` VALUES ('21', '21', '1', '206');
INSERT INTO `maquinas` VALUES ('22', '22', '1', '207');
INSERT INTO `maquinas` VALUES ('23', '23', '1', '601');
INSERT INTO `maquinas` VALUES ('24', '24', '1', '210');
INSERT INTO `maquinas` VALUES ('25', '25', '1', '312');
INSERT INTO `maquinas` VALUES ('26', '26', '1', '209');
INSERT INTO `maquinas` VALUES ('27', '27', '1', '311');
INSERT INTO `maquinas` VALUES ('28', '28', '1', '314');
INSERT INTO `maquinas` VALUES ('29', '29', '1', '313');
INSERT INTO `maquinas` VALUES ('30', '30', '1', '308');
INSERT INTO `maquinas` VALUES ('31', '31', '1', '310');
INSERT INTO `maquinas` VALUES ('32', '32', '1', '58');
INSERT INTO `maquinas` VALUES ('33', '33', '1', '59');
INSERT INTO `maquinas` VALUES ('34', '34', '1', '60');
INSERT INTO `maquinas` VALUES ('35', '35', '1', '61');
INSERT INTO `maquinas` VALUES ('36', '36', '1', '405');
INSERT INTO `maquinas` VALUES ('37', '37', '1', '53');
INSERT INTO `maquinas` VALUES ('38', '38', '1', '51');
INSERT INTO `maquinas` VALUES ('39', '39', '1', '54');
INSERT INTO `maquinas` VALUES ('40', '40', '1', '52');
INSERT INTO `maquinas` VALUES ('41', '41', '1', '62');
INSERT INTO `maquinas` VALUES ('42', '42', '1', '63');
INSERT INTO `maquinas` VALUES ('43', '43', '1', '106');
INSERT INTO `maquinas` VALUES ('44', '44', '1', '64');
INSERT INTO `maquinas` VALUES ('45', '45', '1', '101');
INSERT INTO `maquinas` VALUES ('46', '46', '1', '65');
INSERT INTO `maquinas` VALUES ('47', '47', '1', '108');
INSERT INTO `maquinas` VALUES ('48', '48', '1', '208');
INSERT INTO `maquinas` VALUES ('49', '49', '1', '57');
INSERT INTO `maquinas` VALUES ('50', '50', '1', '107');
INSERT INTO `maquinas` VALUES ('51', '51', '1', '105');
INSERT INTO `maquinas` VALUES ('52', '52', '1', '103');
INSERT INTO `maquinas` VALUES ('53', '53', '1', '104');
INSERT INTO `maquinas` VALUES ('54', '54', '1', '102');

-- ----------------------------
-- Table structure for maquinas_activas
-- ----------------------------
DROP TABLE IF EXISTS `maquinas_activas`;
CREATE TABLE `maquinas_activas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `fecha` date DEFAULT NULL,
  `turno` enum('Turno Especial','Turno 2','Turno 1') DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of maquinas_activas
-- ----------------------------

-- ----------------------------
-- Table structure for maquinas_detalle
-- ----------------------------
DROP TABLE IF EXISTS `maquinas_detalle`;
CREATE TABLE `maquinas_detalle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `maquinas_activas_id` int(11) DEFAULT NULL,
  `maquina_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of maquinas_detalle
-- ----------------------------

-- ----------------------------
-- Table structure for molde_parte
-- ----------------------------
DROP TABLE IF EXISTS `molde_parte`;
CREATE TABLE `molde_parte` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id_molde` int(11) DEFAULT NULL,
  `id_parte` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=356 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of molde_parte
-- ----------------------------
INSERT INTO `molde_parte` VALUES ('1', '2013-06-17 16:17:18', '199', '354');
INSERT INTO `molde_parte` VALUES ('2', '2013-06-17 16:17:18', '199', '353');
INSERT INTO `molde_parte` VALUES ('3', '2013-06-17 16:17:18', '199', '352');
INSERT INTO `molde_parte` VALUES ('4', '2013-06-17 16:17:18', '199', '351');
INSERT INTO `molde_parte` VALUES ('5', '2013-06-17 16:17:18', '199', '350');
INSERT INTO `molde_parte` VALUES ('6', '2013-06-17 16:17:18', '199', '349');
INSERT INTO `molde_parte` VALUES ('7', '2013-06-17 16:17:18', '37', '348');
INSERT INTO `molde_parte` VALUES ('8', '2013-06-17 16:17:18', '161', '347');
INSERT INTO `molde_parte` VALUES ('9', '2013-06-17 16:17:18', '82', '346');
INSERT INTO `molde_parte` VALUES ('10', '2013-06-17 16:17:18', '82', '344');
INSERT INTO `molde_parte` VALUES ('11', '2013-06-17 16:17:18', '150', '344');
INSERT INTO `molde_parte` VALUES ('12', '2013-06-17 16:17:18', '80', '342');
INSERT INTO `molde_parte` VALUES ('13', '2013-06-17 16:17:18', '97', '342');
INSERT INTO `molde_parte` VALUES ('14', '2013-06-17 16:17:18', '80', '339');
INSERT INTO `molde_parte` VALUES ('15', '2013-06-17 16:17:18', '97', '339');
INSERT INTO `molde_parte` VALUES ('16', '2013-06-17 16:17:18', '167', '339');
INSERT INTO `molde_parte` VALUES ('17', '2013-06-17 16:17:18', '82', '338');
INSERT INTO `molde_parte` VALUES ('18', '2013-06-17 16:17:18', '167', '337');
INSERT INTO `molde_parte` VALUES ('19', '2013-06-17 16:17:18', '162', '336');
INSERT INTO `molde_parte` VALUES ('20', '2013-06-17 16:17:18', '76', '334');
INSERT INTO `molde_parte` VALUES ('21', '2013-06-17 16:17:18', '147', '334');
INSERT INTO `molde_parte` VALUES ('22', '2013-06-17 16:17:18', '76', '332');
INSERT INTO `molde_parte` VALUES ('23', '2013-06-17 16:17:18', '147', '332');
INSERT INTO `molde_parte` VALUES ('24', '2013-06-17 16:17:18', '160', '331');
INSERT INTO `molde_parte` VALUES ('25', '2013-06-17 16:17:18', '160', '330');
INSERT INTO `molde_parte` VALUES ('26', '2013-06-17 16:17:18', '159', '329');
INSERT INTO `molde_parte` VALUES ('27', '2013-06-17 16:17:18', '158', '328');
INSERT INTO `molde_parte` VALUES ('28', '2013-06-17 16:17:18', '152', '327');
INSERT INTO `molde_parte` VALUES ('29', '2013-06-17 16:17:18', '76', '325');
INSERT INTO `molde_parte` VALUES ('30', '2013-06-17 16:17:18', '147', '325');
INSERT INTO `molde_parte` VALUES ('31', '2013-06-17 16:17:18', '76', '323');
INSERT INTO `molde_parte` VALUES ('32', '2013-06-17 16:17:18', '147', '323');
INSERT INTO `molde_parte` VALUES ('33', '2013-06-17 16:17:18', '140', '322');
INSERT INTO `molde_parte` VALUES ('34', '2013-06-17 16:17:18', '140', '320');
INSERT INTO `molde_parte` VALUES ('35', '2013-06-17 16:17:18', '145', '320');
INSERT INTO `molde_parte` VALUES ('36', '2013-06-17 16:17:18', '123', '319');
INSERT INTO `molde_parte` VALUES ('37', '2013-06-17 16:17:18', '123', '317');
INSERT INTO `molde_parte` VALUES ('38', '2013-06-17 16:17:18', '175', '317');
INSERT INTO `molde_parte` VALUES ('39', '2013-06-17 16:17:18', '117', '315');
INSERT INTO `molde_parte` VALUES ('40', '2013-06-17 16:17:18', '165', '315');
INSERT INTO `molde_parte` VALUES ('41', '2013-06-17 16:17:18', '141', '314');
INSERT INTO `molde_parte` VALUES ('42', '2013-06-17 16:17:18', '138', '313');
INSERT INTO `molde_parte` VALUES ('43', '2013-06-17 16:17:18', '138', '312');
INSERT INTO `molde_parte` VALUES ('44', '2013-06-17 16:17:18', '137', '311');
INSERT INTO `molde_parte` VALUES ('45', '2013-06-17 16:17:18', '136', '310');
INSERT INTO `molde_parte` VALUES ('46', '2013-06-17 16:17:18', '135', '309');
INSERT INTO `molde_parte` VALUES ('47', '2013-06-17 16:17:18', '134', '308');
INSERT INTO `molde_parte` VALUES ('48', '2013-06-17 16:17:18', '131', '307');
INSERT INTO `molde_parte` VALUES ('49', '2013-06-17 16:17:18', '131', '306');
INSERT INTO `molde_parte` VALUES ('50', '2013-06-17 16:17:18', '131', '305');
INSERT INTO `molde_parte` VALUES ('51', '2013-06-17 16:17:18', '139', '304');
INSERT INTO `molde_parte` VALUES ('52', '2013-06-17 16:17:18', '139', '303');
INSERT INTO `molde_parte` VALUES ('53', '2013-06-17 16:17:18', '130', '302');
INSERT INTO `molde_parte` VALUES ('54', '2013-06-17 16:17:18', '130', '301');
INSERT INTO `molde_parte` VALUES ('55', '2013-06-17 16:17:18', '133', '300');
INSERT INTO `molde_parte` VALUES ('56', '2013-06-17 16:17:18', '133', '299');
INSERT INTO `molde_parte` VALUES ('57', '2013-06-17 16:17:18', '132', '298');
INSERT INTO `molde_parte` VALUES ('58', '2013-06-17 16:17:18', '128', '296');
INSERT INTO `molde_parte` VALUES ('59', '2013-06-17 16:17:18', '129', '296');
INSERT INTO `molde_parte` VALUES ('60', '2013-06-17 16:17:18', '127', '295');
INSERT INTO `molde_parte` VALUES ('61', '2013-06-17 16:17:18', '127', '294');
INSERT INTO `molde_parte` VALUES ('62', '2013-06-17 16:17:18', '108', '293');
INSERT INTO `molde_parte` VALUES ('63', '2013-06-17 16:17:18', '108', '292');
INSERT INTO `molde_parte` VALUES ('64', '2013-06-17 16:17:18', '113', '291');
INSERT INTO `molde_parte` VALUES ('65', '2013-06-17 16:17:19', '113', '290');
INSERT INTO `molde_parte` VALUES ('66', '2013-06-17 16:17:19', '112', '289');
INSERT INTO `molde_parte` VALUES ('67', '2013-06-17 16:17:19', '111', '288');
INSERT INTO `molde_parte` VALUES ('68', '2013-06-17 16:17:19', '110', '287');
INSERT INTO `molde_parte` VALUES ('69', '2013-06-17 16:17:19', '110', '286');
INSERT INTO `molde_parte` VALUES ('70', '2013-06-17 16:17:19', '108', '285');
INSERT INTO `molde_parte` VALUES ('71', '2013-06-17 16:17:19', '108', '284');
INSERT INTO `molde_parte` VALUES ('72', '2013-06-17 16:17:19', '108', '282');
INSERT INTO `molde_parte` VALUES ('73', '2013-06-17 16:17:19', '148', '282');
INSERT INTO `molde_parte` VALUES ('74', '2013-06-17 16:17:19', '108', '280');
INSERT INTO `molde_parte` VALUES ('75', '2013-06-17 16:17:19', '148', '280');
INSERT INTO `molde_parte` VALUES ('76', '2013-06-17 16:17:19', '107', '278');
INSERT INTO `molde_parte` VALUES ('77', '2013-06-17 16:17:19', '144', '278');
INSERT INTO `molde_parte` VALUES ('78', '2013-06-17 16:17:19', '107', '276');
INSERT INTO `molde_parte` VALUES ('79', '2013-06-17 16:17:19', '144', '276');
INSERT INTO `molde_parte` VALUES ('80', '2013-06-17 16:17:19', '107', '274');
INSERT INTO `molde_parte` VALUES ('81', '2013-06-17 16:17:19', '144', '274');
INSERT INTO `molde_parte` VALUES ('82', '2013-06-17 16:17:19', '108', '272');
INSERT INTO `molde_parte` VALUES ('83', '2013-06-17 16:17:19', '148', '272');
INSERT INTO `molde_parte` VALUES ('84', '2013-06-17 16:17:19', '108', '270');
INSERT INTO `molde_parte` VALUES ('85', '2013-06-17 16:17:19', '148', '270');
INSERT INTO `molde_parte` VALUES ('86', '2013-06-17 16:17:19', '109', '268');
INSERT INTO `molde_parte` VALUES ('87', '2013-06-17 16:17:19', '143', '268');
INSERT INTO `molde_parte` VALUES ('88', '2013-06-17 16:17:19', '36', '267');
INSERT INTO `molde_parte` VALUES ('89', '2013-06-17 16:17:19', '78', '266');
INSERT INTO `molde_parte` VALUES ('90', '2013-06-17 16:17:19', '77', '265');
INSERT INTO `molde_parte` VALUES ('91', '2013-06-17 16:17:19', '79', '264');
INSERT INTO `molde_parte` VALUES ('92', '2013-06-17 16:17:19', '89', '263');
INSERT INTO `molde_parte` VALUES ('93', '2013-06-17 16:17:19', '126', '261');
INSERT INTO `molde_parte` VALUES ('94', '2013-06-17 16:17:19', '168', '261');
INSERT INTO `molde_parte` VALUES ('95', '2013-06-17 16:17:19', '90', '260');
INSERT INTO `molde_parte` VALUES ('96', '2013-06-17 16:17:19', '84', '259');
INSERT INTO `molde_parte` VALUES ('97', '2013-06-17 16:17:19', '85', '258');
INSERT INTO `molde_parte` VALUES ('98', '2013-06-17 16:17:19', '73', '257');
INSERT INTO `molde_parte` VALUES ('99', '2013-06-17 16:17:19', '73', '256');
INSERT INTO `molde_parte` VALUES ('100', '2013-06-17 16:17:19', '73', '255');
INSERT INTO `molde_parte` VALUES ('101', '2013-06-17 16:17:19', '73', '254');
INSERT INTO `molde_parte` VALUES ('102', '2013-06-17 16:17:19', '76', '252');
INSERT INTO `molde_parte` VALUES ('103', '2013-06-17 16:17:19', '147', '252');
INSERT INTO `molde_parte` VALUES ('104', '2013-06-17 16:17:19', '76', '250');
INSERT INTO `molde_parte` VALUES ('105', '2013-06-17 16:17:19', '147', '250');
INSERT INTO `molde_parte` VALUES ('106', '2013-06-17 16:17:19', '87', '248');
INSERT INTO `molde_parte` VALUES ('107', '2013-06-17 16:17:19', '166', '248');
INSERT INTO `molde_parte` VALUES ('108', '2013-06-17 16:17:19', '94', '247');
INSERT INTO `molde_parte` VALUES ('109', '2013-06-17 16:17:19', '82', '246');
INSERT INTO `molde_parte` VALUES ('110', '2013-06-17 16:17:19', '83', '245');
INSERT INTO `molde_parte` VALUES ('111', '2013-06-17 16:17:19', '81', '243');
INSERT INTO `molde_parte` VALUES ('112', '2013-06-17 16:17:19', '176', '243');
INSERT INTO `molde_parte` VALUES ('113', '2013-06-17 16:17:19', '52', '242');
INSERT INTO `molde_parte` VALUES ('114', '2013-06-17 16:17:19', '52', '241');
INSERT INTO `molde_parte` VALUES ('115', '2013-06-17 16:17:19', '52', '240');
INSERT INTO `molde_parte` VALUES ('116', '2013-06-17 16:17:19', '52', '239');
INSERT INTO `molde_parte` VALUES ('117', '2013-06-17 16:17:19', '52', '238');
INSERT INTO `molde_parte` VALUES ('118', '2013-06-17 16:17:19', '52', '237');
INSERT INTO `molde_parte` VALUES ('119', '2013-06-17 16:17:19', '52', '236');
INSERT INTO `molde_parte` VALUES ('120', '2013-06-17 16:17:19', '52', '235');
INSERT INTO `molde_parte` VALUES ('121', '2013-06-17 16:17:19', '52', '234');
INSERT INTO `molde_parte` VALUES ('122', '2013-06-17 16:17:19', '52', '233');
INSERT INTO `molde_parte` VALUES ('123', '2013-06-17 16:17:19', '52', '231');
INSERT INTO `molde_parte` VALUES ('124', '2013-06-17 16:17:19', '92', '231');
INSERT INTO `molde_parte` VALUES ('125', '2013-06-17 16:17:19', '52', '229');
INSERT INTO `molde_parte` VALUES ('126', '2013-06-17 16:17:19', '92', '229');
INSERT INTO `molde_parte` VALUES ('127', '2013-06-17 16:17:19', '52', '227');
INSERT INTO `molde_parte` VALUES ('128', '2013-06-17 16:17:19', '92', '227');
INSERT INTO `molde_parte` VALUES ('129', '2013-06-17 16:17:19', '52', '225');
INSERT INTO `molde_parte` VALUES ('130', '2013-06-17 16:17:19', '92', '225');
INSERT INTO `molde_parte` VALUES ('131', '2013-06-17 16:17:19', '52', '223');
INSERT INTO `molde_parte` VALUES ('132', '2013-06-17 16:17:19', '92', '223');
INSERT INTO `molde_parte` VALUES ('133', '2013-06-17 16:17:19', '52', '221');
INSERT INTO `molde_parte` VALUES ('134', '2013-06-17 16:17:19', '92', '221');
INSERT INTO `molde_parte` VALUES ('135', '2013-06-17 16:17:19', '52', '220');
INSERT INTO `molde_parte` VALUES ('136', '2013-06-17 16:17:19', '92', '219');
INSERT INTO `molde_parte` VALUES ('137', '2013-06-17 16:17:19', '122', '217');
INSERT INTO `molde_parte` VALUES ('138', '2013-06-17 16:17:19', '174', '217');
INSERT INTO `molde_parte` VALUES ('139', '2013-06-17 16:17:19', '121', '215');
INSERT INTO `molde_parte` VALUES ('140', '2013-06-17 16:17:19', '125', '215');
INSERT INTO `molde_parte` VALUES ('141', '2013-06-17 16:17:19', '121', '213');
INSERT INTO `molde_parte` VALUES ('142', '2013-06-17 16:17:19', '125', '213');
INSERT INTO `molde_parte` VALUES ('143', '2013-06-17 16:17:19', '72', '211');
INSERT INTO `molde_parte` VALUES ('144', '2013-06-17 16:17:19', '72', '210');
INSERT INTO `molde_parte` VALUES ('145', '2013-06-17 16:17:19', '72', '209');
INSERT INTO `molde_parte` VALUES ('146', '2013-06-17 16:17:19', '72', '208');
INSERT INTO `molde_parte` VALUES ('147', '2013-06-17 16:17:19', '75', '207');
INSERT INTO `molde_parte` VALUES ('148', '2013-06-17 16:17:19', '75', '206');
INSERT INTO `molde_parte` VALUES ('149', '2013-06-17 16:17:19', '74', '205');
INSERT INTO `molde_parte` VALUES ('150', '2013-06-17 16:17:19', '74', '204');
INSERT INTO `molde_parte` VALUES ('151', '2013-06-17 16:17:19', '75', '203');
INSERT INTO `molde_parte` VALUES ('152', '2013-06-17 16:17:19', '74', '202');
INSERT INTO `molde_parte` VALUES ('153', '2013-06-17 16:17:20', '71', '201');
INSERT INTO `molde_parte` VALUES ('154', '2013-06-17 16:17:20', '71', '200');
INSERT INTO `molde_parte` VALUES ('155', '2013-06-17 16:17:20', '61', '199');
INSERT INTO `molde_parte` VALUES ('156', '2013-06-17 16:17:20', '68', '198');
INSERT INTO `molde_parte` VALUES ('157', '2013-06-17 16:17:20', '70', '197');
INSERT INTO `molde_parte` VALUES ('158', '2013-06-17 16:17:20', '69', '196');
INSERT INTO `molde_parte` VALUES ('159', '2013-06-17 16:17:20', '76', '194');
INSERT INTO `molde_parte` VALUES ('160', '2013-06-17 16:17:20', '147', '194');
INSERT INTO `molde_parte` VALUES ('161', '2013-06-17 16:17:20', '76', '192');
INSERT INTO `molde_parte` VALUES ('162', '2013-06-17 16:17:20', '147', '192');
INSERT INTO `molde_parte` VALUES ('163', '2013-06-17 16:17:20', '93', '191');
INSERT INTO `molde_parte` VALUES ('164', '2013-06-17 16:17:20', '95', '190');
INSERT INTO `molde_parte` VALUES ('165', '2013-06-17 16:17:20', '96', '189');
INSERT INTO `molde_parte` VALUES ('166', '2013-06-17 16:17:20', '86', '188');
INSERT INTO `molde_parte` VALUES ('167', '2013-06-17 16:17:20', '163', '187');
INSERT INTO `molde_parte` VALUES ('168', '2013-06-17 16:17:20', '169', '186');
INSERT INTO `molde_parte` VALUES ('169', '2013-06-17 16:17:20', '169', '185');
INSERT INTO `molde_parte` VALUES ('170', '2013-06-17 16:17:20', '56', '184');
INSERT INTO `molde_parte` VALUES ('171', '2013-06-17 16:17:20', '46', '183');
INSERT INTO `molde_parte` VALUES ('172', '2013-06-17 16:17:20', '55', '182');
INSERT INTO `molde_parte` VALUES ('173', '2013-06-17 16:17:20', '55', '181');
INSERT INTO `molde_parte` VALUES ('174', '2013-06-17 16:17:20', '53', '180');
INSERT INTO `molde_parte` VALUES ('175', '2013-06-17 16:17:20', '54', '179');
INSERT INTO `molde_parte` VALUES ('176', '2013-06-17 16:17:20', '54', '178');
INSERT INTO `molde_parte` VALUES ('177', '2013-06-17 16:17:20', '49', '177');
INSERT INTO `molde_parte` VALUES ('178', '2013-06-17 16:17:20', '49', '176');
INSERT INTO `molde_parte` VALUES ('179', '2013-06-17 16:17:20', '48', '175');
INSERT INTO `molde_parte` VALUES ('180', '2013-06-17 16:17:20', '47', '174');
INSERT INTO `molde_parte` VALUES ('181', '2013-06-17 16:17:20', '51', '173');
INSERT INTO `molde_parte` VALUES ('182', '2013-06-17 16:17:20', '50', '172');
INSERT INTO `molde_parte` VALUES ('183', '2013-06-17 16:17:20', '50', '171');
INSERT INTO `molde_parte` VALUES ('184', '2013-06-17 16:17:20', '67', '170');
INSERT INTO `molde_parte` VALUES ('185', '2013-06-17 16:17:20', '67', '169');
INSERT INTO `molde_parte` VALUES ('186', '2013-06-17 16:17:20', '64', '168');
INSERT INTO `molde_parte` VALUES ('187', '2013-06-17 16:17:20', '63', '167');
INSERT INTO `molde_parte` VALUES ('188', '2013-06-17 16:17:20', '66', '166');
INSERT INTO `molde_parte` VALUES ('189', '2013-06-17 16:17:20', '58', '165');
INSERT INTO `molde_parte` VALUES ('190', '2013-06-17 16:17:20', '58', '164');
INSERT INTO `molde_parte` VALUES ('191', '2013-06-17 16:17:20', '58', '163');
INSERT INTO `molde_parte` VALUES ('192', '2013-06-17 16:17:20', '58', '162');
INSERT INTO `molde_parte` VALUES ('193', '2013-06-17 16:17:20', '62', '161');
INSERT INTO `molde_parte` VALUES ('194', '2013-06-17 16:17:21', '65', '160');
INSERT INTO `molde_parte` VALUES ('195', '2013-06-17 16:17:21', '153', '159');
INSERT INTO `molde_parte` VALUES ('196', '2013-06-17 16:17:21', '42', '158');
INSERT INTO `molde_parte` VALUES ('197', '2013-06-17 16:17:21', '43', '157');
INSERT INTO `molde_parte` VALUES ('198', '2013-06-17 16:17:21', '9', '124');
INSERT INTO `molde_parte` VALUES ('199', '2013-06-17 16:17:21', '9', '122');
INSERT INTO `molde_parte` VALUES ('200', '2013-06-17 16:17:21', '9', '120');
INSERT INTO `molde_parte` VALUES ('201', '2013-06-17 16:17:21', '9', '118');
INSERT INTO `molde_parte` VALUES ('202', '2013-06-17 16:17:21', '9', '116');
INSERT INTO `molde_parte` VALUES ('203', '2013-06-17 16:17:21', '9', '114');
INSERT INTO `molde_parte` VALUES ('204', '2013-06-17 16:17:21', '9', '112');
INSERT INTO `molde_parte` VALUES ('205', '2013-06-17 16:17:21', '44', '102');
INSERT INTO `molde_parte` VALUES ('206', '2013-06-17 16:17:21', '38', '101');
INSERT INTO `molde_parte` VALUES ('207', '2013-06-17 16:17:21', '39', '100');
INSERT INTO `molde_parte` VALUES ('208', '2013-06-17 16:17:21', '37', '99');
INSERT INTO `molde_parte` VALUES ('209', '2013-06-17 16:17:21', '40', '98');
INSERT INTO `molde_parte` VALUES ('210', '2013-06-17 16:17:21', '41', '97');
INSERT INTO `molde_parte` VALUES ('211', '2013-06-17 16:17:21', '41', '96');
INSERT INTO `molde_parte` VALUES ('212', '2013-06-17 16:17:21', '35', '95');
INSERT INTO `molde_parte` VALUES ('213', '2013-06-17 16:17:21', '35', '94');
INSERT INTO `molde_parte` VALUES ('214', '2013-06-17 16:17:22', '189', '93');
INSERT INTO `molde_parte` VALUES ('215', '2013-06-17 16:17:22', '190', '92');
INSERT INTO `molde_parte` VALUES ('216', '2013-06-17 16:17:22', '88', '91');
INSERT INTO `molde_parte` VALUES ('217', '2013-06-17 16:17:22', '34', '90');
INSERT INTO `molde_parte` VALUES ('218', '2013-06-17 16:17:22', '32', '89');
INSERT INTO `molde_parte` VALUES ('219', '2013-06-17 16:17:22', '31', '88');
INSERT INTO `molde_parte` VALUES ('220', '2013-06-17 16:17:22', '30', '87');
INSERT INTO `molde_parte` VALUES ('221', '2013-06-17 16:17:22', '30', '86');
INSERT INTO `molde_parte` VALUES ('222', '2013-06-17 16:17:22', '33', '85');
INSERT INTO `molde_parte` VALUES ('223', '2013-06-17 16:17:22', '27', '84');
INSERT INTO `molde_parte` VALUES ('224', '2013-06-17 16:17:22', '29', '83');
INSERT INTO `molde_parte` VALUES ('225', '2013-06-17 16:17:22', '28', '82');
INSERT INTO `molde_parte` VALUES ('226', '2013-06-17 16:17:22', '28', '81');
INSERT INTO `molde_parte` VALUES ('227', '2013-06-17 16:17:22', '120', '80');
INSERT INTO `molde_parte` VALUES ('228', '2013-06-17 16:17:22', '26', '78');
INSERT INTO `molde_parte` VALUES ('229', '2013-06-17 16:17:22', '106', '78');
INSERT INTO `molde_parte` VALUES ('230', '2013-06-17 16:17:22', '142', '77');
INSERT INTO `molde_parte` VALUES ('231', '2013-06-17 16:17:22', '119', '76');
INSERT INTO `molde_parte` VALUES ('232', '2013-06-17 16:17:22', '2', '75');
INSERT INTO `molde_parte` VALUES ('233', '2013-06-17 16:17:22', '22', '73');
INSERT INTO `molde_parte` VALUES ('234', '2013-06-17 16:17:22', '23', '73');
INSERT INTO `molde_parte` VALUES ('235', '2013-06-17 16:17:22', '22', '71');
INSERT INTO `molde_parte` VALUES ('236', '2013-06-17 16:17:22', '23', '71');
INSERT INTO `molde_parte` VALUES ('237', '2013-06-17 16:17:22', '118', '70');
INSERT INTO `molde_parte` VALUES ('238', '2013-06-17 16:17:22', '115', '69');
INSERT INTO `molde_parte` VALUES ('239', '2013-06-17 16:17:22', '116', '68');
INSERT INTO `molde_parte` VALUES ('240', '2013-06-17 16:17:22', '114', '67');
INSERT INTO `molde_parte` VALUES ('241', '2013-06-17 16:17:22', '122', '65');
INSERT INTO `molde_parte` VALUES ('242', '2013-06-17 16:17:22', '174', '65');
INSERT INTO `molde_parte` VALUES ('243', '2013-06-17 16:17:22', '121', '63');
INSERT INTO `molde_parte` VALUES ('244', '2013-06-17 16:17:22', '125', '63');
INSERT INTO `molde_parte` VALUES ('245', '2013-06-17 16:17:22', '121', '61');
INSERT INTO `molde_parte` VALUES ('246', '2013-06-17 16:17:22', '125', '61');
INSERT INTO `molde_parte` VALUES ('247', '2013-06-17 16:17:22', '124', '59');
INSERT INTO `molde_parte` VALUES ('248', '2013-06-17 16:17:22', '146', '59');
INSERT INTO `molde_parte` VALUES ('249', '2013-06-17 16:17:22', '78', '58');
INSERT INTO `molde_parte` VALUES ('250', '2013-06-17 16:17:22', '24', '57');
INSERT INTO `molde_parte` VALUES ('251', '2013-06-17 16:17:22', '25', '56');
INSERT INTO `molde_parte` VALUES ('252', '2013-06-17 16:17:22', '17', '55');
INSERT INTO `molde_parte` VALUES ('253', '2013-06-17 16:17:22', '17', '54');
INSERT INTO `molde_parte` VALUES ('254', '2013-06-17 16:17:22', '17', '53');
INSERT INTO `molde_parte` VALUES ('255', '2013-06-17 16:17:22', '19', '52');
INSERT INTO `molde_parte` VALUES ('256', '2013-06-17 16:17:22', '20', '51');
INSERT INTO `molde_parte` VALUES ('257', '2013-06-17 16:17:22', '11', '50');
INSERT INTO `molde_parte` VALUES ('258', '2013-06-17 16:17:22', '12', '49');
INSERT INTO `molde_parte` VALUES ('259', '2013-06-17 16:17:22', '15', '47');
INSERT INTO `molde_parte` VALUES ('260', '2013-06-17 16:17:22', '57', '46');
INSERT INTO `molde_parte` VALUES ('261', '2013-06-17 16:17:22', '16', '45');
INSERT INTO `molde_parte` VALUES ('262', '2013-06-17 16:17:22', '18', '44');
INSERT INTO `molde_parte` VALUES ('263', '2013-06-17 16:17:22', '13', '42');
INSERT INTO `molde_parte` VALUES ('264', '2013-06-17 16:17:22', '177', '42');
INSERT INTO `molde_parte` VALUES ('265', '2013-06-17 16:17:22', '14', '41');
INSERT INTO `molde_parte` VALUES ('266', '2013-06-17 16:17:22', '198', '40');
INSERT INTO `molde_parte` VALUES ('267', '2013-06-17 16:17:22', '91', '39');
INSERT INTO `molde_parte` VALUES ('268', '2013-06-17 16:17:22', '192', '38');
INSERT INTO `molde_parte` VALUES ('269', '2013-06-17 16:17:22', '194', '37');
INSERT INTO `molde_parte` VALUES ('270', '2013-06-17 16:17:22', '193', '36');
INSERT INTO `molde_parte` VALUES ('271', '2013-06-17 16:17:22', '197', '35');
INSERT INTO `molde_parte` VALUES ('272', '2013-06-17 16:17:22', '196', '34');
INSERT INTO `molde_parte` VALUES ('273', '2013-06-17 16:17:22', '195', '33');
INSERT INTO `molde_parte` VALUES ('274', '2013-06-17 16:17:22', '172', '32');
INSERT INTO `molde_parte` VALUES ('275', '2013-06-17 16:17:22', '21', '31');
INSERT INTO `molde_parte` VALUES ('276', '2013-06-17 16:17:22', '63', '30');
INSERT INTO `molde_parte` VALUES ('277', '2013-06-17 16:17:22', '62', '29');
INSERT INTO `molde_parte` VALUES ('278', '2013-06-17 16:17:22', '22', '28');
INSERT INTO `molde_parte` VALUES ('279', '2013-06-17 16:17:22', '149', '27');
INSERT INTO `molde_parte` VALUES ('280', '2013-06-17 16:17:22', '153', '26');
INSERT INTO `molde_parte` VALUES ('281', '2013-06-17 16:17:22', '186', '25');
INSERT INTO `molde_parte` VALUES ('282', '2013-06-17 16:17:22', '185', '24');
INSERT INTO `molde_parte` VALUES ('283', '2013-06-17 16:17:22', '187', '23');
INSERT INTO `molde_parte` VALUES ('284', '2013-06-17 16:17:22', '187', '22');
INSERT INTO `molde_parte` VALUES ('285', '2013-06-17 16:17:22', '188', '21');
INSERT INTO `molde_parte` VALUES ('286', '2013-06-17 16:17:22', '182', '20');
INSERT INTO `molde_parte` VALUES ('287', '2013-06-17 16:17:22', '181', '19');
INSERT INTO `molde_parte` VALUES ('288', '2013-06-17 16:17:22', '181', '18');
INSERT INTO `molde_parte` VALUES ('289', '2013-06-17 16:17:22', '181', '17');
INSERT INTO `molde_parte` VALUES ('290', '2013-06-17 16:17:22', '181', '16');
INSERT INTO `molde_parte` VALUES ('291', '2013-06-17 16:17:22', '180', '15');
INSERT INTO `molde_parte` VALUES ('292', '2013-06-17 16:17:22', '180', '14');
INSERT INTO `molde_parte` VALUES ('293', '2013-06-17 16:17:22', '180', '13');
INSERT INTO `molde_parte` VALUES ('294', '2013-06-17 16:17:22', '58', '12');
INSERT INTO `molde_parte` VALUES ('295', '2013-06-17 16:17:22', '179', '11');
INSERT INTO `molde_parte` VALUES ('296', '2013-06-17 16:17:22', '45', '10');
INSERT INTO `molde_parte` VALUES ('297', '2013-06-17 16:17:22', '60', '9');
INSERT INTO `molde_parte` VALUES ('298', '2013-06-17 16:17:22', '60', '8');
INSERT INTO `molde_parte` VALUES ('299', '2013-06-17 16:17:22', '59', '7');
INSERT INTO `molde_parte` VALUES ('300', '2013-06-17 16:17:23', '59', '6');
INSERT INTO `molde_parte` VALUES ('301', '2013-06-17 16:17:23', '58', '5');
INSERT INTO `molde_parte` VALUES ('302', '2013-06-17 16:17:23', '58', '4');
INSERT INTO `molde_parte` VALUES ('303', '2013-06-17 16:17:23', '58', '3');
INSERT INTO `molde_parte` VALUES ('304', '2013-06-17 16:17:23', '58', '2');
INSERT INTO `molde_parte` VALUES ('305', '2013-06-17 16:17:23', '58', '1');
INSERT INTO `molde_parte` VALUES ('306', '2013-06-17 16:17:23', '8', '212');
INSERT INTO `molde_parte` VALUES ('307', '2013-06-17 16:17:23', '102', '156');
INSERT INTO `molde_parte` VALUES ('308', '2013-06-17 16:17:23', '101', '155');
INSERT INTO `molde_parte` VALUES ('309', '2013-06-17 16:17:23', '191', '154');
INSERT INTO `molde_parte` VALUES ('310', '2013-06-17 16:17:23', '156', '153');
INSERT INTO `molde_parte` VALUES ('311', '2013-06-17 16:17:23', '10', '152');
INSERT INTO `molde_parte` VALUES ('312', '2013-06-17 16:17:23', '156', '151');
INSERT INTO `molde_parte` VALUES ('313', '2013-06-17 16:17:23', '156', '150');
INSERT INTO `molde_parte` VALUES ('314', '2013-06-17 16:17:23', '151', '149');
INSERT INTO `molde_parte` VALUES ('315', '2013-06-17 16:17:23', '151', '148');
INSERT INTO `molde_parte` VALUES ('316', '2013-06-17 16:17:23', '151', '147');
INSERT INTO `molde_parte` VALUES ('317', '2013-06-17 16:17:23', '155', '146');
INSERT INTO `molde_parte` VALUES ('318', '2013-06-17 16:17:23', '6', '145');
INSERT INTO `molde_parte` VALUES ('319', '2013-06-17 16:17:23', '183', '143');
INSERT INTO `molde_parte` VALUES ('320', '2013-06-17 16:17:23', '184', '143');
INSERT INTO `molde_parte` VALUES ('321', '2013-06-17 16:17:23', '154', '142');
INSERT INTO `molde_parte` VALUES ('322', '2013-06-17 16:17:23', '154', '141');
INSERT INTO `molde_parte` VALUES ('323', '2013-06-17 16:17:23', '154', '140');
INSERT INTO `molde_parte` VALUES ('324', '2013-06-17 16:17:23', '173', '139');
INSERT INTO `molde_parte` VALUES ('325', '2013-06-17 16:17:23', '7', '138');
INSERT INTO `molde_parte` VALUES ('326', '2013-06-17 16:17:23', '164', '137');
INSERT INTO `molde_parte` VALUES ('327', '2013-06-17 16:17:23', '171', '136');
INSERT INTO `molde_parte` VALUES ('328', '2013-06-17 16:17:23', '170', '135');
INSERT INTO `molde_parte` VALUES ('329', '2013-06-17 16:17:24', '1', '133');
INSERT INTO `molde_parte` VALUES ('330', '2013-06-17 16:17:24', '178', '133');
INSERT INTO `molde_parte` VALUES ('331', '2013-06-17 16:17:24', '1', '131');
INSERT INTO `molde_parte` VALUES ('332', '2013-06-17 16:17:24', '178', '131');
INSERT INTO `molde_parte` VALUES ('333', '2013-06-17 16:17:24', '1', '129');
INSERT INTO `molde_parte` VALUES ('334', '2013-06-17 16:17:24', '178', '129');
INSERT INTO `molde_parte` VALUES ('335', '2013-06-17 16:17:24', '1', '127');
INSERT INTO `molde_parte` VALUES ('336', '2013-06-17 16:17:24', '178', '127');
INSERT INTO `molde_parte` VALUES ('337', '2013-06-17 16:17:24', '157', '126');
INSERT INTO `molde_parte` VALUES ('338', '2013-06-17 16:17:24', '5', '124');
INSERT INTO `molde_parte` VALUES ('339', '2013-06-17 16:17:24', '5', '122');
INSERT INTO `molde_parte` VALUES ('340', '2013-06-17 16:17:24', '5', '120');
INSERT INTO `molde_parte` VALUES ('341', '2013-06-17 16:17:24', '5', '118');
INSERT INTO `molde_parte` VALUES ('342', '2013-06-17 16:17:24', '5', '116');
INSERT INTO `molde_parte` VALUES ('343', '2013-06-17 16:17:24', '5', '114');
INSERT INTO `molde_parte` VALUES ('344', '2013-06-17 16:17:24', '5', '112');
INSERT INTO `molde_parte` VALUES ('345', '2013-06-17 16:17:24', '103', '110');
INSERT INTO `molde_parte` VALUES ('346', '2013-06-17 16:17:24', '200', '110');
INSERT INTO `molde_parte` VALUES ('347', '2013-06-17 16:17:24', '100', '109');
INSERT INTO `molde_parte` VALUES ('348', '2013-06-17 16:17:24', '105', '108');
INSERT INTO `molde_parte` VALUES ('349', '2013-06-17 16:17:24', '3', '106');
INSERT INTO `molde_parte` VALUES ('350', '2013-06-17 16:17:24', '104', '106');
INSERT INTO `molde_parte` VALUES ('351', '2013-06-17 16:17:24', '99', '105');
INSERT INTO `molde_parte` VALUES ('352', '2013-06-17 16:17:24', '4', '103');
INSERT INTO `molde_parte` VALUES ('353', '2013-06-17 16:17:24', '98', '103');
INSERT INTO `molde_parte` VALUES ('354', '2013-06-17 16:17:24', '8', '48');
INSERT INTO `molde_parte` VALUES ('355', '2013-07-02 14:50:04', '7', '1');

-- ----------------------------
-- Table structure for moldes
-- ----------------------------
DROP TABLE IF EXISTS `moldes`;
CREATE TABLE `moldes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `molde` varchar(15) DEFAULT NULL,
  `descripcion` varchar(55) DEFAULT NULL,
  `nstatus` enum('produccion','reparacion') DEFAULT 'produccion',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of moldes
-- ----------------------------
INSERT INTO `moldes` VALUES ('1', '11', '', 'produccion');
INSERT INTO `moldes` VALUES ('2', '23', null, 'produccion');
INSERT INTO `moldes` VALUES ('3', '100', '', 'produccion');
INSERT INTO `moldes` VALUES ('4', '101', null, 'produccion');
INSERT INTO `moldes` VALUES ('5', '136', null, 'produccion');
INSERT INTO `moldes` VALUES ('6', '170', null, 'produccion');
INSERT INTO `moldes` VALUES ('7', '295', '', 'produccion');
INSERT INTO `moldes` VALUES ('8', '361', null, 'produccion');
INSERT INTO `moldes` VALUES ('9', '506', null, 'produccion');
INSERT INTO `moldes` VALUES ('10', '535', null, 'produccion');
INSERT INTO `moldes` VALUES ('11', '588', null, 'produccion');
INSERT INTO `moldes` VALUES ('12', '619', null, 'produccion');
INSERT INTO `moldes` VALUES ('13', '635', null, 'produccion');
INSERT INTO `moldes` VALUES ('14', '636', null, 'produccion');
INSERT INTO `moldes` VALUES ('15', '727', null, 'produccion');
INSERT INTO `moldes` VALUES ('16', '748', null, 'produccion');
INSERT INTO `moldes` VALUES ('17', '754', null, 'produccion');
INSERT INTO `moldes` VALUES ('18', '776', null, 'produccion');
INSERT INTO `moldes` VALUES ('19', '779', null, 'produccion');
INSERT INTO `moldes` VALUES ('20', '787', null, 'produccion');
INSERT INTO `moldes` VALUES ('21', '915', null, 'produccion');
INSERT INTO `moldes` VALUES ('22', '916', null, 'produccion');
INSERT INTO `moldes` VALUES ('23', '918', null, 'produccion');
INSERT INTO `moldes` VALUES ('24', '956', null, 'produccion');
INSERT INTO `moldes` VALUES ('25', '1058', null, 'produccion');
INSERT INTO `moldes` VALUES ('26', '1071', null, 'produccion');
INSERT INTO `moldes` VALUES ('27', '1074', null, 'produccion');
INSERT INTO `moldes` VALUES ('28', '1087', null, 'produccion');
INSERT INTO `moldes` VALUES ('29', '1088', null, 'produccion');
INSERT INTO `moldes` VALUES ('30', '1089', null, 'produccion');
INSERT INTO `moldes` VALUES ('31', '1091', null, 'produccion');
INSERT INTO `moldes` VALUES ('32', '1093', null, 'produccion');
INSERT INTO `moldes` VALUES ('33', '1095', null, 'produccion');
INSERT INTO `moldes` VALUES ('34', '1096', null, 'produccion');
INSERT INTO `moldes` VALUES ('35', '1098', null, 'produccion');
INSERT INTO `moldes` VALUES ('36', '1106', null, 'produccion');
INSERT INTO `moldes` VALUES ('37', '1192', null, 'produccion');
INSERT INTO `moldes` VALUES ('38', '1193', null, 'produccion');
INSERT INTO `moldes` VALUES ('39', '1196', null, 'produccion');
INSERT INTO `moldes` VALUES ('40', '1205', null, 'produccion');
INSERT INTO `moldes` VALUES ('41', '1206', null, 'produccion');
INSERT INTO `moldes` VALUES ('42', '1208', null, 'produccion');
INSERT INTO `moldes` VALUES ('43', '1212', null, 'produccion');
INSERT INTO `moldes` VALUES ('44', '1220', null, 'produccion');
INSERT INTO `moldes` VALUES ('45', '1234', null, 'produccion');
INSERT INTO `moldes` VALUES ('46', '1243', null, 'produccion');
INSERT INTO `moldes` VALUES ('47', '1246', null, 'produccion');
INSERT INTO `moldes` VALUES ('48', '1247', null, 'produccion');
INSERT INTO `moldes` VALUES ('49', '1248', null, 'produccion');
INSERT INTO `moldes` VALUES ('50', '1262', null, 'produccion');
INSERT INTO `moldes` VALUES ('51', '1263', null, 'produccion');
INSERT INTO `moldes` VALUES ('52', '1267', null, 'produccion');
INSERT INTO `moldes` VALUES ('53', '1274', null, 'produccion');
INSERT INTO `moldes` VALUES ('54', '1275', null, 'produccion');
INSERT INTO `moldes` VALUES ('55', '1278', null, 'produccion');
INSERT INTO `moldes` VALUES ('56', '1282', null, 'produccion');
INSERT INTO `moldes` VALUES ('57', '1302', null, 'produccion');
INSERT INTO `moldes` VALUES ('58', '1363', null, 'produccion');
INSERT INTO `moldes` VALUES ('59', '1364', null, 'produccion');
INSERT INTO `moldes` VALUES ('60', '1365', null, 'produccion');
INSERT INTO `moldes` VALUES ('61', '1366', null, 'produccion');
INSERT INTO `moldes` VALUES ('62', '1367', null, 'produccion');
INSERT INTO `moldes` VALUES ('63', '1368', null, 'produccion');
INSERT INTO `moldes` VALUES ('64', '1369', null, 'produccion');
INSERT INTO `moldes` VALUES ('65', '1370', null, 'produccion');
INSERT INTO `moldes` VALUES ('66', '1371', null, 'produccion');
INSERT INTO `moldes` VALUES ('67', '1372', null, 'produccion');
INSERT INTO `moldes` VALUES ('68', '1375', null, 'produccion');
INSERT INTO `moldes` VALUES ('69', '1376', null, 'produccion');
INSERT INTO `moldes` VALUES ('70', '1377', null, 'produccion');
INSERT INTO `moldes` VALUES ('71', '1378', null, 'produccion');
INSERT INTO `moldes` VALUES ('72', '1380', null, 'produccion');
INSERT INTO `moldes` VALUES ('73', '1381', null, 'produccion');
INSERT INTO `moldes` VALUES ('74', '1382', null, 'produccion');
INSERT INTO `moldes` VALUES ('75', '1383', null, 'produccion');
INSERT INTO `moldes` VALUES ('76', '3134', null, 'produccion');
INSERT INTO `moldes` VALUES ('77', '3405', null, 'produccion');
INSERT INTO `moldes` VALUES ('78', '3406', null, 'produccion');
INSERT INTO `moldes` VALUES ('79', '3407', null, 'produccion');
INSERT INTO `moldes` VALUES ('80', '3412', null, 'produccion');
INSERT INTO `moldes` VALUES ('81', '3413', null, 'produccion');
INSERT INTO `moldes` VALUES ('82', '3414', null, 'produccion');
INSERT INTO `moldes` VALUES ('83', '3415', null, 'produccion');
INSERT INTO `moldes` VALUES ('84', '3445', null, 'produccion');
INSERT INTO `moldes` VALUES ('85', '3446', null, 'produccion');
INSERT INTO `moldes` VALUES ('86', '3456', null, 'produccion');
INSERT INTO `moldes` VALUES ('87', '3458', null, 'produccion');
INSERT INTO `moldes` VALUES ('88', '3481', null, 'produccion');
INSERT INTO `moldes` VALUES ('89', '3600', null, 'produccion');
INSERT INTO `moldes` VALUES ('90', '3607', null, 'produccion');
INSERT INTO `moldes` VALUES ('91', '3636', null, 'produccion');
INSERT INTO `moldes` VALUES ('92', '3731', null, 'produccion');
INSERT INTO `moldes` VALUES ('93', '3732', null, 'produccion');
INSERT INTO `moldes` VALUES ('94', '3733', null, 'produccion');
INSERT INTO `moldes` VALUES ('95', '3735', null, 'produccion');
INSERT INTO `moldes` VALUES ('96', '3736', null, 'produccion');
INSERT INTO `moldes` VALUES ('97', '3741', null, 'produccion');
INSERT INTO `moldes` VALUES ('98', '3880', null, 'produccion');
INSERT INTO `moldes` VALUES ('99', '3881', null, 'produccion');
INSERT INTO `moldes` VALUES ('100', '3890', null, 'produccion');
INSERT INTO `moldes` VALUES ('101', '3891', null, 'produccion');
INSERT INTO `moldes` VALUES ('102', '3892', null, 'produccion');
INSERT INTO `moldes` VALUES ('103', '3893', null, 'produccion');
INSERT INTO `moldes` VALUES ('104', '3920', null, 'produccion');
INSERT INTO `moldes` VALUES ('105', '3921', null, 'produccion');
INSERT INTO `moldes` VALUES ('106', '4122', null, 'produccion');
INSERT INTO `moldes` VALUES ('107', '4128', null, 'produccion');
INSERT INTO `moldes` VALUES ('108', '4129', null, 'produccion');
INSERT INTO `moldes` VALUES ('109', '4131', null, 'produccion');
INSERT INTO `moldes` VALUES ('110', '4166', null, 'produccion');
INSERT INTO `moldes` VALUES ('111', '4167', null, 'produccion');
INSERT INTO `moldes` VALUES ('112', '4168', null, 'produccion');
INSERT INTO `moldes` VALUES ('113', '4169', null, 'produccion');
INSERT INTO `moldes` VALUES ('114', '4204', null, 'produccion');
INSERT INTO `moldes` VALUES ('115', '4205', null, 'produccion');
INSERT INTO `moldes` VALUES ('116', '4206', null, 'produccion');
INSERT INTO `moldes` VALUES ('117', '4215', null, 'produccion');
INSERT INTO `moldes` VALUES ('118', '4216', null, 'produccion');
INSERT INTO `moldes` VALUES ('119', '4217', null, 'produccion');
INSERT INTO `moldes` VALUES ('120', '4218', null, 'produccion');
INSERT INTO `moldes` VALUES ('121', '4219', null, 'produccion');
INSERT INTO `moldes` VALUES ('122', '4220', null, 'produccion');
INSERT INTO `moldes` VALUES ('123', '4222', null, 'produccion');
INSERT INTO `moldes` VALUES ('124', '4748', null, 'produccion');
INSERT INTO `moldes` VALUES ('125', '4752', null, 'produccion');
INSERT INTO `moldes` VALUES ('126', '4820', null, 'produccion');
INSERT INTO `moldes` VALUES ('127', '4887', null, 'produccion');
INSERT INTO `moldes` VALUES ('128', '4904', null, 'produccion');
INSERT INTO `moldes` VALUES ('129', '4905', null, 'produccion');
INSERT INTO `moldes` VALUES ('130', '4906', null, 'produccion');
INSERT INTO `moldes` VALUES ('131', '4907', null, 'produccion');
INSERT INTO `moldes` VALUES ('132', '4908', null, 'produccion');
INSERT INTO `moldes` VALUES ('133', '4909', null, 'produccion');
INSERT INTO `moldes` VALUES ('134', '4910', null, 'produccion');
INSERT INTO `moldes` VALUES ('135', '4911', null, 'produccion');
INSERT INTO `moldes` VALUES ('136', '4912', null, 'produccion');
INSERT INTO `moldes` VALUES ('137', '4913', null, 'produccion');
INSERT INTO `moldes` VALUES ('138', '4914', null, 'produccion');
INSERT INTO `moldes` VALUES ('139', '4915', null, 'produccion');
INSERT INTO `moldes` VALUES ('140', '4943', null, 'produccion');
INSERT INTO `moldes` VALUES ('141', '4944', null, 'produccion');
INSERT INTO `moldes` VALUES ('142', '5161', null, 'produccion');
INSERT INTO `moldes` VALUES ('143', '5416', null, 'produccion');
INSERT INTO `moldes` VALUES ('144', '5417', null, 'produccion');
INSERT INTO `moldes` VALUES ('145', '5418', null, 'produccion');
INSERT INTO `moldes` VALUES ('146', '5419', null, 'produccion');
INSERT INTO `moldes` VALUES ('147', '5422', null, 'produccion');
INSERT INTO `moldes` VALUES ('148', '5424', null, 'produccion');
INSERT INTO `moldes` VALUES ('149', '5431', null, 'produccion');
INSERT INTO `moldes` VALUES ('150', '5660', null, 'produccion');
INSERT INTO `moldes` VALUES ('151', '5709', null, 'produccion');
INSERT INTO `moldes` VALUES ('152', '5834', null, 'produccion');
INSERT INTO `moldes` VALUES ('153', '5896', null, 'produccion');
INSERT INTO `moldes` VALUES ('154', '5934', null, 'produccion');
INSERT INTO `moldes` VALUES ('155', '5935', null, 'produccion');
INSERT INTO `moldes` VALUES ('156', '5936', null, 'produccion');
INSERT INTO `moldes` VALUES ('157', '6212', null, 'produccion');
INSERT INTO `moldes` VALUES ('158', '6488', null, 'produccion');
INSERT INTO `moldes` VALUES ('159', '6489', null, 'produccion');
INSERT INTO `moldes` VALUES ('160', '6490', null, 'produccion');
INSERT INTO `moldes` VALUES ('161', '6491', null, 'produccion');
INSERT INTO `moldes` VALUES ('162', '6524', null, 'produccion');
INSERT INTO `moldes` VALUES ('163', '6726', null, 'produccion');
INSERT INTO `moldes` VALUES ('164', '6739', null, 'produccion');
INSERT INTO `moldes` VALUES ('165', '6759', null, 'produccion');
INSERT INTO `moldes` VALUES ('166', '6788', null, 'produccion');
INSERT INTO `moldes` VALUES ('167', '6789', null, 'produccion');
INSERT INTO `moldes` VALUES ('168', '6855', null, 'produccion');
INSERT INTO `moldes` VALUES ('169', '6860', null, 'produccion');
INSERT INTO `moldes` VALUES ('170', '6863', null, 'produccion');
INSERT INTO `moldes` VALUES ('171', '6864', null, 'produccion');
INSERT INTO `moldes` VALUES ('172', '6872', null, 'produccion');
INSERT INTO `moldes` VALUES ('173', '7009', null, 'produccion');
INSERT INTO `moldes` VALUES ('174', '7041', null, 'produccion');
INSERT INTO `moldes` VALUES ('175', '7250', null, 'produccion');
INSERT INTO `moldes` VALUES ('176', '7255', null, 'produccion');
INSERT INTO `moldes` VALUES ('177', '7263', null, 'produccion');
INSERT INTO `moldes` VALUES ('178', '7317', null, 'produccion');
INSERT INTO `moldes` VALUES ('179', '7452', null, 'produccion');
INSERT INTO `moldes` VALUES ('180', '7453', null, 'produccion');
INSERT INTO `moldes` VALUES ('181', '7460', null, 'produccion');
INSERT INTO `moldes` VALUES ('182', '7461', null, 'produccion');
INSERT INTO `moldes` VALUES ('183', '7880', null, 'produccion');
INSERT INTO `moldes` VALUES ('184', '7881', null, 'produccion');
INSERT INTO `moldes` VALUES ('185', '7908', null, 'produccion');
INSERT INTO `moldes` VALUES ('186', '7909', null, 'produccion');
INSERT INTO `moldes` VALUES ('187', '7910', null, 'produccion');
INSERT INTO `moldes` VALUES ('188', '7913', null, 'produccion');
INSERT INTO `moldes` VALUES ('189', '8074', null, 'produccion');
INSERT INTO `moldes` VALUES ('190', '8075', null, 'produccion');
INSERT INTO `moldes` VALUES ('191', '8617', null, 'produccion');
INSERT INTO `moldes` VALUES ('192', '8945', null, 'produccion');
INSERT INTO `moldes` VALUES ('193', '8947', null, 'produccion');
INSERT INTO `moldes` VALUES ('194', '8948', null, 'produccion');
INSERT INTO `moldes` VALUES ('195', '8949', null, 'produccion');
INSERT INTO `moldes` VALUES ('196', '8950', null, 'produccion');
INSERT INTO `moldes` VALUES ('197', '8951', null, 'produccion');
INSERT INTO `moldes` VALUES ('198', '8954', null, 'produccion');
INSERT INTO `moldes` VALUES ('199', '9327', null, 'produccion');
INSERT INTO `moldes` VALUES ('200', '9999', null, 'produccion');
INSERT INTO `moldes` VALUES ('256', '989898', 'test', 'produccion');

-- ----------------------------
-- Table structure for moldes2
-- ----------------------------
DROP TABLE IF EXISTS `moldes2`;
CREATE TABLE `moldes2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `molde` varchar(15) DEFAULT NULL,
  `descripcion` varchar(55) DEFAULT NULL,
  `nstatus` enum('produccion','reparacion') DEFAULT 'produccion',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of moldes2
-- ----------------------------
INSERT INTO `moldes2` VALUES ('1', '11', null, 'produccion');
INSERT INTO `moldes2` VALUES ('2', '11', null, 'produccion');
INSERT INTO `moldes2` VALUES ('3', '11', null, 'produccion');
INSERT INTO `moldes2` VALUES ('4', '11', null, 'produccion');
INSERT INTO `moldes2` VALUES ('5', '23', null, 'produccion');
INSERT INTO `moldes2` VALUES ('6', '100', null, 'produccion');
INSERT INTO `moldes2` VALUES ('7', '101', null, 'produccion');
INSERT INTO `moldes2` VALUES ('8', '136', null, 'produccion');
INSERT INTO `moldes2` VALUES ('9', '136', null, 'produccion');
INSERT INTO `moldes2` VALUES ('10', '136', null, 'produccion');
INSERT INTO `moldes2` VALUES ('11', '136', null, 'produccion');
INSERT INTO `moldes2` VALUES ('12', '136', null, 'produccion');
INSERT INTO `moldes2` VALUES ('13', '136', null, 'produccion');
INSERT INTO `moldes2` VALUES ('14', '136', null, 'produccion');
INSERT INTO `moldes2` VALUES ('15', '170', null, 'produccion');
INSERT INTO `moldes2` VALUES ('16', '295', null, 'produccion');
INSERT INTO `moldes2` VALUES ('17', '361', null, 'produccion');
INSERT INTO `moldes2` VALUES ('18', '361', null, 'produccion');
INSERT INTO `moldes2` VALUES ('19', '506', null, 'produccion');
INSERT INTO `moldes2` VALUES ('20', '506', null, 'produccion');
INSERT INTO `moldes2` VALUES ('21', '506', null, 'produccion');
INSERT INTO `moldes2` VALUES ('22', '506', null, 'produccion');
INSERT INTO `moldes2` VALUES ('23', '506', null, 'produccion');
INSERT INTO `moldes2` VALUES ('24', '506', null, 'produccion');
INSERT INTO `moldes2` VALUES ('25', '506', null, 'produccion');
INSERT INTO `moldes2` VALUES ('26', '535', null, 'produccion');
INSERT INTO `moldes2` VALUES ('27', '588', null, 'produccion');
INSERT INTO `moldes2` VALUES ('28', '619', null, 'produccion');
INSERT INTO `moldes2` VALUES ('29', '635', null, 'produccion');
INSERT INTO `moldes2` VALUES ('30', '636', null, 'produccion');
INSERT INTO `moldes2` VALUES ('31', '727', null, 'produccion');
INSERT INTO `moldes2` VALUES ('32', '748', null, 'produccion');
INSERT INTO `moldes2` VALUES ('33', '754', null, 'produccion');
INSERT INTO `moldes2` VALUES ('34', '754', null, 'produccion');
INSERT INTO `moldes2` VALUES ('35', '754', null, 'produccion');
INSERT INTO `moldes2` VALUES ('36', '776', null, 'produccion');
INSERT INTO `moldes2` VALUES ('37', '779', null, 'produccion');
INSERT INTO `moldes2` VALUES ('38', '787', null, 'produccion');
INSERT INTO `moldes2` VALUES ('39', '915', null, 'produccion');
INSERT INTO `moldes2` VALUES ('40', '916', null, 'produccion');
INSERT INTO `moldes2` VALUES ('41', '916', null, 'produccion');
INSERT INTO `moldes2` VALUES ('42', '916', null, 'produccion');
INSERT INTO `moldes2` VALUES ('43', '918', null, 'produccion');
INSERT INTO `moldes2` VALUES ('44', '918', null, 'produccion');
INSERT INTO `moldes2` VALUES ('45', '956', null, 'produccion');
INSERT INTO `moldes2` VALUES ('46', '1058', null, 'produccion');
INSERT INTO `moldes2` VALUES ('47', '1071', null, 'produccion');
INSERT INTO `moldes2` VALUES ('48', '1074', null, 'produccion');
INSERT INTO `moldes2` VALUES ('49', '1087', null, 'produccion');
INSERT INTO `moldes2` VALUES ('50', '1087', null, 'produccion');
INSERT INTO `moldes2` VALUES ('51', '1088', null, 'produccion');
INSERT INTO `moldes2` VALUES ('52', '1089', null, 'produccion');
INSERT INTO `moldes2` VALUES ('53', '1089', null, 'produccion');
INSERT INTO `moldes2` VALUES ('54', '1091', null, 'produccion');
INSERT INTO `moldes2` VALUES ('55', '1093', null, 'produccion');
INSERT INTO `moldes2` VALUES ('56', '1095', null, 'produccion');
INSERT INTO `moldes2` VALUES ('57', '1096', null, 'produccion');
INSERT INTO `moldes2` VALUES ('58', '1098', null, 'produccion');
INSERT INTO `moldes2` VALUES ('59', '1098', null, 'produccion');
INSERT INTO `moldes2` VALUES ('60', '1106', null, 'produccion');
INSERT INTO `moldes2` VALUES ('61', '1192', null, 'produccion');
INSERT INTO `moldes2` VALUES ('62', '1192', null, 'produccion');
INSERT INTO `moldes2` VALUES ('63', '1193', null, 'produccion');
INSERT INTO `moldes2` VALUES ('64', '1196', null, 'produccion');
INSERT INTO `moldes2` VALUES ('65', '1205', null, 'produccion');
INSERT INTO `moldes2` VALUES ('66', '1206', null, 'produccion');
INSERT INTO `moldes2` VALUES ('67', '1206', null, 'produccion');
INSERT INTO `moldes2` VALUES ('68', '1208', null, 'produccion');
INSERT INTO `moldes2` VALUES ('69', '1212', null, 'produccion');
INSERT INTO `moldes2` VALUES ('70', '1220', null, 'produccion');
INSERT INTO `moldes2` VALUES ('71', '1234', null, 'produccion');
INSERT INTO `moldes2` VALUES ('72', '1243', null, 'produccion');
INSERT INTO `moldes2` VALUES ('73', '1246', null, 'produccion');
INSERT INTO `moldes2` VALUES ('74', '1247', null, 'produccion');
INSERT INTO `moldes2` VALUES ('75', '1248', null, 'produccion');
INSERT INTO `moldes2` VALUES ('76', '1248', null, 'produccion');
INSERT INTO `moldes2` VALUES ('77', '1262', null, 'produccion');
INSERT INTO `moldes2` VALUES ('78', '1262', null, 'produccion');
INSERT INTO `moldes2` VALUES ('79', '1263', null, 'produccion');
INSERT INTO `moldes2` VALUES ('80', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('81', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('82', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('83', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('84', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('85', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('86', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('87', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('88', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('89', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('90', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('91', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('92', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('93', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('94', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('95', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('96', '1267', null, 'produccion');
INSERT INTO `moldes2` VALUES ('97', '1274', null, 'produccion');
INSERT INTO `moldes2` VALUES ('98', '1275', null, 'produccion');
INSERT INTO `moldes2` VALUES ('99', '1275', null, 'produccion');
INSERT INTO `moldes2` VALUES ('100', '1278', null, 'produccion');
INSERT INTO `moldes2` VALUES ('101', '1278', null, 'produccion');
INSERT INTO `moldes2` VALUES ('102', '1282', null, 'produccion');
INSERT INTO `moldes2` VALUES ('103', '1302', null, 'produccion');
INSERT INTO `moldes2` VALUES ('104', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('105', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('106', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('107', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('108', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('109', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('110', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('111', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('112', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('113', '1363', null, 'produccion');
INSERT INTO `moldes2` VALUES ('114', '1364', null, 'produccion');
INSERT INTO `moldes2` VALUES ('115', '1364', null, 'produccion');
INSERT INTO `moldes2` VALUES ('116', '1365', null, 'produccion');
INSERT INTO `moldes2` VALUES ('117', '1365', null, 'produccion');
INSERT INTO `moldes2` VALUES ('118', '1366', null, 'produccion');
INSERT INTO `moldes2` VALUES ('119', '1367', null, 'produccion');
INSERT INTO `moldes2` VALUES ('120', '1367', null, 'produccion');
INSERT INTO `moldes2` VALUES ('121', '1368', null, 'produccion');
INSERT INTO `moldes2` VALUES ('122', '1368', null, 'produccion');
INSERT INTO `moldes2` VALUES ('123', '1369', null, 'produccion');
INSERT INTO `moldes2` VALUES ('124', '1370', null, 'produccion');
INSERT INTO `moldes2` VALUES ('125', '1371', null, 'produccion');
INSERT INTO `moldes2` VALUES ('126', '1372', null, 'produccion');
INSERT INTO `moldes2` VALUES ('127', '1372', null, 'produccion');
INSERT INTO `moldes2` VALUES ('128', '1375', null, 'produccion');
INSERT INTO `moldes2` VALUES ('129', '1376', null, 'produccion');
INSERT INTO `moldes2` VALUES ('130', '1377', null, 'produccion');
INSERT INTO `moldes2` VALUES ('131', '1378', null, 'produccion');
INSERT INTO `moldes2` VALUES ('132', '1378', null, 'produccion');
INSERT INTO `moldes2` VALUES ('133', '1380', null, 'produccion');
INSERT INTO `moldes2` VALUES ('134', '1380', null, 'produccion');
INSERT INTO `moldes2` VALUES ('135', '1380', null, 'produccion');
INSERT INTO `moldes2` VALUES ('136', '1380', null, 'produccion');
INSERT INTO `moldes2` VALUES ('137', '1381', null, 'produccion');
INSERT INTO `moldes2` VALUES ('138', '1381', null, 'produccion');
INSERT INTO `moldes2` VALUES ('139', '1381', null, 'produccion');
INSERT INTO `moldes2` VALUES ('140', '1381', null, 'produccion');
INSERT INTO `moldes2` VALUES ('141', '1382', null, 'produccion');
INSERT INTO `moldes2` VALUES ('142', '1382', null, 'produccion');
INSERT INTO `moldes2` VALUES ('143', '1382', null, 'produccion');
INSERT INTO `moldes2` VALUES ('144', '1383', null, 'produccion');
INSERT INTO `moldes2` VALUES ('145', '1383', null, 'produccion');
INSERT INTO `moldes2` VALUES ('146', '1383', null, 'produccion');
INSERT INTO `moldes2` VALUES ('147', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('148', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('149', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('150', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('151', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('152', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('153', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('154', '3134', null, 'produccion');
INSERT INTO `moldes2` VALUES ('155', '3405', null, 'produccion');
INSERT INTO `moldes2` VALUES ('156', '3406', null, 'produccion');
INSERT INTO `moldes2` VALUES ('157', '3406', null, 'produccion');
INSERT INTO `moldes2` VALUES ('158', '3407', null, 'produccion');
INSERT INTO `moldes2` VALUES ('159', '3412', null, 'produccion');
INSERT INTO `moldes2` VALUES ('160', '3412', null, 'produccion');
INSERT INTO `moldes2` VALUES ('161', '3413', null, 'produccion');
INSERT INTO `moldes2` VALUES ('162', '3414', null, 'produccion');
INSERT INTO `moldes2` VALUES ('163', '3414', null, 'produccion');
INSERT INTO `moldes2` VALUES ('164', '3414', null, 'produccion');
INSERT INTO `moldes2` VALUES ('165', '3414', null, 'produccion');
INSERT INTO `moldes2` VALUES ('166', '3415', null, 'produccion');
INSERT INTO `moldes2` VALUES ('167', '3445', null, 'produccion');
INSERT INTO `moldes2` VALUES ('168', '3446', null, 'produccion');
INSERT INTO `moldes2` VALUES ('169', '3456', null, 'produccion');
INSERT INTO `moldes2` VALUES ('170', '3458', null, 'produccion');
INSERT INTO `moldes2` VALUES ('171', '3481', null, 'produccion');
INSERT INTO `moldes2` VALUES ('172', '3600', null, 'produccion');
INSERT INTO `moldes2` VALUES ('173', '3607', null, 'produccion');
INSERT INTO `moldes2` VALUES ('174', '3636', null, 'produccion');
INSERT INTO `moldes2` VALUES ('175', '3731', null, 'produccion');
INSERT INTO `moldes2` VALUES ('176', '3731', null, 'produccion');
INSERT INTO `moldes2` VALUES ('177', '3731', null, 'produccion');
INSERT INTO `moldes2` VALUES ('178', '3731', null, 'produccion');
INSERT INTO `moldes2` VALUES ('179', '3731', null, 'produccion');
INSERT INTO `moldes2` VALUES ('180', '3731', null, 'produccion');
INSERT INTO `moldes2` VALUES ('181', '3731', null, 'produccion');
INSERT INTO `moldes2` VALUES ('182', '3732', null, 'produccion');
INSERT INTO `moldes2` VALUES ('183', '3733', null, 'produccion');
INSERT INTO `moldes2` VALUES ('184', '3735', null, 'produccion');
INSERT INTO `moldes2` VALUES ('185', '3736', null, 'produccion');
INSERT INTO `moldes2` VALUES ('186', '3741', null, 'produccion');
INSERT INTO `moldes2` VALUES ('187', '3741', null, 'produccion');
INSERT INTO `moldes2` VALUES ('188', '3880', null, 'produccion');
INSERT INTO `moldes2` VALUES ('189', '3881', null, 'produccion');
INSERT INTO `moldes2` VALUES ('190', '3890', null, 'produccion');
INSERT INTO `moldes2` VALUES ('191', '3891', null, 'produccion');
INSERT INTO `moldes2` VALUES ('192', '3892', null, 'produccion');
INSERT INTO `moldes2` VALUES ('193', '3893', null, 'produccion');
INSERT INTO `moldes2` VALUES ('194', '3920', null, 'produccion');
INSERT INTO `moldes2` VALUES ('195', '3921', null, 'produccion');
INSERT INTO `moldes2` VALUES ('196', '4122', null, 'produccion');
INSERT INTO `moldes2` VALUES ('197', '4128', null, 'produccion');
INSERT INTO `moldes2` VALUES ('198', '4128', null, 'produccion');
INSERT INTO `moldes2` VALUES ('199', '4128', null, 'produccion');
INSERT INTO `moldes2` VALUES ('200', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('201', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('202', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('203', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('204', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('205', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('206', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('207', '4129', null, 'produccion');
INSERT INTO `moldes2` VALUES ('208', '4131', null, 'produccion');
INSERT INTO `moldes2` VALUES ('209', '4166', null, 'produccion');
INSERT INTO `moldes2` VALUES ('210', '4166', null, 'produccion');
INSERT INTO `moldes2` VALUES ('211', '4167', null, 'produccion');
INSERT INTO `moldes2` VALUES ('212', '4168', null, 'produccion');
INSERT INTO `moldes2` VALUES ('213', '4169', null, 'produccion');
INSERT INTO `moldes2` VALUES ('214', '4169', null, 'produccion');
INSERT INTO `moldes2` VALUES ('215', '4204', null, 'produccion');
INSERT INTO `moldes2` VALUES ('216', '4205', null, 'produccion');
INSERT INTO `moldes2` VALUES ('217', '4206', null, 'produccion');
INSERT INTO `moldes2` VALUES ('218', '4215', null, 'produccion');
INSERT INTO `moldes2` VALUES ('219', '4216', null, 'produccion');
INSERT INTO `moldes2` VALUES ('220', '4217', null, 'produccion');
INSERT INTO `moldes2` VALUES ('221', '4218', null, 'produccion');
INSERT INTO `moldes2` VALUES ('222', '4219', null, 'produccion');
INSERT INTO `moldes2` VALUES ('223', '4219', null, 'produccion');
INSERT INTO `moldes2` VALUES ('224', '4219', null, 'produccion');
INSERT INTO `moldes2` VALUES ('225', '4219', null, 'produccion');
INSERT INTO `moldes2` VALUES ('226', '4220', null, 'produccion');
INSERT INTO `moldes2` VALUES ('227', '4220', null, 'produccion');
INSERT INTO `moldes2` VALUES ('228', '4222', null, 'produccion');
INSERT INTO `moldes2` VALUES ('229', '4222', null, 'produccion');
INSERT INTO `moldes2` VALUES ('230', '4748', null, 'produccion');
INSERT INTO `moldes2` VALUES ('231', '4752', null, 'produccion');
INSERT INTO `moldes2` VALUES ('232', '4752', null, 'produccion');
INSERT INTO `moldes2` VALUES ('233', '4752', null, 'produccion');
INSERT INTO `moldes2` VALUES ('234', '4752', null, 'produccion');
INSERT INTO `moldes2` VALUES ('235', '4820', null, 'produccion');
INSERT INTO `moldes2` VALUES ('236', '4887', null, 'produccion');
INSERT INTO `moldes2` VALUES ('237', '4887', null, 'produccion');
INSERT INTO `moldes2` VALUES ('238', '4904', null, 'produccion');
INSERT INTO `moldes2` VALUES ('239', '4905', null, 'produccion');
INSERT INTO `moldes2` VALUES ('240', '4906', null, 'produccion');
INSERT INTO `moldes2` VALUES ('241', '4906', null, 'produccion');
INSERT INTO `moldes2` VALUES ('242', '4907', null, 'produccion');
INSERT INTO `moldes2` VALUES ('243', '4907', null, 'produccion');
INSERT INTO `moldes2` VALUES ('244', '4907', null, 'produccion');
INSERT INTO `moldes2` VALUES ('245', '4908', null, 'produccion');
INSERT INTO `moldes2` VALUES ('246', '4909', null, 'produccion');
INSERT INTO `moldes2` VALUES ('247', '4909', null, 'produccion');
INSERT INTO `moldes2` VALUES ('248', '4910', null, 'produccion');
INSERT INTO `moldes2` VALUES ('249', '4911', null, 'produccion');
INSERT INTO `moldes2` VALUES ('250', '4912', null, 'produccion');
INSERT INTO `moldes2` VALUES ('251', '4913', null, 'produccion');
INSERT INTO `moldes2` VALUES ('252', '4914', null, 'produccion');
INSERT INTO `moldes2` VALUES ('253', '4914', null, 'produccion');
INSERT INTO `moldes2` VALUES ('254', '4915', null, 'produccion');
INSERT INTO `moldes2` VALUES ('255', '4915', null, 'produccion');
INSERT INTO `moldes2` VALUES ('256', '4943', null, 'produccion');
INSERT INTO `moldes2` VALUES ('257', '4943', null, 'produccion');
INSERT INTO `moldes2` VALUES ('258', '4944', null, 'produccion');
INSERT INTO `moldes2` VALUES ('259', '5161', null, 'produccion');
INSERT INTO `moldes2` VALUES ('260', '5416', null, 'produccion');
INSERT INTO `moldes2` VALUES ('261', '5417', null, 'produccion');
INSERT INTO `moldes2` VALUES ('262', '5417', null, 'produccion');
INSERT INTO `moldes2` VALUES ('263', '5417', null, 'produccion');
INSERT INTO `moldes2` VALUES ('264', '5418', null, 'produccion');
INSERT INTO `moldes2` VALUES ('265', '5419', null, 'produccion');
INSERT INTO `moldes2` VALUES ('266', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('267', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('268', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('269', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('270', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('271', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('272', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('273', '5422', null, 'produccion');
INSERT INTO `moldes2` VALUES ('274', '5424', null, 'produccion');
INSERT INTO `moldes2` VALUES ('275', '5424', null, 'produccion');
INSERT INTO `moldes2` VALUES ('276', '5424', null, 'produccion');
INSERT INTO `moldes2` VALUES ('277', '5424', null, 'produccion');
INSERT INTO `moldes2` VALUES ('278', '5431', null, 'produccion');
INSERT INTO `moldes2` VALUES ('279', '5660', null, 'produccion');
INSERT INTO `moldes2` VALUES ('280', '5709', null, 'produccion');
INSERT INTO `moldes2` VALUES ('281', '5709', null, 'produccion');
INSERT INTO `moldes2` VALUES ('282', '5709', null, 'produccion');
INSERT INTO `moldes2` VALUES ('283', '5834', null, 'produccion');
INSERT INTO `moldes2` VALUES ('284', '5896', null, 'produccion');
INSERT INTO `moldes2` VALUES ('285', '5896', null, 'produccion');
INSERT INTO `moldes2` VALUES ('286', '5934', null, 'produccion');
INSERT INTO `moldes2` VALUES ('287', '5934', null, 'produccion');
INSERT INTO `moldes2` VALUES ('288', '5934', null, 'produccion');
INSERT INTO `moldes2` VALUES ('289', '5935', null, 'produccion');
INSERT INTO `moldes2` VALUES ('290', '5936', null, 'produccion');
INSERT INTO `moldes2` VALUES ('291', '5936', null, 'produccion');
INSERT INTO `moldes2` VALUES ('292', '5936', null, 'produccion');
INSERT INTO `moldes2` VALUES ('293', '6212', null, 'produccion');
INSERT INTO `moldes2` VALUES ('294', '6488', null, 'produccion');
INSERT INTO `moldes2` VALUES ('295', '6489', null, 'produccion');
INSERT INTO `moldes2` VALUES ('296', '6490', null, 'produccion');
INSERT INTO `moldes2` VALUES ('297', '6490', null, 'produccion');
INSERT INTO `moldes2` VALUES ('298', '6491', null, 'produccion');
INSERT INTO `moldes2` VALUES ('299', '6524', null, 'produccion');
INSERT INTO `moldes2` VALUES ('300', '6726', null, 'produccion');
INSERT INTO `moldes2` VALUES ('301', '6739', null, 'produccion');
INSERT INTO `moldes2` VALUES ('302', '6759', null, 'produccion');
INSERT INTO `moldes2` VALUES ('303', '6788', null, 'produccion');
INSERT INTO `moldes2` VALUES ('304', '6789', null, 'produccion');
INSERT INTO `moldes2` VALUES ('305', '6789', null, 'produccion');
INSERT INTO `moldes2` VALUES ('306', '6855', null, 'produccion');
INSERT INTO `moldes2` VALUES ('307', '6860', null, 'produccion');
INSERT INTO `moldes2` VALUES ('308', '6860', null, 'produccion');
INSERT INTO `moldes2` VALUES ('309', '6863', null, 'produccion');
INSERT INTO `moldes2` VALUES ('310', '6864', null, 'produccion');
INSERT INTO `moldes2` VALUES ('311', '6872', null, 'produccion');
INSERT INTO `moldes2` VALUES ('312', '7009', null, 'produccion');
INSERT INTO `moldes2` VALUES ('313', '7041', null, 'produccion');
INSERT INTO `moldes2` VALUES ('314', '7041', null, 'produccion');
INSERT INTO `moldes2` VALUES ('315', '7250', null, 'produccion');
INSERT INTO `moldes2` VALUES ('316', '7255', null, 'produccion');
INSERT INTO `moldes2` VALUES ('317', '7263', null, 'produccion');
INSERT INTO `moldes2` VALUES ('318', '7317', null, 'produccion');
INSERT INTO `moldes2` VALUES ('319', '7317', null, 'produccion');
INSERT INTO `moldes2` VALUES ('320', '7317', null, 'produccion');
INSERT INTO `moldes2` VALUES ('321', '7317', null, 'produccion');
INSERT INTO `moldes2` VALUES ('322', '7452', null, 'produccion');
INSERT INTO `moldes2` VALUES ('323', '7453', null, 'produccion');
INSERT INTO `moldes2` VALUES ('324', '7453', null, 'produccion');
INSERT INTO `moldes2` VALUES ('325', '7453', null, 'produccion');
INSERT INTO `moldes2` VALUES ('326', '7460', null, 'produccion');
INSERT INTO `moldes2` VALUES ('327', '7460', null, 'produccion');
INSERT INTO `moldes2` VALUES ('328', '7460', null, 'produccion');
INSERT INTO `moldes2` VALUES ('329', '7460', null, 'produccion');
INSERT INTO `moldes2` VALUES ('330', '7461', null, 'produccion');
INSERT INTO `moldes2` VALUES ('331', '7880', null, 'produccion');
INSERT INTO `moldes2` VALUES ('332', '7881', null, 'produccion');
INSERT INTO `moldes2` VALUES ('333', '7908', null, 'produccion');
INSERT INTO `moldes2` VALUES ('334', '7909', null, 'produccion');
INSERT INTO `moldes2` VALUES ('335', '7910', null, 'produccion');
INSERT INTO `moldes2` VALUES ('336', '7910', null, 'produccion');
INSERT INTO `moldes2` VALUES ('337', '7913', null, 'produccion');
INSERT INTO `moldes2` VALUES ('338', '8074', null, 'produccion');
INSERT INTO `moldes2` VALUES ('339', '8075', null, 'produccion');
INSERT INTO `moldes2` VALUES ('340', '8617', null, 'produccion');
INSERT INTO `moldes2` VALUES ('341', '8945', null, 'produccion');
INSERT INTO `moldes2` VALUES ('342', '8947', null, 'produccion');
INSERT INTO `moldes2` VALUES ('343', '8948', null, 'produccion');
INSERT INTO `moldes2` VALUES ('344', '8949', null, 'produccion');
INSERT INTO `moldes2` VALUES ('345', '8950', null, 'produccion');
INSERT INTO `moldes2` VALUES ('346', '8951', null, 'produccion');
INSERT INTO `moldes2` VALUES ('347', '8954', null, 'produccion');
INSERT INTO `moldes2` VALUES ('348', '9327', null, 'produccion');
INSERT INTO `moldes2` VALUES ('349', '9327', null, 'produccion');
INSERT INTO `moldes2` VALUES ('350', '9327', null, 'produccion');
INSERT INTO `moldes2` VALUES ('351', '9327', null, 'produccion');
INSERT INTO `moldes2` VALUES ('352', '9327', null, 'produccion');
INSERT INTO `moldes2` VALUES ('353', '9327', null, 'produccion');
INSERT INTO `moldes2` VALUES ('354', '9999', null, 'produccion');

-- ----------------------------
-- Table structure for partes
-- ----------------------------
DROP TABLE IF EXISTS `partes`;
CREATE TABLE `partes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `num_parte` varchar(12) DEFAULT NULL,
  `descripcion` varchar(155) DEFAULT NULL,
  `familia` int(11) DEFAULT NULL,
  `division` enum('Proprietary','HCM') DEFAULT 'Proprietary',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of partes
-- ----------------------------
INSERT INTO `partes` VALUES ('1', '2013-06-14 16:44:33', '117000', 'Body -  Angle - 1.0 -  ASV -  NPT -  NPT', '1', 'Proprietary');
INSERT INTO `partes` VALUES ('3', '2013-06-14 15:01:47', '117010', 'Body -  Angle - 1.0 -  ASV -  Slip -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('4', '2013-06-14 15:01:47', '117015', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('5', '2013-06-14 15:01:47', '117020', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('6', '2013-06-14 15:01:47', '117100', 'PASV Housing 1.0 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('7', '2013-06-14 15:01:47', '117105', 'PASV Housing 1.0 in Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('8', '2013-06-14 15:01:47', '117200', 'PASV Housing 3/4 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('9', '2013-06-14 15:01:47', '117205', 'PASV Housing 3/4 in Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('10', '2013-06-14 15:01:47', '123456', 'Miscellaneous equipment', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('11', '2013-06-14 15:01:47', '133800', 'Drip Zone bonnet filter', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('12', '2013-06-14 15:01:47', '138900', 'Body -  Angle PGV 1.0', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('13', '2013-06-14 15:01:47', '154100', 'Housing -  1 in X 3/4 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('14', '2013-06-14 15:01:47', '154105', 'Housing -  1 in X 1 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('15', '2013-06-14 15:01:47', '154110', 'Housing -  3/4 in X 3/4 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('16', '2013-06-14 15:01:47', '154200', 'Body -  PGV -  ASV -  1.0 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('17', '2013-06-14 15:01:47', '154205', 'Body -  PGV -  ASV -  1.0 in Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('18', '2013-06-14 15:01:47', '154210', 'Body -  PGV -  ASV -  3/4 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('19', '2013-06-14 15:01:47', '154215', 'Body -  PGV -  ASV -  3/4 in Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('20', '2013-06-14 15:01:47', '154300', 'PASV -  Bonnet -  1 Inch', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('21', '2013-06-14 15:01:47', '162300', 'Riser -  2 in -  PSU', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('22', '2013-06-14 15:01:47', '162600', 'Body Cap -  PSU', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('23', '2013-06-14 15:01:47', '162601', 'Body Cap, PSU, OEM', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('24', '2013-06-14 15:01:47', '163100', 'Riser -  6 in -  PSU', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('25', '2013-06-14 15:01:47', '163200', 'Body -  6 in -  PSU', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('26', '2013-06-14 15:01:47', '179100', 'Body -  PGH -  12 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('27', '2013-06-14 15:01:47', '179200', 'Riser -  PGH -  12 in Lower', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('28', '2013-06-14 15:01:47', '179900', 'Body -  PS -  2 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('29', '2013-06-14 15:01:47', '187200', 'Cap -  Housing -  0.75 in -  ASV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('30', '2013-06-14 15:01:47', '187300', 'Cap -  Housing -  1 in -  PGV ASV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('31', '2013-06-14 15:01:47', '192000', 'Body -  PS -  4 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('32', '2013-06-14 15:01:47', '229200', 'Body -  I20 -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('33', '2013-06-14 15:01:47', '242400', 'Body -  Upper -  Accu-Sync', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('34', '2013-06-14 15:01:47', '243200', 'Body -  Lower -  Accu-Sync', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('35', '2013-06-14 15:01:47', '243300', 'Swivel -  Accu-Sync', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('36', '2013-06-14 15:01:47', '243400', 'Knob -  Adjustment -  Accu-Sync -  ADJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('37', '2013-06-14 15:01:47', '247300', 'Body -  Inlet -  Accu-Sunc', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('38', '2013-06-14 15:01:47', '247600', 'Cover -  AccuSync Adj', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('39', '2013-06-14 15:01:47', '252500', 'Seal -  Riser -  I-20', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('40', '2013-06-14 15:01:47', '255200', 'Seal -  Cover -  AccuSync', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('41', '2013-06-14 15:01:47', '259700', 'Fitting -  Barb -  Swinglet -  Blk', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('42', '2013-06-14 15:01:47', '259800', 'Elbow -  � in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('43', '2013-06-14 15:01:47', '259800', 'Elbow -  � in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('44', '2013-06-14 15:01:47', '260300', 'Adaptor � in Threaded', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('45', '2013-06-14 15:01:47', '269200', 'Handle -  FlowCtrl -  HPV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('46', '2013-06-14 15:01:47', '271100', 'Body -  SRM -  4 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('47', '2013-06-14 15:01:47', '273100', 'Riser -  SRM -  12 in -  Lower', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('48', '2013-06-14 15:01:47', '275000', 'Lens. Clear -  L42A Step', '32', 'HCM');
INSERT INTO `partes` VALUES ('49', '2013-06-14 15:01:47', '283100', 'Riser -  LT 1.5 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('50', '2013-06-14 15:01:47', '292300', 'Diaphragm -  Pilot Valve', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('51', '2013-06-14 15:01:47', '292800', 'Support -  Diaph -  Pilot Valve', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('52', '2013-06-14 15:01:47', '296501', 'Bonnet -  HPV -  Blk', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('53', '2013-06-14 15:01:47', '296605', 'Body -  HPV -  Globe -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('54', '2013-06-14 15:01:47', '296610', 'Body -  HPV -  Globe -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('55', '2013-06-14 15:01:47', '296615', 'Body -  HPV -  Globe -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('56', '2013-06-14 15:01:47', '326400', 'Seal -  Riser -  SRM', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('57', '2013-06-14 15:01:47', '326600', 'Riser -  SRM -  4in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('58', '2013-06-14 15:01:47', '328700', '1.5\" & 2\" IBV Seal Support', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('59', '2013-06-14 15:01:47', '330900', 'Diaphragm -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('60', '2013-06-14 15:01:47', '330900', 'Diaphragm -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('61', '2013-06-14 15:01:47', '331000', 'Body -  SRV -  1 in -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('62', '2013-06-14 15:01:47', '331000', 'Body -  SRV -  1 in -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('63', '2013-06-14 15:01:47', '331005', 'Body -  SRV -  1 in -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('64', '2013-06-14 15:01:47', '331005', 'Body -  SRV -  1 in -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('65', '2013-06-14 15:01:47', '331010', 'Body -  SRV -  1 in -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('66', '2013-06-14 15:01:47', '331010', 'Body -  SRV -  1 in -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('67', '2013-06-14 15:01:47', '331200', 'Band -  Diaph -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('68', '2013-06-14 15:01:47', '331300', 'Support -  Diaph -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('69', '2013-06-14 15:01:47', '331400', 'Screen -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('70', '2013-06-14 15:01:47', '331500', 'Plate -  Friction -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('71', '2013-06-14 15:01:47', '339600', 'Body -  HPV -  Angle -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('72', '2013-06-14 15:01:47', '339600', 'Body -  HPV -  Angle -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('73', '2013-06-14 15:01:47', '339605', 'Body -  HPV -  Angle -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('74', '2013-06-14 15:01:47', '339605', 'Body -  HPV -  Angle -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('75', '2013-06-14 15:01:47', '342400', 'Elbow -  3/4 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('76', '2013-06-14 15:01:47', '352700', 'Stem -  Flow Contl -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('77', '2013-06-14 15:01:47', '356000', 'Body Cap -  I20U', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('78', '2013-06-14 15:01:47', '356200', 'Riser- 4 inch I-20U, PL', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('79', '2013-06-14 15:01:47', '356200', 'Riser- 4 inch I-20U, PL', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('80', '2013-06-14 15:01:47', '358500', 'Knob -  Flow Contl -  SRV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('81', '2013-06-14 15:01:47', '365500', 'Body -  2.0 -  ICV -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('82', '2013-06-14 15:01:47', '365505', 'Body -  2.0 -  ICV -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('83', '2013-06-14 15:01:47', '365600', 'Bonnet -  ICV 1.5 in & 2.0 in', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('84', '2013-06-14 15:01:47', '365900', 'Support -  Diaphragm -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('85', '2013-06-14 15:01:47', '368200', 'Seal -  Inlet -  ICV -  Reg', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('86', '2013-06-14 15:01:47', '368300', 'Carrier -  Nut -  ICV -  Reg', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('87', '2013-06-14 15:01:47', '368305', 'Carrier Nut -  G900 Regulator', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('88', '2013-06-14 15:01:47', '368500', 'Knob -  Adjustment -  ICV -  Reg', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('89', '2013-06-14 15:01:47', '368700', 'Housing -  Diaphragm -  ICV -  Reg', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('90', '2013-06-14 15:01:47', '368900', 'Body -  Lower -  ICV -  Reg', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('91', '2013-06-14 15:01:47', '369000', 'Body -  Upper -  ICV -  Reg', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('92', '2013-06-14 15:01:47', '369200', 'Bonnet -  No Flow Control', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('93', '2013-06-14 15:01:47', '369300', 'Bonnet -  Flow Control', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('94', '2013-06-14 15:01:47', '370100', 'Body -  1.5 -   ICV -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('95', '2013-06-14 15:01:47', '370105', 'Body -  1.5 -  ICV -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('96', '2013-06-14 15:01:47', '385300', 'Body 1.0 -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('97', '2013-06-14 15:01:47', '385305', 'Body 1.0 -  ICV -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('98', '2013-06-14 15:01:47', '385400', 'Bonnet 1.0 -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('99', '2013-06-14 15:01:47', '385700', 'Support -  Diaph -  1.0 -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('100', '2013-06-14 15:01:47', '385800', 'Top -  Diaph -  1.0 -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('101', '2013-06-14 15:01:47', '386000', 'Nut -  Flow Control -  1in -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('102', '2013-06-14 15:01:47', '386500', 'Wiper -  1.0 -  ICV -  FS', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('103', '2013-06-14 15:01:47', '390902', 'Plastic Cap -  Top -  8388001', '32', 'HCM');
INSERT INTO `partes` VALUES ('104', '2013-06-14 15:01:47', '390902', 'Plastic Cap -  Top -  8388001', '32', 'HCM');
INSERT INTO `partes` VALUES ('105', '2013-06-14 15:01:47', '390903', 'Plastic Cap -  Top -  8388101', '32', 'HCM');
INSERT INTO `partes` VALUES ('106', '2013-06-14 15:01:47', '390904', 'Plastic Cap -  Top -  8392001', '32', 'HCM');
INSERT INTO `partes` VALUES ('107', '2013-06-14 15:01:47', '390904', 'Plastic Cap -  Top -  8392001', '32', 'HCM');
INSERT INTO `partes` VALUES ('108', '2013-06-14 15:01:47', '390905', 'Plastic Cap -  Top -  8392101', '32', 'HCM');
INSERT INTO `partes` VALUES ('109', '2013-06-14 15:01:47', '390940', 'Plastic Cap -  Top -  83890-01', '32', 'HCM');
INSERT INTO `partes` VALUES ('110', '2013-06-14 15:01:47', '390943', 'Case -  Bottom -  83893-01', '32', 'HCM');
INSERT INTO `partes` VALUES ('111', '2013-06-14 15:01:47', '390943', 'Case -  Bottom -  83893-01', '32', 'HCM');
INSERT INTO `partes` VALUES ('112', '2013-06-14 15:01:47', '391120', 'Barrel Berol Black 1777281', '32', 'HCM');
INSERT INTO `partes` VALUES ('113', '2013-06-14 15:01:47', '391120', 'Barrel Berol Black 1777281', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('114', '2013-06-14 15:01:47', '391121', 'Barrel Berol FL Yellow 1777282', '32', 'HCM');
INSERT INTO `partes` VALUES ('115', '2013-06-14 15:01:47', '391121', 'Barrel Berol FL Yellow 1777282', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('116', '2013-06-14 15:01:47', '391122', 'Barrel Berol FL Green 1777283', '32', 'HCM');
INSERT INTO `partes` VALUES ('117', '2013-06-14 15:01:47', '391122', 'Barrel Berol FL Green 1777283', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('118', '2013-06-14 15:01:47', '391123', 'Barrel Berol FL Orange 1777284', '32', 'HCM');
INSERT INTO `partes` VALUES ('119', '2013-06-14 15:01:47', '391123', 'Barrel Berol FL Orange 1777284', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('120', '2013-06-14 15:01:47', '391124', 'Barrel Berol FL Pink 1777285', '32', 'HCM');
INSERT INTO `partes` VALUES ('121', '2013-06-14 15:01:47', '391124', 'Barrel Berol FL Pink 1777285', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('122', '2013-06-14 15:01:47', '391125', 'Barrel Berol FL Blue 1777286', '32', 'HCM');
INSERT INTO `partes` VALUES ('123', '2013-06-14 15:01:47', '391125', 'Barrel Berol FL Blue 1777286', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('124', '2013-06-14 15:01:47', '391126', 'Barrel Berol White 1777287', '32', 'HCM');
INSERT INTO `partes` VALUES ('125', '2013-06-14 15:01:47', '391126', 'Barrel Berol White 1777287', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('126', '2013-06-14 15:01:47', '391139', 'Clip Holder MI55', '32', 'HCM');
INSERT INTO `partes` VALUES ('127', '2013-06-14 15:01:47', '391148', 'Esterbrook Cap Black 1794810', '32', 'HCM');
INSERT INTO `partes` VALUES ('128', '2013-06-14 15:01:47', '391148', 'Esterbrook Cap Black 1794810', '32', 'HCM');
INSERT INTO `partes` VALUES ('129', '2013-06-14 15:01:47', '391149', 'Esterbrook Cap Blue 1794901', '32', 'HCM');
INSERT INTO `partes` VALUES ('130', '2013-06-14 15:01:47', '391149', 'Esterbrook Cap Blue 1794901', '32', 'HCM');
INSERT INTO `partes` VALUES ('131', '2013-06-14 15:01:47', '391150', 'Esterbrook Cap Green 1794902', '32', 'HCM');
INSERT INTO `partes` VALUES ('132', '2013-06-14 15:01:47', '391150', 'Esterbrook Cap Green 1794902', '32', 'HCM');
INSERT INTO `partes` VALUES ('133', '2013-06-14 15:01:47', '391151', 'Esterbrook Cap Red 14794903', '32', 'HCM');
INSERT INTO `partes` VALUES ('134', '2013-06-14 15:01:47', '391151', 'Esterbrook Cap Red 14794903', '32', 'HCM');
INSERT INTO `partes` VALUES ('135', '2013-06-14 15:01:47', '391174', 'Stand Base -  Montrose -  86863-01', '32', 'HCM');
INSERT INTO `partes` VALUES ('136', '2013-06-14 15:01:47', '391175', 'Stand Holder -  Montrose -  86864-01', '32', 'HCM');
INSERT INTO `partes` VALUES ('137', '2013-06-14 15:01:47', '391178', 'Clear Top -  Saphire -  86739-01', '32', 'HCM');
INSERT INTO `partes` VALUES ('138', '2013-06-14 15:01:47', '391609', 'Mirror Gasket Camry RH & LH', '32', 'HCM');
INSERT INTO `partes` VALUES ('139', '2013-06-14 15:01:47', '391613', 'Cap top PSD', '32', 'HCM');
INSERT INTO `partes` VALUES ('140', '2013-06-14 15:01:47', '391635', '1813708 Adapter Ink Joy -  Black', '32', 'HCM');
INSERT INTO `partes` VALUES ('141', '2013-06-14 15:01:47', '391636', '1813709 Adapter Ink Joy -  Blue', '32', 'HCM');
INSERT INTO `partes` VALUES ('142', '2013-06-14 15:01:47', '391637', '1813710 Adapter Ink Joy -  Red', '32', 'HCM');
INSERT INTO `partes` VALUES ('143', '2013-06-14 15:01:47', '391995', 'Plantronics Moorea Top Cover 87780', '32', 'HCM');
INSERT INTO `partes` VALUES ('144', '2013-06-14 15:01:47', '391995', 'Plantronics Moorea Top Cover 87780', '32', 'HCM');
INSERT INTO `partes` VALUES ('145', '2013-06-14 15:01:47', '392373', 'Avalon Gasket', '32', 'HCM');
INSERT INTO `partes` VALUES ('146', '2013-06-14 15:01:47', '392374', '1852611 Barrel Inkjoy', '32', 'HCM');
INSERT INTO `partes` VALUES ('147', '2013-06-14 15:01:47', '392375', '1854172 Endplug Inkjoy Black', '32', 'HCM');
INSERT INTO `partes` VALUES ('148', '2013-06-14 15:01:47', '392376', '1854173 Endplug Inkjoy Blue', '32', 'HCM');
INSERT INTO `partes` VALUES ('149', '2013-06-14 15:01:47', '392377', '1854174 Endplug Inkjoy Red', '32', 'HCM');
INSERT INTO `partes` VALUES ('150', '2013-06-14 15:01:47', '392378', '1854161 Cap Inkjoy Black', '32', 'HCM');
INSERT INTO `partes` VALUES ('151', '2013-06-14 15:01:47', '392379', '1854167 Cap Inkjoy Blue', '32', 'HCM');
INSERT INTO `partes` VALUES ('152', '2013-06-14 15:01:47', '392385', 'Camry New Gasket', '32', 'HCM');
INSERT INTO `partes` VALUES ('153', '2013-06-14 15:01:47', '392408', '1854169 Cap Inkjoy Red', '32', 'HCM');
INSERT INTO `partes` VALUES ('154', '2013-06-14 15:01:47', '393107', 'Back beat go holder', '32', 'HCM');
INSERT INTO `partes` VALUES ('155', '2013-06-14 15:01:47', '394014', 'Plastic Cap -  Middle -  83891-02', '32', 'HCM');
INSERT INTO `partes` VALUES ('156', '2013-06-14 15:01:47', '394016', 'Headset -  Stand -  83892-02', '32', 'HCM');
INSERT INTO `partes` VALUES ('157', '2013-06-14 15:01:47', '401800', 'Body -  Block Rotor -  1 � ACME', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('158', '2013-06-14 15:01:47', '404100', 'Insert, Threaded, ACME 1.25 Blk', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('159', '2013-06-14 15:01:47', '404200', 'Body -  I-20 -  12 in.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('160', '2013-06-14 15:01:47', '406700', 'Piston -  .75 -  ASV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('161', '2013-06-14 15:01:47', '406900', 'Cap -  Housing -  .75 -  ASV (obsolete)', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('162', '2013-06-14 15:01:47', '407005', 'Body -  Angle - 1.0 -  ASV -  Slip -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('163', '2013-06-14 15:01:47', '407010', 'Body -  Angle - 1.0 -  ASV -  Slip -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('164', '2013-06-14 15:01:47', '407015', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('165', '2013-06-14 15:01:47', '407020', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('166', '2013-06-14 15:01:47', '407400', 'Lid -  Cap -  .75 -  ASV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('167', '2013-06-14 15:01:47', '409300', 'Cap -  Housing -  1.0 -  ASV (obsolete)', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('168', '2013-06-14 15:01:47', '409400', 'Piston -  1.0 -  ASV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('169', '2013-06-14 15:01:47', '409600', 'Lid -  Cap -  1.0 -  ASV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('170', '2013-06-14 15:01:47', '409605', 'Lid -  Cap -  1.0 -  AVB', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('171', '2013-06-14 15:01:47', '412400', 'Body -  1.5 -  PGV NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('172', '2013-06-14 15:01:47', '412405', 'Body PGV 1.5 BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('173', '2013-06-14 15:01:47', '412500', 'Bonnet -  1.5 -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('174', '2013-06-14 15:01:47', '413100', 'Support -  Diaph -  1.5 -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('175', '2013-06-14 15:01:47', '413500', 'Support -  Seal -  1.5 -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('176', '2013-06-14 15:01:47', '413800', 'Plug -  Port -  1.5 -  NPT -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('177', '2013-06-14 15:01:47', '413805', 'Plug -  Port -  1.5 -  BSP -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('178', '2013-06-14 15:01:47', '414300', 'Body -  2.0 -  PGV -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('179', '2013-06-14 15:01:47', '414305', 'Body -  2.0 -  PGV -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('180', '2013-06-14 15:01:47', '414400', 'Bonnet -  2.0 -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('181', '2013-06-14 15:01:47', '415300', 'Plug -  Port -  2.0 -  PGV -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('182', '2013-06-14 15:01:47', '415305', 'Plug -  Port -  2.0 -  PGV -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('183', '2013-06-14 15:01:47', '416700', 'Seal -  1.5 -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('184', '2013-06-14 15:01:47', '416800', 'Seal -  2in -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('185', '2013-06-14 15:01:47', '423900', 'Plug -  Flush -  PROS', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('186', '2013-06-14 15:01:47', '423905', 'Plug -  Flush -  INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('187', '2013-06-14 15:01:47', '424000', 'Body 4 in PROS PRS', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('188', '2013-06-14 15:01:47', '424005', 'Body -  4 in OEM -  MPR40', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('189', '2013-06-14 15:01:47', '431000', 'Clip -  Regulator', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('190', '2013-06-14 15:01:47', '431100', 'Stem -  Regulator', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('191', '2013-06-14 15:01:47', '431300', 'Valve -  Regulator', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('192', '2013-06-14 15:01:47', '433400', 'Body - Male/Male - NPT - 1in - PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('193', '2013-06-14 15:01:47', '433400', 'Body - Male/Male - NPT - 1in - PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('194', '2013-06-14 15:01:47', '433405', 'Body - Male/Male - BSP -  1in -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('195', '2013-06-14 15:01:47', '433405', 'Body - Male/Male - BSP -  1in -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('196', '2013-06-14 15:01:47', '440000', 'Switch Housing', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('197', '2013-06-14 15:01:47', '440100', 'Vent Ring', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('198', '2013-06-14 15:01:47', '440300', 'Cap -  Mini-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('199', '2013-06-14 15:01:47', '440500', 'Spindle -  Mini-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('200', '2013-06-14 15:01:47', '441900', 'Bypass Housing', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('201', '2013-06-14 15:01:47', '442000', 'Bypass Cover', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('202', '2013-06-14 15:01:47', '442100', 'Housing -  Wind Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('203', '2013-06-14 15:01:47', '442200', 'Control Base -  Wind Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('204', '2013-06-14 15:01:47', '442300', 'Wind Vane -  Wind-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('205', '2013-06-14 15:01:47', '442400', 'Hub -  Mini Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('206', '2013-06-14 15:01:47', '442600', 'Pivot -  Bottom -  Wind-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('207', '2013-06-14 15:01:47', '442900', 'Pivot -  Top -  Wind-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('208', '2013-06-14 15:01:47', '443700', 'Acutuator Cam -  Wind-clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('209', '2013-06-14 15:01:47', '443800', 'Level Arm -  Switching -  Wind-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('210', '2013-06-14 15:01:47', '443900', 'Level Reset Spring -  Wind-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('211', '2013-06-14 15:01:47', '444000', 'Arm -  Actuation Reset -  Wind-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('212', '2013-06-14 15:01:47', '455900', 'Lens -  Milky White -  P42J Step Lamp', '32', 'HCM');
INSERT INTO `partes` VALUES ('213', '2013-06-14 15:01:47', '457700', 'Body -  PGV 1.0 NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('214', '2013-06-14 15:01:47', '457700', 'Body -  PGV 1.0 NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('215', '2013-06-14 15:01:47', '457705', 'Body -  PGV 1.0 BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('216', '2013-06-14 15:01:47', '457705', 'Body -  PGV 1.0 BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('217', '2013-06-14 15:01:47', '457710', 'Body -  PGV 1.0 SLIP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('218', '2013-06-14 15:01:47', '457710', 'Body -  PGV 1.0 SLIP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('219', '2013-06-14 15:01:47', '458200', null, '32', 'Proprietary');
INSERT INTO `partes` VALUES ('220', '2013-06-14 15:01:47', '458501', 'CAP -  Pros - CV.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('221', '2013-06-14 15:01:47', '458502', 'CAP -  Pros -  M�xico.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('222', '2013-06-14 15:01:47', '458502', 'CAP -  Pros -  M�xico.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('223', '2013-06-14 15:01:47', '458510', 'Cap Inst', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('224', '2013-06-14 15:01:47', '458510', 'Cap Inst', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('225', '2013-06-14 15:01:47', '458511', 'Cap Inst-CV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('226', '2013-06-14 15:01:47', '458511', 'Cap Inst-CV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('227', '2013-06-14 15:01:47', '458521', 'Cap -  PROS CV -  Purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('228', '2013-06-14 15:01:47', '458521', 'Cap -  PROS CV -  Purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('229', '2013-06-14 15:01:47', '458522', 'Cap -  PROS -  Purple -  Mexico.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('230', '2013-06-14 15:01:47', '458522', 'Cap -  PROS -  Purple -  Mexico.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('231', '2013-06-14 15:01:47', '458531', 'Cap Inst-CV purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('232', '2013-06-14 15:01:47', '458531', 'Cap Inst-CV purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('233', '2013-06-14 15:01:47', '458542', 'Cap MPR40 OEM', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('234', '2013-06-14 15:01:47', '458543', 'Cap MPR OEM', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('235', '2013-06-14 15:01:47', '458550', 'Cap MPR  40 OEM Purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('236', '2013-06-14 15:01:47', '458551', 'Cap PROSPRS 30 -  Brn.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('237', '2013-06-14 15:01:47', '458553', 'Cap PROSPRS 30 CV -  Brn.', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('238', '2013-06-14 15:01:47', '458555', 'Cap PROSPRS 40 CV-Gray', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('239', '2013-06-14 15:01:47', '458557', 'Cap PROSPRS 30 Purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('240', '2013-06-14 15:01:47', '458559', 'Cap PROSPRS 30 CV-Purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('241', '2013-06-14 15:01:47', '458561', 'Cap PROSPRS 40 CV Purple', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('242', '2013-06-14 15:01:47', '458563', 'Cap PROSPRS 40 CV-Green', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('243', '2013-06-14 15:01:47', '458700', 'Riser -  6 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('244', '2013-06-14 15:01:47', '458700', 'Riser -  6 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('245', '2013-06-14 15:01:47', '458900', 'Riser -  12 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('246', '2013-06-14 15:01:47', '459000', null, '32', 'Proprietary');
INSERT INTO `partes` VALUES ('247', '2013-06-14 15:01:47', '459200', 'Support -  Check Valve', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('248', '2013-06-14 15:01:47', '460600', 'Plug -  Port -  PSL', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('249', '2013-06-14 15:01:47', '460600', 'Plug -  Port -  PSL', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('250', '2013-06-14 15:01:47', '460700', 'Body - Male/Barb - NPT - 1in - PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('251', '2013-06-14 15:01:47', '460700', 'Body - Male/Barb - NPT - 1in - PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('252', '2013-06-14 15:01:47', '460705', 'Body - Male/Barb - BSP -  1in -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('253', '2013-06-14 15:01:47', '460705', 'Body - Male/Barb - BSP -  1in -  PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('254', '2013-06-14 15:01:47', '461600', 'Knob -  Actuation Adjuster -  Wclik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('255', '2013-06-14 15:01:47', '461700', 'Cam -  Actuation Adjuster -  Wclik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('256', '2013-06-14 15:01:47', '461800', 'Knob -  Reset Adjuster -  Wclik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('257', '2013-06-14 15:01:47', '461900', 'Cam -  Reset Adjuster -  Wclik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('258', '2013-06-14 15:01:47', '471800', 'Bracket -  Mounting -  Freeze Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('259', '2013-06-14 15:01:47', '472000', 'Enclosure -  Thermoswitch -  F-Clik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('260', '2013-06-14 15:01:47', '472100', 'Mount -  Conduit', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('261', '2013-06-14 15:01:47', '472300', 'Riser -  Two Shot Seal', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('262', '2013-06-14 15:01:47', '472300', 'Riser -  Two Shot Seal', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('263', '2013-06-14 15:01:47', '472500', 'Body -  1.0 -  AVB', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('264', '2013-06-14 15:01:47', '473200', 'Diaphragm -  Bottom -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('265', '2013-06-14 15:01:47', '473300', 'Diaphragm -  Top -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('266', '2013-06-14 15:01:47', '473400', 'Support -  Seal -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('267', '2013-06-14 15:01:47', '473500', 'Wiper -  ICV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('268', '2013-06-14 15:01:47', '476100', 'Ring -  Jar', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('269', '2013-06-14 15:01:47', '476100', 'Ring -  Jar', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('270', '2013-06-14 15:01:47', '476200', 'Body -  JT -  M X B -  1 in -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('271', '2013-06-14 15:01:47', '476200', 'Body -  JT -  M X B -  1 in -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('272', '2013-06-14 15:01:47', '476205', 'Body -  JT -  M X B -  1 in -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('273', '2013-06-14 15:01:47', '476205', 'Body -  JT -  M X B -  1 in -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('274', '2013-06-14 15:01:47', '476300', 'Body -  JT -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('275', '2013-06-14 15:01:47', '476300', 'Body -  JT -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('276', '2013-06-14 15:01:47', '476305', 'Body -  JT -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('277', '2013-06-14 15:01:47', '476305', 'Body -  JT -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('278', '2013-06-14 15:01:47', '476310', 'Body -  JT -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('279', '2013-06-14 15:01:47', '476310', 'Body -  JT -  Slip', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('280', '2013-06-14 15:01:47', '476600', 'Body -  JT -  MxM -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('281', '2013-06-14 15:01:47', '476600', 'Body -  JT -  MxM -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('282', '2013-06-14 15:01:47', '476605', 'Body -  JT -  MxM -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('283', '2013-06-14 15:01:47', '476605', 'Body -  JT -  MxM -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('284', '2013-06-14 15:01:47', '477300', 'Body -  JT -  MxB -  1-1/4 in -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('285', '2013-06-14 15:01:47', '477305', 'Body -  JT -  MxB -  1-1/4 in -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('286', '2013-06-14 15:01:47', '501700', 'Shell -  RainClik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('287', '2013-06-14 15:01:47', '501715', 'Shell -  RFC -  Wired', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('288', '2013-06-14 15:01:47', '501800', 'Base -  RainClik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('289', '2013-06-14 15:01:47', '501900', 'Vent Cap -  RainClik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('290', '2013-06-14 15:01:47', '502000', 'Spindle -  Single Disc -  RainClik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('291', '2013-06-14 15:01:47', '502100', 'MC Spindler -  RainClik', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('292', '2013-06-14 15:01:47', '529600', 'Body -  JT -  MxB -  3/4in -  NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('293', '2013-06-14 15:01:47', '529605', 'Body -  JT -  MxB -  3/4in -  BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('294', '2013-06-14 15:01:47', '534900', 'Adapter Tee -  SCH80', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('295', '2013-06-14 15:01:47', '534905', 'Adapter Tee -  SCH40', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('296', '2013-06-14 15:01:47', '537300', 'Body -  4 in -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('297', '2013-06-14 15:01:47', '537300', 'Body -  4 in -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('298', '2013-06-14 15:01:47', '537400', 'Cap -  Body -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('299', '2013-06-14 15:01:47', '537500', 'Gear -  Adj -  Lower -  PGJ -  Gray', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('300', '2013-06-14 15:01:47', '537505', 'Gear -  Adj -  Lower -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('301', '2013-06-14 15:01:47', '537600', 'Housing -  Nozzle -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('302', '2013-06-14 15:01:47', '537605', 'Hsng -  Nzl-Check Vlv -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('303', '2013-06-14 15:01:47', '537720', 'Nozzle -  2.0 gpm -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('304', '2013-06-14 15:01:47', '537721', 'Nozzle -  2.0 gpm -  Orbit Green', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('305', '2013-06-14 15:01:47', '537800', 'Cover -  Rubber -  Logo -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('306', '2013-06-14 15:01:47', '537801', 'Cover, Rubber Logo, CEPEX', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('307', '2013-06-14 15:01:47', '537810', 'Cap -  Rubber -  Logo -  Recl -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('308', '2013-06-14 15:01:47', '538000', 'Shaft -  Gear -  Adj -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('309', '2013-06-14 15:01:47', '538100', 'Seal -  Riser -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('310', '2013-06-14 15:01:47', '538400', 'Body -  6 in -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('311', '2013-06-14 15:01:47', '538500', 'Body -  12 in -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('312', '2013-06-14 15:01:47', '538700', 'Rack -  Nozzle -  PGJ', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('313', '2013-06-14 15:01:47', '538701', 'Rack Nozzle Orbit Green', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('314', '2013-06-14 15:01:47', '546700', 'Bleed Screw', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('315', '2013-06-14 15:01:47', '547200', 'Bleed Screw Bonnet -  1 in PGV -  FC', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('316', '2013-06-14 15:01:47', '547200', 'Bleed Screw Bonnet -  1 in PGV -  FC', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('317', '2013-06-14 15:01:47', '548100', 'Bleed Screw Bonnet -  1 in PGV - NF', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('318', '2013-06-14 15:01:47', '548100', 'Bleed Screw Bonnet -  1 in PGV - NF', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('319', '2013-06-14 15:01:47', '548105', 'Bleed Screw Bonnet -  1in PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('320', '2013-06-14 15:01:47', '549700', 'Bleed Screw Bonnet -  NFJT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('321', '2013-06-14 15:01:47', '549700', 'Bleed Screw Bonnet -  NFJT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('322', '2013-06-14 15:01:47', '549800', 'Bleed Screw Bonnet -  FCBS', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('323', '2013-06-14 15:01:47', '604800', 'Body -  Male x Barb 1 1/4 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('324', '2013-06-14 15:01:47', '604800', 'Body -  Male x Barb 1 1/4 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('325', '2013-06-14 15:01:47', '604805', 'Body -  Male x Barb 1 1/4 in BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('326', '2013-06-14 15:01:47', '604805', 'Body -  Male x Barb 1 1/4 in BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('327', '2013-06-14 15:01:47', '605500', 'Arm Interface -  WRC', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('328', '2013-06-14 15:01:47', '606900', 'Flow Control Stem 1.5 in PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('329', '2013-06-14 15:01:47', '607000', 'Flow Control Stem 2.0 in PGV', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('330', '2013-06-14 15:01:47', '607100', 'Large PGV FC Knob', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('331', '2013-06-14 15:01:47', '607105', 'Knob, FC PGV 1.5 & 2\" Purple reclaimed', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('332', '2013-06-14 15:01:47', '612200', 'Body Male x Barb 3/4 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('333', '2013-06-14 15:01:47', '612200', 'Body Male x Barb 3/4 in NPT', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('334', '2013-06-14 15:01:47', '612205', 'Body Male x Barb 3/4 in BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('335', '2013-06-14 15:01:47', '612205', 'Body Male x Barb 3/4 in BSP', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('336', '2013-06-14 15:01:47', '630200', 'Cover -  RFC -  wired', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('337', '2013-06-14 15:01:47', '645700', 'Body 6 in  Inst. No Side Inlet', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('338', '2013-06-14 15:01:47', '645800', 'Body 12 in  Pros. No Side inlet', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('339', '2013-06-14 15:01:47', '646800', 'Body -  6 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('340', '2013-06-14 15:01:47', '646800', 'Body -  6 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('341', '2013-06-14 15:01:47', '646800', 'Body -  6 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('342', '2013-06-14 15:01:47', '646805', 'Body -  6 Inch No Hunter Art', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('343', '2013-06-14 15:01:47', '646805', 'Body -  6 Inch No Hunter Art', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('344', '2013-06-14 15:01:47', '646900', 'Body -  12 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('345', '2013-06-14 15:01:47', '646900', 'Body -  12 Inch PROS/INST', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('346', '2013-06-14 15:01:47', '646905', 'Body -  12 Inch No Hunter Art', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('347', '2013-06-14 15:01:47', '654900', 'Bleed  Screw FC', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('348', '2013-06-14 15:01:47', '711900', 'Diaph Support W/Ledge ICV 101', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('349', '2013-06-14 15:01:47', '775300', 'Oulet point source emitter 2L, Blk', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('350', '2013-06-14 15:01:47', '775301', 'Oulet point source emitter 4L, Blk', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('351', '2013-06-14 15:01:47', '775302', 'Oulet point source emitter 8L, Blk', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('352', '2013-06-14 15:01:47', '775303', null, '32', 'Proprietary');
INSERT INTO `partes` VALUES ('353', '2013-06-14 15:01:47', '775304', 'Oulet point source emitter 23L, Blk', '32', 'Proprietary');
INSERT INTO `partes` VALUES ('354', '2013-06-14 15:01:47', '775305', 'Oulet point source emitter 8L, Red', '32', 'Proprietary');

-- ----------------------------
-- Table structure for partes_familia
-- ----------------------------
DROP TABLE IF EXISTS `partes_familia`;
CREATE TABLE `partes_familia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familia` varchar(25) NOT NULL,
  `descripcion` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of partes_familia
-- ----------------------------
INSERT INTO `partes_familia` VALUES ('1', 'ASV', null);
INSERT INTO `partes_familia` VALUES ('2', 'PASV', null);
INSERT INTO `partes_familia` VALUES ('4', 'Drip Zone', null);
INSERT INTO `partes_familia` VALUES ('5', 'PGV', null);
INSERT INTO `partes_familia` VALUES ('6', 'PROS', null);
INSERT INTO `partes_familia` VALUES ('7', 'PSU', null);
INSERT INTO `partes_familia` VALUES ('8', 'PGH', null);
INSERT INTO `partes_familia` VALUES ('9', 'I-20', null);
INSERT INTO `partes_familia` VALUES ('10', 'Accuset', null);
INSERT INTO `partes_familia` VALUES ('11', 'SJ', null);
INSERT INTO `partes_familia` VALUES ('12', 'HPV', null);
INSERT INTO `partes_familia` VALUES ('13', 'SRM', null);
INSERT INTO `partes_familia` VALUES ('14', 'Ficosa', null);
INSERT INTO `partes_familia` VALUES ('15', 'IBV', null);
INSERT INTO `partes_familia` VALUES ('16', 'SRV', null);
INSERT INTO `partes_familia` VALUES ('17', 'I20 U', null);
INSERT INTO `partes_familia` VALUES ('18', 'Riser I20U', null);
INSERT INTO `partes_familia` VALUES ('19', 'ICV', null);
INSERT INTO `partes_familia` VALUES ('20', 'Plantronics', null);
INSERT INTO `partes_familia` VALUES ('21', 'Sanford', null);
INSERT INTO `partes_familia` VALUES ('22', 'ACME', null);
INSERT INTO `partes_familia` VALUES ('23', 'Sensores', null);
INSERT INTO `partes_familia` VALUES ('24', 'AVB', null);
INSERT INTO `partes_familia` VALUES ('25', 'JT', null);
INSERT INTO `partes_familia` VALUES ('26', 'Flow Click', null);
INSERT INTO `partes_familia` VALUES ('27', 'PGJ', null);
INSERT INTO `partes_familia` VALUES ('32', 'No Seleccion', null);

-- ----------------------------
-- Table structure for partes_familia2
-- ----------------------------
DROP TABLE IF EXISTS `partes_familia2`;
CREATE TABLE `partes_familia2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `familia` varchar(12) DEFAULT NULL,
  `descripcion` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of partes_familia2
-- ----------------------------
INSERT INTO `partes_familia2` VALUES ('1', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('2', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('3', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('4', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('5', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('6', 'PASV', null);
INSERT INTO `partes_familia2` VALUES ('7', 'PASV', null);
INSERT INTO `partes_familia2` VALUES ('8', 'PASV', null);
INSERT INTO `partes_familia2` VALUES ('9', 'PASV', null);
INSERT INTO `partes_familia2` VALUES ('10', null, null);
INSERT INTO `partes_familia2` VALUES ('11', 'Drip Zone', null);
INSERT INTO `partes_familia2` VALUES ('12', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('13', 'Drip Zone', null);
INSERT INTO `partes_familia2` VALUES ('14', 'Drip Zone', null);
INSERT INTO `partes_familia2` VALUES ('15', 'Drip Zone', null);
INSERT INTO `partes_familia2` VALUES ('16', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('17', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('18', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('19', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('20', 'PASV', null);
INSERT INTO `partes_familia2` VALUES ('21', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('22', 'PSU', null);
INSERT INTO `partes_familia2` VALUES ('23', 'PSU', null);
INSERT INTO `partes_familia2` VALUES ('24', 'PSU', null);
INSERT INTO `partes_familia2` VALUES ('25', 'PSU', null);
INSERT INTO `partes_familia2` VALUES ('26', 'PGH', null);
INSERT INTO `partes_familia2` VALUES ('27', 'PGH', null);
INSERT INTO `partes_familia2` VALUES ('28', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('29', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('30', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('31', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('32', 'I-20', null);
INSERT INTO `partes_familia2` VALUES ('33', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('34', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('35', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('36', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('37', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('38', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('39', 'I-20', null);
INSERT INTO `partes_familia2` VALUES ('40', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('41', 'SJ', null);
INSERT INTO `partes_familia2` VALUES ('42', 'SJ', null);
INSERT INTO `partes_familia2` VALUES ('43', 'SJ', null);
INSERT INTO `partes_familia2` VALUES ('44', 'SJ', null);
INSERT INTO `partes_familia2` VALUES ('45', 'HPV', null);
INSERT INTO `partes_familia2` VALUES ('46', 'SRM', null);
INSERT INTO `partes_familia2` VALUES ('47', 'SRM', null);
INSERT INTO `partes_familia2` VALUES ('48', 'Ficosa', null);
INSERT INTO `partes_familia2` VALUES ('49', null, null);
INSERT INTO `partes_familia2` VALUES ('50', null, null);
INSERT INTO `partes_familia2` VALUES ('51', null, null);
INSERT INTO `partes_familia2` VALUES ('52', 'HPV', null);
INSERT INTO `partes_familia2` VALUES ('53', 'HPV', null);
INSERT INTO `partes_familia2` VALUES ('54', 'HPV', null);
INSERT INTO `partes_familia2` VALUES ('55', 'HPV', null);
INSERT INTO `partes_familia2` VALUES ('56', null, null);
INSERT INTO `partes_familia2` VALUES ('57', 'SRM', null);
INSERT INTO `partes_familia2` VALUES ('58', 'IBV', null);
INSERT INTO `partes_familia2` VALUES ('59', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('60', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('61', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('62', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('63', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('64', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('65', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('66', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('67', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('68', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('69', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('70', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('71', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('72', 'HPV', null);
INSERT INTO `partes_familia2` VALUES ('73', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('74', 'HPV', null);
INSERT INTO `partes_familia2` VALUES ('75', 'SJ', null);
INSERT INTO `partes_familia2` VALUES ('76', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('77', 'I20 U', null);
INSERT INTO `partes_familia2` VALUES ('78', 'I20 U', null);
INSERT INTO `partes_familia2` VALUES ('79', 'Riser I20U', null);
INSERT INTO `partes_familia2` VALUES ('80', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('81', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('82', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('83', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('84', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('85', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('86', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('87', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('88', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('89', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('90', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('91', 'Accuset', null);
INSERT INTO `partes_familia2` VALUES ('92', null, null);
INSERT INTO `partes_familia2` VALUES ('93', null, null);
INSERT INTO `partes_familia2` VALUES ('94', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('95', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('96', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('97', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('98', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('99', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('100', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('101', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('102', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('103', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('104', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('105', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('106', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('107', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('108', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('109', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('110', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('111', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('112', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('113', null, null);
INSERT INTO `partes_familia2` VALUES ('114', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('115', null, null);
INSERT INTO `partes_familia2` VALUES ('116', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('117', null, null);
INSERT INTO `partes_familia2` VALUES ('118', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('119', null, null);
INSERT INTO `partes_familia2` VALUES ('120', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('121', null, null);
INSERT INTO `partes_familia2` VALUES ('122', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('123', null, null);
INSERT INTO `partes_familia2` VALUES ('124', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('125', null, null);
INSERT INTO `partes_familia2` VALUES ('126', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('127', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('128', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('129', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('130', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('131', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('132', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('133', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('134', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('135', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('136', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('137', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('138', 'Ficosa', null);
INSERT INTO `partes_familia2` VALUES ('139', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('140', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('141', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('142', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('143', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('144', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('145', 'Ficosa', null);
INSERT INTO `partes_familia2` VALUES ('146', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('147', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('148', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('149', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('150', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('151', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('152', 'Ficosa', null);
INSERT INTO `partes_familia2` VALUES ('153', 'Sanford', null);
INSERT INTO `partes_familia2` VALUES ('154', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('155', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('156', 'Plantronics', null);
INSERT INTO `partes_familia2` VALUES ('157', null, null);
INSERT INTO `partes_familia2` VALUES ('158', 'ACME', null);
INSERT INTO `partes_familia2` VALUES ('159', 'PGH', null);
INSERT INTO `partes_familia2` VALUES ('160', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('161', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('162', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('163', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('164', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('165', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('166', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('167', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('168', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('169', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('170', 'ASV', null);
INSERT INTO `partes_familia2` VALUES ('171', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('172', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('173', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('174', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('175', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('176', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('177', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('178', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('179', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('180', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('181', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('182', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('183', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('184', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('185', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('186', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('187', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('188', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('189', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('190', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('191', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('192', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('193', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('194', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('195', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('196', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('197', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('198', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('199', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('200', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('201', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('202', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('203', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('204', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('205', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('206', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('207', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('208', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('209', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('210', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('211', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('212', 'Ficosa', null);
INSERT INTO `partes_familia2` VALUES ('213', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('214', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('215', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('216', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('217', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('218', 'SRV', null);
INSERT INTO `partes_familia2` VALUES ('219', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('220', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('221', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('222', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('223', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('224', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('225', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('226', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('227', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('228', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('229', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('230', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('231', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('232', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('233', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('234', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('235', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('236', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('237', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('238', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('239', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('240', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('241', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('242', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('243', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('244', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('245', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('246', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('247', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('248', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('249', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('250', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('251', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('252', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('253', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('254', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('255', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('256', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('257', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('258', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('259', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('260', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('261', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('262', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('263', 'AVB', null);
INSERT INTO `partes_familia2` VALUES ('264', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('265', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('266', 'IBV', null);
INSERT INTO `partes_familia2` VALUES ('267', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('268', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('269', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('270', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('271', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('272', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('273', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('274', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('275', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('276', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('277', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('278', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('279', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('280', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('281', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('282', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('283', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('284', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('285', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('286', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('287', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('288', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('289', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('290', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('291', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('292', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('293', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('294', 'Flow Click', null);
INSERT INTO `partes_familia2` VALUES ('295', 'Flow Click', null);
INSERT INTO `partes_familia2` VALUES ('296', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('297', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('298', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('299', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('300', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('301', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('302', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('303', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('304', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('305', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('306', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('307', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('308', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('309', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('310', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('311', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('312', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('313', 'PGJ', null);
INSERT INTO `partes_familia2` VALUES ('314', null, null);
INSERT INTO `partes_familia2` VALUES ('315', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('316', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('317', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('318', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('319', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('320', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('321', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('322', 'JT', null);
INSERT INTO `partes_familia2` VALUES ('323', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('324', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('325', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('326', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('327', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('328', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('329', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('330', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('331', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('332', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('333', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('334', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('335', 'PGV', null);
INSERT INTO `partes_familia2` VALUES ('336', 'Sensores', null);
INSERT INTO `partes_familia2` VALUES ('337', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('338', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('339', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('340', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('341', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('342', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('343', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('344', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('345', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('346', 'PROS', null);
INSERT INTO `partes_familia2` VALUES ('347', null, null);
INSERT INTO `partes_familia2` VALUES ('348', 'ICV', null);
INSERT INTO `partes_familia2` VALUES ('349', null, null);
INSERT INTO `partes_familia2` VALUES ('350', null, null);
INSERT INTO `partes_familia2` VALUES ('351', null, null);
INSERT INTO `partes_familia2` VALUES ('352', null, null);
INSERT INTO `partes_familia2` VALUES ('353', null, null);
INSERT INTO `partes_familia2` VALUES ('354', null, null);

-- ----------------------------
-- Table structure for prueba_conceptos
-- ----------------------------
DROP TABLE IF EXISTS `prueba_conceptos`;
CREATE TABLE `prueba_conceptos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prueba` varchar(255) DEFAULT NULL,
  `form_name_muestra` varchar(200) DEFAULT NULL,
  `form_name_rechazado` varchar(200) DEFAULT NULL,
  `nstatus` enum('1','0') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prueba_conceptos
-- ----------------------------
INSERT INTO `prueba_conceptos` VALUES ('1', 'Burst test', 'burst_test', 'burst_test_rechazado', '1');
INSERT INTO `prueba_conceptos` VALUES ('2', 'Prueba de horno', 'prueba_horno', 'prueba_horno_rechazado', '1');

-- ----------------------------
-- Table structure for temporal
-- ----------------------------
DROP TABLE IF EXISTS `temporal`;
CREATE TABLE `temporal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `molde` varchar(12) DEFAULT NULL,
  `parte` varchar(15) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `familia` varchar(25) DEFAULT NULL,
  `division` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of temporal
-- ----------------------------
INSERT INTO `temporal` VALUES ('1', '9327', '775305', 'Oulet point source emitter 8L, Red', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('2', '9327', '775304', 'Oulet point source emitter 23L, Blk', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('3', '9327', '775303', null, null, 'Proprietary');
INSERT INTO `temporal` VALUES ('4', '9327', '775302', 'Oulet point source emitter 8L, Blk', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('5', '9327', '775301', 'Oulet point source emitter 4L, Blk', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('6', '9327', '775300', 'Oulet point source emitter 2L, Blk', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('7', '1192', '711900', 'Diaph Support W/Ledge ICV 101', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('8', '6491', '654900', 'Bleed  Screw FC', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('9', '3414', '646905', 'Body -  12 Inch No Hunter Art', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('10', '3414', '646900', 'Body -  12 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('11', '5660', '646900', 'Body -  12 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('12', '3412', '646805', 'Body -  6 Inch No Hunter Art', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('13', '3741', '646805', 'Body -  6 Inch No Hunter Art', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('14', '3412', '646800', 'Body -  6 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('15', '3741', '646800', 'Body -  6 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('16', '6789', '646800', 'Body -  6 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('17', '3414', '645800', 'Body 12 in  Pros. No Side inlet', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('18', '6789', '645700', 'Body 6 in  Inst. No Side Inlet', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('19', '6524', '630200', 'Cover -  RFC -  wired', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('20', '3134', '612205', 'Body Male x Barb 3/4 in BSP', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('21', '5422', '612205', 'Body Male x Barb 3/4 in BSP', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('22', '3134', '612200', 'Body Male x Barb 3/4 in NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('23', '5422', '612200', 'Body Male x Barb 3/4 in NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('24', '6490', '607105', 'Knob, FC PGV 1.5 & 2\" Purple reclaimed', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('25', '6490', '607100', 'Large PGV FC Knob', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('26', '6489', '607000', 'Flow Control Stem 2.0 in PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('27', '6488', '606900', 'Flow Control Stem 1.5 in PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('28', '5834', '605500', 'Arm Interface -  WRC', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('29', '3134', '604805', 'Body -  Male x Barb 1 1/4 in BSP', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('30', '5422', '604805', 'Body -  Male x Barb 1 1/4 in BSP', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('31', '3134', '604800', 'Body -  Male x Barb 1 1/4 in NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('32', '5422', '604800', 'Body -  Male x Barb 1 1/4 in NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('33', '4943', '549800', 'Bleed Screw Bonnet -  FCBS', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('34', '4943', '549700', 'Bleed Screw Bonnet -  NFJT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('35', '5418', '549700', 'Bleed Screw Bonnet -  NFJT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('36', '4222', '548105', 'Bleed Screw Bonnet -  1in PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('37', '4222', '548100', 'Bleed Screw Bonnet -  1 in PGV - NF', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('38', '7250', '548100', 'Bleed Screw Bonnet -  1 in PGV - NF', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('39', '4215', '547200', 'Bleed Screw Bonnet -  1 in PGV -  FC', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('40', '6759', '547200', 'Bleed Screw Bonnet -  1 in PGV -  FC', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('41', '4944', '546700', 'Bleed Screw', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('42', '4914', '538701', 'Rack Nozzle Orbit Green', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('43', '4914', '538700', 'Rack -  Nozzle -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('44', '4913', '538500', 'Body -  12 in -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('45', '4912', '538400', 'Body -  6 in -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('46', '4911', '538100', 'Seal -  Riser -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('47', '4910', '538000', 'Shaft -  Gear -  Adj -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('48', '4907', '537810', 'Cap -  Rubber -  Logo -  Recl -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('49', '4907', '537801', 'Cover, Rubber Logo, CEPEX', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('50', '4907', '537800', 'Cover -  Rubber -  Logo -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('51', '4915', '537721', 'Nozzle -  2.0 gpm -  Orbit Green', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('52', '4915', '537720', 'Nozzle -  2.0 gpm -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('53', '4906', '537605', 'Hsng -  Nzl-Check Vlv -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('54', '4906', '537600', 'Housing -  Nozzle -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('55', '4909', '537505', 'Gear -  Adj -  Lower -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('56', '4909', '537500', 'Gear -  Adj -  Lower -  PGJ -  Gray', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('57', '4908', '537400', 'Cap -  Body -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('58', '4904', '537300', 'Body -  4 in -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('59', '4905', '537300', 'Body -  4 in -  PGJ', 'PGJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('60', '4887', '534905', 'Adapter Tee -  SCH40', 'Flow Click', 'Proprietary');
INSERT INTO `temporal` VALUES ('61', '4887', '534900', 'Adapter Tee -  SCH80', 'Flow Click', 'Proprietary');
INSERT INTO `temporal` VALUES ('62', '4129', '529605', 'Body -  JT -  MxB -  3/4in -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('63', '4129', '529600', 'Body -  JT -  MxB -  3/4in -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('64', '4169', '502100', 'MC Spindler -  RainClik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('65', '4169', '502000', 'Spindle -  Single Disc -  RainClik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('66', '4168', '501900', 'Vent Cap -  RainClik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('67', '4167', '501800', 'Base -  RainClik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('68', '4166', '501715', 'Shell -  RFC -  Wired', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('69', '4166', '501700', 'Shell -  RainClik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('70', '4129', '477305', 'Body -  JT -  MxB -  1-1/4 in -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('71', '4129', '477300', 'Body -  JT -  MxB -  1-1/4 in -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('72', '4129', '476605', 'Body -  JT -  MxM -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('73', '5424', '476605', 'Body -  JT -  MxM -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('74', '4129', '476600', 'Body -  JT -  MxM -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('75', '5424', '476600', 'Body -  JT -  MxM -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('76', '4128', '476310', 'Body -  JT -  Slip', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('77', '5417', '476310', 'Body -  JT -  Slip', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('78', '4128', '476305', 'Body -  JT -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('79', '5417', '476305', 'Body -  JT -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('80', '4128', '476300', 'Body -  JT -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('81', '5417', '476300', 'Body -  JT -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('82', '4129', '476205', 'Body -  JT -  M X B -  1 in -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('83', '5424', '476205', 'Body -  JT -  M X B -  1 in -  BSP', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('84', '4129', '476200', 'Body -  JT -  M X B -  1 in -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('85', '5424', '476200', 'Body -  JT -  M X B -  1 in -  NPT', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('86', '4131', '476100', 'Ring -  Jar', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('87', '5416', '476100', 'Ring -  Jar', 'JT', 'Proprietary');
INSERT INTO `temporal` VALUES ('88', '1106', '473500', 'Wiper -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('89', '3406', '473400', 'Support -  Seal -  ICV', 'IBV', 'Proprietary');
INSERT INTO `temporal` VALUES ('90', '3405', '473300', 'Diaphragm -  Top -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('91', '3407', '473200', 'Diaphragm -  Bottom -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('92', '3600', '472500', 'Body -  1.0 -  AVB', 'AVB', 'Proprietary');
INSERT INTO `temporal` VALUES ('93', '4820', '472300', 'Riser -  Two Shot Seal', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('94', '6855', '472300', 'Riser -  Two Shot Seal', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('95', '3607', '472100', 'Mount -  Conduit', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('96', '3445', '472000', 'Enclosure -  Thermoswitch -  F-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('97', '3446', '471800', 'Bracket -  Mounting -  Freeze Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('98', '1381', '461900', 'Cam -  Reset Adjuster -  Wclik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('99', '1381', '461800', 'Knob -  Reset Adjuster -  Wclik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('100', '1381', '461700', 'Cam -  Actuation Adjuster -  Wclik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('101', '1381', '461600', 'Knob -  Actuation Adjuster -  Wclik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('102', '3134', '460705', 'Body - Male/Barb - BSP -  1in -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('103', '5422', '460705', 'Body - Male/Barb - BSP -  1in -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('104', '3134', '460700', 'Body - Male/Barb - NPT - 1in - PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('105', '5422', '460700', 'Body - Male/Barb - NPT - 1in - PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('106', '3458', '460600', 'Plug -  Port -  PSL', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('107', '6788', '460600', 'Plug -  Port -  PSL', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('108', '3733', '459200', 'Support -  Check Valve', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('109', '3414', '459000', null, 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('110', '3415', '458900', 'Riser -  12 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('111', '3413', '458700', 'Riser -  6 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('112', '7255', '458700', 'Riser -  6 Inch PROS/INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('113', '1267', '458563', 'Cap PROSPRS 40 CV-Green', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('114', '1267', '458561', 'Cap PROSPRS 40 CV Purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('115', '1267', '458559', 'Cap PROSPRS 30 CV-Purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('116', '1267', '458557', 'Cap PROSPRS 30 Purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('117', '1267', '458555', 'Cap PROSPRS 40 CV-Gray', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('118', '1267', '458553', 'Cap PROSPRS 30 CV -  Brn.', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('119', '1267', '458551', 'Cap PROSPRS 30 -  Brn.', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('120', '1267', '458550', 'Cap MPR  40 OEM Purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('121', '1267', '458543', 'Cap MPR OEM', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('122', '1267', '458542', 'Cap MPR40 OEM', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('123', '1267', '458531', 'Cap Inst-CV purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('124', '3731', '458531', 'Cap Inst-CV purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('125', '1267', '458522', 'Cap -  PROS -  Purple -  Mexico.', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('126', '3731', '458522', 'Cap -  PROS -  Purple -  Mexico.', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('127', '1267', '458521', 'Cap -  PROS CV -  Purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('128', '3731', '458521', 'Cap -  PROS CV -  Purple', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('129', '1267', '458511', 'Cap Inst-CV', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('130', '3731', '458511', 'Cap Inst-CV', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('131', '1267', '458510', 'Cap Inst', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('132', '3731', '458510', 'Cap Inst', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('133', '1267', '458502', 'CAP -  Pros -  M�xico.', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('134', '3731', '458502', 'CAP -  Pros -  M�xico.', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('135', '1267', '458501', 'CAP -  Pros - CV.', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('136', '3731', '458200', null, 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('137', '4220', '457710', 'Body -  PGV 1.0 SLIP', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('138', '7041', '457710', 'Body -  PGV 1.0 SLIP', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('139', '4219', '457705', 'Body -  PGV 1.0 BSP', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('140', '4752', '457705', 'Body -  PGV 1.0 BSP', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('141', '4219', '457700', 'Body -  PGV 1.0 NPT', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('142', '4752', '457700', 'Body -  PGV 1.0 NPT', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('143', '1380', '444000', 'Arm -  Actuation Reset -  Wind-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('144', '1380', '443900', 'Level Reset Spring -  Wind-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('145', '1380', '443800', 'Level Arm -  Switching -  Wind-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('146', '1380', '443700', 'Acutuator Cam -  Wind-clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('147', '1383', '442900', 'Pivot -  Top -  Wind-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('148', '1383', '442600', 'Pivot -  Bottom -  Wind-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('149', '1382', '442400', 'Hub -  Mini Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('150', '1382', '442300', 'Wind Vane -  Wind-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('151', '1383', '442200', 'Control Base -  Wind Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('152', '1382', '442100', 'Housing -  Wind Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('153', '1378', '442000', 'Bypass Cover', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('154', '1378', '441900', 'Bypass Housing', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('155', '1366', '440500', 'Spindle -  Mini-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('156', '1375', '440300', 'Cap -  Mini-Clik', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('157', '1377', '440100', 'Vent Ring', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('158', '1376', '440000', 'Switch Housing', 'Sensores', 'Proprietary');
INSERT INTO `temporal` VALUES ('159', '3134', '433405', 'Body - Male/Male - BSP -  1in -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('160', '5422', '433405', 'Body - Male/Male - BSP -  1in -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('161', '3134', '433400', 'Body - Male/Male - NPT - 1in - PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('162', '5422', '433400', 'Body - Male/Male - NPT - 1in - PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('163', '3732', '431300', 'Valve -  Regulator', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('164', '3735', '431100', 'Stem -  Regulator', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('165', '3736', '431000', 'Clip -  Regulator', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('166', '3456', '424005', 'Body -  4 in OEM -  MPR40', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('167', '6726', '424000', 'Body 4 in PROS PRS', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('168', '6860', '423905', 'Plug -  Flush -  INST', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('169', '6860', '423900', 'Plug -  Flush -  PROS', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('170', '1282', '416800', 'Seal -  2in -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('171', '1243', '416700', 'Seal -  1.5 -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('172', '1278', '415305', 'Plug -  Port -  2.0 -  PGV -  BSP', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('173', '1278', '415300', 'Plug -  Port -  2.0 -  PGV -  NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('174', '1274', '414400', 'Bonnet -  2.0 -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('175', '1275', '414305', 'Body -  2.0 -  PGV -  BSP', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('176', '1275', '414300', 'Body -  2.0 -  PGV -  NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('177', '1248', '413805', 'Plug -  Port -  1.5 -  BSP -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('178', '1248', '413800', 'Plug -  Port -  1.5 -  NPT -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('179', '1247', '413500', 'Support -  Seal -  1.5 -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('180', '1246', '413100', 'Support -  Diaph -  1.5 -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('181', '1263', '412500', 'Bonnet -  1.5 -  PGV', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('182', '1262', '412405', 'Body PGV 1.5 BSP', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('183', '1262', '412400', 'Body -  1.5 -  PGV NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('184', '1372', '409605', 'Lid -  Cap -  1.0 -  AVB', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('185', '1372', '409600', 'Lid -  Cap -  1.0 -  ASV', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('186', '1369', '409400', 'Piston -  1.0 -  ASV', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('187', '1368', '409300', 'Cap -  Housing -  1.0 -  ASV (obsolete)', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('188', '1371', '407400', 'Lid -  Cap -  .75 -  ASV', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('189', '1363', '407020', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  Slip', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('190', '1363', '407015', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  NPT', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('191', '1363', '407010', 'Body -  Angle - 1.0 -  ASV -  Slip -  Slip', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('192', '1363', '407005', 'Body -  Angle - 1.0 -  ASV -  Slip -  NPT', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('193', '1367', '406900', 'Cap -  Housing -  .75 -  ASV (obsolete)', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('194', '1370', '406700', 'Piston -  .75 -  ASV', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('195', '5896', '404200', 'Body -  I-20 -  12 in.', 'PGH', 'Proprietary');
INSERT INTO `temporal` VALUES ('196', '1208', '404100', 'Insert, Threaded, ACME 1.25 Blk', 'ACME', 'Proprietary');
INSERT INTO `temporal` VALUES ('197', '1212', '401800', 'Body -  Block Rotor -  1 � ACME', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('198', '506', '391126', 'Barrel Berol White 1777287', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('199', '506', '391125', 'Barrel Berol FL Blue 1777286', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('200', '506', '391124', 'Barrel Berol FL Pink 1777285', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('201', '506', '391123', 'Barrel Berol FL Orange 1777284', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('202', '506', '391122', 'Barrel Berol FL Green 1777283', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('203', '506', '391121', 'Barrel Berol FL Yellow 1777282', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('204', '506', '391120', 'Barrel Berol Black 1777281', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('205', '1220', '386500', 'Wiper -  1.0 -  ICV -  FS', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('206', '1193', '386000', 'Nut -  Flow Control -  1in -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('207', '1196', '385800', 'Top -  Diaph -  1.0 -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('208', '1192', '385700', 'Support -  Diaph -  1.0 -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('209', '1205', '385400', 'Bonnet 1.0 -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('210', '1206', '385305', 'Body 1.0 -  ICV -  BSP', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('211', '1206', '385300', 'Body 1.0 -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('212', '1098', '370105', 'Body -  1.5 -  ICV -  BSP', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('213', '1098', '370100', 'Body -  1.5 -   ICV -  NPT', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('214', '8074', '369300', 'Bonnet -  Flow Control', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('215', '8075', '369200', 'Bonnet -  No Flow Control', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('216', '3481', '369000', 'Body -  Upper -  ICV -  Reg', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('217', '1096', '368900', 'Body -  Lower -  ICV -  Reg', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('218', '1093', '368700', 'Housing -  Diaphragm -  ICV -  Reg', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('219', '1091', '368500', 'Knob -  Adjustment -  ICV -  Reg', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('220', '1089', '368305', 'Carrier Nut -  G900 Regulator', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('221', '1089', '368300', 'Carrier -  Nut -  ICV -  Reg', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('222', '1095', '368200', 'Seal -  Inlet -  ICV -  Reg', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('223', '1074', '365900', 'Support -  Diaphragm -  ICV', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('224', '1088', '365600', 'Bonnet -  ICV 1.5 in & 2.0 in', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('225', '1087', '365505', 'Body -  2.0 -  ICV -  BSP', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('226', '1087', '365500', 'Body -  2.0 -  ICV -  NPT', 'ICV', 'Proprietary');
INSERT INTO `temporal` VALUES ('227', '4218', '358500', 'Knob -  Flow Contl -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('228', '1071', '356200', 'Riser- 4 inch I-20U, PL', 'I20 U', 'Proprietary');
INSERT INTO `temporal` VALUES ('229', '4122', '356200', 'Riser- 4 inch I-20U, PL', 'Riser I20U', 'Proprietary');
INSERT INTO `temporal` VALUES ('230', '5161', '356000', 'Body Cap -  I20U', 'I20 U', 'Proprietary');
INSERT INTO `temporal` VALUES ('231', '4217', '352700', 'Stem -  Flow Contl -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('232', '23', '342400', 'Elbow -  3/4 in NPT', 'SJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('233', '916', '339605', 'Body -  HPV -  Angle -  BSP', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('234', '918', '339605', 'Body -  HPV -  Angle -  BSP', 'HPV', 'Proprietary');
INSERT INTO `temporal` VALUES ('235', '916', '339600', 'Body -  HPV -  Angle -  NPT', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('236', '918', '339600', 'Body -  HPV -  Angle -  NPT', 'HPV', 'Proprietary');
INSERT INTO `temporal` VALUES ('237', '4216', '331500', 'Plate -  Friction -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('238', '4205', '331400', 'Screen -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('239', '4206', '331300', 'Support -  Diaph -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('240', '4204', '331200', 'Band -  Diaph -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('241', '4220', '331010', 'Body -  SRV -  1 in -  Slip', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('242', '7041', '331010', 'Body -  SRV -  1 in -  Slip', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('243', '4219', '331005', 'Body -  SRV -  1 in -  BSP', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('244', '4752', '331005', 'Body -  SRV -  1 in -  BSP', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('245', '4219', '331000', 'Body -  SRV -  1 in -  NPT', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('246', '4752', '331000', 'Body -  SRV -  1 in -  NPT', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('247', '4748', '330900', 'Diaphragm -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('248', '5419', '330900', 'Diaphragm -  SRV', 'SRV', 'Proprietary');
INSERT INTO `temporal` VALUES ('249', '3406', '328700', '1.5\" & 2\" IBV Seal Support', 'IBV', 'Proprietary');
INSERT INTO `temporal` VALUES ('250', '956', '326600', 'Riser -  SRM -  4in', 'SRM', 'Proprietary');
INSERT INTO `temporal` VALUES ('251', '1058', '326400', 'Seal -  Riser -  SRM', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('252', '754', '296615', 'Body -  HPV -  Globe -  Slip', 'HPV', 'Proprietary');
INSERT INTO `temporal` VALUES ('253', '754', '296610', 'Body -  HPV -  Globe -  BSP', 'HPV', 'Proprietary');
INSERT INTO `temporal` VALUES ('254', '754', '296605', 'Body -  HPV -  Globe -  NPT', 'HPV', 'Proprietary');
INSERT INTO `temporal` VALUES ('255', '779', '296501', 'Bonnet -  HPV -  Blk', 'HPV', 'Proprietary');
INSERT INTO `temporal` VALUES ('256', '787', '292800', 'Support -  Diaph -  Pilot Valve', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('257', '588', '292300', 'Diaphragm -  Pilot Valve', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('258', '619', '283100', 'Riser -  LT 1.5 in', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('259', '727', '273100', 'Riser -  SRM -  12 in -  Lower', 'SRM', 'Proprietary');
INSERT INTO `temporal` VALUES ('260', '1302', '271100', 'Body -  SRM -  4 in', 'SRM', 'Proprietary');
INSERT INTO `temporal` VALUES ('261', '748', '269200', 'Handle -  FlowCtrl -  HPV', 'HPV', 'Proprietary');
INSERT INTO `temporal` VALUES ('262', '776', '260300', 'Adaptor � in Threaded', 'SJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('263', '635', '259800', 'Elbow -  � in NPT', 'SJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('264', '7263', '259800', 'Elbow -  � in NPT', 'SJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('265', '636', '259700', 'Fitting -  Barb -  Swinglet -  Blk', 'SJ', 'Proprietary');
INSERT INTO `temporal` VALUES ('266', '8954', '255200', 'Seal -  Cover -  AccuSync', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('267', '3636', '252500', 'Seal -  Riser -  I-20', 'I-20', 'Proprietary');
INSERT INTO `temporal` VALUES ('268', '8945', '247600', 'Cover -  AccuSync Adj', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('269', '8948', '247300', 'Body -  Inlet -  Accu-Sunc', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('270', '8947', '243400', 'Knob -  Adjustment -  Accu-Sync -  ADJ', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('271', '8951', '243300', 'Swivel -  Accu-Sync', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('272', '8950', '243200', 'Body -  Lower -  Accu-Sync', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('273', '8949', '242400', 'Body -  Upper -  Accu-Sync', 'Accuset', 'Proprietary');
INSERT INTO `temporal` VALUES ('274', '6872', '229200', 'Body -  I20 -  NPT', 'I-20', 'Proprietary');
INSERT INTO `temporal` VALUES ('275', '915', '192000', 'Body -  PS -  4 in', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('276', '1368', '187300', 'Cap -  Housing -  1 in -  PGV ASV', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('277', '1367', '187200', 'Cap -  Housing -  0.75 in -  ASV', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('278', '916', '179900', 'Body -  PS -  2 in', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('279', '5431', '179200', 'Riser -  PGH -  12 in Lower', 'PGH', 'Proprietary');
INSERT INTO `temporal` VALUES ('280', '5896', '179100', 'Body -  PGH -  12 in', 'PGH', 'Proprietary');
INSERT INTO `temporal` VALUES ('281', '7909', '163200', 'Body -  6 in -  PSU', 'PSU', 'Proprietary');
INSERT INTO `temporal` VALUES ('282', '7908', '163100', 'Riser -  6 in -  PSU', 'PSU', 'Proprietary');
INSERT INTO `temporal` VALUES ('283', '7910', '162601', 'Body Cap, PSU, OEM', 'PSU', 'Proprietary');
INSERT INTO `temporal` VALUES ('284', '7910', '162600', 'Body Cap -  PSU', 'PSU', 'Proprietary');
INSERT INTO `temporal` VALUES ('285', '7913', '162300', 'Riser -  2 in -  PSU', 'PROS', 'Proprietary');
INSERT INTO `temporal` VALUES ('286', '7461', '154300', 'PASV -  Bonnet -  1 Inch', 'PASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('287', '7460', '154215', 'Body -  PGV -  ASV -  3/4 in Slip', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('288', '7460', '154210', 'Body -  PGV -  ASV -  3/4 in NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('289', '7460', '154205', 'Body -  PGV -  ASV -  1.0 in Slip', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('290', '7460', '154200', 'Body -  PGV -  ASV -  1.0 in NPT', 'PGV', 'Proprietary');
INSERT INTO `temporal` VALUES ('291', '7453', '154110', 'Housing -  3/4 in X 3/4 in', 'Drip Zone', 'Proprietary');
INSERT INTO `temporal` VALUES ('292', '7453', '154105', 'Housing -  1 in X 1 in', 'Drip Zone', 'Proprietary');
INSERT INTO `temporal` VALUES ('293', '7453', '154100', 'Housing -  1 in X 3/4 in', 'Drip Zone', 'Proprietary');
INSERT INTO `temporal` VALUES ('294', '1363', '138900', 'Body -  Angle PGV 1.0', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('295', '7452', '133800', 'Drip Zone bonnet filter', 'Drip Zone', 'Proprietary');
INSERT INTO `temporal` VALUES ('296', '1234', '123456', 'Miscellaneous equipment', null, 'Proprietary');
INSERT INTO `temporal` VALUES ('297', '1365', '117205', 'PASV Housing 3/4 in Slip', 'PASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('298', '1365', '117200', 'PASV Housing 3/4 in NPT', 'PASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('299', '1364', '117105', 'PASV Housing 1.0 in Slip', 'PASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('300', '1364', '117100', 'PASV Housing 1.0 in NPT', 'PASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('301', '1363', '117020', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  Slip', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('302', '1363', '117015', 'Body -  Angle - 1.0 -  ASV -  Slip -  .75 -  NPT', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('303', '1363', '117010', 'Body -  Angle - 1.0 -  ASV -  Slip -  Slip', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('304', '1363', '117005', 'Body -  Angle - 1.0 -  ASV -  Slip -  NPT', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('305', '1363', '117000', 'Body -  Angle - 1.0 -  ASV -  NPT -  NPT', 'ASV', 'Proprietary');
INSERT INTO `temporal` VALUES ('306', '361', '455900', 'Lens -  Milky White -  P42J Step Lamp', 'Ficosa', 'HCM');
INSERT INTO `temporal` VALUES ('307', '3892', '394016', 'Headset -  Stand -  83892-02', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('308', '3891', '394014', 'Plastic Cap -  Middle -  83891-02', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('309', '8617', '393107', 'Back beat go holder', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('310', '5936', '392408', '1854169 Cap Inkjoy Red', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('311', '535', '392385', 'Camry New Gasket', 'Ficosa', 'HCM');
INSERT INTO `temporal` VALUES ('312', '5936', '392379', '1854167 Cap Inkjoy Blue', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('313', '5936', '392378', '1854161 Cap Inkjoy Black', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('314', '5709', '392377', '1854174 Endplug Inkjoy Red', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('315', '5709', '392376', '1854173 Endplug Inkjoy Blue', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('316', '5709', '392375', '1854172 Endplug Inkjoy Black', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('317', '5935', '392374', '1852611 Barrel Inkjoy', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('318', '170', '392373', 'Avalon Gasket', 'Ficosa', 'HCM');
INSERT INTO `temporal` VALUES ('319', '7880', '391995', 'Plantronics Moorea Top Cover 87780', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('320', '7881', '391995', 'Plantronics Moorea Top Cover 87780', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('321', '5934', '391637', '1813710 Adapter Ink Joy -  Red', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('322', '5934', '391636', '1813709 Adapter Ink Joy -  Blue', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('323', '5934', '391635', '1813708 Adapter Ink Joy -  Black', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('324', '7009', '391613', 'Cap top PSD', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('325', '295', '391609', 'Mirror Gasket Camry RH & LH', 'Ficosa', 'HCM');
INSERT INTO `temporal` VALUES ('326', '6739', '391178', 'Clear Top -  Saphire -  86739-01', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('327', '6864', '391175', 'Stand Holder -  Montrose -  86864-01', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('328', '6863', '391174', 'Stand Base -  Montrose -  86863-01', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('329', '11', '391151', 'Esterbrook Cap Red 14794903', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('330', '7317', '391151', 'Esterbrook Cap Red 14794903', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('331', '11', '391150', 'Esterbrook Cap Green 1794902', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('332', '7317', '391150', 'Esterbrook Cap Green 1794902', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('333', '11', '391149', 'Esterbrook Cap Blue 1794901', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('334', '7317', '391149', 'Esterbrook Cap Blue 1794901', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('335', '11', '391148', 'Esterbrook Cap Black 1794810', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('336', '7317', '391148', 'Esterbrook Cap Black 1794810', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('337', '6212', '391139', 'Clip Holder MI55', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('338', '136', '391126', 'Barrel Berol White 1777287', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('339', '136', '391125', 'Barrel Berol FL Blue 1777286', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('340', '136', '391124', 'Barrel Berol FL Pink 1777285', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('341', '136', '391123', 'Barrel Berol FL Orange 1777284', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('342', '136', '391122', 'Barrel Berol FL Green 1777283', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('343', '136', '391121', 'Barrel Berol FL Yellow 1777282', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('344', '136', '391120', 'Barrel Berol Black 1777281', 'Sanford', 'HCM');
INSERT INTO `temporal` VALUES ('345', '3893', '390943', 'Case -  Bottom -  83893-01', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('346', '9999', '390943', 'Case -  Bottom -  83893-01', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('347', '3890', '390940', 'Plastic Cap -  Top -  83890-01', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('348', '3921', '390905', 'Plastic Cap -  Top -  8392101', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('349', '100', '390904', 'Plastic Cap -  Top -  8392001', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('350', '3920', '390904', 'Plastic Cap -  Top -  8392001', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('351', '3881', '390903', 'Plastic Cap -  Top -  8388101', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('352', '101', '390902', 'Plastic Cap -  Top -  8388001', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('353', '3880', '390902', 'Plastic Cap -  Top -  8388001', 'Plantronics', 'HCM');
INSERT INTO `temporal` VALUES ('354', '361', '275000', 'Lens. Clear -  L42A Step', 'Ficosa', 'HCM');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tress_id` varchar(15) DEFAULT NULL,
  `nombre` varchar(55) DEFAULT NULL,
  `apellidos` varchar(95) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `usuario` varchar(25) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `nstatus` enum('1','0') DEFAULT '1',
  `rol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', '2013-06-10 08:33:35', '1940', 'Daniel', 'Gomez Morales', 'danielgm78@msn.com', 'dgomez', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2');
INSERT INTO `usuarios` VALUES ('2', '2013-06-28 09:55:52', '1545', 'Usuario test', 'test', 'test@t.com', 'test', '81dc9bdb52d04dc20036dbd8313ed055', '1', '3');
INSERT INTO `usuarios` VALUES ('3', '2013-07-02 15:50:49', '', 'Itzia', 'Solis', 'isolis@hunterindustries.com', 'isolis', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2');
INSERT INTO `usuarios` VALUES ('4', '2013-08-22 13:44:00', '', 'Veronica', 'Aranda', 'varanda@hunterindustries.com', 'varanda', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2');
INSERT INTO `usuarios` VALUES ('5', '2013-07-02 15:52:08', '', 'Dulce', 'Gastelum', 'dgastelum@hunterindustries.com', 'dgastelum', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2');
INSERT INTO `usuarios` VALUES ('6', '2013-08-20 11:40:03', '12', 'Caludia', 'Rosado', 'crosado@hunterindustries.com', 'crosado', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2');

-- ----------------------------
-- Table structure for usuarios_acl_permisos
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_acl_permisos`;
CREATE TABLE `usuarios_acl_permisos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roles_id` int(11) DEFAULT NULL,
  `recurso` int(11) DEFAULT NULL,
  `accion` varchar(255) DEFAULT NULL,
  `permiso` enum('deny','allow') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios_acl_permisos
-- ----------------------------
INSERT INTO `usuarios_acl_permisos` VALUES ('1', '1', '1', 'login', 'allow');
INSERT INTO `usuarios_acl_permisos` VALUES ('2', '1', '1', 'auth', 'allow');
INSERT INTO `usuarios_acl_permisos` VALUES ('3', '1', '2', 'index', 'allow');
INSERT INTO `usuarios_acl_permisos` VALUES ('4', '1', '3', 'index', 'allow');
INSERT INTO `usuarios_acl_permisos` VALUES ('5', '1', '1', 'logout', 'allow');

-- ----------------------------
-- Table structure for usuarios_acl_recursos
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_acl_recursos`;
CREATE TABLE `usuarios_acl_recursos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modulo` varchar(25) DEFAULT NULL,
  `controlador` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios_acl_recursos
-- ----------------------------
INSERT INTO `usuarios_acl_recursos` VALUES ('1', 'core', 'Authenticate');
INSERT INTO `usuarios_acl_recursos` VALUES ('2', 'core', 'Inicio');
INSERT INTO `usuarios_acl_recursos` VALUES ('3', 'core', 'Usuarios');
INSERT INTO `usuarios_acl_recursos` VALUES ('4', 'core', 'Log');
INSERT INTO `usuarios_acl_recursos` VALUES ('5', 'core', 'Asyn');
INSERT INTO `usuarios_acl_recursos` VALUES ('6', 'core', 'Maquinas');
INSERT INTO `usuarios_acl_recursos` VALUES ('7', 'core', 'Moldes');
INSERT INTO `usuarios_acl_recursos` VALUES ('8', 'core', 'Partes');
INSERT INTO `usuarios_acl_recursos` VALUES ('9', 'core', 'Partesmoldes');
INSERT INTO `usuarios_acl_recursos` VALUES ('10', 'Captura', 'Maquinasactivas');
INSERT INTO `usuarios_acl_recursos` VALUES ('11', 'Captura', 'Async');
INSERT INTO `usuarios_acl_recursos` VALUES ('12', 'Captura', 'RegistroInspeccion');
INSERT INTO `usuarios_acl_recursos` VALUES ('13', 'Captura', 'InspDimensional');
INSERT INTO `usuarios_acl_recursos` VALUES ('14', 'Captura', 'InspVisual');

-- ----------------------------
-- Table structure for usuarios_acl_roles
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_acl_roles`;
CREATE TABLE `usuarios_acl_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(15) DEFAULT NULL,
  `herencia` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios_acl_roles
-- ----------------------------
INSERT INTO `usuarios_acl_roles` VALUES ('1', 'guest', null);
INSERT INTO `usuarios_acl_roles` VALUES ('2', 'admin', null);
INSERT INTO `usuarios_acl_roles` VALUES ('3', 'inspector', 'guest');
INSERT INTO `usuarios_acl_roles` VALUES ('4', 'ingeniero', '');

-- ----------------------------
-- Table structure for visual_fuera_especificacion
-- ----------------------------
DROP TABLE IF EXISTS `visual_fuera_especificacion`;
CREATE TABLE `visual_fuera_especificacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visual_id` int(11) DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `cavidad` varchar(15) DEFAULT NULL,
  `dimension` double DEFAULT NULL,
  `zona_dibujo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of visual_fuera_especificacion
-- ----------------------------
INSERT INTO `visual_fuera_especificacion` VALUES ('2', '1', '124', '2', '23', 'A3');
INSERT INTO `visual_fuera_especificacion` VALUES ('4', '1', '12', '12', '2', 'A3');
INSERT INTO `visual_fuera_especificacion` VALUES ('7', '4', '54', '1', '0', 'AD');
